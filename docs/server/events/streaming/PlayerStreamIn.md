# PlayerStreamIn

Event triggered when a player enters or loads into the stream distance of another player.

## Syntax

```js
mp.events.add("playerStreamIn", (player, forPlayer) => {
    // Do what you want.
});
```

## Example 

This native doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b></li>

<li><b>forPlayer</b>: <b><span style="color:#008017">Player</span></b></li>
