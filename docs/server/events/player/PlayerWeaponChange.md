# PlayerWeaponChange

Event triggered when a player changes or switches their weapon.

## Syntax

```js
mp.events.add("playerWeaponChange", (player, oldWeapon, newWeapon) => {
    // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b></li>
<li><b>oldWeapon</b>: <b><span style="color:#008017">Number</span></b> - hash of the old weapon</li>
<li><b>newWeapon</b>: <b><span style="color:#008017">Number</span></b> - hash of the new weapon</li>

