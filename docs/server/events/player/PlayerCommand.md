# PlayerCommand

This event is triggered when a player sends a command.

## Syntax

```js
mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
        
    if (commandName === "meetme") {
        player.outputChatBox("Hello!");
    }
});
        
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - <a href="/index.php?title=Category:Player_API" title="Category:Player API">player</a>, who send command.</li>

<li><b>command</b>: <b><span style="color:#008017">String</span></b> - command (without slash) with arguments.</li>

