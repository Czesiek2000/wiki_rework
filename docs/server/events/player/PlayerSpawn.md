# PlayerSpawn
This event is triggered when a player spawns.

## Syntax

```js
function playerSpawn(player) {
  console.log(`${player.name} has spawned`);
}

mp.events.add("playerSpawn", playerSpawn);
```

## Example 

This example outputs a console chat message to the client when they spawns.

```js
mp.events.add("playerSpawn", (player) => {
  mp.gui.chat.push("Hey " + player.name + ", you just spawned");
});
```


## Paramters

<li><li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, which has spawned</li>

