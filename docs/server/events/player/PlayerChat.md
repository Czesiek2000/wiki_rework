# PlayerChat

This event is triggered when a player send a message in the chat.


## Syntax

```js
function checkChatMessage(player, text) {
    if (player.name === "Modernß" &amp;&amp; text == "sorry") {
        player.money += 300;
    }
};

mp.events.add("playerChat", checkChatMessage);
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, who send message</li>

<li><b>text</b>: <b><span style="color:#008017">String</span></b> - the text that was sent</li>

