# PlayerQuit

This event is triggered when a player quits/disconnects/leaves the server.

## Syntax

```js
function playerQuitHandler(player, exitType, reason) {
    let str = player.name;

    if (exitType != "kicked") {

        str += " quit.";
    } else {
        str = ` kicked. Reason: ${reason}.`;
    }

    console.log(str);
    }

    mp.events.add("playerQuit", playerQuitHandler);
        
```

## Example 

This example displays a notification for the player, when another player disconnected from server.

```js
mp.events.add("playerQuit", (player) => {
    mp.game.graphics.notify(`<C>${player.name}</C> (ID:${player.remoteId}) left the server`);
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, which quit from the server</li>

<li><b>exitType</b>: <b><span style="color:#008017">String</span></b> - exit types:
disconnect
timeout
kicked</li>

<li><b>reason</b>: <b><span style="color:#008017">String</span></b> - kick reason</li>

