# PlayerDamage

Event triggered when a player gets damage.

## Syntax

```js
mp.events.add("playerDamage", (player, healthLoss, armorLoss) => {
        // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<ul>
<li><b>player:</b> <b><span style="color:#008017">Player</span></b></li>
<li><b>healthLoss:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>armorLoss:</b> <b><span style="color:#008017">Number</span></b></li>
</ul>

