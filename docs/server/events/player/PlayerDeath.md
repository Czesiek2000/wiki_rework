# PlayerDeath

This event is triggered when a player dies.

## Syntax

```js
function playerDeathHandler(player, reason, killer) {
    const deathName = player.name;
    const killerName = killer.name;
    
    if(reason == 341774354) {
        mp.players.broadcast(`${deathName} died in a chopper!`);
        return;
    }

    mp.players.broadcast(`${killerName} killed ${deathName}. Reason: ${reason}`);
    
}

mp.events.add("playerDeath", playerDeathHandler);
```

## Example 

```js
mp.events.add("playerDeath", (player, reason, killer) => {
    mp.game.graphics.startScreenEffect("DeathFailNeutralIn", 5000, false);
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - victim, the player which died.</li>

<li><b>reason</b>: <b><span style="color:#008017">Number</span></b> - cause hash of death (<b>list causes:</b> <a href="/index.php?title=Causes_of_death" title="Causes of death"> Causes</a>.</li>

