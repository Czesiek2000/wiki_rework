# PlayerJoin

This event is triggered when a player joins the server.

## Syntax

```js
mp.events.add('playerJoin', (player) => {
    console.log(`[SERVER]: ${player.name} has joined the server!`);
});
```

## Example 

```js
mp.events.add("playerJoin", (player) => {
    mp.gui.chat.push(`${player.name} has joined the server!`);
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The player who joined.</li>

<li><b>player</b>: The player joining the server, expects <b>RAGE.Elements.Player</b> type.</li>

