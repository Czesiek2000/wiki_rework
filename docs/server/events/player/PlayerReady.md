# PlayerReady


## Syntax

```js
mp.events.add("playerReady", player => {
  if (player.name != "George")
    player.kick("You're not George");
});       
```

## Example 

Event triggered when a player has downloaded or loaded every resource packages and is ready to play in your server.

> Server-side

```js
// This example makes you god.
mp.events.add("playerReady", player => {
 if (player.name != "George")
  player.kick("You're not George");
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b></li>
