# EntityDestroyed

Event triggered when an entity is destroyed

## Syntax

```js
mp.events.add("entityDestroyed", entity => {
    // Do what you want
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>entity</b>: <b><span style="color:#008017">Entity</span></b></li>

