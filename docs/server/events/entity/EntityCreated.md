# EntityCreated
This event is triggered when an Entity Is Created. From my testing, this event is only accessible on the serverside.

## Syntax


```js
function entityCreatedHandler(entity) {
    console.log(`An Entity with the ID of ${entity.id} was created at ${entity.position}`);
}

mp.events.add("entityCreated", entityCreatedHandler);   
```

## Example 

This event doesn't have example


## Paramters
<li><b>entity</b>: <b><span style="color:#008017">Entity</span></b> - the <a href="/index.php?title=Category:Entity_API" title="Category:Entity API">entity</a> that was created</li>

