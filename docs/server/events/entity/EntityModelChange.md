# EntityModelChange
Event triggered when an entity model changes.

## Syntax

```js
mp.events.add("entityModelChange", (entity, oldModel) => {
    // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>entity</b>: <b><span style="color:#008017">Entity</span></b></li>

<li><b>oldModel</b>: <b><span style="color:#008017">Number</span></b> - hash of the old model</li>

