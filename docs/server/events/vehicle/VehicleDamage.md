# VehicleDamage

Event triggered when a vehicle gets damage.

## Syntax


```js
mp.events.add("vehicleDamage", (vehicle, bodyHealthLoss, engineHealthLoss) => {
        // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b></li>

<li><b>engineHealthLoss</b>: <b><span style="color:#008017">Number</span></b></li>

