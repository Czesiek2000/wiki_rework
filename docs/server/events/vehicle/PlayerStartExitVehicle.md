# PlayerStartExitVehicle

This event is triggered when a player starts exiting from a vehicle.

## Syntax

<p>This example outputs chat message, when player starts exit the vehicle.</p>

```js
function playerStartExitVehicleHandler(player) {
   const playerName = player.name;
   const vehicleID = player.vehicle.id;

   mp.players.broadcast(`${playerName} started to exit the vehiclewith ID: ${vehicleID}`);
}

mp.events.add("playerStartExitVehicle", playerStartExitVehicleHandler);
```

## Example

This native doesn't have example


## Paramters

<li><li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API" target="_blank">player</a> player, which going out.</li></li>

