# VehicleDeath

This event is triggered when a vehicle is exploded.

## Syntax

```js
mp.events.add("vehicleDeath", (vehicle) => {
    vehicle.destroy()
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b> - vehicle, that is exploded</li>

