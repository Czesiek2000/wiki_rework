# VehicleSirenToggle

Event triggered when a vehicle's siren is toggled

## Syntax

```js
mp.events.add("vehicleSirenToggle", (vehicle, toggle) => {
    // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b></li>

<li><b>toggle</b>: <b><span style="color:#008017">Boolean</span></b></li>

