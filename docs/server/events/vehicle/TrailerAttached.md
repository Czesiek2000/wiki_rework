# TrailerAttached

Event triggered when a vehicle's trailer is attached.

## Syntax

```js
mp.events.add("trailerAttached", (vehicle, trailer) => {
        // Do what you want.
});
```

## Example 

This native doesn't have example


## Paramters

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b></li>

<li><b>trailer</b>: <b><span style="color:#008017">Vehicle</span></b></li>

