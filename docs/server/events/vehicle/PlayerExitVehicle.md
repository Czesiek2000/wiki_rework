# PlayerExitVehicle
This event is triggered when a player went out from a vehicle.

## Syntax

```js
function playerExitVehicleHandler(player, vehicle) {
        console.log(`${player.name} when out from vehicle with ID: ${vehicle.id}`);
}

mp.events.add("playerExitVehicle", playerExitVehicleHandler);        
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API" target="_blank">player</a>, which left the vehicle</li>

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b> - <a href="/index.php?title=Category:Vehicle_API" title="Category:Vehicle API" target="_blank">vehicle</a>, which was left by the player.</li>

