# VehicleHornToggle

Event triggered when a vehicle's horn is toggled

## Syntax

```js
mp.events.add("vehicleHornToggle", (vehicle, toggle) => {
    // Do what you want.
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>toggle</b>: <b><span style="color:#008017">Boolean</span></b></li>

