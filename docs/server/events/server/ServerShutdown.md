# ServerShutdown

This event is triggered when the server is shutting down.

# Syntax

This event is triggered when the server is shutting down.

```js
mp.events.add("serverShutdown", async () =>{
  mp.events.delayShutdown = true;
  await yourAsyncFunction();
  mp.events.delayShutdown = false;
});
```

# Example 

This event doesn't have example


# Paramters

<p>There are no parameters</p>