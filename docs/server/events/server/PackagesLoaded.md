# PackagesLoaded

This event is triggered when the server has loaded all packages resources' folders in **packages** directory. You can delay this event's trigger through <a href="../event/delayInitialization">mp.events.delayInitialization</a>.

## Syntax

```js
mp.events.add('packagesLoaded', () =>{
    console.log('Loaded all resources successfully');
});     
```

## Example 

This native doesn't have example


## Paramters

<p>There are no parameters</p>