# IncomingConnection

This event is triggered when an player establishes a connection with the server before loading the resources.

## Syntax

```js
const rgscIdBlackList = [123];
mp.events.add('incomingConnection', (ip, serial, rgscName, rgscId, gameType) => { // This event checks whether the specified social club ID (123) or social club username includes (kemperrr), therefore, cancels connection on match.  
    return rgscIdBlackList.includes(rgscId) || rgscName.includes('kemperrr');
});    
```

## Example 

This event doesn't have example


## Paramters

<ul>
<li><b>ip</b>: <b><span style="color:#008017">String</span></b> - player's IP</li>

<li><b>serial</b>: <b><span style="color:#008017">String</span></b> - player's serial</li>

<li><b>rgscName</b>: <b><span style="color:#008017">String</span></b> - player's social club username.</li>

<li><b>rgscId</b>: <b><span style="color:#008017">String</span></b> - player's social club ID.</li>

<li><b>gameType</b>: <b><span style="color:#008017">String</span></b> - game types:</li>

* steam   
* rgsc
* egs

</ul>

