# Label::text

Updates the text of a created label.

# Syntax

```js
String label.text
```

# Example

Updates the text of a label through a command.

```js
let startLabel = mp.labels.new("Use /labeltext to update me.", new mp.Vector3(-431.88, 1146.86, 327), {
    los: false,
    font: 1,
    drawDistance: 100,
});

mp.events.addCommand("labeltext", (player, _, text) => {
    startLabel.text = text;
    player.outputChatBox("Label Updated");
});

```

# Parameters

This function has no parameters.