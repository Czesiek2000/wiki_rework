# Label::color

Update the color of the selected label


# Syntax

```js
Number label.color
```

# Example

Updates the color of the label through the command /labelcolor

```js
let startLabel = mp.labels.new("Use /labelcolor to update the color of this label.", new mp.Vector3(-431.88, 1146.86, 327), {
    los: false,
    font: 1,
    drawDistance: 100,
    color: [255, 255, 255, 255]
});

mp.events.addCommand("labelcolor", (player, _, colorR, colorG, colorB, colorAlpha) => {
   startLabel.color = parseInt(colorR, colorG, colorB, colorAlpha);
   player.outputChatBox(`You have updated the color of the label to: ${colorR} ${colorG} ${colorB} ${colorAlpha}`);
});
```

# Parameters
<li><b>colorR</b> - <b><span style="color:#008017">Number</span></b></li>
<li><b>colorG</b> - <b><span style="color:#008017">Number</span></b></li>
<li><b>colorB</b> - <b><span style="color:#008017">Number</span></b></li>
<li><b>colorAlpha</b> - <b><span style="color:#008017">Number</span></b></li>
