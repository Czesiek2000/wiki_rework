# Label::Label

Creates a label in your world.

# Syntax

```js
mp.labels.new(text, position, {
    los: los,
    font: font,
    drawDistance: drawDistance,
    color: color,
    dimension: dimension
});
```

# Example
Creates a label with the text "Welcome to Los Santos"

```js
mp.labels.new("Welcome to Los Santos", new mp.Vector3(-431.88, 1146.86, 327), {
    los: true,
    font: 1,
    drawDistance: 100,
});
```

> Labels also supports multiple lines of text, this can be achieved using "\n".


# Parameters
<ul>
<li><b><span style="font-weight:bold; color:red;">*</span>text</b>: <b><span style="color:#008017">String</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>los</b>: <b><span style="color:#008017">Boolean</span></b> - Line of Sight (Text will hide if blocked by an entity).</li>
<li><b>font</b>: <b><span style="color:#008017">Number</span></b></li>
<li><b>drawDistance</b>: <b><span style="color:#008017">Number</span></b></li>
<li><b>color</b>: <b><span style="color:#008017">[Number, Number, Number, Number]</span></b></li>
<li><b>dimension</b>: <b><span style="color:#008017">dimension</span></b></li></ul>

<span style="font-weight:bold; color:red;">*</span> - Required