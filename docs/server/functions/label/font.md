# Label::font
Update the font type of the selected label


Updates the font of the label through the command /labelfont


# Syntax

```js
Number label.font
```

# Example

```js
let startLabel = mp.labels.new("Use /labelfont to update the font of this label.", new mp.Vector3(-431.88, 1146.86, 327),
{
    los: false,
    font: 1,
    drawDistance: 100,
});

mp.events.addCommand("labelfont", (player, _, fontID) => {
    startLabel.font = parseInt(fontID);
    player.outputChatBox(`You have updated the font of the label to Font ID: ${fontID}`);
});
```

# Parameters
<ul>
<li><b>fontID</b>: <b><span style="color:#008017">Number</span></b> - id of font available values (0,1,2,4,7)</li>
</ul>