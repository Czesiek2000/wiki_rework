# Label::drawDistance

Update the draw distance of the selected label


# Syntax

```js
Number label.drawDistance
```

# Example

Updates the draw distance of the label through the command /labeldistance

```js
let startLabel = mp.labels.new("Use /labeldistance to update the color of this label.", new mp.Vector3(-431.88, 1146.86, 327), {
    los: false,
    font: 1,
    drawDistance: 100,
});

mp.events.addCommand("labeldistance", (player, _, dist) => {
    startLabel.drawDistance = parseInt(dist);
    player.outputChatBox(`You have updated the draw distance of the label to: ${dist}`);
});
```

# Parameters
<li><b>distance</b> - <b><span style="color:#008017">Number</span></b></li>