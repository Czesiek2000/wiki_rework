# Label::los

Updates the los(Line of Sight) on the selected label.

# Syntax

```js
Boolean label.los
```

# Example

Updates the los of the label through the command /labellos

```js
let startLabel = mp.labels.new("Use /labellos to update the los of this label.", new mp.Vector3(-431.88, 1146.86, 327), {
    los: false,
    font: 1,
    drawDistance: 100,
});

mp.events.addCommand("labellos", (player) => {
    if(startLabel.los === true){
        startLabel.los = false;
    } else {
        startLabel.los = true;
    }
    player.outputChatBox("Label Updated");
});

```

# Parameters

This function has no parameters.