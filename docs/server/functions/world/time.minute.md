# World::time.minute
This property gets / sets the minute time in the game.


# Syntax

No syntax here

# Example

```js
var minute = mp.world.time.minute; // GETTER

mp.world.time.minute= 30; // SETTER
```

# Parameters

No parameters here