# World::requestIpl

This function will request an IPL and sync it to every client.

# Syntax

```js
mp.world.requestIpl(ipl);
```

# Example

```js
mp.world.requestIpl("gr_case10_bunkerclosed"); // THIS ADDS THE BUNKER ENTRANCE NEAR ZANCUDO. POSITION: (-3058.714, 3329.19, 12.5844)

```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>ipl</b>: <b><span style="color:#008017">String</span></b> (<a href="/index.php?title=Interiors_and_Locations" title="Interiors and Locations">IPLs</a>)</li>


<span style="font-weight:bold; color:red;">*</span> - Required