# World::trafficLights.state

This property set the traffic lights state. (If you want to make your own traffic lights system, make sure of locking the traffic lights to avoid the game to change them by itself)


Sequence of traffic lights in GTA V : [0, 39, 87, 48, 87, 39]


# Syntax

No syntax here

# Example

```js
mp.world.trafficLights.state = 0; // Will put one way on Red light and the other on Green light.
```


# Parameters

No parameters here