# World::trafficLights.locked

This property locks the traffic lights in their current position.


# Syntax

No syntax here

# Example

```js
mp.world.trafficLights.locked = true // LOCK THE POSITION
mp.world.trafficLights.locked = false // UNLOCK THE POSITION
```


# Parameters

No parameters here