# World::time.set

This function sets time.

# Syntax

```js
mp.world.time.set(hour, minute, second);

```

# Example

In this example, we will set a custom time via command (e.g. 12:20:10 -> /time 12 20 10).

You can also set time by changing its properties and then call 'set' function without parameters.

```js
mp.events.addCommand('time', (player, fulltext, hour, minute, second) => mp.world.time.set(hour, minute, second));

```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>hour</b>: <b><span style="color:#008017">Int</span></b> (0 - 23)</li>


<li><span style="font-weight:bold; color:red;">*</span><b>minute</b>: <b><span style="color:#008017">Int</span></b> (0 - 59)</li>


<li><span style="font-weight:bold; color:red;">*</span><b>second</b>: <b><span style="color:#008017">Int</span></b> (0 - 59)</li>
