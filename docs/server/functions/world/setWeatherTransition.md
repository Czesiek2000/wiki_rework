# World::setWeatherTransition

This function start a weather transition to the weather specified and sync it to all clients.


**Note: the second use with easeTime field seems to be buggy, it has graphical issues with transition from SMOG to FOGGY weather (and probably with some other weather), so right now the best choice would be to do weather transition on client side and sync it manually.**




# Syntax

```js
mp.world.setWeatherTransition(weather, easeTime)
```

# Example

```js
mp.world.setWeatherTransition("CLEARING"); // Set's the server's weather to 'CLEARING' immediately

```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>weather</b>: <b><span style="color:#008017">String</span></b> (<a href="/index.php?title=Weather" title="Weather">Weather</a>)</li>


<li><b>easeTime</b>: <b><span style="color:#008017">Int</span></b> (Weather transitioning time)</li>
