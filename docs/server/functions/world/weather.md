# World::weather

This property gets/sets game weather.


# Syntax

No syntax here

# Example

```js
var weather = mp.world.weather; // GETTER

mp.world.weather = 'CLEAR'; // SETTER
```


# Parameters

No parameters here