# World::time.second
This property gets/sets second time in game.


# Syntax

No syntax here

# Example

```js
var second = mp.world.time.second ; // GETTER

mp.world.time.second = 30; // SETTER
```

# Parameters

No parameters here