# World::time.hour
This property gets/sets the hour in the game.


# Syntax

No syntax here

# Example

```js
var hour = mp.world.time.hour; // GETTER

mp.world.time.hour = 4; // SETTER
```

# Parameters

No parameters here