# Checkpoint::hideFor
Hiding a checkpoint for a particular player


# Syntax

```js
const checkpoint = mp.checkpoints.new(2, new mp.Vector3(10, 10, 72), new mp.Vector3(), 4, 255, 255, 255, 255, true);
const player = mp.players.at(0);
checkpoint.hideFor(player);
```

# Example
This page has no example

# Parameters

<li><b>player: </b><span style="color:#008017">Player</span><b> </b></li>
