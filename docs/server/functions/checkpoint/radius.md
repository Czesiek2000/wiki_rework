# Checkpoint::radius
This property is used to set or get the radius of the checkpoint.


# Syntax

```js
const radius = checkpoint.radius; // get checkpoint radius
console.log(radius);

checkpoint.radius = 5; // set checkpoint radius

```

# Example
This page has no example

# Parameters

<li><b><span style="color:#008017">Number</span></b> in float</li>
