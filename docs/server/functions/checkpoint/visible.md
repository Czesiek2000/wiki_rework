# Checkpoint::visible
This property is used to set or get the visible of the checkpoint.


# Syntax

```js
const visible = checkpoint.visible; // get checkpoint visible
console.log(visible);

checkpoint.visible = true; // set checkpoint visible

```

# Example
This page has no example

# Parameters

<li><b><span style="color:#008017">Boolean</span></b></li>
