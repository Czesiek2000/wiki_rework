# Checkpoint::destination
This property is used to set or retrieve the direction of the checkpoint.


# Syntax

```js
const destination = checkpoint.destination; // get checkpoint destination
console.log(destination);

checkpoint.destination = new mp.Vector3(10, 10, 10); // set checkpoint destination

```

# Example
This page has no example

# Parameters

<li><b><span style="color:#008017">Vector3</span></b></li>
