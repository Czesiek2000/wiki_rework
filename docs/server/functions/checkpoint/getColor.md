# Checkpoint::getColor
Function: Returns an array of 4 numbers, with a checkpoint color

# Syntax

```js
const checkpoint = mp.checkpoints.new(2, new mp.Vector3(10, 10, 72), new mp.Vector3(), 4, 150, 255, 255, 255, true);
const color = checkpoint.getColor();

console.log(color); // -> [150, 255, 255, 255]
console.log(color[0]); // -> 150
```

# Example
This page has no example

# Parameters

<li>No parameters, but has return type</li>
<li><b><span style="color:#008017">Array <Number></span></b></li>
