# Checkpoint::Checkpoint

Creates a Checkpoint.

# Syntax

```js
mp.checkpoints.new(1, new mp.Vector3(0, 0, 75), 10, {
    direction: new mp.Vector3(0, 0, 75),
    color: [ 255, 255, 255, 255 ],
    visible: true,
    dimension: 0
});

```

# Example
This page has no example

# Parameters

<li><b>direction</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>color</b>: <b><span style="color:#008017">number[4]</span></b>
<ul><li><b>red</b>: <b><span style="color:#008017">number</span></b> (0-255)</li>
<li><b>green</b>: <b><span style="color:#008017">number</span></b> (0-255)</li>
<li><b>blue</b>: <b><span style="color:#008017">number</span></b> (0-255)</li>
<li><b>alpha</b>: <b><span style="color:#008017">number</span></b> (0-255)</li></ul></li>
<li><b>visible</b>: <b><span style="color:#008017">Boolean</span></b></li>
<li><b>dimension</b>: <b><span style="color:#008017">Number</span></b></li>
