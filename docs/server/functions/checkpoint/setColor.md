# Checkpoint::setColor
Sets the checkpoint color

?> Server-Side

# Syntax

```js
checkpoint.setColour(r, g, b, a);
```

# Example

```js
const checkpoint = mp.checkpoints.new(2, new mp.Vector3(10, 10, 72), new mp.Vector3(), 4, 150, 255, 255, 255, true);

checkpoint.setColour(255, 0, 0, 255);
```

# Parameters

<li><b>r:</b> <b><span style="color:#008017">Number</span></b> Red color value (0 to 255)</li>
<li><b>g:</b> <b><span style="color:#008017">Number</span></b> Green color value (0 to 255)</li>
<li><b>b:</b> <b><span style="color:#008017">Number</span></b> Blue color value (0 to 255)</li>
<li><b>a:</b> <b><span style="color:#008017">Number</span></b> Alpha color value (0 to 255)</li>
