# Globals::joaat

This function generates hashes/arrays of hashes (hash is integer) using strings/arrays of strings. Those hashes could be used to set entity model.

The name "joaat" stands for Jenkin's One At A Time hashing function.

## Syntax
```js
mp.joaat(String);
mp.joaat(String[]);
mp.game.joaat(String);
mp.game.joaat(String[]);
```

## Example
?> Server-side code

```js
mp.joaat("bati");
mp.joaat(["bati", "benson"]);
```
!> Client-side code

```js
mp.game.joaat("bati");
mp.game.joaat(["bati", "benson"]);
```