# Vehicle::setNeonColor

This function is used to set the neon lights of a vehicle.

# Syntax
```js
vehicle.setNeonColor(int r, int g, int b)
```

# Example

This example sets yellow neon lights for the vehicle, in which the player sits.

```js
mp.events.add('playerCommand', (player, cmd) => {
    let arr = cmd.split(' ');
    if (arr[0] == 'neon' && player.vehicle) {
        player.vehicle.setNeonColor(255, 255, 0);
    }
});
```


# Parameters

<ul><li><b>r:</b> Red Value 0 - 255.</li>
<li><b>g:</b> Green Value 0 - 255.</li>
<li><b>b:</b> Blue Value 0 - 255.</li></ul>