# Vehicle::rotation

This property is being used to get or set vehicle rotation.


# Syntax

No syntax here

# Example

```js
let rotation = vehicle.rotation;
vehicle.rotation = new mp.Vector3(0, 0, 90);
console.log(vehicle.rotation.x, vehicle.rotation.y, vehicle.rotation.z);

```

# Parameters

No syntax parameters
