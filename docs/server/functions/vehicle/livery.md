# Vehicle::livery
This property using for getting or setting vehicle livery.


"police" has 5 liveries. (Rooftop numbers: 32, 76, 05, 84, 43).


# Syntax

No syntax here

# Example

```js
mp.events.addCommand({
    //... other commands
    'livery' : (player, _, livery) => {
        if(!player.vehicle) return; // check if player in vehicle or not
        if(!livery || livery < 0) return; // check player input livery or not, and livery less than 0
        player.vehicle.livery = JSON.parse([livery]); // set vehicle's livery
    },
    //... other commands
});
```

# Parameters

<li><b><span style="color:#008017">int</span></b> livery value - for setter and getter</li>

<br />

## Example liveries
<ul style="list-style-type: none; display: flex">
<li style="margin-right: 15px"><img src="../../../images/liveries/Livery0.png"></li>
<li style="margin-right: 15px"><img src="../../../images/liveries/Livery1.png"></li>
<li><img src="../../../images/liveries/Livery2.png"></li>
</ul>