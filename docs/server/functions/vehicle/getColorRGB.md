# Vehicle::getColorRGB

This function used to get the vehicle RGB body color. Returns null if never set explicitly.

# Syntax
```js
Array vehicle.getColorRGB(int slot);
```

# Example

```js
let primaryColor = vehicle.getColorRGB(0);
let secondaryColor = vehicle.getColorRGB(1); 
// primaryColor output: [ 255, 0, 0 ]
```

# Parameters
<li><b>slot:</b> The color you want to get. (valid is 0 or 1)</li>