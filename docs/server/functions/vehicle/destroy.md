# Vehicle::destroy

This functions destroys a vehicle.

# Syntax
```js
vehicle.destroy()
```

# Example

This example destroys a random car from command.

```js
mp.events.add('playerCommand', (player, cmd) => {
	let arr = cmd.split(' ');
	if (arr[0] == 'destroyrandomveh') {
		let vehicles = mp.vehicles.toArray();
		if (vehicles.length > 0) {
			let randomValue = Math.floor(Math.random() * vehicles.length);
			vehicles[randomValue].destroy();
		} else {
			player.outputChatBox('Vehicles does not exist!');
		}
	}
});
```