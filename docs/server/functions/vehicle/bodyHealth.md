# Vehicle::bodyHealth
This functions using for getting or setting body health.


**Note: this property is read-only. For edit health use [Vehicle::repair](./repair.md)**


# Syntax
No syntax here


# Example

```js
let bodyHealth = vehicle.bodyHealth
```

# Parameters
No parameters here
