# Vehicle::getNeonColor

This function is used to get the current neon lights of a vehicle.

# Syntax
```js
vehicle.getNeonColor()
```

# Example
```js
mp.events.add('playerCommand', (player, cmd) => {
    let arr = cmd.split(' ');
    if (arr[0] == 'getneon' && player.vehicle) {
        player.notify(player.vehicle.getNeonColor().toString());
    }
});
```

# Return value

<li><b>object</b></li>