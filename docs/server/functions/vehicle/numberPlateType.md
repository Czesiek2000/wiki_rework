# Vehicle::numberPlateType

This function sets the number plate type. Default number plate type is 0.


# Syntax

```js
vehicle.numberPlateType = Platetype;
```

# Example

```js
vehicle.numberPlateType = 1;
```

# Parameters

<li>Platetype: <b><span style="color:#008017">Int</span></b> - for setter and getter.</li>


## Number plates colors
<ul style="list-style-type: none; display: flex;">
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate0.png">Blue / White: 0</div></li>
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate1.png">Yellow/black: 1</div></li>
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate2.png">Yellow/Blue: 2</div></li>
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate3.png">Blue/White2: 3</div></li>
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate4.png">Blue/White3: 4</div></li>
<li><div style="text-align: center; margin-right: 10px"><img src="../../../images/plates/Plate5.png">Yankton: 5</div></li>
</ul>