# Vehicle::velocity
This property using for getting vehicle velocity.


# Syntax

No syntax here

# Example

```js
let velocity = vehicle.velocity;
console.log(`velocityX: ${velocity.x} | velocityY: ${velocity.y} | velocityZ: ${velocity.z}`);

```


# Parameters

No parameters here
