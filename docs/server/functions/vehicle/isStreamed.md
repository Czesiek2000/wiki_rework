# Vehicle::isStreamed

This function is used for check, vehicle is located in stream distance for player or not.

*THIS IS **SHARED** FUNCTION*

# Syntax
```js
vehicle.isStreamed(Player player);
```

# Example
```js
let player = mp.players.at(0);
mp.vehicles.forEach((vehicle) => {
    if (vehicle.isStreamed(player)) {
        console.log(`Vehicle with id ${vehicle.id} in stream distance!`);
    }
});
```


# Parameters
<li><b>player</b>: <b><span style="color:#008017">Player</span></b></li>

<br />

# Return value
<li><b><span style="color:#008017">Boolean</span></b></li>