# Vehicle::neonEnabled

This property is used to check whether vehicle neon light is enabled or disabled


# Syntax

No syntax here

# Example

```js
vehicle.neonEnabled = true; // Enable this vehicle neon
```


# Parameters

<li><b><span style="color:#008017">Boolean</span></b> <code>true</code> if neon is enabled, <code>false</code> if neon is disabled. Used for setter and getter</li>

