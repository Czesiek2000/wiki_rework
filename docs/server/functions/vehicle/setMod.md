# Vehicle::setMod

Applies the specified mod onto the vehicle.

# Syntax
```js
vehicle.setMod(modType, modIndex);
```

# Example
```js
mp.events.addCommand('mod', (player, _, modType , modIndex) => {
	if(!player.vehicle) return player.outputChatBox("You need to be in a vehicle to use this command.");
	player.vehicle.setMod(parseInt(modType), parseInt(modIndex));
	player.outputChatBox(`Mod Type ${modType} with Mod Index ${modIndex} applied.`);
});
```


# Parameters
<ul><li><b>modType</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>modIndex</b>: <b><span style="color:#008017">Int</span></b></li></ul>