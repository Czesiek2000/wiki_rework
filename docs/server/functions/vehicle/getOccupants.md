# Vehicle::getOccupants

Gets the occupants inside of a vehicle.

# Syntax
```js
vehicle.getOccupants();
```

# Example
```js
//To do
```

# Return value

<li><b>Array</b>: <b><span style="color:#008017">Players</span></b></li>