# Vehicle::brake

This property returns brake state.


**Note: this property is read-only.**


# Syntax

No syntax here

# Example

```js
let brakeUsing = vehicle.brake
```

# Parameters

No parameters here