# Vehicle::setColor

This function sets vehicle body color. [Vehicle colors](../../../tables/vehicleColors.md)

# Syntax
```js
vehicle.setColor(primaryColor, secondaryColor);
```

# Example
```js
var theVehicle = mp.vehicles.new(418536135, new mp.Vector3(-17.460, 39.787, 71.318)); // spawn Infernus
theVehicle.setColor(0,0); // set black colour
```


# Parameters
<ul><li><b>primaryColor:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>secondaryColor:</b> <b><span style="color:#008017">Int</span></b></li></ul>