# Vehicle::windowTint
This property gets/sets vehicle window tint.


# Syntax

```js
var windowTint = vehicle.windowTint; // GETTER

vehicle.windowTint = 50; // SETTER

```

# Example


# Parameters

No parameters here
