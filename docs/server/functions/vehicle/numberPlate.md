# Vehicle::numberPlate

Number plate change (Maximum length: 8 char)


# Syntax

No syntax here

# Example

```js
mp.events.addCommand('changeNumberPlate', (player, _, plate) => {
    if(player.vehicle) {
        player.vehicle.numberPlate = plate;
    }
});

```

# Parameters

<li><b><span style="color:#008017">String</span></b> for setter and getter.</li>
