# Vehicle::dead

The getter property, says that the vehicle is dead or not.

The Setter property, sets the vehicle dead state.


# Syntax

No syntax here

# Example

```js
let vehicleDead = vehicle.dead;
console.log(vehicleDead);

```

```js
// 0.3.7
vehicle.dead = true/false;

```

# Parameters

<li><b><span style="color:#008017">Boolean</span></b> - for setter and getter</li>