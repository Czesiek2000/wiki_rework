# Vehicle::highbeams

This property returns high beams state.


**Note: this property is read-only.**


# Syntax

No syntax here

# Example

```js
let highbeamsEnabled = vehicle.highbeams
```


# Parameters

No parameters here