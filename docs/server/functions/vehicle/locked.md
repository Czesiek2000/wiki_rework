# Vehicle::locked


A vehicle property used for locking or unlocking a vehicle.


# Syntax

```js
vehicle.locked;
```

# Example

This example will lock your vehicle.

```js
mp.events.addCommand('lock', (player) => {
	let vehicle = player.vehicle;
	if (vehicle) {
		let newState = !vehicle.locked;
		vehicle.locked = newState;
		player.outputChatBox(`Your vehicle doors are now ${newState ? `locked` : `unlocked`}.`);
	};
});
```

# Parameters

<li><b><span style="color:#008017">Boolean</span></b> for setter and getter</li>
