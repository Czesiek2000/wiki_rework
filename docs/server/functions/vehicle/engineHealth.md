# Vehicle::engineHealth
This functions using for getting engine health.


**Note: this property is read-only. For edit health use [Vehicle::repair](./repair.md)**


# Syntax

No syntax here

# Example

```js
let engineHealth = vehicle.engineHealth
```

# Parameters

