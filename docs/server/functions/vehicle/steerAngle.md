# Vehicle::steerAngle
This property returns steer angle value: from -1 to 1.


Note: this property is read-only.


# Syntax

No syntax here

# Example

```js
let steerAngle = vehicle.steerAngle
```


# Parameters

No parameters here
