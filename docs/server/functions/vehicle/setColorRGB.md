# Vehicle::setColorRGB

This function used for set vehicle RGB body color.

# Syntax
```js
void vehicle.setColorRGB(int r1, int g1, int b1, int r2, int g2, int b2);
```

# Example
```js
// Sets yellow to the primary color, and white for the secondary color
vehicle.setColorRGB(215, 142, 16, 250, 250, 250);
```


# Parameters
<ul><li><b>r1</b>: <b><span style="color:#008017">Int</span></b> Primary Red Colour [0 - 255]</li>
<li><b>g1</b>: <b><span style="color:#008017">Int</span></b> Primary Green Colour [0 - 255]</li>
<li><b>b1</b>: <b><span style="color:#008017">Int</span></b> Primary Blue Colour [0 - 255]</li>
<li><b>r2</b>: <b><span style="color:#008017">Int</span></b> Secondary Red Colour [0 - 255]</li>
<li><b>g2</b>: <b><span style="color:#008017">Int</span></b> Secondary Green Colour [0 - 255]</li>
<li><b>b2</b>: <b><span style="color:#008017">Int</span></b> Secondary Blue Colour [0 - 255]</li></ul>