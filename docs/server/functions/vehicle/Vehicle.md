# Vehicle::Vehicle

<p>Creates a vehicle.<br>
<b>Notes:</b> <br>
- Colors don't always work as expected if you use them inside the function. Also number plate won't work properly. Better set those after vehicle is created. <br>
- Heading means Z Rotation.<br>
- in 0.3.7 numberPlate property is bugged, so set the numberPlate after vehicle creation with vehicle.numberPlate = 'plate';
</p>

## Syntax
```js
mp.vehicles.new(model, position, {
    heading: heading,
    numberPlate: numberPlate,
    alpha: alpha,
    color: color,
    locked: locked,
    engine: engine,
    dimension: dimension
});
```

## Example
An example of creating a red Turismo with the license plate ADMIN

!> Client-side code

```js
mp.vehicles.new(mp.game.joaat("turismor"), new mp.Vector3(-421.88, 1136.86, 326), {
    numberPlate: "ADMIN",
    color: [[255, 0, 0],[255,0,0]]
});
```

!> Server-side code

Same example but creating a blue Turismo and using it server-side
```js
mp.vehicles.new(mp.joaat("turismor"), new mp.Vector3(-441.88, 1156.86, 326),{
    numberPlate: "ADMIN",
    color: [[0, 255, 0],[0, 255, 0]]
});
```


## Parameters

<ul><li><b>model</b>: <b><span style="color:#008017">Hash</span></b> (use <a href="../globals/joaat.md">mp.joaat</a>) or <b><span style="color:#008017">String</span></b></li>
<li><b>position</b>: <b><span style="color:#008017"><a href="../vector3/Vector3.md">Vector3</a></span></b></li>
<li><b>heading</b>: <b><span style="color:#008017">Number</span></b> [-180:180]</li>
<li><b>numberPlate</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>alpha</b>: <b><span style="color:#008017">Number</span></b></li>
<li><b>color</b>: <b><span style="color:#008017">[[Number, Number, Number], [Number, Number, Number]]</span></b></li>
<li><b>locked</b>: <b><span style="color:#008017">Boolean</span></b></li>
<li><b>engine</b>: <b><span style="color:#008017">Boolean</span></b></li>
<li><b>dimension</b>: <b><span style="color:#008017">Number</span></b></li></ul>