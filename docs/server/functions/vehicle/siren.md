# Vehicle::siren
This property returns siren state.

**Note: this property is read-only.**


# Syntax

No syntax here

# Example

```js
let sirenEnabled = vehicle.siren
```


# Parameters

No parameters here
