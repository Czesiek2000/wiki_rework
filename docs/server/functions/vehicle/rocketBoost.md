# Vehicle::rocketBoost
This property returns rocket boost state (voltic2).


**Note: this property is read-only.**


# Syntax

No syntax here

# Example

```js
let rocketBoost = vehicle.rocketBoost
```


# Parameters

No parameters here
