# Vehicle::spawn

This function spawns a vehicle.

# Syntax
```js
vehicle.spawn(Vector3 position, float heading);
```

# Example
```js
mp.events.addCommand("spawnVehicle", (player, vehId) => {
  const vehicle = mp.vehicles.at(vehId);
  if (vehicle) {
    vehicle.spawn(player.position, player.heading);
  }
});
```


# Parameters

<ul><li><b>position:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li></ul>