# Vehicle::getExtra

Get the extra currently applied on vehicle in the target extra id.

# Syntax
```js
vehicle.getExtra(extraId);
```

# Example
```js
let vehicle = player.vehicle;
if(vehicle) {
  vehicle.getExtra(1);
}
```

# Parameters

<li><b>extraId:</b> int</li>

<br />

# Return value

<li><b>toggle:</b> boolean</li>