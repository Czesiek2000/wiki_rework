# Vehicle::getColor

Zentorno: On the client-side, this function requires three args (red: int, green: int, blue: int), and will return an object: r,g,b

# Syntax
```js
vehicle.getColor(id);
```

# Example
```js
let primaryColor = player.vehicle.getColor(0)
let secondaryColor = player.vehicle.getColor(1)

// If the vehicle primary color was black the following would return 0
console.log(primaryColor)
```

# Parameters
<li><b>id:</b> int</li>
<p>(0 - Primary Color, 1 - Secondary Color)</p>

# Return value
<li><b>integer:</b> color</li>