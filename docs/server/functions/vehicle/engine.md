# Vehicle::engine

This property returns engine state.


# Syntax
No syntax here

# Example

```js
let engineEnabled = vehicle.engine;
console.log(engineEnabled);
```


# Parameters

<li><b><span style="color:#008017">Boolean</span></b> - for setter and getter</li>