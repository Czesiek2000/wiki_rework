# Vehicle::explode

Explodes the target vehicle.

# Syntax
```js
vehicle.explode();
```

# Example

Creates a bomb command which sets the car you're sitting in to explode in 10 seconds.

```js
mp.events.addCommand('bomb', (player) => {
	if(player.vehicle){
		let veh_id = player.vehicle.id;
		player.outputChatBox('Bomb activated, vehicle will blow in 10 seconds.')
		setTimeout(() => {
			mp.vehicles.at(veh_id).explode();
		}, 10000);
	}
});
```