# Vehicle::horn

This property returns horn state.


**Note: this property is read-only.**


# Syntax

No syntax here

# Example

```js
let hornEnabled = vehicle.horn
```


# Parameters

No parameters here 
