# Vehicle::setExtra

**Note: only some vehicle have extras**

extra ids are from 1 - 9 depending on the vehicle

-------------------------------------------------

^ not sure if outdated or simply wrong. Max extra ID for b944 is 14

-------------------------------------------------
p2 is not a on/off toggle. mostly 0 means on and 1 means off.
not sure if it really should be a BOOL.


# Syntax
```js
vehicle.setExtra(extraId, toggle);
```

# Example
```js
let vehicle = player.vehicle;
if(vehicle) {
  vehicle.setExtra(1, true); // Enable extra 1
}
```


# Parameters

<ul><li><b>extraId:</b> int</li>
<li><b>toggle:</b> boolean</li></ul>

<br />

# Return value

<li><b>Undefined</b></li>