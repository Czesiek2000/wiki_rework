# Vehicle::wheelColor
This property gets/sets vehicle wheels color.


# Syntax

No syntax here

# Example


```js
var color = vehicle.wheelColor; // GETTER

vehicle.wheelColor = 67; // SETTER
```

# Parameters

No parameters here 
