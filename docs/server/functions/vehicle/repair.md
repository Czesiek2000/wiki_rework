# Vehicle::repair

This function repairs a vehicle.

# Syntax
```js
vehicle.repair()
```

# Example

This example repairs a vehicle, in which the player sits.

```js
mp.events.add('playerCommand', (player, cmd) => {
	let arr = cmd.split(' ');
	if (arr[0] == 'repair' && player.vehicle) {
		player.vehicle.repair();
	}
});
```