# Vehicle::pearlescentColor
This function is used to set the pearlescent color of a vehicle. Using the Vehicle colors.


This example sets red pearlescent color for the vehicle, in which the player sits.


# Syntax

```js
int vehicle.pearlescentColor
```

# Example

```js
mp.events.add('playerCommand', (player, cmd) => {
    let arr = cmd.split(' ');
    if (arr[0] == 'pcolor' && player.vehicle) {
        player.vehicle.pearlescentColor = 45;
    }
});
```

# Parameters

add parameters
