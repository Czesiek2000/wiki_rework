# Colshapes::newCuboid
 
Creates a cuboid ColShape in 3D space
        
# Syntax

```js
mp.colshapes.newCuboid(x, y, z, width, depth, height, dimension)
```

# Example
This page has no example

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>y:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>z:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>width:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>depth:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>height:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>dimension:</b> <b><span style="color:#008017">Number</span></b> (optional)</li>
