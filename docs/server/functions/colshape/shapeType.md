# Colshape::shapeType

Returns type of colshape.

# Syntax

```js
const colshape_one = mp.colshapes.newRectangle(0, 0, 10, 10);
console.log(colshape_one.shapeType); // -> 'rectangle'

const colshape_two = mp.colshapes.newSphere(0, 0, 0, 10);
console.log(colshape_two.shapeType); // -> 'sphere'
        
```

# Example
This page has no example


# Parameters
This function has no parameters


# Available types

<li>cuboid</li>
<li>circle</li>
<li>sphere</li>
<li>tube</li>
<li>rectangle</li>
