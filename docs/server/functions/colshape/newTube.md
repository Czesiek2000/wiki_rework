# Colshapes::newTube

Creates a Colshape into the shape of a Tube.


# Syntax

```js
mp.colshapes.newTube(x, y, z, height, range)
```

# Example
This page has no example

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>y:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>width:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>height:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>dimension:</b> <b><span style="color:#008017">Number</span></b> (optional)</li>
