# Colshapes::newCircle

Create a ColShape of circle in the 2D plane

# Syntax

```js
mp.colshapes.newCircle(x, y, radius, dimension)
```

# Example

```js
// TODO
```

# Parameters

<li><b>x: </b><span style="color:#008017">Number</span><b> in float</b></li>
<li><b>y: </b><span style="color:#008017">Number</span><b> in float</b></li>
<li><b>radius: </b><span style="color:#008017">Number</span><b> in float</b></li>
<li>dimension: <b><span style="color:#008017">Number</span></b> (optional parameter)</li>
