# Colshape::isPointWithin
Checking if a point is within the colshape. 

# Syntax

```js
Boolean colshape.isPointWithin(position);
```

# Example

```js
let position = player.position;
if (colshape.isPointWithin(position)) {
    console.log("Inside Colshape");
} else {
    console.log("Outside Colshape");
}
```

# Parameters

<li><b>position:</b> <b><span style="color:#008017">Vector3</span></b></li>
