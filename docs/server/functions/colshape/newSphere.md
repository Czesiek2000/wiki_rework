# Colshapes::newSphere

Creates a Sphere ColShape
        

# Syntax

```js
mp.colshapes.newSphere(x, y, z, range, dimension)
```

# Example
This page has no example

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>y:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>width:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>height:</b> <b><span style="color:#008017">Number</span></b> in float</li>
<li><b>dimension:</b> <b><span style="color:#008017">Number</span></b> (optional)</li>
