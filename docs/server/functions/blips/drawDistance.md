# Blip::drawDistance
Property used to have a fade in/out of the blip when you're in range of the draw distance.


# Syntax

```js
blip.drawDistance = 45.5; // The blip will be shown to each player near at 45.5 meters.
```

# Example
This page has no example

# Parameters

<li>distance</li>
