# Blip::alpha
This function is used to change the blip alpha.

!> Server-Side code

# Syntax

```js
blip.alpha = 255;
```

# Example

This will change a blip to be transparent.
```js
let createdBlip = mp.blips.new(1, new mp.Vector3(0, 0, 0));

createdBlip.alpha = 0;
```

# Parameters

<li>alpha</li>
