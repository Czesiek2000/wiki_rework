# Blip::shortRange
Property: Changes the behavior of the Blip on the minimap.


!> Server-Side


# Syntax

```js
blip.shortRange = true; // Auto-hide the blip if it's not longer in range of the minimap
blip.shortRange = false; // Always show the blip
```

# Example
This page has no example

# Parameters

<li>range</li>
