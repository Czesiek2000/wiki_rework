# Blip::scale
This function is used to change the blip scale.

!> Server-Side

# Syntax

```js
blip.scale = 2;
```

# Example

This will change a blip scale to 2.
```js
let createdBlip = mp.blips.new(1, new mp.Vector3(0, 0, 0));

createdBlip.scale = 2;
```

# Parameters

<li>scale</li>
