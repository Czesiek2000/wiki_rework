# Blip::routeFor

Creates a route to the blip from the player's location.

# Syntax

```js
blip.routeFor(player, color, scale);

```

# Example
This example will create a command for creating blip and add route to the player who entered it:

```js
mp.events.addCommand("routeBlip", (player) => {
	const position = player.position;
	const blip = mp.blips.new(1, position);

	blip.routeFor(player, 2, 1);
});
```

# Parameters
<li><b>player:</b> Player object or an array of player objects</li>
<li><b>color:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>

Colors and blips list available on [blips](../../../tables/blips.md) page