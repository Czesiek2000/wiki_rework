# Blip::rotation
This function is used to change the blip rotation.

!> Server-Side


# Syntax

```js
blip.rotation = 0;
```

# Example
This will change the blip to be upside-down.

```js
let createdBlip = mp.blips.new(1, new mp.Vector3(0, 0, 0));

createdBlip.rotation = 180;
```

# Parameters

<li>rotation</li>
