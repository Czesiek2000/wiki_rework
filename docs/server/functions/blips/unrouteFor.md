# Blip::unrouteFor

This function used for remove route to blip for player.

# Syntax

```js
blip.unrouteFor(Player player);
blip.unrouteFor(Player[] players);
```

# Example

That's example get's first blip's and remove route from player
```js
mp.events.addCommand("unrouteFirstBlip", (player) => {
  const blip = mp.blips.at(0);
  if (blip) {
    blip.unrouteFor(player);
  }
});
```

# Parameters

<li><b>player(s):</b> Array or object of player to which to remove route</li>
