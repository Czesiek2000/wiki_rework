# Blip::name
This function is used to change the name of the blip shown on the map. When you press **Esc** and **hover** over the blip, it will have this name.

!> Server-Side

# Syntax

```js
blip.name = 'String';
```

# Example
This will change a blip's name to 24/7 Shop.

```js
let createdBlip = mp.blips.new(140, new mp.Vector3(0, 0, 0));

createdBlip.name = '24/7 Shop';
```

# Parameters

<li>String</li>
