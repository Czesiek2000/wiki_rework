# Blip::color
Property related to the Blip's color.

!> Server-Side

# Syntax

```js
blip.color = 1;
```

# Example

This will change a blip to the color red.
```js
let createdBlip = mp.blips.new(140, new mp.Vector3(0, 0, 0));
createdBlip.color = 1;
```

# Parameters

<li>color</li>
