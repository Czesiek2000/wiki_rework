# Events::callLocal

This function calls an event registered in C#.

# Syntax

```js
mp.events.callLocal('eventName', [params...]);
```

# Example

?> Server-Side code

```js
mp.events.add("heathenEvent", (player) => {
  console.log("This event has been triggered.");
});

mp.events.addCommand("callheathen", (player) => { 
  mp.events.callLocal("heathenEvent", [player]);
});
```

# Parameters

* eventName <span style="color:#008017">String</span></b> - name of event
* params <span style="color:#008017">Array</span></b> - parameters you want to pass between events.