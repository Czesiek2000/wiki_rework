# Events::getAllOf
This function gets all handlers of the specified event.

?> Server-Side code


# Syntax

```js
let handlers = mp.events.getAllOf(eventName);
```

# Example

The example below gets all handlers of "myEvent".

```js
// server side
eventHandlers = mp.events.getAllOf('myEvent');
```

# Parameters
<li><b>Array</b>: Function</li>

<br/>

# Return value 
* <span style="color:#008017">Array</span></b>: function