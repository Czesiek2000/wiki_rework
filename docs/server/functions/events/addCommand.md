# Events::addCommand

This function registers a command handler.

# Syntax

```js
mp.events.addCommand(commandName, handlerFunction);
```

# Example
This example gives a weapon to the current player with the specified number of ammo. If not specified, it will give 10000.
```js
mp.events.addCommand("weapon", (player, fullText, weapon, ammo) => {
	var weaponHash = mp.joaat(weapon);

	player.giveWeapon(weaponHash, parseInt(ammo) || 10000);
});

// function parameters:
* player: Object
* fullText: Array // All arguments after the command name
* arg1, arg2, ...: Any // Each argument after the command name

// This example implements /me command
mp.events.addCommand("me", (player, message) => {
	mp.players.broadcast(`* ${player.name}: ${message}`);
});
```

# Parameters

<li><b>commandName:</b> <b><span style="color:#008017">String</span></b> (The name of the command you wish to attach a handler to)</li>
<li><b>handlerFunction:</b> <b><span style="color:#008017">Void</span></b> (The function that you want the command to trigger, which has to be defined before you add the handler)</li>
