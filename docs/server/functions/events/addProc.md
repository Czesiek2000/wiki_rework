# Events::addProc

This function register the specified player's Remote Prodecure Call (RPC) event and expects a callback.

# Syntax

```js
mp.events.addProc('eventProcName', callback);
```

# Example

```js
// register RPC 'test_proc' in server-side
mp.events.addProc('test_proc', (player, text) => {
  return 'hey beast: ' + text;
});

// client side
const response = await mp.events.callRemoteProc('test_proc', 'test');
// calls RPC 'test_proc' from server side and wait for result
mp.gui.chat.push(`response: ${response}`);
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>eventProcName</b>: <b><span style="color:#008017">String</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>callback</b>: <b><span style="color:#008017">Any</span></b></li>

<br />
<br />
<span style="font-weight:bold; color:red;">*</span> - required arguments