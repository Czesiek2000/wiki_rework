# Events::add
This function is use to register event inside game. This functions is works the same on client and server.


# Syntax

```js
events.add(name, ...arguments);
    
events.add( associativeArray );
```

# Example
This function registers event handlers.

Returning true will destroy automatically the event handler.

```js
function onPlayerDeath(player, reason, killer) {
  console.log(player.name + " died.");
}

mp.events.add({
  "playerJoin" : player => {
    console.log("New player: " + player.name);
  },

  "playerQuit" : (player, reason) => {
    console.log(player.name + " quit");
  },

  "playerDeath" : onPlayerDeath
});
    
```

# Parameters
<li>name - event name.</li>
<li>arguments <b><span style="color:#008017">Array</span></b> - list of arguments.</li>