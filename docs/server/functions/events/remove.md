# Events::remove


Removes the specified event from events tree.

# Syntax

```js
mp.events.remove("eventName"); // Removes all eventName instances
mp.events.remove("eventName", functionInstance); // Removes specified event instance.
mp.events.remove(["eventName1", "eventName2"]); // Removes all specified events instances.
```

# Example

```js
// server side

function onPlayerDeath(player, reason, killer){
    console.log(player.name + " died.");
    mp.events.remove("playerDeath", onPlayerDeath); // Once someone died, remove the event.
}

mp.events.add("playerDeath", onPlayerDeath);
```

# Parameters
* eventName - name of event