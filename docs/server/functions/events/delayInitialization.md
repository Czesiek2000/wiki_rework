# Events::delayInitialization


This function delays server's initialization of packages to run early functions.

# Syntax
No syntax on this page.

# Example

```js
mp.events.delayInitialization = true;
let asyncDone = false;

(async () => {
  // async calls done, now let it invoke "packagesLoaded"
  await someAsyncFunction();
  mp.events.delayInitialization = false;
  asyncDone = true;
})();

mp.events.add('packagesLoaded', () => {
  console.log(`async done: ${asyncDone}`); // Doesn't get triggered till mp.events.delayInitialization is true.
});
    
```


# Parameters

This function has no parameters

<br/>

# Setter
<li><b><span style="color:#008017">Boolean</span></b></li>