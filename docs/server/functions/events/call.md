# Events::call

This function calls registered event handlers. This function can call serverside events from serverside and clientside events from clientside.

# Syntax

```js
events.call(eventName, ...args);
```

# Example
Creates an event called 'anyCallbackName' and then calls that event which will then output "yea" to the console.

```js
mp.events.add("anyCallbackName", (anything) => {
  console.log(anything);
});

mp.events.call("anyCallbackName", "yea");
```

# Parameters
<ul><li><b>eventName</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>args</b>: Any</li></ul>
