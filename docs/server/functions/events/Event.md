# Events::Event

OOP constructor of Events' add function

# Syntax

```js
let ev = new mp.Event(name, functionHandler);
```

# Example

```js
let ev = new mp.Event("playerDeath", (player, reason, killer) =>{
  mp.players.broadcast('First blood!');
  ev.destroy(); // this event handler will be not called anymore since it's destroyed
});

ev.destroy(); // due to this line the event is never going to be executed if we call this before it
```

# Parameters
<ul><li><b>name</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>functionHandler</b>: <b><span style="color:#008017">Void</span></b></li></ul>

<br/>

# Return value

<li><b><span style="color:#008017">Event Handler</span></b></li>
