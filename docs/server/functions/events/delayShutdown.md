# Events::delayShutdown


    

This function delays server's shutdown till you finish all your async tasks.
    

# Syntax
No syntax on this page.


# Example
```js
mp.events.add("serverShutdown", async () => {
  mp.events.delayShutdown = true;
  await yourAsyncFunction();
  mp.events.delayShutdown = false;
});
    
```

# Parameters
This function has no parameters.

<br />

# Setter
<li><b><span style="color:#008017">Boolean</span></b></li>