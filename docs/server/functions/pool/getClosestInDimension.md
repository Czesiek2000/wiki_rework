# Pool::getClosestInDimension




Gets the closest set of entities to a position in the defined dimension.



# Syntax

```js
mp.pool.getClosestInDimension(position, dimension[, numberOfEntities])
```

# Example

```js
mp.events.addCommand('closest', (player) => {
			let closest = mp.vehicles.getClosestInDimension(player.position, 0, 3);
			player.outputChatBox(`The three closest vehicles: ${closest}`)
		});
		
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>dimension</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>numberOfEntities</b>: <b><span style="color:#008017">Int</span></b></li>
