# Pool::length
This property is used to get the element count of a pool.

**Note: this property is read-only.**

# Syntax

```js
Number pool.length;
```

# Example

This example will return vehicles and players count.

```js
let getStuffCount = () => {
	return [mp.players.length, mp.vehicles.length];
};

let [players, vehicles] = getStuffCount();
console.log(`Server have ${vehicles} vehicles and ${players} players.`)
```

# Parameters

no parameters
