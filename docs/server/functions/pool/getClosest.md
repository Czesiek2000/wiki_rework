# Pool::getClosest


Sorts the closest entities to a certain specified point in the entities pool.



# Syntax

```js
mp.Pool.getClosest(pos, limit)
```

# Example

```js
let player = mp.players.getClosest([0,0,0]);
console.log(player.id);

let players = mp.players.getClosest([0,0,0], 2);
console.log(players[0].id);

let allPlayersSortedByDistanceToPoint = mp.players.getClosest([0,0,0], mp.players.length);
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>pos</b>: <b><span style="color:#008017">Array</span></b> ([x, y, z])</li>
<li><b>limit</b>: <b><span style="color:#008017">Int</span></b> (Default is 1)</li>
