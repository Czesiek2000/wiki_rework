# Pool::forEachInDimension

This function is used to call a function for each elements in the pool.


# Syntax

```js
void pool.forEachInDimension(int dimension, function callingFunction);
```

# Example

This example will print the names of players in dimension 0 to the server console. (Dimension 0 is the default dimension)

```js
mp.players.forEachInDimension(0, (player, id) => {
	console.log("Player: " + player.name);
});
```

# Parameters

<li><b>Dimension:</b> Int, dimension</li>
<li><b>callingFunction:</b> Function, what will be called.</li>
