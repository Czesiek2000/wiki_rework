# Pool::exists

This function is used for check, exists entity with ID in pool or not.


# Syntax

```js
Boolean pool.exists(Number ID)
```

# Example

This example will check player with ID in pool, and write player name into chat if he exists.

```js
let isPlayerExists = mp.players.exists(1488);
if (isPlayerExists) {
	let player = mp.players.at(1488);
	console.log(`Player with id 1488 exists and have nickname ${player.name}`);
} else {
	console.log(`Player by id 1488 does not exists...`);
};
```

# Parameters

<li><b>ID:</b> Element ID, what you need get from the pool.</li>


# Return value

Return true if entity exists, false if not exists.