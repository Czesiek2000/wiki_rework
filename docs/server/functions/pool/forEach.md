# Pool::forEach

This function is used for calling a function for each element in a pool.


# Syntax

```js
pool.forEach(Function callingFunction);
		
```

# Example

This example will generate text with all player nicknames.

```js
let getNicknamesText = () => {
    let text = ``;
    mp.players.forEach(
        (player, id) => {
            text = text == `` ? player.name : `${text} , ${player.name}`;
        }
    );
    return text;
};

let blahBlah = getNicknamesText();
console.log(blahBlah != `` ? blahBlah : `Server not have players.`);
```

This example will add a command to remove all server vehicles by forEach.
		
```js
mp.events.addCommand(`removeAll`, 
	(player) => {
		mp.vehicles.forEach(
			(vehicle) => {
				vehicle.destroy();
			}
		);
		mp.players.broadcast(`${player.name} DESTROY ALL CARS!`);
	}
);
```

This example will teleport a player that types the command /port to the player whose name he writes as 1st argument.

```js
mp.events.addCommand('port', (player, name) => {
	mp.players.forEach(_player => {
		if(_player.name === name)
			player.position = _player.position;
	});
});
```

# Parameters

<li><b>callingFunction:</b> Function, what will be called.</li>
