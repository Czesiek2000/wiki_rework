# Pool::forEachInRange

This function is used for calling a function for each element in a pool, but only if it in range of position.

# Syntax

```js
pool.forEachInRange(Vector3 position, int range [, int dimension], function callingFunction);
```

# Example

This example will generate an array of vehicles within 100 units of the player (game distance, meters).

```js
let getVehiclesNearbyMe = (player) => {
	const returnVehicles = [];
	
	mp.vehicles.forEachInRange(player.position, 100,
		(vehicle) => {
			returnVehicles.push(vehicle);
		}
	);
	
	return returnVehicles;
};

let vehiclesNearbyMe = getVehiclesNearbyMe(mp.players.at(0));
console.log(`Vehicles nearby me: x ${vehiclesNearbyMe.length}`);
		
```

# Parameters
<li><b>Dimension:</b> Int, dimension</li>
<li><b>callingFunction:</b> Function, what will be called.</li>
