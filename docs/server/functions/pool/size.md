# Pool::size

A property to get an element pool size, useful to be used in explicit array size declaration or manual for loop size (non foreach).
		

**Note: This property is read only**
		

# Syntax

This function has no syntax

# Example

```js
// todo
		
```

# Parameters

<li><b><span style="color:#008017">Number</span></b> - Size of pool</li>
