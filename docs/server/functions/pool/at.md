# Pool::at

This function is used to return an element from a pool at an ID.

# Syntax

```js
Entity pool.at(Number ID)
```

# Example

This example will return a player by ID and output his name in the console.

```js
let player = mp.players.at(1488);
if (player) {
	console.log(`Player with id 1488 have nickname ${player.name}`);
} else {
	console.log(`Player by id 1488 does not exists...`);
};
		
```

# Parameters

<li><b>ID:</b> Element ID, what you need get from the pool.</li>

<br />

# Return value
An entity with the selected ID from a pool, or undefined if entity with this ID does not exists.
