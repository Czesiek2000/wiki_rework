# Entity::getOwnVariable

Allows to get the value set with entity.setOwnVariable(key, value).

# Syntax

```js
entity.getOwnVariable(key);
```

# Example

This will set the job id for this player, which will only be available to him.

```js
player.setOwnVariable("jobId", 9);
```

So we get the value that we set earlier.
```js
player.getOwnVariable("jobId"); // 9
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>entity</b>: <b><span style="color:#008017">Object</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>key</b>: <b><span style="color:#008017">String</span></b></li>


