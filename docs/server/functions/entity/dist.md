# Entity::dist

Gets the distance between two entities


# Syntax

```js
entity.dist(position)
```

# Example

Gets the distance between a player and a vehicle

```js
let distVeh = mp.vehicles.new(mp.joaat('turismor'), new mp.Vector3(-423.88, 1128.86, 326));
mp.events.addCommand('dist', (player) => {
	let dist = player.dist(distVeh.position);
	player.outputChatBox(`Distance: ${dist}`)
});

```

# Parameters
<li><b>position:</b> <b><span style="color:#008017">Vector3</span></b></li>
