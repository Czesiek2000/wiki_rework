# Entity::setVariables

Set multiple custom data variables to an entity.


# Syntax

```js
entity.setVariables({'variable1': value1, 'variable2': value2});

```

# Example

This creates a vehicle and sets multiple variables upon creation

```js
mp.events.addCommand('car', (player) => {
    mp.vehicles.new(mp.joaat('turismor'), player.position);
});

mp.events.add("entityCreated", (entity) => {
    if(entity.type === "vehicle"){
        entity.setVariables({
            "miles": 0,
            "fuel": 100
        });
    }
});

```

# Parameters

This page has no parameters