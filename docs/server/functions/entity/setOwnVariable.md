# Entity::setOwnVariable

Sets the data available to the player as opposed to entity.setVariable(key, value).

# Syntax

```js
entity.setOwnVariable(key, value);
```

# Example

This will set the job id for this player, which will only be available to him.

```js
// server side
player.setOwnVariable("jobId", 9);
```

```js
mp.players.local.getVariable("jobId"); // 9
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>entity</b>: <b><span style="color:#008017">Object</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>key</b>: <b><span style="color:#008017">String</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>value</b>: <b><span style="color:#008017">Any</span></b></li>


<span style="font-weight:bold; color:red;">*</span> - required arguments