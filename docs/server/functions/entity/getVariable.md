# Entity::getVariable

Retrieves the custom data from the entity.

# Syntax

```js
entity.getVariable('variableName');
```

# Example

This example will retrieve the data from the variable 'veh' that is assigned to the player

```js
mp.events.addCommand('setNick', (player, nickname) => {
  player.setVariable('oldNick', player.name);
  player.name = nickname;
  player.setVariable('newNick', nickname);
  console.log(`Player[${player.id}] Changed nick from ${player.getVariable('oldNick')} to ${player.name}`);
});
```

# Parameters
<li><span style="font-weight:bold; color:red;">*</span><b>entity</b>: <b><span style="color:#008017">Object</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>name</b>: <b><span style="color:#008017">String</span></b></li>