# Entity::destroy

This function is used to destroy a created entity.

# Syntax

```js
entity.destroy();
```

# Example
This example creates a vehicle, and destroys after waiting 5 seconds.

```js
mp.events.addCommand('veh', (player) => {
    let vehicle = mp.vehicles.new(mp.joaat(`zentorno`), player.position); //Create a Vehicle
    setTimeout(()=>{ //Create a Timer
        if(vehicle){
            vehicle.destroy(); //Destroy the Vehicle
        }
    },5000);
});

```

# Parameters

This function has no parameters