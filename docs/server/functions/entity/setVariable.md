# Entity::setVariable

Set custom data to an entity.

# Syntax

```js
// To set one variable at a time
entity.setVariable(name, value);

// To set multiple variables at a time.
entity.setVariable({name1: value1, name2: value2});

```

# Example

This binds the vehicle to the player

```js
let veh = mp.vehicles.new(vehName, player.position);
player.setVariable('veh', veh);

```

This is an example of setting multiple variables to a player which is **not implemented and does not work (yet)**

```js
player.setVariable({
  name: 'Player',
  age: 20
});
```

# Parameters

<ul><li><span style="font-weight:bold; color:red;">*</span><b>entity</b>: <b><span style="color:#008017">Object</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>name</b>: <b><span style="color:#008017">String</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>value</b>: <b><span style="color:#008017">Any</span></b></li></ul>

<span style="font-weight:bold; color:red;">*</span> - required arguments