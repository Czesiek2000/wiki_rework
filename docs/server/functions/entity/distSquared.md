# Entity::distSquared

Function that gets the squared distance between two entities.

?> Server-side code
# Syntax

```js
entity.distSquared(entity2.position);
```

# Example
This page has no example

# Parameters

<li><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
