# Marker::Marker

Function: Create a marker


# Syntax

```js
mp.markers.new(type, position, scale, {
    direction: direction,
    rotation: rotation,
    color: color,
    visible: visible,
    dimension: dimension
});
```

# Example


# Parameters

<ul>
<li><b>type</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>scale</b>: <b><span style="color:#008017">Float</span></b></li>
<li><b>direction</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>rotation</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>color</b>: <b><span style="color:#008017">[Int, Int, Int, Int]</span></b> [0:255]</li>
<li><b>visible</b>: <b><span style="color:#008017">Boolean</span></b></li>
<li><b>dimension</b>: <b><span style="color:#008017">Int</span></b></li>
</ul>