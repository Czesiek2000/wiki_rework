# Marker::setColor
Function: Sets the marker's color.


?> Server-Side code


# Syntax

```js
marker.setColor(r, g, b, a);
```

# Example
This page has no example.

<br />

# Parameters

<ul>
<li>r: <b><span style="color:#008017">number</span></b></li>
<li>g: <b><span style="color:#008017">number</span></b></li>
<li>b: <b><span style="color:#008017">number</span></b></li>
<li>a: <b><span style="color:#008017">number</span></b></li>
</ul>