# Marker::scale

Sets the scale of the selected marker

!> Client side code

# Syntax
```js
marker.scale = number;
```

# Example
Changes the scale of the marker from 3.0 to 10

```js
let testMarker = mp.markers.new(1, new mp.Vector3(-431.88, 1146.86, 325), 2.0, {
    scale: 3.0
});

testMarker.scale = 10;
```

<br />

# Parameters
<li><b>number</b>: <b><span style="color:#008017">number</span></b></li>