# Marker::getColor
Gets the marker's color.


?> Server-Side code


# Syntax

```js
[r,g,b,a] = marker.getColor();
```

# Example
This function has no example.

# Parameters
<ul>
<li>r: <b><span style="color:#008017">number</span></b></li>
<li>g: <b><span style="color:#008017">number</span></b></li>
<li>b: <b><span style="color:#008017">number</span></b></li>
<li>a: <b><span style="color:#008017">number</span></b></li>
</ul>