# Player::streamedPlayers

Returns array of streamed-in players for the player.


**NOTE: This property is read-only.** 

# Syntax

```js
player.streamedPlayers
```

# Example

```js
// TODO
```

# Parameters

This function has no parameters
