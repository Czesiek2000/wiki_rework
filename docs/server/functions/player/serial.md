# Player::serial

This property returns player's client serial.


**Note: this property is read-only.**


# Syntax

This function has no syntax

# Example
```js
let playerSerial = player.serial;
player.outputChatBox("Your Serial is: " + playerSerial);
```


# Parameters

This function has no parameters