# Player::aimTarget
This property is the aim target of a player.

**Note: this property is read-only. If player currently [isn't aiming](./isAiming.md), this property will be last aim target**


# Syntax
This function has no syntax

# Example
```js
if (player.isAiming)
  player.outputChatBox('Your aim target is ' + player.aimTarget);
```


# Parameters
This function has no parameters
