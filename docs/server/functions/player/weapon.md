# Player::weapon

This property returns players' current weapon.


**Note: this property is read-only.**


# Syntax

```js
player.weapon
```

# Example

```js
let playerWeapon = player.weapon
```


# Parameters
This function has no parameters
