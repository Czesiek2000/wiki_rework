# Player::health

This property using for getting or setting player health.


# Syntax

This page has no syntax

# Example

```js
let playerHealth = player.health
player.health = 100
```

# Parameters

<li><b><span style="color:#008017">number</span></b> health value (0 to 100)</li>
