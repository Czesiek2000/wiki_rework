# Player::isOnLadder
Returns whether the specified ped is on ladder.


**Note: this property is read-only.**


# Syntax

This function has no syntax

# Example

```js
if (player.isOnLadder) {
	player.outputChatBox("You are climbing on ladder right now!");
}
```


# Parameters

<li><b><span style="color:#008017">Boolean</span></b> - true if player ped is on ladder, false otherwise.</li>

