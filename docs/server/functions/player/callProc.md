# Player::callProc

This function calls the specified player's clientside Remote prodecure call (RPC) event and expects a callback.


# Syntax

```js
player.callProc('eventProcName', [...args]);
```

# Example

```js
// server side
(async () => {
  try {
    let res = await player.callProc('test_proc', ['ok']);
    console.log('succ', res);
  } catch(e) {
    console.error('Error: ' + e);
  }
})();

// client side
mp.events.addProc('test_proc', (player, text) => {
  return 'hey beast: ' + text;
});

// also supports async functions
mp.events.addProc('test_proc', async (text) => {
  await doAsyncJob();
  return 'hey beast: ' + text;
});
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>eventProcName</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>args</b>: <b><span style="color:#008017">Any</span></b></li>
