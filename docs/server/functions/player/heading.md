# Player::heading

This property using for getting or setting player rotation.


# Syntax
This page has no syntax


# Example
```js
let playerRotate = player.heading
player.heading = 90
```

# Parameters
<li><b><span style="color:#008017">number</span></b> facing angle in degree</li>
