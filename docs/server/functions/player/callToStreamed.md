# Player::callToStreamed

This function calls all streamed in players' clientside from the specified player passing data.


# Syntax

```js
player.callToStreamed(includeSelf, 'eventName', [...args]);
```

# Example

```js
// server side
mp.players.at(0).callToStreamed(false, 'catchedSuckers', [mp.players.at(0).name]);

// client side
mp.events.add('catchedSuckers', (name) => {
  mp.gui.chat.push("You sucker got catched by" + name);
});
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>includeSelf</b>: <b><span style="color:#008017">String</span></b> (Calls the specified player's clientside also along with the streamed players to him)</li>
<li><span style="font-weight:bold; color:red;">*</span><b>eventProcName</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>args</b>: <b><span style="color:#008017">Any</span></b></li>
