# Player::setHeadOverlay
OverlayID ranges from **0** to **12**, index from **0** to `_GET_NUM_OVERLAY_VALUES(overlayID) - 1`, and opacity from `0.0` to `1.0`.

First and second color you can take in the list of hair colors.

List of colors: https://wiki.gtanet.work/index.php?title=Hair_Colors

**To disable any overlay use 255 as index.**

<table class="wikitable">
<tbody><tr>
<td>OverlayID
</td>
<td>Part
</td>
<td>Index
</td></tr>
<tr>
<td>0
</td>
<td>Blemishes
</td>
<td>0-23
</td></tr>
<tr>
<td>1
</td>
<td>Facial Hair
</td>
<td>0-28
</td></tr>
<tr>
<td>2
</td>
<td>Eyebrows
</td>
<td>0-33
</td></tr>
<tr>
<td>3
</td>
<td>Ageing
</td>
<td>0-14
</td></tr>
<tr>
<td>4
</td>
<td>Makeup
</td>
<td>0-74
</td></tr>
<tr>
<td>5
</td>
<td>Blush
</td>
<td>0-32
</td></tr>
<tr>
<td>6
</td>
<td>Complexion
</td>
<td>0-11
</td></tr>
<tr>
<td>7
</td>
<td>Sun Damage
</td>
<td>0-10
</td></tr>
<tr>
<td>8
</td>
<td>Lipstick
</td>
<td>0-9
</td></tr>
<tr>
<td>9
</td>
<td>Moles/Freckles
</td>
<td>0-17
</td></tr>
<tr>
<td>10
</td>
<td>Chest Hair
</td>
<td>0-16
</td></tr>
<tr>
<td>11
</td>
<td>Body Blemishes
</td>
<td>0-11
</td></tr>
<tr>
<td>12
</td>
<td>Add Body Blemishes
</td>
<td>0-1
</td></tr>
</tbody></table>



# Syntax

```js
player.setHeadOverlay(overlayID, index, opacity, firstColor, secondColor);

// The variation of this function for peds (mp.peds) does not take values to set the overlay color. It uses the syntax used in the native.
ped.setHeadOverlay(overlayID, index, opacity);

```

```js
player.setHeadOverlay(overlayID, [index, opacity, firstColor, secondColor]);

```

# Example
On the wiki there is C# example but this is JS API wiki, so it is not included here. If you want checkout [wiki](https://wiki.rage.mp/index.php?title=Player::setHeadOverlay) 

# Parameters
<ul><li><b>overlayID:</b> int</li>
<li><b>index:</b> int</li>
<li><b>opacity:</b> float</li>
<li><b>firstColor:</b> int</li>
<li><b>secondColor:</b> int</li></ul>

<br />

# Return value
<li><b>Undefined</b></li>

