# Player::getClothes
This function return a hash of player clothes.

# Syntax

```js
Object player.getClothes(Number componentNumber)
```

# Example

This example outputs player clothes for component ID.

```js
mp.events.addCommand('getclothes', (player, text, component) => {
  if (component == null || !parseInt(component)) {
    return player.outputChatBox('Use syntax: /getclothes [component_id]');
  } else {
    let clothes = player.getClothes(parseInt(component));
    player.outputChatBox('drawable: ' + clothes.drawable + ' texture: ' + clothes.texture + ' palette: ' + palette.texture);
  }
});
```

# Parameters

This function has no parameters


# List of components:
<ul><li>0 - Head</li>
<li>1 - Beard</li>
<li>2 - Hair</li>
<li>3 - Torso</li>
<li>4 - Legs</li>
<li>5 - Hands</li>
<li>6 - Foot</li>
<li>7 - None?</li>
<li>8 - Accessories like parachute, scuba tank</li>
<li>9 - Accessories like bags, mask, scuba mask</li>
<li>10- Decals and mask</li>
<li>11 - Auxiliary parts for torso</li></ul>

Object keys:
<ul><li>drawable - ID of clothing.</li>
<li>texture - ID of texture.</li>
<li>palette - ID of palette.</li></ul>