# Player::seat


This property returns the player's current vehicle seat.


Seats:
<ul><li>0 - driver seat</li>
<li>1 - passenger seat 1</li>
<li>2 - passenger seat 2</li>
<li>3 - passenger seat 3</li></ul>

In version 0.3.7 or earlier, seats:
<ul><li>-1 - driver seat</li>
<li>0 - passenger seat 1</li>
<li>1 - passenger seat 2</li>
<li>2 - passenger seat 3</li></ul>




# Syntax

```js
player.seat
```

# Example

```js
if (player.vehicle)
  player.outputChatBox('Your vehicle seat ID is ' + player.seat);
else
  player.outputChatBox('You are not in the vehicle right now!');

```

# Parameters

<li><b><span style="color:#008017">Int</span></b></li>
