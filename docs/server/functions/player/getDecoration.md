# Player::getDecoration

This function gets the tattoo (decoration) from a collection specified.


List of Collections - [root collection](https://github.com/root-cause/v-tattoos)


# Syntax

```js
player.getDecoration(Hash collection, Hash hash)
```

# Example

This example get ped decoration.

```js
const decoration = player.getDecoration(4292880523, mp.joaat('FM_Tat_F_004')); // aka mpBusiness_overlays (mp.joaat('mpBusiness_overlays'))

console.log(decoration); // {"collection":598190139,"hash":false}

```

# Parameters

This function has no parameters