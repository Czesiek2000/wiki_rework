# Player::action

This property returns player action.

**Note: this property is read-only.**

# Syntax
This function has no syntax

# Example
This function has no example
```js
if (player.action == 'in_cover') {
  player.outputChatBox('You are in the cover!');
}
```

# Parameters
This function has no parameters

# List of actions
<ul>
<li>climbing</li>
<li>in_cover</li>
<li>aiming_from_cover</li>
<li>diving</li>
<li>entering_vehicle</li>
<li>exiting_vehicle</li>
<li>jumping</li>
<li>moving</li>
<li>moving_aiming</li>
<li>moving_reloading</li>
<li>parachuting</li>
<li>ragdoll</li>
<li>aiming</li>
<li>reloading</li>
<li>stopped</li></ul>