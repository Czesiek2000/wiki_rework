# Player::giveWeapon


This function gives a weapon(see) for the player.


# Syntax

```js
player.giveWeapon(Number weaponHash, Number ammo)
player.giveWeapon(Array weaponHash, Number ammo)
player.giveWeapon(Array weaponHash, Array ammo)
player.giveWeapon(Object weapons)
```

# Example

Example 1. This example gives 1 weapon for player.

```js
mp.events.add('weapon', (player, _, weapon = "weapon_specialcarbine_mk2") => {
    player.giveWeapon(mp.joaat(weapon), 1000);
});
```

Example 2. This example gives 2 weapons for player.

```js
mp.events.add('playerCommand', (player, command) => {
  let arr = command.split(' ');
  if (arr[0] == 'weapon') {
    player.giveWeapon([3220176749, 2210333304], 1000); // Assault Rifle, Carbine Rifle
  }
});
```

# Parameters

<li><b>weaponHash</b>: <b><span style="color:#008017">Number</span></b></li>
<li><b>ammo</b>: <b><span style="color:#008017">Number</span></b></li>

<li><b>weaponHash</b>: <b><span style="color:#008017">Array</span></b></li>
<li><b>ammo</b>: <b><span style="color:#008017">Number</span></b></li>

<li><b>weaponHash</b>: <b><span style="color:#008017">Array</span></b></li>
<li><b>ammo</b>: <b><span style="color:#008017">Number</span></b></li>
