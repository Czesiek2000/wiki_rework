# Player::outputChatBox

This functions writes a chat message to player.


# Syntax

```js
player.outputChatBox(text);
```

# Example

This example welcomes player on connect.

```js
mp.events.add(`playerJoin`,	player => {
	player.outputChatBox(`Welcome to the server, ${player.name}!`);
});
```

This example just output color texts.

**Note: chat.colors property should be enabled - if you wanna use that.**

```js
let red = `#ff0000`;
let player = mp.players.at(1337); // 1337 is a player id
if (player) {
	player.outputChatBox(`!{#dddddd}This is Grey! !{#ffffff}This is White! !{${red}}This is Red!`);
	player.outputChatBox(` !{255, 0, 0}This is too Red! !{green}This is Green! !{255, 0, 255, 0.5}This is opacity Pink!`);
};
```

# Parameters
<li><b>text:</b> <b><span style="color:#008017">String</span></b> Text what should be output in player chat.</li>
