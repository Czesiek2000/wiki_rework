# Player::voiceListeners

This property is used to return all active voice listeners as array, which got added by player.enableVoiceTo(target).





# Syntax

This function has no syntax

# Example

This example will remove all active listeners from the player list.

```js
player.voiceListeners.forEach((listener) => {
	player.disableVoiceTo(listener);
});

```

# Parameters

This function has no parameters
