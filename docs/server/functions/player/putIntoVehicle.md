# Player::putIntoVehicle

This function puts player into vehicle.


Seats:
<ul><li>-1 - driver seat</li>
<li>0 - passenger seat 1</li>
<li>1 - passenger seat 2</li>
<li>2 - passenger seat 3</li></ul>

In version >1.0 Seats:
<ul><li>0 - driver seat</li>
<li>1 - passenger seat 1</li>
<li>2 - passenger seat 2</li>
<li>3 - passenger seat 3</li></ul>

**NOTE: You can't put player in vehicle immediately after creating vehicle, use timeout (200 ms will be fine)**


# Syntax

```js
player.putIntoVehicle(Vehicle vehicle, Number seat)

```

# Example

```js
// 0.3.7
mp.events.addCommand('veh', (player, vehicle = "T20") => {
  const veh = mp.vehicles.new(mp.joaat(vehicle), player.position, {
    dimension: player.dimension
  });
  player.putIntoVehicle(veh, -1);
});

// 1.0 +
mp.events.addCommand('veh', (player, vehicle = "T20") => {
  const veh = mp.vehicles.new(mp.joaat(vehicle), player.position, {
    dimension: player.dimension
  });
  player.putIntoVehicle(veh, 0);
});

```

# Parameters

<li><b>vehicle: <span style="color:#008017">Vehicle</span></b></li>
<li><b>seat: <span style="color:#008017">Number</span></b></li>
