# Player::setHeadBlend
Shape / Skin First ID: "Mother"


Shape / Skin Second ID: "Father"


Shape / Skin Mix: Float values ranging from 0.0 ("Mother") to 1.0 ("Father").


Third ID: If you feel like another ID should be mixed in. Personally I leave 3rd IDs to 0 and 0.0f mix. Basically they appear to relate to the parent / legacy character customization in GTAO.


# Syntax

```js
player.setHeadBlend(Number shapeFirstID, Number shapeSecondID,
  Number shapeThirdID, Number skinFirstID, Number skinSecondID,
  Number skinThirdID, Number shapeMix, Number skinMix, Number thirdMix)
```

# Example
This function has no example

# Parameters
<li><b>shapeFirstID: <span style="color:#008017">Number</span></b></li>
<li><b>shapeSecondID: <span style="color:#008017">Number</span></b></li>
<li><b>shapeThirdID: <span style="color:#008017">Number</span></b></li>
<li><b>skinFirstID: <span style="color:#008017">Number</span></b></li>
<li><b>skinSecondID: <span style="color:#008017">Number</span></b></li>
<li><b>skinThirdID: <span style="color:#008017">Number</span></b></li>
<li><b>shapeMix: <span style="color:#008017">Number</span></b></li>
<li><b>skinMix: <span style="color:#008017">Number</span></b></li>
<li><b>thirdMix: <span style="color:#008017">Number</span></b></li>
