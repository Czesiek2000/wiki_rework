# Player::callUnreliable

This function triggers a client-side event for the selected player unreliably, which means it will be affected by potential packet loss, but it will be triggered way more faster, useful for when you need frequent triggers.


# Syntax

```js
player.callUnreliable('eventName', [...args]);
```

# Example

```js
// server side
let player = mp.player.at(1337); //Get player by ID
if (player) {
  player.callUnreliable('syncUnreliableData', [123]); // example
}


// client side

// may not always be called because of possible packetloss
mp.events.add('syncUnreliableData', (data) => {
  mp.gui.chat.push(`we got data: ${data}`);
});
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>eventName</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>args</b>: <b><span style="color:#008017">Any</span></b></li>
