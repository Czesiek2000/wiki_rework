# Player::enableVoiceTo

This function is used to enable voice listening to a certain player.


# Syntax

```js
player.enableVoiceTo(Player player)
```

# Example

```js
let player = mp.players.at(0);

mp.players.forEach((_player) => {
  if(player == _player) return false;
  
  player.enableVoiceTo(_player);
});
```

# Parameters

<li><b>player</b>: The Player you want to listen to.</li>