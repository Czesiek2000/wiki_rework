# Player::name

Property for getting or setting player name.


# Syntax
This function has no syntax


# Example

```js
let playerName = player.name
player.name = 'Bobby'
```


# Parameters

<li><b><span style="color:#008017">String</span></b> RAGE:MP nickname</li>
