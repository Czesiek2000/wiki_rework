# Player::kickSilent


Silently kicks the player which will then reconnect them back to the server. Useful for quick reconnects without going through the UI.

The client will act as if it has timed out.





# Syntax

```js
player.kickSilent();
```

# Example

```js
mp.events.addCommand('reconnect', (player) => {
    player.outputChatBox("Reconnecting you to the server...")
    player.kickSilent();
});
```

# Parameters

This function has no parameters
