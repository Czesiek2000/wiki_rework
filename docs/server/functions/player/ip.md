# Player::ip
This property returns the player's IP address.


**Note: this property is read-only.**


# Syntax

This page has no syntax


# Example

```js
const player = mp.players.at(0);

const playerIP = player.ip;
player.outputChatBox("Your IP is: " + playerIP);

```

# Parameters
<li><b><span style="color:#008017">string</span></b> The player's IP Address</li>
