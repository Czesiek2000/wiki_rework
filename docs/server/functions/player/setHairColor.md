# Player::setHairColor

Used for freemode (online) characters.


# Syntax

```js
player.setHairColor(colorID, highlightColorID);
```

# Example

```js
// todo
```

# Parameters

<ul><li><b>colorID:</b> int</li>
<li><b>highlightColorID:</b> int</li></ul>

<br />

# Return value
<li><b>Undefined</b></li>
