# Player::setCustomization

This method set player customization (NB: This resets your weapons also).


# Syntax

```js
player.setCustomization(Boolean gender, Number shapeFirst, Number shapeSecond, Number shapeThird, Number skinFirst, Number skinSecond, Number skinThird, Number shapeMix, Number skinMix, Number thirdMix, Number eyeColor, Number hairColor, Number hightlightColor, Number[] faceFeatures)

```

# Example

```js
var bGender = true;
if(player.model != 1885233650) {
	bGender = false;
}

var MotherBlend = 21, FatherBlend = 41, fBlendShape = 0.5, fBlendSkin = 0.5, HairHighlight = 0, HairColour = 0;

var NoseWidth = 0, NoseHeight = 0, NoseLength = 0, NoseBridge = 0, NoseTip = 0, NoseBridgeShift = 0;
var BrowHeight = 0, BrowWidth = 0, CBoneHeight = 0, CBoneWidth = 0, CheekWidth = 0, Eyes = 0, Lips = 0;
var JawWidth = 0, jawHeight = 0, ChinLength = 0, ChinPos = 0, ChinWidth = 0, ChinShape = 0, NeckWidth = 0;

player.setCustomization(bGender, MotherBlend, FatherBlend, 0, MotherBlend, FatherBlend, 0, fBlendShape, fBlendSkin, 0, 1, HairColour, HairHighlight,
	[
		NoseWidth, NoseHeight, NoseLength, NoseBridge, NoseTip, NoseBridgeShift, 
		BrowHeight, BrowWidth, CBoneHeight, CBoneWidth, CheekWidth, Eyes, Lips,
		JawWidth, jawHeight, ChinLength, ChinPos, ChinWidth, ChinShape, NeckWidth
	]
);

```

# Parameters

<ul><li><b>gender: <span style="color:#008017">Boolean</span></b></li>
<li><b>shapeFirst: <span style="color:#008017">Number</span></b></li>
<li><b>shapeSecond: <span style="color:#008017">Number</span></b></li>
<li><b>shapeThird: <span style="color:#008017">Number</span></b></li>
<li><b>skinFirst: <span style="color:#008017">Number</span></b></li>
<li><b>skinSecond: <span style="color:#008017">Number</span></b></li>
<li><b>skinThird: <span style="color:#008017">Number</span></b></li>
<li><b>shapeMix: <span style="color:#008017">Number</span></b></li>
<li><b>skinMix: <span style="color:#008017">Number</span></b></li>
<li><b>thirdMix: <span style="color:#008017">Number</span></b></li>
<li><b>eyeColor: <span style="color:#008017">Number</span></b></li>
<li><b>hairColor: <span style="color:#008017">Number</span></b></li>
<li><b>hightlightColor: <span style="color:#008017">Number</span></b></li>
<li><b>faceFeatures: <span style="color:#008017">Float[]</span>. <b>IMPORTANT</b>: The array must contain 20 elements</b></li></ul>
