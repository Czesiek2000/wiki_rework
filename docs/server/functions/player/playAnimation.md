# Player::playAnimation
Starts animation

Animations list - [up to date animation list](https://alexguirre.github.io/animations-list/)


Animation Flags :
<ul><li><b>NORMAL:</b> 0,</li>
<li><b>REPEAT:</b> 1,</li>
<li><b>STOP_LAST_FRAME:</b> 2,</li>
<li><b>UPPERBODY:</b> 16,</li>
<li><b>ENABLE_PLAYER_CONTROL:</b> 32,</li>
<li><b>CANCELABLE:</b> 120,</li>
<li><b>Odd number&nbsp;:</b> loop infinitely</li>
<li><b>Even number&nbsp;:</b> Freeze at last frame</li>
<li><b>Multiple of 4:</b> Freeze at last frame but controllable</li>
<li><b>01 to 15&nbsp;:</b> Full body</li>
<li><b>10 to 31&nbsp;:</b> Upper body</li>
<li><b>32 to 47&nbsp;:</b> Full body &gt; Controllable</li>
<li><b>48 to 63&nbsp;:</b> Upper body &gt; Controllable</li>
<li><b>001 to 255&nbsp;:</b> Normal</li>
<li><b>256 to 511&nbsp;:</b> Garbled</li></ul>

# Syntax

```js
player.playAnimation(dict, name, speed, flag)

```

# Example

```js
player.playAnimation('mp_arresting', 'idle', 1, 49)

```

# Parameters
<ul>
<li><b>dict: <span style="color:#008017">String</span></b></li>
<li><b>name: <span style="color:#008017">String</span></b></li>
<li><b>speed: <span style="color:#008017">Number</span></b></li>
<li><b>flag: <span style="color:#008017">Number</span></b></li></ul>
