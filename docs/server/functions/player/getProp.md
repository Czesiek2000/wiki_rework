# Player::getProp
This function returns a prop of player from ID.


Props:
<ul><li>0 - Helmets, hats, earphones, masks</li>
<li>1 - Glasses</li>
<li>2 - Ear accessories</li></ul>


# Syntax

```js
let prop = player.getProp(Number propID)
```

# Example

This example output a prop from prop ID.

```js
mp.events.add('playerCommand', (player, command) => {
  let arr = command.split(' ');
  if (arr[0] == 'getprop') {
    if (arr.length < 2 || !parseInt(arr[1])) {
      return player.outputChatBox('Use syntax: /getprop [prop_id]');
    } else {
      let prop = player.getProp(parseInt(arr[1]));
      player.outputChatBox('drawable: ' + prop.drawable + ' texture: ' + prop.texture);
    }
  }
});
```

# Parameters

<li><b>propID:</b> <b><span style="color:#008017">Number</span></b></li>
