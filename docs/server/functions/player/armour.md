# Player::armour

This property using for getting or setting player armour.


# Syntax
This function has no syntax


# Example
```js
let playerArmour = player.armour
player.armour = 100
```

# Parameters
This function has no parameters
