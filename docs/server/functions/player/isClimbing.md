# Player::isClimbing
This property returns true or false of player climbing state.


**Note: this property is read-only.**


# Syntax

This function has no syntax

# Example
```js
let playerIsClimbing = player.isClimbing
if (playerIsClimbing)
  player.outputChatBox('You are climbing right now!');
else
  player.outputChatBox('You are not climbing right now!');
```


# Parameters

This function has no parameters
