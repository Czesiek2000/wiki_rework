# Player::invoke
Invokes specified native function


# Syntax

```js
player.invoke(hash, [, ...args])

```

# Example

```js
function playerJoinHandle(player)
{
    player.invoke('0xBBAF4B768DDB7572', player, true) // 0xBBAF4B768DDB7572 - FREEZE_ENTITY_POSITION (1.39)
}

mp.events.add('playerJoin', playerJoinHandle)

```

# Parameters
<ul>
<li><b>hash: <span style="color:#008017">String</span></b></li>
<li><b>args: <span style="color:#008017">Any</span></b></li>
</ul>
