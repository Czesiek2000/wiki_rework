# Player::notify

This function sends a notification to the player.

You can use the colour codes found here: [Fonts and colors](../../../tables/colors.md)

# Syntax

```js
player.notify(message)
```

# Example

Creates a command to let the player enter a message which will then come up as a notification.

```js
mp.events.addCommand('notify', (player, message) => {
	if(!message) return player.outputChatBox("You need to enter a message.");
	player.notify(message);
});

```

# Parameters

<li><b>message</b> - <b><span style="color:#008017">String</span></b></li>
