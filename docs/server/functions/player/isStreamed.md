# Player::isStreamed

This function is used for check, player is located in stream distance for another player or not.

# Syntax

```js
player.isStreamed(Player player);

```

# Example

```js
let player = mp.players.at(1488);
mp.players.forEach((_player) => {
    if (player.isStreamed(_player)) {
        console.log(`Player by name ${_player.name} in stream distance!`);
    }
});

```

# Parameters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b></li>


# Return value
<li><b><span style="color:#008017">Boolean</span></b></li>
