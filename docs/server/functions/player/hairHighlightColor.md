# Player::hairHighlightColor

This property returns the player's secondary hair color.


**Note: This property is read-only.**


# Syntax
This page has no syntax


# Example
```js
const player = mp.players.at(0);

player.setHairColor(5, 3);

const highlightColor = player.hairHighlightColor; // highlightColor is 3

```

# Parameters

add parameters
