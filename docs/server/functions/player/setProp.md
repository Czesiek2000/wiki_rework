# Player::setProp

This function sets the prop for the player


Props:
<ul><li>0 - Helmets, hats, earphones, masks</li>
<li>1 - Glasses</li>
<li>2 - Ear accessories</li>
<li>6 - Watches</li>
<li>7 - Bracelets</li></ul>

# Syntax

```js
player.setProp(propID, drawableID, textureID)
```

# Example

Creates a command called setprops which lets you try out different props

```js
mp.events.addCommand('setprop', (player, args) => {
	if(!args || isNaN(args[0]) || isNaN(args[1]) || isNaN(args[2])){
		return player.outputChatBox("Syntax: /setprop [propID] [drawableID] [textureID]");
	}

	let cmd = args.split(' ');
	player.setProp(parseInt(cmd[0]), parseInt(cmd[1]), parseInt(cmd[2]));
});

```

# Parameters

<li><b>propID</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>drawableID</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>textureID</b>: <b><span style="color:#008017">Int</span></b></li>
