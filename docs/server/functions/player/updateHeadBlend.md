# Player::updateHeadBlend

Update player head blend data


# Syntax

```js
player.updateHeadBlend(shapeMix, skinMix, thirdMix)

```

# Example

```js
// todo

```

# Parameters

<ul><li><b>shapeMix: <span style="color:#008017">Number</span></b></li>
<li><b>skinMix: <span style="color:#008017">Number</span></b></li>
<li><b>thirdMix: <span style="color:#008017">Number</span></b></li></ul>
