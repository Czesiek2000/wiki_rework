# Player::setFaceFeature

Sets the various freemode face features, e.g. nose length, chin shape. Scale ranges from **-1.0** to **1.0**.

Index can be `0 - 19`.

Face feature [list](../../../tables/faceFeature.md)


# Syntax

```js
player.setFaceFeature(index, scale);
```

# Example

```js
function updateAppearance(genderChanged) {

  if (genderChanged) {
    mp.players.local.model = characterData.gender ? mp.game.joaat("mp_m_freemode_01") : mp.game.joaat("mp_f_freemode_01");

    setTimeout(sUpdateAppearance, 100);
  }

  else
    sUpdateAppearance();
  }

  function sUpdateAppearance() {
      
    for (var i = 0; i < 20; i++)
      mp.players.local.setFaceFeature(i, characterData.faceFeatures[i]);
  }
}
```

# Parameters

<ul><li><b>index:</b> int</li>
<li><b>scale:</b> float</li></ul>

<br />

# Return value
<li><b>Undefined</b></li>