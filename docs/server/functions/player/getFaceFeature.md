# Player::getFaceFeature

Gets the various freemode face features, e.g. nose length, chin shape. Scale ranges from -1.0 to 1.0.

Index can be 0 - 19.

See all face features [here](../../../tables/faceFeature.md)
# Syntax

```js
player.getFaceFeature(index);
```

# Example


# Parameters
<li><b>index:</b> int</li>

# Return value
<li><b>Scale</b></li>
