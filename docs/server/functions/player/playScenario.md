# Player::playScenario
This function makes the player play a scenario. Scenario List


This example will run an Paparazzi Scenario.


# Syntax

```js
player.playScenario(scenarioName);
```

# Example

```js
player.playScenario(scenario)
```

This example will run an Paparazzi Scenario.
```js
player.playScenario("WORLD_HUMAN_PAPARAZZI");
```

# Parameters

<li><b>scenario: <span style="color:#008017">String</span></b></li>