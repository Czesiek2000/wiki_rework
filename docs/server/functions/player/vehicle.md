# Player::vehicle

Returns the vehicle object the player is currently sitting in.


# Syntax

```js
player.vehicle
```

# Example

```js
if (player.vehicle)
  player.outputChatBox('You are in the vehicle right now!');
else
  player.outputChatBox('You are not in the vehicle right now!');

```

# Parameters

<li><b><span style="color:#008017">Object</span></b> - Vehicle</li>
