# Player::socialClub

This property returns players social club name.


**Note: this property is read-only.**

# Syntax


# Example

Show socialClub

```js
mp.events.add('playerJoin', player => {
    console.log(`[SERVER]: SocialClub: ${player.socialClub}`);
});

```

# Parameters

<li><b><span style="color:#008017">String</span></b> Rockstar Social Club player name</li>