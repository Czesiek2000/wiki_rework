# Player::rgscId

This property returns the player's Social club ID.

**Note: this property is read-only.**

# Syntax
This function has no syntax

# Example

```js
mp.events.addCommand('scID', (player) => {
 let socialID = player.rgscId;
 player.outputChatBox("Your Social club ID is: " + socialID);
});

```

# Parameters

<li><b><span style="color:#008017">string</span></b> The player's Social club ID.</li>
