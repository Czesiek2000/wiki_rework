# Player::getWeaponAmmo

Get the weapon's ammo.


# Syntax

```js
player.getWeaponAmmo(weaponHash);
```

# Example

Get the ammo of the weapon you are using.

```js
var weapon = player.weapon;
var ammo = player.getWeaponAmmo(weapon);
player.outputChatBox("The weapon has "+ammo+" bullets.");
```

# Parameters

<li><b>weaponHash</b>: <b><span style="color:#008017">Int</span></b></li>
