# Player::removeWeapon
This function removes a weapon of player.

[Weapon's hashes list](https://Czesiek2000.gitlab.io/docs/weapons) (currently work in progress)


# Syntax

```js
player.removeWeapon(weaponHash);
```

# Example

```js
player.removeWeapon(736523883); // Remove SMG if Player has it.
```

# Parameters

<li><b>weaponHash: <span style="color:#008017">Number</span></b></li>