# Player::kick

Kicks a player from the server.


# Syntax

```js
player.kick(reason);
```

# Example

```js
mp.events.addCommand('kick', (player, target) => {
	let newTarget = mp.players.at(target);
	if(!target || isNaN(target)) return player.outputChatBox("Syntax: /kick [playerID]");
	if(newTarget === null) return player.outputChatBox("There is no player online with the ID given.")
	newTarget.outputChatBox("You have been kicked from the server.");
	newTarget.kick('Kicked.');
});

```

# Parameters

<li><b>reason</b>: <b><span style="color:#008017">String</span></b> (This message does not show up for the player being kicked)</li>
