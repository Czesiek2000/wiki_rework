# Player::hairColor

This property returns the player's primary hair color.


**Note: This property is read-only.**


# Syntax
This page has no syntax


# Example
```js
const player = mp.players.at(0);

player.setHairColor(5, 0);

const hairColor = player.hairColor; // hairColor is 5

```

# Parameters

<li><b><span style="color:#008017">number</span></b> The player's hair color</li>