# Player::disableVoiceTo

This function is used to disable voice listening to a certain player.


# Syntax

```js
player.disableVoiceTo(Player target)

```

# Example

This example below disables all players from hearing the player with ID '0'.

```js
let player = mp.players.at(0);

mp.players.forEach((_player) => {
    if(player == _player) return false;
    
    player.disableVoiceTo(_player);
});
```

# Parameters
<li><b>target</b>: The Player you want to stop listen to.</li>