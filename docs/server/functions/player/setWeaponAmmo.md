# Player::setWeaponAmmo

Sets the amount of ammo for the selected weapon

# Syntax

```js
player.setWeaponAmmo(weaponHash, ammo);
```

# Example

Gives you a weapon and lets you change the amount of ammo for that weapon. We use the assault rifle weapon has in this example.

```js
mp.events.addCommand('weapon', (player) => {
  player.giveWeapon(3220176749, 1000);
})

mp.events.addCommand('setammo', (player, _, ammo) => {
  player.setWeaponAmmo(3220176749, parseInt(ammo));
});

```

# Parameters
<ul><li><b>weaponHash</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>ammo</b>: <b><span style="color:#008017">Int</span></b></li></ul>
