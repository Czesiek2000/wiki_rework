# Player::removeAllWeapons
Removes all weapons from the player




# Syntax

```js
player.removeAllWeapons();

```

# Example

Uses /removeweapons to remove all weapons from a player. Use /weapons to give yourself weapons to test this out.


```js
mp.events.addCommand('weapons', (player) => {
  player.giveWeapon(mp.joaat('weapon_pumpshotgun'), 100);
  player.giveWeapon(mp.joaat('weapon_microsmg'), 1000);
});

mp.events.addCommand('removeweapons', (player) => {
  player.removeAllWeapons();
  player.outputChatBox(`You have taken off all your weapons.`);
});

```

# Parameters

<li><b><span style="color:#008017">Object</span></b> - Weapon</li>
<li><b>ammo: <span style="color:#008017">Number</span></b></li>
