# Player::weaponAmmo

Gets ammo from the current player's weapon

**Note: this property is read-only.**




# Syntax

```js
Number player.weaponAmmo;
```

# Example

It will show ammo from the current player's weapon

```js
mp.events.addCommand('ammo', (player) => {
  player.notify(`Ammo: ${player.weaponAmmo}`);
});

```

# Parameters

This function has no parameters
