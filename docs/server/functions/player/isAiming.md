# Player::isAiming

This property returns true or false of player aim state.


**Note: this property is read-only.**


# Syntax

This page has no syntax

# Example
```js
let playerIsAiming = player.isAiming
if (playerIsAiming)
  player.outputChatBox('You are aiming right now!');
else
  player.outputChatBox('You are not aiming right now!');
```


# Parameters


This page has no parameters
