# Player::ping
This property returns players ping.


**Note: this property is read-only.**


# Syntax
This function has no syntax


# Example
```js
let playerPing = player.ping;
player.outputChatBox("Your ping is: " + playerPing);

```

# Parameters
This function has no parameters
