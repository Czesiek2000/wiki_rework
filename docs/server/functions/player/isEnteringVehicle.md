# Player::isEnteringVehicle
This property returns true or false of player vehicle enter state.


**Note: this property is read-only.**


# Syntax

This function has no syntax

# Example
```js
let playerIsEnteringVehicle = player.isEnteringVehicle
if (playerIsEnteringVehicle)
  player.outputChatBox('You are entering to the vehicle right now!');
else
  player.outputChatBox('You are not entering to the vehicle right now!');
```


# Parameters
This function has no parameters
