# Player::eyeColor

Sets/gets the players eye color



# Syntax

```js
Number player.eyeColor;

```

# Example


This lets you set the eye color of your player and will output the value of the players eye color in the chat.

```js
mp.events.addCommand('eyecolor', (player, _, id) => {
  player.eyeColor = parseInt(id);
  player.outputChatBox("Eye Color: " + eColor);
});

```

# Parameters
This function has no parameters
