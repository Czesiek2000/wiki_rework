# Player::isLeavingVehicle
This property returns true or false of player vehicle leave state.


Note: this property is read-only.


# Syntax
This function has no syntax

# Example

```js
let playerIsLeavingVehicle = player.isLeavingVehicle
if (playerIsLeavingVehicle)
  player.outputChatBox('You are leaving the vehicle right now!');
else
  player.outputChatBox('You are not leaving the vehicle right now!');
```

# Parameters

This function has no parameters
