# Player::packetLoss
This property returns players packet loss.


**Note: this property is read-only.**


# Syntax
This function has no syntax

# Example

```js
let playerPacketLoss = player.packetLoss;
player.outputChatBox("Your packet loss is: " + playerPacketLoss);

```

# Parameters
This function has no parameters
