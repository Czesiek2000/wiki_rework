# Player::ban
This function bans the player from your server.


<strong>Note: The ban reason doesn't display for the player, you need to use something else to display it for the player. Also, all bans that use this function are cleared once the server restarts. You need to save the bans yourself if you want them to stay after restarting your server.</strong>


# Syntax

```js
player.ban(reason)

```

# Example
Creates a ban command.
```js
mp.events.addCommand('ban', (player, target) => {
	let newTarget = mp.players.at(target);
	if(!target || isNaN(target)) return player.outputChatBox("Syntax: /ban [playerID]");
	if(newTarget === null) return player.outputChatBox("There is no player online with the ID given.")
	newTarget.outputChatBox("You have been banned from the server.");
	newTarget.ban('Banned.');
});

```

# Parameters

<li><b>reason</b>: <b><span style="color:#008017">String</span></b></li>
