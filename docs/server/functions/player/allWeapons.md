# Player::allWeapons
Gets the player's weapon hash and ammo

**Note: this property is read-only.**

# Syntax

```js
Object player.allWeapons;

```

# Example

It will show player's weapon hash and ammo


```js
mp.events.addCommand('weapons', (player) => {
  const weapons = player.allWeapons;
  for (let key in weapons) {
    player.notify(`Hash: ${key}, ammo: ${weapons[key]}`);
  }
});

```

# Parameters
This function has no parameters
