# Player::setClothes

This function set clothing for player.

Alternative of client-side function: [Player::setComponentVariation](./setComponentVariation.md)


**palette** = 2

### Known Issue

This function can't handle drawable over 255. If you need, use player.data & entityDataChange + entityStreamIn events so you can set it client-side with setComponentVariation. Fixed in 0.4.


List of components:
<ul><li>0 - Head</li>
<li>1 - Beard</li>
<li>2 - Hair</li>
<li>3 - Torso</li>
<li>4 - Legs</li>
<li>5 - Hands</li>
<li>6 - Foot</li>
<li>7 - Eyes</li>
<li>8 - Accessories like parachute, scuba tank</li>
<li>9 - Accessories like bags, mask, scuba mask</li>
<li>10- Decals and mask</li>
<li>11 - Auxiliary parts for torso</li></ul>

# Syntax

```js
player.setClothes(Number componentNumber, Number drawable, Number texture, Number palette)

```

# Example

This example changes clothes.

```js
mp.events.add('playerCommand', (player, command) => {
  let arr = command.split(' ');
  if (arr[0] == 'setclothes') {
    if (arr.length < 5 || parseInt(arr[1]) === undefined || parseInt(arr[2]) === undefined || parseInt(arr[3]) === undefined || parseInt(arr[4]) === undefined) {
      return player.outputChatBox('Use syntax: /setclothes [component_id] [drawable_id] [texture_id] [palette_id]');
    } else {
      player.setClothes(parseInt(arr[1]), parseInt(arr[2]), parseInt(arr[3]), parseInt(arr[4]));
    }
  }
});

```

# Parameters

<li><b>componentNumber: <span style="color:#008017">Number</span></b></li>
<li><b>drawable: <span style="color:#008017">Number</span></b></li>
<li><b>texture: <span style="color:#008017">Number</span></b></li>
<li><b>palette: <span style="color:#008017">Number</span></b></li>
