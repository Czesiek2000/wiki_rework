# Player::removeFromVehicle
This functions eject player from vehicle.


# Syntax

```js
player.removeFromVehicle()
```

# Example
This function has no example

# Parameters
This function has no parameters
