# Player::setDecoration

Applies an Item from a PedDecorationCollection to a player. These include tattoos and shirt decals.

collection - PedDecorationCollection filename hash

overlay - Item name hash

```xml
Example: 
Entry inside 'mpbeach_overlays.xml' -

<Item> 
<uvPos x='0.500000' y='0.500000' />
<scale x='0.600000' y='0.500000' /> 
<rotation value='0.000000' /> 
<nameHash>FM_Hair_Fuzz</nameHash> 
<txdHash>mp_hair_fuzz</txdHash> 
<txtHash>mp_hair_fuzz</txtHash> 
<zone>ZONE_HEAD</zone> 
<type>TYPE_TATTOO</type> 
<faction>FM</faction> 
<garment>All</garment> 
<gender>GENDER_DONTCARE</gender> 
<award /> 
<awardLevel />
</Item>
```
```cpp
Code:Player::_0x5F5D1665E352A839(PLAYER::PLAYER_Player_ID(), GAMEPLAY::GET_HASH_KEY('mpbeach_overlays'), GAMEPLAY::GET_HASH_KEY('fm_hair_fuzz'))
```

# Syntax

```js
player.setDecoration(collection, overlay);
```

# Example

```js
// todo
```

# Parameters
<ul><li><b>collection:</b> Model hash or name</li>
<li><b>overlay:</b> Model hash or name</li></ul>

<br/>

# Return value
<li><b>Undefined</b></li>
