# Player::stopAnimation

This method stops animation of the player


# Syntax

```js
player.stopAnimation()
```

# Example

```js
// call this events from client side to do animation for 10s and stop it

mp.events.add("DoMedicalCheck", (player) => {
  player.taskStartScenarioInPlace('CODE_HUMAN_MEDIC_KNEEL', 0, true)
  setTimeout(() => {
    player.stopAnimation()
  }, 10000)
})
```

# Parameters

This function has no parameters
