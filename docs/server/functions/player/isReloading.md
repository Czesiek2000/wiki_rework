# Player::isReloading

Returns whether the specified ped is reloading.


# Syntax

```js
player.isReloading();
```


# Example

```js
// todo
```

# Parameters

This function has no parameters
