# Player::isInCover
This property returns true or false of player cover state.


**Note: this property is read-only.**


# Syntax

This function has no syntax

# Example
```js
let playerIsInCover = player.isInCover
if (playerIsInCover)
  player.outputChatBox('You are in the cover right now!');
else
  player.outputChatBox('You are not in the cover right now!');
```


# Parameters

This function has no parameters
