# Vector3::divide

This function is used to divide a Vector3 by another Vector3 or scalar.


# Syntax

```js
vector.divide(Vector3 otherVec);
vector.divide(number scalar);
```

# Example

```js
const vec1 = new mp.Vector3(50, 10, 30);
const vec2 = new mp.Vector3(10, 20, 15);

const quotient = vec1.divide(vec2); // quotient = {x: 5, y: 2, z: 2}
```

```js
const vec1 = new mp.Vector3(50, 40, 30);
const scalar = 10;

const quotient = vec1.divide(scalar); // quotient = {x: 5, y: 4, z: 3}
```

# Parameters

<li><b>otherVec <span style="color:#008017">Vector3</span></b>: The vector or scalar to divide the callee by.</li>

<br />

# Return value

<li><b><span style="color:#008017">Vector3</span></b> The quotient.</li>
