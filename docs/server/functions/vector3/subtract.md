# Vector3::subtract
This function is used to subtract a Vector3 or scalar from another Vector3.


# Syntax

```js
vector.subtract(Vector3 otherVec);
vector.subtract(number scalar);

```

# Example

```js
const vec1 = new mp.Vector3(50, 40, 30);
const vec2 = new mp.Vector3(10, 20, 15);

const difference = vec1.subtract(vec2); // difference = {x: 40, y: 20, z: 15}

```

# Parameters

<li><b><span style="color:#008017">Vector3</span></b> The difference.</li>
