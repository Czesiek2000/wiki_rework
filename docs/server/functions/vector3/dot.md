# Vector3::dot

This function is used to calculate the dot product of two vectors.


The dot product is a number calculated by multiplying the magnitudes of both vectors together, then multiplying that number by cosine of the angle between them.


For normalized vectors, the dot product will be:


# Syntax


<li><b>-1</b> - If the vectors point in the exact opposite direction</li>
<li><b>0</b> - If the vectors are perpendicular</li>
<li><b>1</b> - If the vectors point the same direction</li>

# Example

```js
vector.dot(Vector3 otherVec);
```

# Parameters
<li><b>otherVec:</b> Vector3: The other vector.</li>

<br />

# Return value

<li><b><span style="color:#008017">Number</span></b> The dot product.</li>
