# Vector3::length
This function returns the magnitude of a Vector3.


It's calculated by square rooting the result of x * x + y * y + z * z.


This example calculates the distance between two [
'add parameters'
].


# Syntax

```js
vector.length();

```

# Example

```js
const vec1 = mp.[
'add parameters'
].at(0).position;
const vec2 = mp.[
'add parameters'
].at(1).position;

const distance = vec1.subtract(vec2).length();

// distance is the distance between the two [
'add parameters'
]

```

# Parameters

