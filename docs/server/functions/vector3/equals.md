# Vector3::equals
This function is used to test where two Vector3s equal each other.


# Syntax

```js
vector.equals(Vector3 otherVec);

```

# Example

```js
const vec1 = new mp.Vector3(5, 5, 5);
const vec2 = new mp.Vector3(5, 5, 5);

vec1.equals(vec2); // true
vec1.equals(new mp.Vector3(0, 0, 0)); // false

```

# Parameters

<li><b><span style="color:#008017">boolean</span></b> Whether the vectors are equal.</li>
