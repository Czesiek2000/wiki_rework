# Vector3::clone

This function returns a copy of a Vector3.


# Syntax

```js
vector.clone();
```

# Example
This function has no example


# Parameters

This function has no parameters

# Return value
<li><b><span style="color:#008017">Vector3</span></b> A new vector with the same values.</li>