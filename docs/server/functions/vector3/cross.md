# Vector3::cross

This function is used to calculate the cross product of two vectors. The cross product is a vector that is perpendicular to both input vectors.


# Syntax

```js
vector.add(Vector3 otherVec);
```

# Example

This function has no example

# Parameters

<li><b>otherVec:</b> <span style="color:#008017">Vector3</span> - The other vector.</li>

<br />

# Return value

<li><b><span style="color:#008017">Vector3</span></b> The cross product.</li>
