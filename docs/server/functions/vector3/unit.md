# Vector3::unit
This function returns a normalized copy of a Vector3- one that has the same direction but with a magnitude of 1.


# Syntax

```js
vector.unit();

```

# Example


# Parameters

<li>Vector3::x</li>


<li>Vector3::y</li>


<li>Vector3::z</li>
