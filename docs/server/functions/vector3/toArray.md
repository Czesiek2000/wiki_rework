# Vector3::toArray
This function returns an array of the partials of a Vector3.


# Syntax

```js
vector.toArray();

```

# Example

```js
const vec1 = new mp.Vector3(10, 30, 100);

const array = vec1.toArray(); // [10, 30, 100]

```

# Parameters

