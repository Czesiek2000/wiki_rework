# Vector3::multiply
This function is used to multiply a Vector3 by another Vector3 or scalar.


# Syntax

```js
vector.add(Vector3 otherVec);
vector.add(number scalar);

```

# Example

```js
const vec1 = new mp.Vector3(100, 100, 100);
const vec2 = new mp.Vector3(2, 3, 4);

const product = vec1.multiply(vec2); // total = {x: 200, y: 300, z: 400}

```

# Parameters

<li><b><span style="color:#008017">Vector3</span></b> The product.</li>
