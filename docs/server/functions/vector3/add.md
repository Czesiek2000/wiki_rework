# Vector3::add

This function is used to add a Vector3 to another Vector3 or scalar.



# Syntax

```js
vector.add(Vector3 otherVec);
vector.add(number scalar);
```

# Example

```js
const vec1 = new mp.Vector3(10, 30, 100);
const vec2 = new mp.Vector3(40, 20, 60);

const total = vec1.add(vec2); // total = {x: 50, y: 50, z: 160}
```

```js
const vec1 = new mp.Vector3(10, 30, 100);
const scalar = 20;

const total = vec1.add(scalar); // total = {x: 30, y: 50, z: 120}
```

```js
// This will throw all players 500 units into the air.
mp.players.forEach(player => {
    player.position = player.position.add(new mp.Vector3(0, 0, 500));
});
```

# Parameters
<li><b>otherVec:</b> Vector3 or number: The vector or scalar to be added to the callee.</li>

<br />

# Return value
<li><b><span style="color:#008017">Vector3</span></b> The sum.</li>
