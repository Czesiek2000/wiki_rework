# Vector3::Vector3

This function create a vector object.


# Syntax

```js
let vector = new mp.Vector3(Number x, Number y, Number z)
```

# Example

This function doesn't have example

# Parameters
<li><b>x: <span style="color:#008017">Number</span></b></li>
<li><b>y: <span style="color:#008017">Number</span></b></li>
<li><b>z: <span style="color:#008017">Number</span></b></li>
