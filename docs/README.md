# Wiki

<div style="display: flex;">

![MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat)
<p style="margin-left: 10px">

![docsify](https://img.shields.io/badge/docsify-4.12.0-yellow?style=flat)
</p>
<p style="margin-left: 10px">

![contribution](https://img.shields.io/badge/contributions-welcome-brightgreen?style=flat)
</p>
</div>

## Overview
This project is redesign of current RAGE Multiplayer Wiki. It includes client-side and server-side events and functions.

## Usage
To use this wiki navigate between pages with the bottom navigation that is included in every page. Also there is a posibility to search native or function inside sidebar that is presented on the left side of the page.

## Licence
All rights belongs to RAGEMP and RAGEMP Wiki.

This project is under MIT Licence.

!> **Time** is money, my friend!

<p class="warn">
This is warn class paragraph
</p>

```js
mp.events.add('test', () => {});

function clear() {
    const test = document.querySelector()
}
```

<br/>
<br/>

<div align="center" style="margin-bottom: 10px">
Made with <span style="box-shadow: 0 8px 6px -6px black; margin-bottom: 14px">:heart:</span> by Czesiek2000, hosted on gitlab pages.
</div>

&copy; Czesiek2000 2021 - now

*Powered by [docsify](https://docsify.js.org/)*