![logo](logo3.png)

# RAGE Wiki

> Enchanced version of docs

- :thumbsup: Simple and easy usage
- :globe_with_meridians: Running in browser on one page
- :new_moon: Uses dark theme on the page
- :page_facing_up: Contains all data from wiki
- :clipboard: Easy copy syntax or example

[Gitlab](https://gitlab.com/Czesiek2000/wiki_rework)
[Get Started](#Wiki)
