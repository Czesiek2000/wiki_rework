# Face Features

|Index|Face feature|Min|Max|
|--- |--- |--- |--- |
|0|Nose width|-1.0 narrow|1.0 wide|
|1|Nose height|-1.0 top|1.0 bottom|
|2|Nose length|-1.0 grand|1.0 petite|
|3|Nose bridge|-1.0 round|1.0 hollow|
|4|Nose tip|-1.0 upward|1.0 downward|
|5|Nose bridge shift|-1.0 to the right|1.0 to the left|
|6|Brow height|-1.0 top|1.0 bottom|
|7|Brow width|-1.0 inward|1.0 outward|
|8|Cheekbone height|-1.0 top|1.0 bottom|
|9|Cheekbone width|-1.0 narrow|1.0 wide|
|10|Cheeks width|-1.0 wide|1.0 narrow|
|11|Eyes|-1.0 opened|1.0 closed|
|12|Lips|-1.0 wide|1.0 narrow|
|13|Jaw width|-1.0 narrow|1.0 wide|
|14|Jaw height|-1.0 top|1.0 bottom|
|15|Chin length|-1.0 small|1.0 long|
|16|Chin position|-1.0 inward|1.0 outward|
|17|Chin width|-1.0 narrow|1.0 grand|
|18|Chin shape|-1.0 simple chin|1.0 double chin|
|19|Neck width|-1.0 narrow|1.0 wide|