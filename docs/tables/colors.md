# Colors
<ul>
<li>~r~ - <font color="#DE3232">Red</font></li>
<li>~b~ - <font color="#5CB4E3">Blue</font></li>
<li>~g~ - <font color="#71CA71">Green</font></li>
<li>~y~ - <font color="#EEC650">Yellow</font></li>
<li>~p~ - <font color="#8365E0">Purple</font></li>
<li>~q~ - <font color="#e24f80">Pink</font></li>
<li>~o~ - <font color="#FD8455">Orange</font></li>
<li>~c~ - <font color="#8B8B8B">Grey</font></li>
<li>~m~ - <font color="#636363">Darker Grey</font></li>
<li>~u~ - Black</li>
<li>~s~ - Default White</li>
<li>~w~ - White</li>
</ul>

## Symbold
* ~1~ - Dynamic Number
* ~a~ - Area Title / Dynamic String (Used in Vigilante and Taxi missions. To be used with Opcode 0384)
* ~d~ - ▼ Down Symbol
* ~h~ - Text Highlighter (Used for main menu. Text will light up when selected.)
* ~k~ - Key Definition
* ~n~ - New Line
* ~s~ - subtitle Default Text
* ~u~ - ▲ Up Symbol
* ~z~ - Subtitle Text (Invisible when subtitles are turned off).
* ~u~ - Up arrow.
* ~d~ - Down arrow.
* ~<~ - Left arrow.
* ~>~ - Right arrow.
* ~h~ - <b>Bold Text</b>
* ~ws~ -  Wanted Star Icon
~¦~ - Rockstar Verified Icon
~÷~ - Rockstar Icon
~∑~ - Rockstar Icon 2
~Ω~ - lock icon

> This values can be used on client and server side.

```js
// server side
player.notify('~w~Hello ~b~World');
```
```js
// client side
mp.game.graphics.notify('~w~Hello ~b~World');
```

## Hud colors

<table class="wikitable">
<tbody><tr>
<th style="padding:20px;">ID
</th>
<th style="padding:20px;">Name
</th>
<th style="padding:20px;">Preview
</th></tr>
<tr>
<td>0
</td>
<td>HUD_COLOUR_PURE_WHITE
</td>
<td bgcolor="FFFFFF">
</td></tr>
<tr>
<td>1
</td>
<td>HUD_COLOUR_WHITE
</td>
<td bgcolor="F0F0F0">
</td></tr>
<tr>
<td>2
</td>
<td>HUD_COLOUR_BLACK
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>3
</td>
<td>HUD_COLOUR_GREY
</td>
<td bgcolor="9B9B9B">
</td></tr>
<tr>
<td>4
</td>
<td>HUD_COLOUR_GREYLIGHT
</td>
<td bgcolor="CDCDCD">
</td></tr>
<tr>
<td>5
</td>
<td>HUD_COLOUR_GREYDARK
</td>
<td bgcolor="4D4D4D">
</td></tr>
<tr>
<td>6
</td>
<td>HUD_COLOUR_RED
</td>
<td bgcolor="E03232">
</td></tr>
<tr>
<td>7
</td>
<td>HUD_COLOUR_REDLIGHT
</td>
<td bgcolor="F09999">
</td></tr>
<tr>
<td>8
</td>
<td>HUD_COLOUR_REDDARK
</td>
<td bgcolor="701919">
</td></tr>
<tr>
<td>9
</td>
<td>HUD_COLOUR_BLUE
</td>
<td bgcolor="5DB6E5">
</td></tr>
<tr>
<td>10
</td>
<td>HUD_COLOUR_BLUELIGHT
</td>
<td bgcolor="AEDBF2">
</td></tr>
<tr>
<td>11
</td>
<td>HUD_COLOUR_BLUEDARK
</td>
<td bgcolor="2F5C73">
</td></tr>
<tr>
<td>12
</td>
<td>HUD_COLOUR_YELLOW
</td>
<td bgcolor="F0C850">
</td></tr>
<tr>
<td>13
</td>
<td>HUD_COLOUR_YELLOWLIGHT
</td>
<td bgcolor="FEEBA9">
</td></tr>
<tr>
<td>14
</td>
<td>HUD_COLOUR_YELLOWDARK
</td>
<td bgcolor="7E6B29">
</td></tr>
<tr>
<td>15
</td>
<td>HUD_COLOUR_ORANGE
</td>
<td bgcolor="FF8555">
</td></tr>
<tr>
<td>16
</td>
<td>HUD_COLOUR_ORANGELIGHT
</td>
<td bgcolor="FFC2AA">
</td></tr>
<tr>
<td>17
</td>
<td>HUD_COLOUR_ORANGEDARK
</td>
<td bgcolor="7F422A">
</td></tr>
<tr>
<td>18
</td>
<td>HUD_COLOUR_GREEN
</td>
<td bgcolor="72CC72">
</td></tr>
<tr>
<td>19
</td>
<td>HUD_COLOUR_GREENLIGHT
</td>
<td bgcolor="B9E6B9">
</td></tr>
<tr>
<td>20
</td>
<td>HUD_COLOUR_GREENDARK
</td>
<td bgcolor="396639">
</td></tr>
<tr>
<td>21
</td>
<td>HUD_COLOUR_PURPLE
</td>
<td bgcolor="8466E2">
</td></tr>
<tr>
<td>22
</td>
<td>HUD_COLOUR_PURPLELIGHT
</td>
<td bgcolor="C0B3EF">
</td></tr>
<tr>
<td>23
</td>
<td>HUD_COLOUR_PURPLEDARK
</td>
<td bgcolor="43396F">
</td></tr>
<tr>
<td>24
</td>
<td>HUD_COLOUR_PINK
</td>
<td bgcolor="CB3694">
</td></tr>
<tr>
<td>25
</td>
<td>HUD_COLOUR_RADAR_HEALTH
</td>
<td bgcolor="359A47">
</td></tr>
<tr>
<td>26
</td>
<td>HUD_COLOUR_RADAR_ARMOUR
</td>
<td bgcolor="5DB6E5">
</td></tr>
<tr>
<td>27
</td>
<td>HUD_COLOUR_RADAR_DAMAGE
</td>
<td bgcolor="EB2427">
</td></tr>
<tr>
<td>28
</td>
<td>HUD_COLOUR_NET_PLAYER1
</td>
<td bgcolor="C25050">
</td></tr>
<tr>
<td>29
</td>
<td>HUD_COLOUR_NET_PLAYER2
</td>
<td bgcolor="9C6EAF">
</td></tr>
<tr>
<td>30
</td>
<td>HUD_COLOUR_NET_PLAYER3
</td>
<td bgcolor="FF7BC4">
</td></tr>
<tr>
<td>31
</td>
<td>HUD_COLOUR_NET_PLAYER4
</td>
<td bgcolor="F79F7B">
</td></tr>
<tr>
<td>32
</td>
<td>HUD_COLOUR_NET_PLAYER5
</td>
<td bgcolor="B29084">
</td></tr>
<tr>
<td>33
</td>
<td>HUD_COLOUR_NET_PLAYER6
</td>
<td bgcolor="8DCEA7">
</td></tr>
<tr>
<td>34
</td>
<td>HUD_COLOUR_NET_PLAYER7
</td>
<td bgcolor="71A9AF">
</td></tr>
<tr>
<td>35
</td>
<td>HUD_COLOUR_NET_PLAYER8
</td>
<td bgcolor="D3D1E7">
</td></tr>
<tr>
<td>36
</td>
<td>HUD_COLOUR_NET_PLAYER9
</td>
<td bgcolor="907F99">
</td></tr>
<tr>
<td>37
</td>
<td>HUD_COLOUR_NET_PLAYER10
</td>
<td bgcolor="6AC4BF">
</td></tr>
<tr>
<td>38
</td>
<td>HUD_COLOUR_NET_PLAYER11
</td>
<td bgcolor="D6C499">
</td></tr>
<tr>
<td>39
</td>
<td>HUD_COLOUR_NET_PLAYER12
</td>
<td bgcolor="EA8E50">
</td></tr>
<tr>
<td>40
</td>
<td>HUD_COLOUR_NET_PLAYER13
</td>
<td bgcolor="98CBEA">
</td></tr>
<tr>
<td>41
</td>
<td>HUD_COLOUR_NET_PLAYER14
</td>
<td bgcolor="B26287">
</td></tr>
<tr>
<td>42
</td>
<td>HUD_COLOUR_NET_PLAYER15
</td>
<td bgcolor="908E7A">
</td></tr>
<tr>
<td>43
</td>
<td>HUD_COLOUR_NET_PLAYER16
</td>
<td bgcolor="A6755E">
</td></tr>
<tr>
<td>44
</td>
<td>HUD_COLOUR_NET_PLAYER17
</td>
<td bgcolor="AFA8A8">
</td></tr>
<tr>
<td>45
</td>
<td>HUD_COLOUR_NET_PLAYER18
</td>
<td bgcolor="E88E9B">
</td></tr>
<tr>
<td>46
</td>
<td>HUD_COLOUR_NET_PLAYER19
</td>
<td bgcolor="BBD65B">
</td></tr>
<tr>
<td>47
</td>
<td>HUD_COLOUR_NET_PLAYER20
</td>
<td bgcolor="0C7B56">
</td></tr>
<tr>
<td>48
</td>
<td>HUD_COLOUR_NET_PLAYER21
</td>
<td bgcolor="7BC4FF">
</td></tr>
<tr>
<td>49
</td>
<td>HUD_COLOUR_NET_PLAYER22
</td>
<td bgcolor="AB3CE6">
</td></tr>
<tr>
<td>50
</td>
<td>HUD_COLOUR_NET_PLAYER23
</td>
<td bgcolor="CEA90D">
</td></tr>
<tr>
<td>51
</td>
<td>HUD_COLOUR_NET_PLAYER24
</td>
<td bgcolor="4763AD">
</td></tr>
<tr>
<td>52
</td>
<td>HUD_COLOUR_NET_PLAYER25
</td>
<td bgcolor="2AA6B9">
</td></tr>
<tr>
<td>53
</td>
<td>HUD_COLOUR_NET_PLAYER26
</td>
<td bgcolor="BA9D7D">
</td></tr>
<tr>
<td>54
</td>
<td>HUD_COLOUR_NET_PLAYER27
</td>
<td bgcolor="C9E1FF">
</td></tr>
<tr>
<td>55
</td>
<td>HUD_COLOUR_NET_PLAYER28
</td>
<td bgcolor="F0F096">
</td></tr>
<tr>
<td>56
</td>
<td>HUD_COLOUR_NET_PLAYER29
</td>
<td bgcolor="ED8CA1">
</td></tr>
<tr>
<td>57
</td>
<td>HUD_COLOUR_NET_PLAYER30
</td>
<td bgcolor="F98A8A">
</td></tr>
<tr>
<td>58
</td>
<td>HUD_COLOUR_NET_PLAYER31
</td>
<td bgcolor="FCEFA6">
</td></tr>
<tr>
<td>59
</td>
<td>HUD_COLOUR_NET_PLAYER32
</td>
<td bgcolor="F0F0F0">
</td></tr>
<tr>
<td>60
</td>
<td>HUD_COLOUR_SIMPLEBLIP_DEFAULT
</td>
<td bgcolor="9FC9A6">
</td></tr>
<tr>
<td>61
</td>
<td>HUD_COLOUR_MENU_BLUE
</td>
<td bgcolor="8C8C8C">
</td></tr>
<tr>
<td>62
</td>
<td>HUD_COLOUR_MENU_GREY_LIGHT
</td>
<td bgcolor="8C8C8C">
</td></tr>
<tr>
<td>63
</td>
<td>HUD_COLOUR_MENU_BLUE_EXTRA_DARK
</td>
<td bgcolor="282828">
</td></tr>
<tr>
<td>64
</td>
<td>HUD_COLOUR_MENU_YELLOW
</td>
<td bgcolor="F0A000">
</td></tr>
<tr>
<td>65
</td>
<td>HUD_COLOUR_MENU_YELLOW_DARK
</td>
<td bgcolor="F0A000">
</td></tr>
<tr>
<td>66
</td>
<td>HUD_COLOUR_MENU_GREEN
</td>
<td bgcolor="F0A000">
</td></tr>
<tr>
<td>67
</td>
<td>HUD_COLOUR_MENU_GREY
</td>
<td bgcolor="8C8C8C">
</td></tr>
<tr>
<td>68
</td>
<td>HUD_COLOUR_MENU_GREY_DARK
</td>
<td bgcolor="3C3C3C">
</td></tr>
<tr>
<td>69
</td>
<td>HUD_COLOUR_MENU_HIGHLIGHT
</td>
<td bgcolor="1E1E1E">
</td></tr>
<tr>
<td>70
</td>
<td>HUD_COLOUR_MENU_STANDARD
</td>
<td bgcolor="8C8C8C">
</td></tr>
<tr>
<td>71
</td>
<td>HUD_COLOUR_MENU_DIMMED
</td>
<td bgcolor="4B4B4B">
</td></tr>
<tr>
<td>72
</td>
<td>HUD_COLOUR_MENU_EXTRA_DIMMED
</td>
<td bgcolor="323232">
</td></tr>
<tr>
<td>73
</td>
<td>HUD_COLOUR_BRIEF_TITLE
</td>
<td bgcolor="5F5F5F">
</td></tr>
<tr>
<td>74
</td>
<td>HUD_COLOUR_MID_GREY_MP
</td>
<td bgcolor="646464">
</td></tr>
<tr>
<td>75
</td>
<td>HUD_COLOUR_NET_PLAYER1_DARK
</td>
<td bgcolor="5D2727">
</td></tr>
<tr>
<td>76
</td>
<td>HUD_COLOUR_NET_PLAYER2_DARK
</td>
<td bgcolor="4D3759">
</td></tr>
<tr>
<td>77
</td>
<td>HUD_COLOUR_NET_PLAYER3_DARK
</td>
<td bgcolor="7C3E63">
</td></tr>
<tr>
<td>78
</td>
<td>HUD_COLOUR_NET_PLAYER4_DARK
</td>
<td bgcolor="785050">
</td></tr>
<tr>
<td>79
</td>
<td>HUD_COLOUR_NET_PLAYER5_DARK
</td>
<td bgcolor="574842">
</td></tr>
<tr>
<td>80
</td>
<td>HUD_COLOUR_NET_PLAYER6_DARK
</td>
<td bgcolor="4A6753">
</td></tr>
<tr>
<td>81
</td>
<td>HUD_COLOUR_NET_PLAYER7_DARK
</td>
<td bgcolor="3C5558">
</td></tr>
<tr>
<td>82
</td>
<td>HUD_COLOUR_NET_PLAYER8_DARK
</td>
<td bgcolor="696940">
</td></tr>
<tr>
<td>83
</td>
<td>HUD_COLOUR_NET_PLAYER9_DARK
</td>
<td bgcolor="483F4C">
</td></tr>
<tr>
<td>84
</td>
<td>HUD_COLOUR_NET_PLAYER10_DARK
</td>
<td bgcolor="35625F">
</td></tr>
<tr>
<td>85
</td>
<td>HUD_COLOUR_NET_PLAYER11_DARK
</td>
<td bgcolor="6B624C">
</td></tr>
<tr>
<td>86
</td>
<td>HUD_COLOUR_NET_PLAYER12_DARK
</td>
<td bgcolor="754728">
</td></tr>
<tr>
<td>87
</td>
<td>HUD_COLOUR_NET_PLAYER13_DARK
</td>
<td bgcolor="4C6575">
</td></tr>
<tr>
<td>88
</td>
<td>HUD_COLOUR_NET_PLAYER14_DARK
</td>
<td bgcolor="41232F">
</td></tr>
<tr>
<td>89
</td>
<td>HUD_COLOUR_NET_PLAYER15_DARK
</td>
<td bgcolor="48473D">
</td></tr>
<tr>
<td>90
</td>
<td>HUD_COLOUR_NET_PLAYER16_DARK
</td>
<td bgcolor="553A2F">
</td></tr>
<tr>
<td>91
</td>
<td>HUD_COLOUR_NET_PLAYER17_DARK
</td>
<td bgcolor="575454">
</td></tr>
<tr>
<td>92
</td>
<td>HUD_COLOUR_NET_PLAYER18_DARK
</td>
<td bgcolor="74474D">
</td></tr>
<tr>
<td>93
</td>
<td>HUD_COLOUR_NET_PLAYER19_DARK
</td>
<td bgcolor="5D6B2D">
</td></tr>
<tr>
<td>94
</td>
<td>HUD_COLOUR_NET_PLAYER20_DARK
</td>
<td bgcolor="063D2B">
</td></tr>
<tr>
<td>95
</td>
<td>HUD_COLOUR_NET_PLAYER21_DARK
</td>
<td bgcolor="3D627F">
</td></tr>
<tr>
<td>96
</td>
<td>HUD_COLOUR_NET_PLAYER22_DARK
</td>
<td bgcolor="551E73">
</td></tr>
<tr>
<td>97
</td>
<td>HUD_COLOUR_NET_PLAYER23_DARK
</td>
<td bgcolor="675406">
</td></tr>
<tr>
<td>98
</td>
<td>HUD_COLOUR_NET_PLAYER24_DARK
</td>
<td bgcolor="233156">
</td></tr>
<tr>
<td>99
</td>
<td>HUD_COLOUR_NET_PLAYER25_DARK
</td>
<td bgcolor="15535C">
</td></tr>
<tr>
<td>100
</td>
<td>HUD_COLOUR_NET_PLAYER26_DARK
</td>
<td bgcolor="5D623E">
</td></tr>
<tr>
<td>101
</td>
<td>HUD_COLOUR_NET_PLAYER27_DARK
</td>
<td bgcolor="64707F">
</td></tr>
<tr>
<td>102
</td>
<td>HUD_COLOUR_NET_PLAYER28_DARK
</td>
<td bgcolor="78784B">
</td></tr>
<tr>
<td>103
</td>
<td>HUD_COLOUR_NET_PLAYER29_DARK
</td>
<td bgcolor="984C5D">
</td></tr>
<tr>
<td>104
</td>
<td>HUD_COLOUR_NET_PLAYER30_DARK
</td>
<td bgcolor="7C4545">
</td></tr>
<tr>
<td>105
</td>
<td>HUD_COLOUR_NET_PLAYER31_DARK
</td>
<td bgcolor="0A2B32">
</td></tr>
<tr>
<td>106
</td>
<td>HUD_COLOUR_NET_PLAYER32_DARK
</td>
<td bgcolor="5F5F0A">
</td></tr>
<tr>
<td>107
</td>
<td>HUD_COLOUR_BRONZE
</td>
<td bgcolor="B48261">
</td></tr>
<tr>
<td>108
</td>
<td>HUD_COLOUR_SILVER
</td>
<td bgcolor="9699A1">
</td></tr>
<tr>
<td>109
</td>
<td>HUD_COLOUR_GOLD
</td>
<td bgcolor="D6B563">
</td></tr>
<tr>
<td>110
</td>
<td>HUD_COLOUR_PLATINUM
</td>
<td bgcolor="A6DDBE">
</td></tr>
<tr>
<td>111
</td>
<td>HUD_COLOUR_GANG1
</td>
<td bgcolor="1D6499">
</td></tr>
<tr>
<td>112
</td>
<td>HUD_COLOUR_GANG2
</td>
<td bgcolor="D6740F">
</td></tr>
<tr>
<td>113
</td>
<td>HUD_COLOUR_GANG3
</td>
<td bgcolor="877D8E">
</td></tr>
<tr>
<td>114
</td>
<td>HUD_COLOUR_GANG4
</td>
<td bgcolor="E577B9">
</td></tr>
<tr>
<td>115
</td>
<td>HUD_COLOUR_SAME_CREW
</td>
<td bgcolor="FCEFA6">
</td></tr>
<tr>
<td>116
</td>
<td>HUD_COLOUR_FREEMODE
</td>
<td bgcolor="2D6EB9">
</td></tr>
<tr>
<td>117
</td>
<td>HUD_COLOUR_PAUSE_BG
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>118
</td>
<td>HUD_COLOUR_FRIENDLY
</td>
<td bgcolor="5DB6E5">
</td></tr>
<tr>
<td>119
</td>
<td>HUD_COLOUR_ENEMY
</td>
<td bgcolor="C25050">
</td></tr>
<tr>
<td>120
</td>
<td>HUD_COLOUR_LOCATION
</td>
<td bgcolor="F0C850">
</td></tr>
<tr>
<td>121
</td>
<td>HUD_COLOUR_PICKUP
</td>
<td bgcolor="72CC72">
</td></tr>
<tr>
<td>122
</td>
<td>HUD_COLOUR_PAUSE_SINGLEPLAYER
</td>
<td bgcolor="72CC72">
</td></tr>
<tr>
<td>123
</td>
<td>HUD_COLOUR_FREEMODE_DARK
</td>
<td bgcolor="16375C">
</td></tr>
<tr>
<td>124
</td>
<td>HUD_COLOUR_INACTIVE_MISSION
</td>
<td bgcolor="9A9A9A">
</td></tr>
<tr>
<td>125
</td>
<td>HUD_COLOUR_DAMAGE
</td>
<td bgcolor="C25050">
</td></tr>
<tr>
<td>126
</td>
<td>HUD_COLOUR_PINKLIGHT
</td>
<td bgcolor="FC73C9">
</td></tr>
<tr>
<td>127
</td>
<td>HUD_COLOUR_PM_MITEM_HIGHLIGHT
</td>
<td bgcolor="FCB131">
</td></tr>
<tr>
<td>128
</td>
<td>HUD_COLOUR_SCRIPT_VARIABLE
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>129
</td>
<td>HUD_COLOUR_YOGA
</td>
<td bgcolor="6DF7CC">
</td></tr>
<tr>
<td>130
</td>
<td>HUD_COLOUR_TENNIS
</td>
<td bgcolor="F16522">
</td></tr>
<tr>
<td>131
</td>
<td>HUD_COLOUR_GOLF
</td>
<td bgcolor="D6BD61">
</td></tr>
<tr>
<td>132
</td>
<td>HUD_COLOUR_SHOOTING_RANGE
</td>
<td bgcolor="701919">
</td></tr>
<tr>
<td>133
</td>
<td>HUD_COLOUR_FLIGHT_SCHOOL
</td>
<td bgcolor="2F5C73">
</td></tr>
<tr>
<td>134
</td>
<td>HUD_COLOUR_NORTH_BLUE
</td>
<td bgcolor="5DB6E5">
</td></tr>
<tr>
<td>135
</td>
<td>HUD_COLOUR_SOCIAL_CLUB
</td>
<td bgcolor="EA991C">
</td></tr>
<tr>
<td>136
</td>
<td>HUD_COLOUR_PLATFORM_BLUE
</td>
<td bgcolor="0B377B">
</td></tr>
<tr>
<td>137
</td>
<td>HUD_COLOUR_PLATFORM_GREEN
</td>
<td bgcolor="92C83E">
</td></tr>
<tr>
<td>138
</td>
<td>HUD_COLOUR_PLATFORM_GREY
</td>
<td bgcolor="EA991C">
</td></tr>
<tr>
<td>139
</td>
<td>HUD_COLOUR_FACEBOOK_BLUE
</td>
<td bgcolor="425994">
</td></tr>
<tr>
<td>140
</td>
<td>HUD_COLOUR_INGAME_BG
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>141
</td>
<td>HUD_COLOUR_DARTS
</td>
<td bgcolor="72CC72">
</td></tr>
<tr>
<td>142
</td>
<td>HUD_COLOUR_WAYPOINT
</td>
<td bgcolor="A44CF2">
</td></tr>
<tr>
<td>143
</td>
<td>HUD_COLOUR_MICHAEL
</td>
<td bgcolor="65B4D4">
</td></tr>
<tr>
<td>144
</td>
<td>HUD_COLOUR_FRANKLIN
</td>
<td bgcolor="ABEDAB">
</td></tr>
<tr>
<td>145
</td>
<td>HUD_COLOUR_TREVOR
</td>
<td bgcolor="FFA357">
</td></tr>
<tr>
<td>146
</td>
<td>HUD_COLOUR_GOLF_P1
</td>
<td bgcolor="F0F0F0">
</td></tr>
<tr>
<td>147
</td>
<td>HUD_COLOUR_GOLF_P2
</td>
<td bgcolor="EBEF1E">
</td></tr>
<tr>
<td>148
</td>
<td>HUD_COLOUR_GOLF_P3
</td>
<td bgcolor="FF950E">
</td></tr>
<tr>
<td>149
</td>
<td>HUD_COLOUR_GOLF_P4
</td>
<td bgcolor="F63CA1">
</td></tr>
<tr>
<td>150
</td>
<td>HUD_COLOUR_WAYPOINTLIGHT
</td>
<td bgcolor="D2A6F9">
</td></tr>
<tr>
<td>151
</td>
<td>HUD_COLOUR_WAYPOINTDARK
</td>
<td bgcolor="522679">
</td></tr>
<tr>
<td>152
</td>
<td>HUD_COLOUR_PANEL_LIGHT
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>153
</td>
<td>HUD_COLOUR_MICHAEL_DARK
</td>
<td bgcolor="486774">
</td></tr>
<tr>
<td>154
</td>
<td>HUD_COLOUR_FRANKLIN_DARK
</td>
<td bgcolor="557655">
</td></tr>
<tr>
<td>155
</td>
<td>HUD_COLOUR_TREVOR_DARK
</td>
<td bgcolor="7F512B">
</td></tr>
<tr>
<td>156
</td>
<td>HUD_COLOUR_OBJECTIVE_ROUTE
</td>
<td bgcolor="F0C850">
</td></tr>
<tr>
<td>157
</td>
<td>HUD_COLOUR_PAUSEMAP_TINT
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>158
</td>
<td>HUD_COLOUR_PAUSE_DESELECT
</td>
<td bgcolor="646464">
</td></tr>
<tr>
<td>159
</td>
<td>HUD_COLOUR_PM_WEAPONS_PURCHASABLE
</td>
<td bgcolor="2D6EB9">
</td></tr>
<tr>
<td>160
</td>
<td>HUD_COLOUR_PM_WEAPONS_LOCKED
</td>
<td bgcolor="F0F0F0">
</td></tr>
<tr>
<td>161
</td>
<td>HUD_COLOUR_END_SCREEN_BG
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>162
</td>
<td>HUD_COLOUR_CHOP
</td>
<td bgcolor="E03232">
</td></tr>
<tr>
<td>163
</td>
<td>HUD_COLOUR_PAUSEMAP_TINT_HALF
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>164
</td>
<td>HUD_COLOUR_NORTH_BLUE_OFFICIAL
</td>
<td bgcolor="004785">
</td></tr>
<tr>
<td>165
</td>
<td>HUD_COLOUR_SCRIPT_VARIABLE_2
</td>
<td bgcolor="000000">
</td></tr>
<tr>
<td>166
</td>
<td>HUD_COLOUR_H
</td>
<td bgcolor="217625">
</td></tr>
<tr>
<td>167
</td>
<td>HUD_COLOUR_HDARK
</td>
<td bgcolor="256628">
</td></tr>
<tr>
<td>168
</td>
<td>HUD_COLOUR_T
</td>
<td bgcolor="EA991C">
</td></tr>
<tr>
<td>169
</td>
<td>HUD_COLOUR_TDARK
</td>
<td bgcolor="E18C08">
</td></tr>
<tr>
<td>170
</td>
<td>HUD_COLOUR_HSHARD
</td>
<td bgcolor="142800">
</td></tr>
<tr>
<td>171
</td>
<td>HUD_COLOUR_CONTROLLER_MICHAEL
</td>
<td bgcolor="30FFFF">
</td></tr>
<tr>
<td>172
</td>
<td>HUD_COLOUR_CONTROLLER_FRANKLIN
</td>
<td bgcolor="30FF00">
</td></tr>
<tr>
<td>173
</td>
<td>HUD_COLOUR_CONTROLLER_TREVOR
</td>
<td bgcolor="B05000">
</td></tr>
<tr>
<td>174
</td>
<td>HUD_COLOUR_CONTROLLER_CHOP
</td>
<td bgcolor="7F0000">
</td></tr>
<tr>
<td>175
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_VIDEO
</td>
<td bgcolor="35A6E0">
</td></tr>
<tr>
<td>176
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AUDIO
</td>
<td bgcolor="A24F9D">
</td></tr>
<tr>
<td>177
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_TEXT
</td>
<td bgcolor="68C08D">
</td></tr>
<tr>
<td>178
</td>
<td>HUD_COLOUR_HB_BLUE
</td>
<td bgcolor="1D6499">
</td></tr>
<tr>
<td>179
</td>
<td>HUD_COLOUR_HB_YELLOW
</td>
<td bgcolor="EA991C">
</td></tr>
<tr>
<td>180
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_SCORE
</td>
<td bgcolor="F0A001">
</td></tr>
<tr>
<td>181
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AUDIO_FADEOUT
</td>
<td bgcolor="3B2239">
</td></tr>
<tr>
<td>182
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_TEXT_FADEOUT
</td>
<td bgcolor="294435">
</td></tr>
<tr>
<td>183
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_SCORE_FADEOUT
</td>
<td bgcolor="523A0A">
</td></tr>
<tr>
<td>184
</td>
<td>HUD_COLOUR_HEIST_BACKGROUND
</td>
<td bgcolor="256628">
</td></tr>
<tr>
<td>185
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AMBIENT
</td>
<td bgcolor="F0C850">
</td></tr>
<tr>
<td>186
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AMBIENT_FADEOUT
</td>
<td bgcolor="504622">
</td></tr>
<tr>
<td>187
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AMBIENT_DARK
</td>
<td bgcolor="FF8555">
</td></tr>
<tr>
<td>188
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AMBIENT_LIGHT
</td>
<td bgcolor="FFC2AA">
</td></tr>
<tr>
<td>189
</td>
<td>HUD_COLOUR_VIDEO_EDITOR_AMBIENT_MID
</td>
<td bgcolor="FF8555">
</td></tr>
<tr>
<td>190
</td>
<td>HUD_COLOUR_LOW_FLOW
</td>
<td bgcolor="F0C850">
</td></tr>
<tr>
<td>191
</td>
<td>HUD_COLOUR_LOW_FLOW_DARK
</td>
<td bgcolor="7E6B29">
</td></tr>
<tr>
<td>192
</td>
<td>HUD_COLOUR_G1
</td>
<td bgcolor="F79F7B">
</td></tr>
<tr>
<td>193
</td>
<td>HUD_COLOUR_G2
</td>
<td bgcolor="E286BB">
</td></tr>
<tr>
<td>194
</td>
<td>HUD_COLOUR_G3
</td>
<td bgcolor="EFEE97">
</td></tr>
<tr>
<td>195
</td>
<td>HUD_COLOUR_G4
</td>
<td bgcolor="71A9AF">
</td></tr>
<tr>
<td>196
</td>
<td>HUD_COLOUR_G5
</td>
<td bgcolor="A08CC1">
</td></tr>
<tr>
<td>197
</td>
<td>HUD_COLOUR_G6
</td>
<td bgcolor="8DCEA7">
</td></tr>
<tr>
<td>198
</td>
<td>HUD_COLOUR_G7
</td>
<td bgcolor="B5D6EA">
</td></tr>
<tr>
<td>199
</td>
<td>HUD_COLOUR_G8
</td>
<td bgcolor="B29084">
</td></tr>
<tr>
<td>200
</td>
<td>HUD_COLOUR_G9
</td>
<td bgcolor="008472">
</td></tr>
<tr>
<td>201
</td>
<td>HUD_COLOUR_G10
</td>
<td bgcolor="D85575">
</td></tr>
<tr>
<td>202
</td>
<td>HUD_COLOUR_G11
</td>
<td bgcolor="1E6498">
</td></tr>
<tr>
<td>203
</td>
<td>HUD_COLOUR_G12
</td>
<td bgcolor="2BB575">
</td></tr>
<tr>
<td>204
</td>
<td>HUD_COLOUR_G13
</td>
<td bgcolor="E98D4F">
</td></tr>
<tr>
<td>205
</td>
<td>HUD_COLOUR_G14
</td>
<td bgcolor="89D2D7">
</td></tr>
<tr>
<td>206
</td>
<td>HUD_COLOUR_G15
</td>
<td bgcolor="867D8D">
</td></tr>
<tr>
<td>207
</td>
<td>HUD_COLOUR_ADVERSARY
</td>
<td bgcolor="6D2221">
</td></tr>
<tr>
<td>208
</td>
<td>HUD_COLOUR_DEGEN_RED
</td>
<td bgcolor="FF0000">
</td></tr>
<tr>
<td>209
</td>
<td>HUD_COLOUR_DEGEN_YELLOW
</td>
<td bgcolor="FFFF00">
</td></tr>
<tr>
<td>210
</td>
<td>HUD_COLOUR_DEGEN_GREEN
</td>
<td bgcolor="00FF00">
</td></tr>
<tr>
<td>211
</td>
<td>HUD_COLOUR_DEGEN_CYAN
</td>
<td bgcolor="00FFFF">
</td></tr>
<tr>
<td>212
</td>
<td>HUD_COLOUR_DEGEN_BLUE
</td>
<td bgcolor="0000FF">
</td></tr>
<tr>
<td>213
</td>
<td>HUD_COLOUR_DEGEN_MAGENTA
</td>
<td bgcolor="FF00FF">
</td></tr>
<tr>
<td>214
</td>
<td>HUD_COLOUR_STUNT_1
</td>
<td bgcolor="2688EA">
</td></tr>
<tr>
<td>215
</td>
<td>HUD_COLOUR_STUNT_2
</td>
<td bgcolor="E03232">
</td></tr>
<tr>
<td>216
</td>
<td>HUD_COLOUR_SPECIAL_RACE_SERIES
</td>
<td bgcolor="9AB236">
</td></tr>
<tr>
<td>217
</td>
<td>HUD_COLOUR_SPECIAL_RACE_SERIES_DARK
</td>
<td bgcolor="5D6B2D">
</td></tr>
<tr>
<td>218
</td>
<td>HUD_COLOUR_CS
</td>
<td bgcolor="CEA90D">
</td></tr>
<tr>
<td>219
</td>
<td>HUD_COLOUR_CS_DARK
</td>
<td bgcolor="675406">
</td></tr>
<tr>
<td>220
</td>
<td>HUD_COLOUR_TECH_GREEN
</td>
<td bgcolor="009797">
</td></tr>
<tr>
<td>221
</td>
<td>HUD_COLOUR_TECH_GREEN_DARK
</td>
<td bgcolor="057771">
</td></tr>
<tr>
<td>222
</td>
<td>HUD_COLOUR_TECH_RED
</td>
<td bgcolor="970000">
</td></tr>
<tr>
<td>223
</td>
<td>HUD_COLOUR_TECH_GREEN_VERY_DARK
</td>
<td bgcolor="002828">
</td></tr></tbody></table>

