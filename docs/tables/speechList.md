# Speech list

A list of Speech Names and Speech Parameters used for [Audio::playAmbientSpeechWithVoice](../client/functions/globals/audio/playAmbientSpeechWithVoice.md)

```js
mp.game.audio.playAmbientSpeechWithVoice(p0, speechName, voiceName, speechParam, p4);
```

| Name |  Params |
| --- | --- | 
| Apology_No_Trouble |  Speech_Params_Force_Shouted_Critical |
| Blocked_Generic |  Speech_Params_Force_Shouted_Critical |
| Chat_Resp |  Speech_Params_Force |
| Chat_State |  Speech_Params_Force |
| Fall_Back |  Speech_Params_Force_Shouted_Critical |
| Generic_Curse_High |  Speech_Params_Force_Shouted_Critical |
| Generic_Curse_Med |  Speech_Params_Force_Shouted_Critical |
| Generic_Fuck_You |  Speech_Params_Force |
| Generic_Frightened_High |  Speech_Params_Force_Shouted_Critical |
| Generic_Frightened_Med |  Speech_Params_Force_Shouted_Critical |
| Generic_Hi |  Speech_Params_Force |
| Generic_Hows_It_Going |  Speech_Params_Force |
| Generic_Insult_High |  Speech_Params_Force |
| Generic_Insult_Med |  Speech_Params_Force |
| Generic_Shocked_High |  Speech_Params_Force |
| Generic_Shocked_Med |  Speech_Params_Force_Shouted_Critical |
| Generic_Thanks |  Speech_Params_Force_Shouted_Critical |
| Hooker_Car_Incorrect |  Speech_Params_Force_Shouted_Clear |
| Hooker_Decline_Service |  Speech_Params_Force_Shouted_Clear |
| Hooker_Declined |  Speech_Params_Force_Shouted_Clear |
| Hooker_Declined_Trevor |  Speech_Params_Force_Shouted_Clear |
| Hooker_Had_Enough |  Speech_Params_Force_Shouted_Clear |
| Hooker_Leaves_Angry |  Speech_Params_Force_Shouted_Clear |
| Hooker_Offer_Again |  Speech_Params_Force_Shouted_Clear |
| Hooker_Offer_Service |  Speech_Params_Force_Shouted_Clear |
| Hooker_Request |  Speech_Params_Force_Shouted_Clear |
| Hooker_Secluded |  Speech_Params_Force_Shouted_Clear |
| Hooker_Story_Revulsion_Resp |  Speech_Params_Force_Shouted_Clear |
| Hooker_Story_Sarcastic_Resp |  Speech_Params_Force_Shouted_Clear |
| Hooker_Story_Sympathetic_Resp |  Speech_Params_Force_Shouted_Clear |
| Provoke_Trespass |  Speech_Params_Force_Shouted_Critical |
| Rollercoaster_Chat_Excited |  Speech_Params_Force_Frontend |
| Rollercoaster_Chat_Normal |  Speech_Params_Force_Frontend |
| Sex_Climax |  Speech_Params_Force_Shouted_Clear |
| Sex_Finished |  Speech_Params_Force_Shouted_Clear |
| Sex_Generic |  Speech_Params_Force_Normal_Clear |
| Sex_Generic_Fem |  Speech_Params_Force_Shouted_Clear |
| Sex_Oral |  Speech_Params_Force_Shouted_Clear |
| Sex_Oral_Fem |  Speech_Params_Force_Shouted_Clear |
| Shout_Threaten_Gang |  Speech_Params_Force_Shouted_Critical |
| Shout_Threaten_Ped |  Speech_Params_Force_Shouted_Critical |
| Solicit_Franklin |  Speech_Params_Force_Shouted_Clear |
| Solicit_Franklin_Return |  Speech_Params_Force_Shouted_Critical |
| Solicit_Michael |  Speech_Params_Force_Shouted_Clear |
| Solicit_Michael_Return |  Speech_Params_Force_Shouted_Clear |
| Solicit_Trevor |  Speech_Params_Force_Shouted_Critical |
| Solicit_Trevor_Return |  Speech_Params_Force_Shouted_Clear |
| Stay_Down |  Speech_Params_Force |
