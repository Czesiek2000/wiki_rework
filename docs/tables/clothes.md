# Clothes

<table>
<tbody><tr>
<th colspan="4">Clothes
</th></tr>
<tr>
<th>Component ID</th>
<th>Part</th>
<th colspan="2">Gender
</th></tr>
<tr>
<td style="text-align:center">0</td>
<td>Head</td>
<td colspan="2">
</td></tr>
<tr>
<td style="text-align:center">1</td>
<td>Masks</td>
<td colspan="2"><a href="https://wiki.rage.mp/index.php?title=Masks" title="Masks">Male + Female</a>
</td></tr>
<tr>
<td style="text-align:center">2</td>
<td>Hair Styles</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Hair_Styles" title="Male Hair Styles">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Hair_Styles" title="Female Hair Styles">Female</a>
</td></tr>
<tr>
<td style="text-align:center">3</td>
<td>Torsos</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Torsos" title="Male Torsos">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Torsos" title="Female Torsos">Female</a>
</td></tr>
<tr>
<td style="text-align:center">4</td>
<td>Legs</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Legs" title="Male Legs">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Legs" title="Female Legs">Female</a>
</td></tr>
<tr>
<td style="text-align:center">5</td>
<td>Bags and Parachutes</td>
<td colspan="2"><a href="https://wiki.rage.mp/index.php?title=Bags_and_Parachutes" title="Bags and Parachutes">Male + Female</a>
</td></tr>
<tr>
<td style="text-align:center">6</td>
<td>Shoes</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Shoes" title="Male Shoes">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Shoes" title="Female Shoes">Female</a>
</td></tr>
<tr>
<td style="text-align:center">7</td>
<td>Accessories</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Accessories" title="Male Accessories">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Accessories" title="Female Accessories">Female</a>
</td></tr>
<tr>
<td style="text-align:center">8</td>
<td>Undershirts</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Undershirts" title="Male Undershirts">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Undershirts" title="Female Undershirts">Female</a>
</td></tr>
<tr>
<td style="text-align:center">9</td>
<td>Body Armors</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Body_Armors" title="Male Body Armors">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Body_Armors" title="Female Body Armors">Female</a>
</td></tr>
<tr>
<td style="text-align:center">10</td>
<td>Decals</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Decals" title="Male Decals">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Decals" title="Female Decals">Female</a>
</td></tr>
<tr>
<td style="text-align:center">11</td>
<td>Tops</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Tops" title="Male Tops">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Tops" title="Female Tops">Female</a>
</td></tr>
</tbody></table>

## Props
<table class="wikitable">
<tbody><tr>
<th colspan="4">Props
</th></tr>
<tr>
<th>Component ID</th>
<th>Part</th>
<th colspan="2">Gender
</th></tr>
<tr>
<td style="text-align:center">0
</td>
<td>Hats</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Hats" title="Male Hats">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Hats" title="Female Hats">Female</a>
</td></tr>
<tr>
<td style="text-align:center">1
</td>
<td>Glasses</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Glasses" title="Male Glasses">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Glasses" title="Female Glasses">Female</a>
</td></tr>
<tr>
<td style="text-align:center">2</td>
<td>Ears</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Ears" title="Male Ears">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Ears" title="Female Ears">Female</a>
</td></tr>
<tr>
<td style="text-align:center">6</td>
<td>Watches</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Watches" title="Male Watches">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Watches" title="Female Watches">Female</a>
</td></tr>
<tr>
<td style="text-align:center">7</td>
<td>Bracelets</td>
<td><a href="https://wiki.rage.mp/index.php?title=Male_Bracelets" title="Male Bracelets">Male</a></td>
<td><a href="https://wiki.rage.mp/index.php?title=Female_Bracelets" title="Female Bracelets">Female</a>
</td></tr>
</tbody></table>