# Player Config Flags

This a list of the known player config flags.

| Flag Name | Flag ID|
| --- | --- |
| PED_FLAG_CAN_PUNCH | 18 |
| PED_FLAG_CAN_FLY_THRU_WINDSCREEN | 32 |
| PED_FLAG_DIES_BY_RAGDOLL | 33 |
| PED_FLAG_CAN_PUT_MOTORCYCLE_HELMET | 35 |
| PED_FLAG_NO_COLLISION | 52 |
| PED_FLAG_IS_SHOOTING | 58 |
| PED_FLAG_IS_ON_GROUND |60 |
| PED_FLAG_NO_COLLIDE | 62 |
| PED_FLAG_DEAD | 71 |
| PED_FLAG_IS_SNIPER_SCOPE_ACTIVE | 72 |
| PED_FLAG_SUPER_DEAD | 73 |
| PED_FLAG_IS_IN_AIR | 76 |
| PED_FLAG_IS_AIMING | 78 |
| PED_FLAG_DRUNK | 100 |
| PED_FLAG_IS_NOT_RAGDOLL_AND_NOT_PLAYING_ANIM | 104 |
| PED_FLAG_NO_PLAYER_MELEE | 122 |
| PED_FLAG_NM_MESSAGE_466 |125 |
| PED_FLAG_ (Removes HELMET DAMAGE REDUCTION)|149 |
| PED_FLAG_INJURED_LIMP | 166 |
| PED_FLAG_INJURED_LIMP_2 |170 |
| PED_FLAG_AUTOMATIC_SEAT_SHUFFLE | 184 |
| PED_FLAG_INJURED_DOWN | 187 |
| PED_FLAG_SHRINK | 223 |
| PED_FLAG_MELEE_COMBAT | 224 |
| PED_FLAG_DISABLE_STOPPING_VEH_ENGINE | 241 |
| PED_FLAG_IS_ON_STAIRS | 253 |
| PED_FLAG_HAS_ONE_LEG_ON_GROUND | 276 |
| PED_FLAG_NO_WRITHE | 281 |
| PED_FLAG_FREEZE | 292 |
| PED_FLAG_IS_STILL | 301 |
| PED_FLAG_NO_PED_MELEE | 314 |
| PED_SWITCHING_WEAPON | 331 |
| PED_FLAG_ALPHA | 410 |
| PED_FLAG_FLAMING_FOOTPRINTS | 421 |
| PED_FLAG_DISABLE_PROP_KNOCK_OFF | 423 |
| PED_FLAG_STOP_ENGINE_TURNING | 429 |
| PED_FLAG_ (Removes HELMET DAMAGE REDUCTION) | 438 |
