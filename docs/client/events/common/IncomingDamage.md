# IncomingDamage

[1.1]
Triggered upon damage that is about to be given to the player.
This event is also cancellable.

## Syntax

This event doesn't have syntax


## Example 

```js
// To do
```


## Paramters

<li><b>sourceEntity</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>sourcePlayer</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>targetEntity</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>weapon</b>: <b><span style="color:#008017">Int</span></b></li>

<li><b>boneIndex</b>: <b><span style="color:#008017">Int</span></b></li>

<li><b>damage</b>: <b><span style="color:#008017">Int</span></b></li>
