# PlayerJoin

This event is triggered when a player joins the server.

## Syntax

```js
mp.events.add('playerJoin', (player) => {
    console.log(`[SERVER]: ${player.name} has joined the server!`);
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The player who joined.</li>
