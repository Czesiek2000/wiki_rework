# Click

Triggered when a player use mouse click.

## Syntax

```js
mp.events.add('click', (x, y, upOrDown, leftOrRight, relativeX, relativeY, worldPosition, hitEntity) => {
	mp.gui.chat.push("Mouse X:" + x + " | Mouse Y:" + y); // Displays mouse position on click.
        
	if (upOrDown == "up")
     	mp.gui.chat.push("Mouse Clicked Up with " + leftOrRight + " button.");
        
	if (upOrDown == "down")
     	mp.gui.chat.push("Mouse Clicked Down with " + leftOrRight + " button.");
});
```

## Example 

This event doesn't have example


## Paramters

<li><b>absoluteX</b>: <b>Number</b></li>

<li><b>absoluteY</b>: <b>Number</b></li>

<li><b>upOrDown</b>: <b>String</b></li>

<li><b>leftOrRight</b>: <b>String</b></li>

<li><b>relativeX</b>: <b>Number</b></li>

<li><b>relativeY</b>: <b>Number</b></li>

<li><b>worldPosition</b>: <b>Vector3</b></li>

<li><b>hitEntity</b>: <b>Number</b></li>
