# PlayerCommand

This event is triggered when a player sends a command.

## Syntax

```js
// client-side
mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
        
    if (commandName === "meetme") {
        // here your action
    }
});   
``` 

## Example

This example will write "Hello!" to player, who will enter command /meetme into chatbox.s

```js
// client-side
mp.events.add("playerCommand", (player, command) => {
    const args = command.split(/[ ]+/);
    const commandName = args.splice(0, 1)[0];
        
    if (commandName === "meetme") {
        player.outputChatBox("Hello!");
    }
});   
``` 

```js
// server-side
mp.events.add('playerCommand', (player, command) => {        
    player.outputChatBox(`${command} is not a valid command. Use /help to find a list of commands.`);
});
```

Detecting Invalid Commands:
```js
mp.events.add('playerCommand', (player, command) => {        
    player.outputChatBox(`${command} is not a valid command. Use /help to find a list of commands.`);
});
```

## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API">player</a>, who send command.</li>

<li><b>command</b>: <b><span style="color:#008017">String</span></b> - command (without slash) with arguments.</li>
