# addDataHandler

This Function is used to alert client-side with entity's data change for a specified variable

## Syntax

```js
mp.events.addDataHandler(key, handlerFunction);
```

## Example 

```js
/*
    Handles testVar change.
    Do "player.data.testVar = 1234;" on server-side to trigger this.
*/

mp.events.addDataHandler("testVar", (entity, value, oldValue) => {
  mp.gui.chat.push(`testVar changed to ${value} on entity ${entity.handle}.`);
});
```

This example sets nickname for player and triggers players' clientside data handler

```js
// server-side
mp.events.addCommand('setNick', (player, nickname) => {
  player.setVariable('oldNick', player.name);
  player.name = nickname;
  player.setVariable('newNick', nickname);
});   
```

```js
// client-side
mp.events.addDataHandler('newNick', function (entity, value, oldValue) {
  if (entity.type === 'player') 
    mp.gui.chat.push(`${player.getVariable('oldNick')} has changed nickname to ${value}`)
})
```


## Paramters

<li><b>key</b>: <b><span style="color:#008017">String</span></b> - Shared data key's name.</li>

<li><b>handlerFunction</b>: Handler function with parameters:

<li><b>entity</b>: <b><span style="color:#008017">Entity</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Entity_API" title="Category:Entity API">entity</a> that had the data change.
<b>value</b>: <b><span style="color:#008017">Any</span></b> - The new value of the data key.

* <b>oldValue</b>: <b><span style="color:#008017">Any</span></b> - The old value of the data key.
* <b>value</b>: <b><span style="color:#008017">Any</span></b> - The new value of the data key.
* <b>oldValue</b>: <b><span style="color:#008017">Any</span></b> - The old value of the data key.
