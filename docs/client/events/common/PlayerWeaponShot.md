# PlayerWeaponShot

This event is called every time a player shoots a weapon.

## Syntax

```js
mp.events.add('playerWeaponShot', (targetPosition, targetEntity) => {});
```

## Example 

The example below prints a message to the player's chat every time the player fires a weapon.

```js
mp.events.add('playerWeaponShot', (targetPosition, targetEntity) => {
	mp.gui.chat.push('You fired a weapon!');
});
```


## Paramters

<li><b>targetPosition</b>: Target's position, output expects a <b><a href="/index.php?title=Vector3::Vector3" title="Vector3::Vector3">Vector3</a></b> type.</li>

<li><b>targetEntity</b>: Target's entity, output expects <b>any</b> type. Returns undefined if no target entity.</li>
