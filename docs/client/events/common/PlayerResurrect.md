# PlayerResurrect

This event is called everytime a player resurrects.

## Syntax

```js
mp.events.add('playerResurrect', () => {});
```

## Example 

The example below shows up a message for a player when he resurrects.

```js
mp.events.add('playerResurrect', () => {
	mp.gui.chat.push('Like a fenix raised from the ashes, you came back to us!');
});
```


## Paramters

There are no parameters