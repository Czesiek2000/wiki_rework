# PlayerSpawn

This event is triggered when a player spawns.

## Syntax

This event doesn't have syntax example

## Example 

This example outputs a console chat message, when a player spawns.

```js
function playerSpawn(player) {
  console.log(`${player.name} has spawned`);
}

mp.events.add("playerSpawn", playerSpawn);
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, which has spawned</li>
