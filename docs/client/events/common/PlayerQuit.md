# PlayerQuit

This event is triggered when a player quits/disconnects/leaves the server.

## Syntax

<p>This example outputs chat message, when player quits or kicked from the server.</p>

```js
function playerQuitHandler(player, exitType, reason) {
    let str = player.name;

    if (exitType != "kicked") {
        str += " quit.";
    } else {
        str = ' kicked. Reason: ' + reason.;
    }

    console.log(str);
    
    }

mp.events.add("playerQuit", playerQuitHandler);       
```

## Example 
This event doesn't have example


## Paramters

<ul>
<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, which quit from the server</li>

<li><b>exitType</b>: <b><span style="color:#008017">String</span></b> - exit types:

* disconnect
* timeout
* kicked
* disconnect

<li><b>reason</b>: <b><span style="color:#008017">String</span></b> kick reason</li>

</ul>
