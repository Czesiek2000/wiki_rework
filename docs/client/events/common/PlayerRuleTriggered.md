# PlayerRuleTriggered

This event is triggered when server rule check failed for local player.

## Syntax

This event doesn't have syntax example

## Example 

```js
mp.events.add("playerRuleTriggered", (rule, counter) => {
	let color = counter > 20 ? "red" : (counter > 10 ? "yellow" : "green");
	mp.gui.chat.push(`Warning: {${color}}Your ${rule} is bad.`);
});
```


## Paramters

<li><b>rule</b> - name of rule</li>

<li><b>counter</b> - counter of failed attempts</li>
