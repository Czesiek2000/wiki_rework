# OutgoingDamage

Triggered upon damage that an entity is about to give another entity.
This event is also cancellable.

## Syntax

This example doesn't have syntax example


## Example 

```js
// To do
```


## Paramters

<li><b>sourceEntity</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>targetEntity</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>targetPlayer</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>weapon</b>: <b><span style="color:#008017">Int</span></b></li>

<li><b>boneIndex</b>: <b><span style="color:#008017">Int</span></b></li>

<li><b>damage</b>: <b><span style="color:#008017">Int</span></b></li>
