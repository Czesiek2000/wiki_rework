# DummyEntityDestroyed

Triggered when a dummy has been destroyed.

## Syntax

```js
	// To do
```


## Example 

This event doesn't have example


## Paramters

<li><b>dummyType</b>: <b><span style="color:#008017">Int</span></b> -  the type of the dummy entity</li>

<li><b>dummy</b>: <b><span style="color:#008017">Object</span></b> - the destroyed dummy entity</li>

