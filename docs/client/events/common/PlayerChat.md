# PlayerChat

This event is triggered when a player send a message in the chat.

## Syntax

```js
mp.events.add("playerChat", () => {
    // Your code here
})
```

## Example 

This example will gives 300 money a player if his nickname is "Modernß" and he types "sorry" in chat.

```js
// serverside
function checkChatMessage(player, text) {
    if (player.name === "Modernß" &amp;&amp; text == "sorry") {
        player.money += 300;
    }
};

mp.events.add("playerChat", checkChatMessage);
```

```js
// client-side
mp.events.add("playerChat", (text) => {
    if (text === "test") {
        mp.gui.chat.push("You wrote 'test' in chat.");
    }
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, who send message</li>

<li><b>text</b>: <b><span style="color:#008017">String</span></b> - the text that was sent</li>
