# PlayerEnterColshape

This event is triggered when a player enters a colshape.

Default dimension for Colshapes is 0, and the player must be in the same dimension as the Colshape for them to collide.

## Syntax

```js
let someColShape = mp.colshapes.newRectangle(0, 0, 100, 100);

function playerEnterColshapeHandler(player, shape) {
  if(shape == someColShape) {
    console.log(`${player.name} entered the colshape`);
  }
}

mp.events.add("playerEnterColshape", playerEnterColshapeHandler);
```

## Example 

This native doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API">player</a> which entered the colshape</li>

<li><b>shape</b>: <b><span style="color:#008017">ColShape</span></b> - The <a href="/index.php?title=Category:ColShape_API" title="Category:ColShape API">colshape</a> the player entered</li>
