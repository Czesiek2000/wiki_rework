# PlayerExitColshape

This esvent is triggered when a player exits a colshape. 

Default dimension for ColShapes is 0, and the player must be in the same dimension as the Colshape for them to collide.

## Syntax

```js
let someColShape = mp.colshapes.newRectangle(0, 0, 100, 100);

function playerExitColshapeHandler(player, shape) {
  if(shape == someColShape) {
    console.log(`${player.name} left the colshape!`);
  }
}

mp.events.add("playerExitColshape", playerExitColshapeHandler);
```

## Example 

This event doesn't have example


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API">player</a> which exited the colshape</li>

<li><b>shape</b>: <b><span style="color:#008017">ColShape</span></b> - The <a href="/index.php?title=Category:ColShape_API" title="Category:ColShape API">colshape</a> the player left</li>
