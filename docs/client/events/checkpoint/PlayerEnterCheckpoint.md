# PlayerEnterCheckpoint

Event triggered when a player enters into a checkpoint

## Syntax

```js
mp.events.add("playerEnterCheckpoint", eventHandler);
```

## Example 

```js
mp.events.add("playerEnterCheckpoint", (player, checkpoint) => {
	// Do what you want.
});
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API">player</a> who entered checkpoint.</li>

<li><b>checkpoint</b>: <b><span style="color:#008017">Checkpoint</span></b> - The <a href="/index.php?title=Category:Checkpoint_API" title="Category:Checkpoint API">checkpoint</a> that player entered.</li>
