# PlayerReachWaypoint

Event triggered when a player reaches a waypoint

# Syntax

```js
mp.events.add("playerReachWaypoint", (player) => {
    // Do what you want.
});
```

# Example 

This function doesn't have example


# Paramters

<li><b>player</b></li>
