# PlayerCreateWaypoint

Triggered when a player creates a waypoint on the map.

# Syntax

**Clientside event:**

```js
mp.events.add("playerCreateWaypoint", (position) => {
mp.console.logInfo(`New waypoint created at: $ {position.x}, $ {position.y}, $ {position.z}`);
});  
```

# Example 

This native doesn't have example


# Paramters

<li><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
