# EntityStreamIn

This event is triggered when a car or player enters the stream zone

## Syntax

This event doesn't have syntax example

## Example 

```js
// Other player streams in
mp.events.add('entityStreamIn', (entity) => {
	const isInvincible = entity.getVariable('isInvincible');
	entity.setInvincible(!!isInvincible);
});
        
```


## Paramters

<p>There are no parameters</p>
