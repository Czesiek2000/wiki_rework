# EntityStreamOut

This event is called everytime a entity goes out of player's streaming range.

## Syntax

```js
mp.events.add('entityStreamOut', (entity) => {});
```

## Example 

```js
mp.events.add('entityStreamOut', (entity) => {
	mp.gui.chat.push('Entity is gone, but never gonna give you up, never gonna let you down...');
});
```


## Paramters

<li><b>entity</b>: The entity handler, output expects <b>any</b> type.</li>
