# Render(JS) Tick(CSharp)

This event is invoked every frame on client-side, it is not accessible on the server. It is similar to the OnUpdate in GTA:Network C# Code where the function/event is called every tick on the server.

## Syntax

This event doesn't have syntax example

## Example 

```js
mp.events.add('render', () => {
	mp.game.graphics.drawText('This is called every frame.', [0.5, 0.005], {
        font: 4,
        color: [255, 255, 255, 255],
        scale: [1.0, 1.0],
        outline: true
    });
});
        
```


## Paramters

<li><b>nametags (Optional)</b></li>
