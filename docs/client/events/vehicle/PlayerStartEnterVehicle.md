# PlayerStartEnterVehicle

This event is triggered when a player starts to get into a vehicle. This event is cancellable.

## Syntax

This event doesn't have syntax example

## Example 

```js
function playerStartEnterVehicleHandler(player, vehicle, seat) {
   const playerName = player.name;
   const vehicleID = vehicle.id;

   mp.players.broadcast(`${playerName} started to get into the car ID: ${vehicleID}. Seat: ${seat}`);
}

mp.events.add("playerStartEnterVehicle", playerStartEnterVehicleHandler);
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - player, which sits down (serverside only)</li>

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b> - vehicle in which the player sits.</li>

<li><b>seat</b>: <b><span style="color:#008017">Number</span></b> - the place where he sits down.</li>
