# PlayerLeaveVehicle

Triggers when the player exits the vehicle.

## Syntax

This function doesn't have syntax example

## Example 

```js
mp.events.add("playerLeaveVehicle", (vehicle, seat) => {
    mp.gui.chat.push(`You are now leaving the vehicle from seat ${seat}`);
})
```


## Paramters

<li><b>vehicle</b>: <b><span style="color:#008017">Object</span></b></li>

<li><b>seat</b>: <b><span style="color:#008017">Int</span></b></li>

