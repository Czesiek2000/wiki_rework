# PlayerEnterVehicle

This event is triggered when a player entered vehicle.

## Syntax

This function doesn't have syntax example

## Example 

```js
function playerEnterVehicleHandler(player, vehicle, seat) {
	player.outputChatBox(`${player.name} got into the car with ID: ${vehicle.id}. Seat: ${seat}`);
}
 
mp.events.add("playerEnterVehicle", playerEnterVehicleHandler);
```


## Paramters

<li><b>player</b>: <b><span style="color:#008017">Player</span></b> - The <a href="https://wiki.rage.mp/index.php?title=Category:Player_API" title="Category:Player API">player</a> which entered vehicle.</li>

<li><b>vehicle</b>: <b><span style="color:#008017">Vehicle</span></b> - The current <a href="/index.php?title=Category:Vehicle_API" title="Category:Vehicle API">vehicle</a> the player is sitting in.</li>

<li><b>seat</b>: <b><span style="color:#008017">Number</span></b> - The seat ID the player sits down on.</li>
