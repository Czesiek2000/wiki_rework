# ConsoleCommand

### Version 1.1+

This event is triggered when console (F11) input is done.

## Syntax

```js
mp.events.add("consoleCommand", (command) => {
	mp.console.logInfo('Hello world');
});
```

## Example 

This function doesn't have example


## Paramters

<li><b>command:</b> <b><span style="color:#008017">String</span></b></li>
