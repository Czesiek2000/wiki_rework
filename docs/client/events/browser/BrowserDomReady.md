# BrowserDomReady

This event is called every time a CEF Browser dom is ready.

## Syntax

```js
mp.events.add('browserDomReady', (browser) => {});
```

## Example 

The example below shows up a message for a player when a CEF Browser DOM is ready.

```js
mp.events.add('browserDomReady', (browser) => {
	mp.gui.chat.push('Hold on, get set, goooo! Your CEF Browser DOM is now ready.');
});
```


## Paramters

<li><b>browser</b>: The browser handler, output expects <b>any</b> type.</li>
