# BrowserCreated

This event is called everytime a browser is created.

## Syntax

```js
mp.events.add('browserCreated', (browser) => {});
```

## Example 

The example below shows up a message for a player when a browser is created.

```js
mp.events.add('browserCreated', (browser) => {
	mp.gui.chat.push('WOOOOOOAH, a CEF Browser has been created!');
});
```


## Paramters

<b>browser</b>: The created browser handler, output expects <b>any</b> type.
