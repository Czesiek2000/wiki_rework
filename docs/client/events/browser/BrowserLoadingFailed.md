# BrowserLoadingFailed

This event is called everytime a CEF Browser loading fails.

## Syntax

```js
mp.events.add('browserLoadingFailed', (browser) => {});
```

## Example 

The example below shows up a message for a player when a CEF Browser loading fails.

```js
mp.events.add('browserLoadingFailed', (browser) => {
	mp.gui.chat.push('Oh boy, your browser failed to load :/');
});
```


## Paramters

<li><b>browser</b>: The browser handler, output expects <b>any</b> type.</li>
