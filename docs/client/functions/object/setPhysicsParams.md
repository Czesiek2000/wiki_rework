# Object::setPhysicsParams

Known parameters: 

p1 = weight, units unknown

p6 = object (downwards? (No shit, Sherlock...)) gravity

p11 = buoyancy, affects how much the object floats. Set to at least 2.0f to make an object float


# Syntax

```js
object.setPhysicsParams(weight, p2, p3, p4, p5, gravity, p7, p8, p9, p10, buoyancy);
```

# Example

```js
// todo
```

# Parameters

<li><b>weight:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>gravity:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>p10:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>buoyancy:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>