# Object::ObjectWeak

Creates an instance of a multiplayer object from a game object handle

**This is shared function**

# Syntax

```js
mp.objects.newWeak(handle);
```

# Example


# Parameters

<li><b>handle</b>: <b><span style="color:#008017">Hash</span></b></li>

# Return value

<li><b>Undefined</b></li>