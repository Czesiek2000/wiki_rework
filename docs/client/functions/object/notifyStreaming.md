# Object::notifyStreaming

This property, if true, will call [EntityStreamIn](../../events/streaming/EntityStreamIn.md) for the Object.


Default: false


# Syntax


# Example

```js
const object = mp.objects.new('prop_cs_box_clothes', new mp.Vector3(500, 500, 500)); // Untested inside Options
object.notifyStreaming = true;
```

# Parameters


# Return value

<li><b>Undefined</b></li>