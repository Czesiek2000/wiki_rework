# Object::Object

Creates a new Object.

<li><b><a class="external text" href="https://cdn.rage.mp/public/odb/index.html" rel="nofollow">Objects gallery</a></b></li>

# Syntax

```js
mp.objects.new(model, position, {
    rotation: rotation,
    alpha: alpha,
    dimension: dimension
});
```

# Example

```js
mp.events.addCommand('flag', (player) => {
    mp.objects.new('apa_prop_flag_portugal', player.position, {
            rotation: player.rotation,
            alpha: 250,
            dimension: player.dimension
        });
});

//Following this code, you'll create a portuguese flag (apa_prop_flag_portugal) at the players position, facing the same way the player is facing, with no transparency (250 on alpha) and at the same dimension the player is.
```

# Parameters

<ul><li><b>model</b>: <b><span style="color:#008017">Hash</span></b></li>
<li><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>rotation</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>alpha</b>: <b><span style="color:#008017">Number</span></b> - The object's transparency.</li>
<li><b>dimension</b>: <b><span style="color:#008017">Number</span></b></li></ul>

# Return value

<li><b>Undefined</b></li>