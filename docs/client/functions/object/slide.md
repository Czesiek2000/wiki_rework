# Object::slide

Returns true if the object has finished moving. If false, moves the object towards the specified **X**, **Y** and **Z** coordinates with the specified **X**, **Y** and **Z** speed.

See also: [gta gaming site](gtag.gtagaming.com/opcode-database/opcode/034E/).

Has to be called in a render or it will not slide but move some amount and stop.

# Syntax

```js
object.slide(toX, toY, toZ, speedX, speedY, speedZ, collision);
```

# Example

```js
mp.events.add('render', () => {
  object.slide(position.x, position.y, position.z, 1.0, 1.0, 1.0, false);
});
```

# Parameters

<li><b>toX:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>toY:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>toZ:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>speedX:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>speedY:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>speedZ:</b> <b><span style="color:#008017">Float</span></b></li>


<li><b>collision:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>