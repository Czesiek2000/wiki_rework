# Pool::toArray

This function converts a pool to a [JavaScript array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array).

# Syntax

```js
pool.toArray();
```

# Example

```js
var suchJavascript = mp.players.toArray();
if(suchJavascript.every((player) => { return player.isAiming; } ))
    console.log("Everyone is aiming.");
```

# Parameters
No parameters here

# Return value
<li><b><span style="color:#008017">Array</span></b></li>