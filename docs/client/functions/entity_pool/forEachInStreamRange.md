# Pool::forEachInStreamRange
This function is used for calling a function for each element that is in a client's streaming range in a pool.


# Syntax

```js
pool.forEachInStreamRange(Function callingFunction);
```

# Example

This example will generate text with all player nicknames within the client's streaming range.

```js
let getNicknamesText = () => {
	let text = ``;
	mp.players.forEachInStreamRange(
		(player, id) => {
			text = text == `` ? player.name : `${text} , ${player.name}`;
		}
	);
	return text;
};

let blahBlah = getNicknamesText();
console.log(blahBlah != `` ? blahBlah : `There are no players in your streaming range.`);
```

# Parameters

<li><b>callingFunction:</b> Function, what will be called.</li>

# Return value

<li><b>Undefined</b></li>