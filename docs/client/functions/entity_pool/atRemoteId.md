# Pool::atRemoteId
This function returns the entity object, if valid from the server-side generated entity.id.
This is important to know, because the client-side entity.id may be different to the entity.id server-side.




# Syntax

```js
Entity pool.atRemoteId(Number ID)
```

# Example

This example will check player with ID in pool, and write player name into chat if he exists.

```js
// client-side entity.remoteId would return the server-side entity.id
let localPlayer = mp.players.local;
let player = mp.players.atRemoteId(localPlayer.remoteId);

// usually it's used as you have sent the server-side entity.id to clientside and want to grab the correct entity
// in this example, we got the ID "1" from the server-side and there are more vehicles on the server, where the client-side vehicle ID is "0"
let vehicleID = 1;

let vehicle = mp.vehicles.atRemoteId(vehicleID);

if(vehicle) {
	// now we have the vehicle entity, which is the same like server-side
}
```

# Parameters

<li><b>ID:</b> Entity remote ID ( entity.remoteId )</li>

# Return value

An entity with the selected remote ID from a pool, or undefined if entity with this remote ID does not exists.