# Pool::atHandle
Function to get the entity with the given handle from his entity pool.


# Syntax

```js
Entity pool.atHandle(Number handle)
```

# Example

This example will get a player by handle and output his name on the chat.

```js
function outputPlayerNameWithHandle(handle) {
    const player = mp.players.atHandle(handle);
    if (player) {
    	mp.gui.chat.push(`Player with handle ${handle} is named ${player.name}.`);
    } else {
	    mp.gui.chat.push(`Player with handle ${handle} does not exist.`);
    }
}
```

# Parameters

<li><b>handle:</b> Handle of the entity</li>

# Return value

<li><b><span style="color:#008017">Entity</span></b> Entity with specified handle, <b>undefined</b> if none exists.</li>