# Vehicle::getClass

Gets the Vehicle Class.
Vehicle Classes:
* 0: Compacts
* 1: Sedans
* 2: SUVs
* 3: Coupes
* 4: Muscle
* 5: Sports Classics
* 6: Sports
* 7: Super
* 8: Motorcycles
* 9: Off-road
* 10: Industrial
* 11: Utility
* 12: Vans
* 13: Cycles
* 14: Boats
* 15: Helicopters
* 16: Planes
* 17: Service
* 18: Emergency
* 19: Military
* 20: Commercial
* 21: Trains
* 22: Open Wheels



## Syntax

```js
vehicle.getClass();
```

## Example

```js
let vehicle = mp.players.local.vehicle;
let vehClass = vehicle.getClass(); // Defining a variable with the return value of vehicle.getClass()
```
## Parameters

No parameters here



## Return value

<b><span style="color:#008017">Int</span></b>