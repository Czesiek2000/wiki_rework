# Vehicle::getNumberPlateText

Returns the license plate text from a vehicle. 8 chars maximum.

## Syntax

```js
vehicle.getNumberPlateText();
```

## Example

```js
let vehicle = mp.players.local.vehicle;
mp.gui.chat.push("NumberPlate: " + vehicle.getNumberPlateText());
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">String</span></b>