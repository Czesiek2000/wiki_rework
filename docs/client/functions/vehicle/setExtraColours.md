# Vehicle::setExtraColours

They use the same color indexs as `SET_VEHICLE_COLOURS`.

## Syntax

```js
vehicle.setExtraColours(pearlescentColor, wheelColor);
```

## Example

```js
// todo
```
## Parameters

<li><b>pearlescentColor: <span style="color:#008017">Int</span></b></li>
<li><b>wheelColor: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>