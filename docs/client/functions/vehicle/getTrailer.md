# Vehicle::getTrailer

Returns the trailer of a vehicle.


**This function is being updated, it might not work correctly in the current version!**



## Syntax

```js
vehicle.getTrailer(trailer);
```

## Example

```js
// todo

```
## Parameters

<b>trailer<span style="color:#008017">Vehicle handle</span></b>

<br />


## Return value

<b><span style="color:#008017">Vehicle</span></b>