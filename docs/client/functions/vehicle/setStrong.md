# Vehicle::setStrong

If set to true, vehicle will not take crash damage, but is still susceptible to damage from bullets and explosives

## Syntax

```js
vehicle.setStrong(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>