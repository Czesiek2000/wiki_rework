# Vehicle::resetStuckTimer

nullAttributes is always 0 in the scripts. In IDA it calls a second function that's entirely dependent on the 2nd parameter being true, and all it does is set a bunch of offsets to 0.

*MulleDK19*: That's not true at all. The second parameter is an int, not a bool. The inner function has a switch on the second parameter. It's the stuck timer index.Here's some pseudo code I wrote for the inner function:
```cpp
void __fastcall NATIVE_RESET_VEHICLE_STUCK_TIMER_INNER(CUnknown* unknownClassInVehicle, int timerIndex){ 
    switch (timerIndex) { 
        case 0: 
        unknownClassInVehicle->FirstStuckTimer = (WORD)0u; case 1: unknownClassInVehicle->SecondStuckTimer = (WORD)0u; 
        case 2: unknownClassInVehicle->ThirdStuckTimer = (WORD)0u; 
        case 3: unknownClassInVehicle->FourthStuckTimer = (WORD)0u; 
        case 4: unknownClassInVehicle->FirstStuckTimer = (WORD)0u; unknownClassInVehicle->SecondStuckTimer = (WORD)0u; unknownClassInVehicle->ThirdStuckTimer = (WORD)0u; unknownClassInVehicle->FourthStuckTimer = (WORD)0u; 
        break; 
    };
}
```


## Syntax

```js
vehicle.resetStuckTimer(nullAttributes);
```

## Example

```js
// todo
```
## Parameters

<b>nullAttributes: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>