# Vehicle::isCargobobMagnetActive

Returns true only when the magnet is active, will return false if the hook is active

## Syntax

```js
vehicle.isCargobobMagnetActive();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>