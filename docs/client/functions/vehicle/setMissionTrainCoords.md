# Vehicle::setMissionTrainCoords


## Syntax

```js
vehicle.setMissionTrainCoords(x, y, z);
```

## Example

```js
// todo
```
## Parameters

<li><b>x: <span style="color:#008017">Float</span></b></li>
<li><b>y: <span style="color:#008017">Float</span></b></li>
<li><b>z: <span style="color:#008017">Float</span></b></li>


<br />


## Return value

<b>Undefined</b>