# Vehicle::setExplodesOnHighExplosionDamage

Sets a vehicle to be strongly resistant to explosions. p0 is the vehicle; set p1 to false to toggle the effect on/off.



## Syntax

```js
vehicle.setExplodesOnHighExplosionDamage(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>