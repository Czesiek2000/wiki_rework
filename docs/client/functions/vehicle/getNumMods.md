# Vehicle::getNumMods

Returns how many possible mods a vehicle has for a given mod type.



## Syntax

```js
vehicle.getNumMods(modType);
```

## Example

```js
mp.players.local.vehicle.getNumMods(48); // get the number of vinyls for the car in which the player sits

```
## Parameters

<b>modType: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Int</span></b>