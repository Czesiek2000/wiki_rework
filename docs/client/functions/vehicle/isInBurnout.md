# Vehicle::isInBurnout

Returns whether the specified vehicle is currently in a burnout.

vb.net
```vb
Public Function isVehicleInBurnout(vh As Vehicle) As Boolean 

Return Native.Function.Call(Of Boolean)(Hash.IS_VEHICLE_IN_BURNOUT, vh)
```
End Function



## Syntax

```js
vehicle.isInBurnout();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>