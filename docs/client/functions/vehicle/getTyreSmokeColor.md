# Vehicle::getTyreSmokeColor


## Syntax

```js
vehicle.getTyreSmokeColor(r, g, b);
```

## Example

```js
// todo
```
## Parameters

<li><b>r: <span style="color:#008017">Int</span></b></li>
<li><b>g: <span style="color:#008017">Int</span></b></li>
<li><b>b: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>r,g,b: <span style="color:#008017">Object</span></b>