# Vehicle::isVisible

must be called after `TRACK_VEHICLE_VISIBILITY` 

it's not instant so probabilly must pass an 'update' to see correct result.



## Syntax

```js
vehicle.isVisible();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>