# Vehicle::getModVariation

Only used for wheels(ModType = 23/24) Returns true if the wheels are custom wheels



## Syntax

```js
vehicle.getModVariation(modType);
```

## Example

```js
// todo
```
## Parameters

<b>modType: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>