# Vehicle::setDoorBroken

doorIndex:
* 0: Front Right Door
* 1: Front Left Door
* 2: Back Right Door
* 3: Back Left Door
* 4: Hood
* 5: Trunk

Changed last paramater from `CreateDoorObject` To `NoDoorOnTheFloor` because when on false, the door object is created, and not created when on true...

the former parameter name was counter intuitive...(by *Calderon*)

Calderon is a moron. 

On 1.1 DP#1 the object will not be created for your self, it will be created for others though



## Syntax

```js
vehicle.setDoorBroken(doorIndex, createDoorObject);
```

## Example

```js
// todo
```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>
<b>createDoorObject: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>