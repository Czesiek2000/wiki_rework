# Vehicle::setHeliBladesFullSpeed

Equivalent of `SET_HELI_BLADES_SPEED(vehicleHandle, 1.0f);`



## Syntax

```js
vehicle.setHeliBladesFullSpeed();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>