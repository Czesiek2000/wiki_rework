# Vehicle::setPedTargettableDestroy

destroyType is 1 for opens on damage, 2 for breaks on damage.



## Syntax

```js
vehicle.setPedTargettableDestroy(vehicleComponent, destroyType);
```

## Example

```js
// todo
```
## Parameters

<li><b>vehicleComponent: <span style="color:#008017">Int</span></b></li>
<li><b>destroyType: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>