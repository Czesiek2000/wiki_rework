# Vehicle::setNeonLightEnabled

**THIS PAGE IS NOT COMPLETED**


Sets the neon lights of the specified vehicle on/off.Indices:

### Indices:
<ul><li>0 = Left</li>
<li>1 = Right</li>
<li>2 = Front</li>
<li>3 = Back</li></ul>

## Syntax

```js
vehicle.setNeonLightEnabled(index, toggle);
```

## Example

```js
// todo
```
## Parameters

<b>index: <span style="color:#008017">int(indice)</span></b>
<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>