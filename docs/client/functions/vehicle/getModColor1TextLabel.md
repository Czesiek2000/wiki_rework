# Vehicle::getModColor1TextLabel

returns a string which is the codename of the vehicle's currently selected primary color

p1 is always 0



## Syntax

```js
vehicle.getModColor1TextLabel(p1);
```

## Example

```js
// todo
```
## Parameters

<b>p1: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b><span style="color:#008017">String</span></b>