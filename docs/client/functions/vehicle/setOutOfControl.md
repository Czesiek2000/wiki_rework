# Vehicle::setOutOfControl

Tested on the player's current vehicle. Unless you kill the driver, the vehicle doesn't loose control, however, if enabled, `explodeOnImpact` is still active. 

The moment you crash, boom.



## Syntax

```js
vehicle.setOutOfControl(killDriver, explodeOnImpact);
```

## Example

```js
// todo
```
## Parameters

<li><b>killDriver: <span style="color:#008017">Boolean</span></b></li>
<li><b>explodeOnImpact: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>