# Vehicle::setDoorsLockedForAllPlayers

After some analysis, I've decided that these are what the parameters are.

We can see this being used in R* scripts such as `am_mp_property_int.ysc.c4`:
```cpp
l_11A1 = Player::GET_VEHICLE_Player_IS_IN(PLAYER::PLAYER_Player_ID(), 1);
...
VEHICLE::SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(l_11A1, 1);
```


## Syntax

```js
vehicle.setDoorsLockedForAllPlayers(toggle);
```

## Example

```js
//Locks vehicle for all client-sided:
vehicle.setDoorsLockedForAllPlayers(true);
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>