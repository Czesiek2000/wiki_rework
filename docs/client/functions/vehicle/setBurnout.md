# Vehicle::setBurnout

On accelerating, spins the driven wheels with the others braked, so you don't go anywhere.

## Syntax

```js
vehicle.setBurnout(toggle);
```

## Example

```js
mp.events.add('breaktyres', (vehicle) => { //Event when called defaltes the 4 tyres of the vehicle and Set the vehicle out of control for 2 seconds.
    vehicle.setTyreBurst(0, false, 1000);
    vehicle.setTyreBurst(1, false, 1000);
    vehicle.setTyreBurst(4, false, 1000);
    vehicle.setTyreBurst(5, false, 1000);
    vehicle.setBurnout(true);
    setTimeout(_ => {
        vehicle.setBurnout(false);
    }, 2000)
})
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>