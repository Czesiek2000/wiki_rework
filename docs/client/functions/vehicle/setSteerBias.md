# Vehicle::setSteerBias

Locks the vehicle's steering to the desired angle, explained below.

Requires to be called onTick. Steering is unlocked the moment the function stops being called on the vehicle.Steer bias:
* -1.0 = full right
* 0.0 = centered steering
* 1.0 = full left



## Syntax

```js
vehicle.setSteerBias(value);
```

## Example

```js
// todo
```
## Parameters

<b>value: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>