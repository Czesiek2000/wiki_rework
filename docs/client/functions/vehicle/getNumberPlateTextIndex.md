# Vehicle::getNumberPlateTextIndex

Returns the PlateType of a vehicle 
* Blue_on_White_1 = 3,
* Blue_on_White_2 = 0,
* Blue_on_White_3 = 4,
* Yellow_on_Blue = 2,
* Yellow_on_Black = 1,
* North_Yankton = 5,



## Syntax

```js
vehicle.getNumberPlateTextIndex();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>