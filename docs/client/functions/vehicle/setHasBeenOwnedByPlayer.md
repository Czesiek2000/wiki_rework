# Vehicle::setHasBeenOwnedByPlayer


## Syntax

```js
vehicle.setHasBeenOwnedByPlayer(owned);
```

## Example

```js
// todo
```
## Parameters

<b>owned: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>