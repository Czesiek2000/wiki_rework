# Vehicle::setCanBeVisiblyDamaged


## Syntax

```js
vehicle.setCanBeVisiblyDamaged(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>