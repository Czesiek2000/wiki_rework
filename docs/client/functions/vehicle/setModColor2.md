# Vehicle::setModColor2

Changes the secondary paint type and color

### paintType:
* 0:Normal
* 1:Metallic
* 2:Pearl
* 3:Matte
* 4:Metal
* 5:Chrome

color: number of the color



## Syntax

```js
vehicle.setModColor2(paintType, color);
```

## Example

```js
// todo
```
## Parameters

<li>paintType: <b><span style="color:#008017">Int</span></b></li>
<li><b>color: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>