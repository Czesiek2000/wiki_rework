# Vehicle::setDoorShut





Shuts the door of the vehicle.


### doorIndex:
<ul><li>0 = Front Left Door</li>
<li>1 = Front Right Door</li>
<li>2 = Back Left Door</li>
<li>3 = Back Right Door</li>
<li>4 = Hood</li>
<li>5 = Trunk</li>
<li>6 = Back</li>
<li>7 = Back2</li></ul>





## Syntax

```js
vehicle.setDoorShut(doorIndex, closeInstantly);
```

## Example

When in a vehicle, pressing the *N* key will open/close the hood

```js
mp.keys.bind(0x4E, true, function() {   // N Key
    if(mp.players.local.vehicle){
        if (mp.players.local.vehicle.hood){
            mp.players.local.vehicle.hood = false;
            mp.players.local.vehicle.setDoorShut(4, true);
        } else {
            mp.players.local.vehicle.hood = true;
            mp.players.local.vehicle.setDoorOpen(4, false, true);
        }
    }
});

```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>
<b>closeInstantly: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>