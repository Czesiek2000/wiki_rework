# Vehicle::isSeatFree

Check if a vehicle seat is free.

-1 being the driver seat.

Use `GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle) - 1` for last seat index.



## Syntax

```js
vehicle.isSeatFree(seatIndex);
```

## Example

```js
// todo
```
## Parameters

<b>seatIndex: <span style="color:#008017">Int</span></b>
<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>