# Vehicle::getColours


## Syntax

```js
vehicle.getColours(colorPrimary, colorSecondary);
```

## Example

```js
// todo
```
## Parameters

<b>colorPrimary<span style="color:#008017">Int</span></b>
<b>colorSecondary<span style="color:#008017">Int</span></b>

<br />


## Return value

<b>object: colorPrimary, colorSecondary</b>