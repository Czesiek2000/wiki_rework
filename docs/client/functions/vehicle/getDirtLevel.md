# Vehicle::getDirtLevel

Dirt level *0..15*



## Syntax

```js
vehicle.getDirtLevel();
```

## Example

```js
// todo
```
## Parameters


## Return value

<b><span style="color:#008017">Float</span></b>