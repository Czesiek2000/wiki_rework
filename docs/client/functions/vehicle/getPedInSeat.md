# Vehicle::getPedInSeat

-1 (driver) <= index < `GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle)`


**If a player enters the driver seat from the passenger side, getPedInSeat(-1) will return 0 until the player actually reaches the seat.**



## Syntax

```js
vehicle.getPedInSeat(index);
```

## Example

```js
// todo
```
## Parameters

<b><span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Ped handle or object</b>