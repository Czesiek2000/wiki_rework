# Vehicle::setExtra

Max extra ID is 16.

## Syntax

```js
vehicle.setExtra(extraId, disabled);
```

## Example

```js
const vehicle = mp.players.local.vehicle;
if(vehicle) {
  vehicle.setExtra(1, false); // Enable extra 1
}
```
## Parameters

<li>extraId: <b><span style="color:#008017">Int</span></b></li>
<li>disabled: <b><span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>