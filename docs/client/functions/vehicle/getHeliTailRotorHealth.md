# Vehicle::getHeliTailRotorHealth

Max 1000.

At 0 the tail rotor will stall.



## Syntax

```js
vehicle.getHeliTailRotorHealth();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>