# Vehicle::getDoorsLockedForPlayer


## Syntax

```js
vehicle.getDoorsLockedForPlayer(player);
```

## Example

```js
// todo
```
## Parameters

<b>player: <span style="color:#008017">Player</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>