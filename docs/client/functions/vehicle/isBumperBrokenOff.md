# Vehicle::isBumperBrokenOff


## Syntax

```js
vehicle.isBumperBrokenOff(front);
```

## Example

```js
// todo
```
## Parameters

<b>front: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>