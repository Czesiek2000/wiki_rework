# Vehicle::isStopped

Returns true if the vehicle's current speed is less than, or equal to `0.0025f`.



## Syntax

```js
vehicle.isStopped();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>