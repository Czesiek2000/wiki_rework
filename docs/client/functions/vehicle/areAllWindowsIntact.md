# Vehicle::areAllWindowsIntact

Appears to return false if any window is broken.



## Syntax

```js
vehicle.areAllWindowsIntact();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>