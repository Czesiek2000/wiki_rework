# Vehicle::setCanBeTargetted

This has not yet been tested - it's just an assumption of what the types could be.

## Syntax

```js
vehicle.setCanBeTargetted(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>