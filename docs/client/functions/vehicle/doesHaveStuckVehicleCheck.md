# Vehicle::doesHaveStuckVehicleCheck

Maximum amount of vehicles with vehicle stuck check appears to be 16.

## Syntax

```js
vehicle.doesHaveStuckVehicleCheck();
```

## Example

```js
// todo
```
## Parameters

No parameters here


## Return value

<b><span style="color:#008017">Boolean</span></b>