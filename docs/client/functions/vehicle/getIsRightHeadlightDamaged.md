# Vehicle::getIsRightHeadlightDamaged

From the driver's perspective, is the right headlight broken.



## Syntax

```js
vehicle.getIsRightHeadlightDamaged();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>