# Vehicle::setEngineTorqueMultiplier

<1.0 - Decreased torque

=1.0 - Default torque

<p>>1.0 - Increased torque</p>

Negative values will cause the vehicle to go backwards instead of forwards while accelerating.value - is between 0.2 and 1.8 in the decompiled scripts. This needs to be called every frame to take effect.



## Syntax

```js
vehicle.setEngineTorqueMultiplier(value);
```

## Example

```js
mp.events.add('render', () => {
    let vehicle = mp.players.local.vehicle;
    vehicle.setEngineTorqueMultiplier(1.5); // Multiplies by 1.5 the engine torque
});

```
## Parameters

<b>value: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>