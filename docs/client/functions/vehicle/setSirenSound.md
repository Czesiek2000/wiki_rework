# Vehicle::setSirenSound

Activate siren sound on vehicle (Only works if the vehicle has a siren).


Make sure you use true for silence.

## Syntax

```js
vehicle.setSirenSound(toggle);
```

## Example

```js
mp.players.local.vehicle.setSirenSound(true)
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>