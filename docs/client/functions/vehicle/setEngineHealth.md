# Vehicle::setEngineHealth

1000 is max health

Begins leaking gas at around 650 health-999.

90002441406 appears to be minimum health, although nothing special occurs



## Syntax

```js
vehicle.setEngineHealth(health);
```

## Example

```js
// todo
```
## Parameters

<b>health: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>