# Vehicle::cargobobMagnetGrab

Console Hash: 0xF57066DAWon't attract or magnetize to any helicopters or planes of course, but that's common sense.



## Syntax

```js
vehicle.cargobobMagnetGrab(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>