# Vehicle::isAnySeatEmpty

Returns false if every seat is occupied.



## Syntax

```js
vehicle.isAnySeatEmpty();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>