# Vehicle::setDoorsLockedForTeam


## Syntax

```js
vehicle.setDoorsLockedForTeam(team, toggle);
```

## Example

```js
// todo
```
## Parameters

<b>team: <span style="color:#008017">Int</span></b>
<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>