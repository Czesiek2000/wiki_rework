# Vehicle::setWheelType

Sets the vehicle's wheel type.

### Wheel Types:
<ul><li>-1: Stock</li>
<li>0: Sport</li>
<li>1: Muscle</li>
<li>2: Lowrider</li>
<li>3: SUV</li>
<li>4: Offroad</li>
<li>5: Tuner</li>
<li>6: Bike Wheels</li>
<li>7: High End</li>
<li>8: Benny's Original</li>
<li>9: Benny's Bespoke</li>
<li>10: Open Wheel</li>
<li>11: Street</li></ul>

## Syntax

```js
vehicle.setWheelType(WheelType);
```

## Example

```js
let localPlayer = mp.players.local;
localPlayer.vehicle.setWheelType(8); // Epic Benny
```
## Parameters

<b>WheelType: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>