# Vehicle::setSilent

If set to TRUE, it seems to suppress door noises and doesn't allow the horn to be continuous.

Doesn't seem to suppress door noises for me, at least with the vehicle add-on I tried



## Syntax

```js
vehicle.setSilent(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>