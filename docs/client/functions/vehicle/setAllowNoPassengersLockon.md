# Vehicle::setAllowNoPassengersLockon

Makes the vehicle accept no passengers.

## Syntax

```js
vehicle.setAllowNoPassengersLockon(p1);
```

## Example

```js
// todo
```
## Parameters

<b>p1: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>