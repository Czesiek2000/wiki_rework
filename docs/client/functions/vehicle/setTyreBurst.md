# Vehicle::setTyreBurst

Pops out the tyre.

## Syntax

```js
vehicle.setTyreBurst(index, onRim, p3);
```

## Example

```js
let vehicle = mp.players.local.vehicle;
    vehicle.setTyreBurst(0, false, 1000);
    vehicle.setTyreBurst(1, false, 1000);
    vehicle.setTyreBurst(4, false, 1000);
    vehicle.setTyreBurst(5, false, 1000);
// All the 4 Tyres of the vehicle will be deflated completely
```

## Parameters

<ul><li><b>index:</b> <b><font color="red">int</font></b> (<b>Wheel</b>)</li>
<li><b>onRim:</b> <b><font color="blue">Boolean</font></b> (<b>The Tyre deflate when hitting a object</b>)</li>
<li><b>p3:</b> <b><font color="green">float</font></b> (<b>1000 Deflates the Tyre completely</b>)</li></ul>

<br />


## Return value

<b>Undefined</b>