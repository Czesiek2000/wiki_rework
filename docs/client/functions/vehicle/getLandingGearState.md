# Vehicle::getLandingGearState

landing gear states:
* 0: Deployed
* 1: Closing
* 2: Opening
* 3: Retracted



## Syntax

```js
vehicle.getLandingGearState();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>