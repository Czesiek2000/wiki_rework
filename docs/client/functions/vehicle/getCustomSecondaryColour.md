# Vehicle::getCustomSecondaryColour


## Syntax

```js
vehicle.getCustomSecondaryColour(r, g, b);
```

## Example

```js
// todo
```

## Parameters

<li>r: <b><span style="color:#008017">Int</span></b></li>
<li>g: <b><span style="color:#008017">Int</span></b></li>
<li>b: <b><span style="color:#008017">Int</span></b></li>

<br />


## Return value

<li>r,g,b: <b><span style="color:#008017">object</span></b></li>