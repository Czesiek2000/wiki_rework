# Vehicle::getExtraColours


## Syntax

```js
vehicle.getExtraColours(pearlescentColor, wheelColor);
```

## Example

```js
let obj = mp.players.local.vehicle.getExtraColours(1, 1);
mp.gui.chat.push(`${obj.pearlescentColor} ${obj.wheelColor}`);
```
## Parameters

<li>pearlescentColor<b><span style="color:#008017">Int</span></b></li>
<li>wheelColor<b><span style="color:#008017">int</span></b></li>

<br />


## Return value

<li>pearlescentColor, wheelColor: <b><span style="color:#008017">object</span></b></li>