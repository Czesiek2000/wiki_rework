# Vehicle::jitter

When I called this with what the script was doing, which was -190f for yaw pitch and roll, all my car did was jitter a little. I also tried 0 and 190f. I altered the p1 variable between TRUE and FALSE and didn't see a difference.This might have something to do with the physbox of the vehicle, but I'm not sure.



## Syntax

```js
vehicle.jitter(p1, yaw, pitch, roll);
```

## Example

```js
// todo
```

## Parameters
<b>p1: <span style="color:#008017">Float</span></b>
<b>yaw: <span style="color:#008017">Float</span></b>
<b>pitch: <span style="color:#008017">Float</span></b>
<b>roll: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>