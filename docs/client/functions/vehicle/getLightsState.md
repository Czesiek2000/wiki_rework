# Vehicle::getLightsState


## Syntax

```js
vehicle.getLightsState(lightsOn, highbeamsOn);
```

## Example

```js
let lightState = vehicle.getLightsState(1,1);

mp.gui.chat.push(`Low Beam: ${lightState.lightsOn}, High Beam: ${lightState.highbeamsOn}`);

```
## Parameters

No parameters here

<li><b>lightsOn: <span style="color:#008017">Boolean</span></b></li>
<li><b>highbeamsOn: <span style="color:#008017">Boolean</span></b></li>
<br />


## Return value

<b>lightsOn, highbeamsOn<span style="color:#008017">object</span></b>