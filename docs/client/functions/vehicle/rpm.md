# Vehicle::rpm

This property used for getting vehicle's rpm.


## Syntax

```js
Float vehicle.rpm;
```

## Example

No example here

<br />


## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>