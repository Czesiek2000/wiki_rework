# Vehicle::setReduceGrip

Reduces grip significantly so it's hard to go anywhere.

## Syntax

```js
vehicle.setReduceGrip(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>