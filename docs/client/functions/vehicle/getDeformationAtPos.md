# Vehicle::getDeformationAtPos

The only example I can find of this function in the scripts, is this:

```cpp
struct _s = VEHICLE::GET_VEHICLE_DEFORMATION_AT_POS(rPtr((A_0) + 4), 1.21f, 6.15f, 0.3f);
```

PC scripts:
```cpp
v_5/*{3}*/ = VEHICLE::GET_VEHICLE_DEFORMATION_AT_POS(a_0._f1, 1.21, 6.15, 0.3);
```


## Syntax

```js
vehicle.getDeformationAtPos(offsetX, offsetY, offsetZ);
```

## Example

```js
// todo
```
## Parameters

<b>OffsetX:<span style="color:#008017">Float</span></b>
<b>OffsetY:<span style="color:#008017">Float</span></b>
<b>OffsetZ:<span style="color:#008017">Float</span></b>

<br />


## Return value

<b><span style="color:#008017">Vector3</span></b>