# Vehicle::attachToTowTruck

HookOffset defines where the hook is attached. leave at 0 for default attachment.



## Syntax

```js
vehicle.attachToTowTruck(vehicle, rear, hookOffsetX, hookOffsetY, hookOffsetZ);
```

## Example

```js
// todo
```
## Parameters


<ul><li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>rear:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>hookOffsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>hookOffsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>hookOffsetZ:</b> <b><span style="color:#008017">Float</span></b></li></ul>

<br />


## Return value

<b>Undefined</b>