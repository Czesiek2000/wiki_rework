# Vehicle::isSearchlightOn

Possibly: Returns whether the searchlight (found on police vehicles) is toggled on.@Author Nac



## Syntax

```js
vehicle.isSearchlightOn();
```

## Example

```js
// todo
```
<b><span style="color:#008017">Float</span></b>
## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>