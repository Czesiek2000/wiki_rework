# Vehicle::getWheelType

Returns an intWheel Types:
* 0: Sport
* 1: Muscle
* 2: Lowrider
* 3: SUV
* 4: Offroad
* 5: Tuner
* 6: Bike Wheels
* 7: High EndTested in Los Santos Customs

## Syntax

```js
vehicle.getWheelType();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>