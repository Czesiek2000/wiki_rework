# Vehicle::setModKit

Set modKit to 0 if you plan to call `SET_VEHICLE_MOD`. That's what the game does. 

Most body modifications through `SET_VEHICLE_MOD` will not take effect until this is set to 0.



## Syntax

```js
vehicle.setModKit(modKit);
```

## Example

```js
// todo
```
## Parameters

<b>modKit: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>