# Vehicle::setTimedExplosion

```cpp
VEHICLE::SET_VEHICLE_TIMED_EXPLOSION(v_3, 
PLAYER::GET_PLAYER_PED(v_5), 1);
```


## Syntax

```js
vehicle.setTimedExplosion(ped, toggle);
```

## Example

```js
// todo
```
## Parameters

<li><b>ped: Ped handle or object</b></li>
<li><b>toggle: <span style="color:#008017">Boolean</span></b></li>
No parameters here

<br />


## Return value

<b>Undefined</b>