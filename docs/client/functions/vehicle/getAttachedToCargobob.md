# Vehicle::getAttachedToCargobob

Returns attached vehicle (Vehicle in parameter must be cargobob)

## Syntax

```js
vehicle.getAttachedToCargobob();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<b>Vehicle handle or object</b>