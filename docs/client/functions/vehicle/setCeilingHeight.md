# Vehicle::setCeilingHeight

Previously named `GET_VEHICLE_DEFORMATION_GET_TREE` (hash collision) 

from Decrypted Scripts I found
`VEHICLE::SET_VEHICLE_CEILING_HEIGHT(l_BD9[2/*2*/], 420.0);`



## Syntax

```js
vehicle.setCeilingHeight(p1);
```

## Example

```js
// todo
```
## Parameters

<b>p1: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>