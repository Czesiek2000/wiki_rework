# Vehicle::getNeonLightsColour

Gets the color of the neon lights of the specified vehicle.



## Syntax

```js
vehicle.getNeonLightsColour(r, g, b);
```

## Example

```js
let obj = mp.players.local.vehicle.getNeonLightsColour(1, 1, 1);
mp.gui.chat.push(`${obj.r} ${obj.g} ${obj.b}`);
```
## Parameters


<b>r: <span style="color:#008017">Int</span></b>
<b>g: <span style="color:#008017">Int</span></b>
<b>b: <span style="color:#008017">Int</span></b>
<br />


## Return value

<b>r,g,b<span style="color:#008017">object</span></b>