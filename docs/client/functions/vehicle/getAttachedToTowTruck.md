# Vehicle::getAttachedToTowTruck


## Syntax

```js
vehicle.getAttachedToTowTruck();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Entity handle or object</b>