# Vehicle::getModTextLabel

Returns the text label of a mod type for a given vehicle.

Use `_GET_LABEL_TEXT` to get the part name in the game's language.

See [Vehicle::geNumMods(rage wiki)](https://wiki.rage.mp/index.php?title=Vehicle::getNumMods), [Vehicle::geNumMods(this wiki)](./getNumMods.md) to get the maximum number of mods (Usually 0 - numMods).



## Syntax

```js
vehicle.getModTextLabel(modType, modIndex);
```

## Example

```js
// todo
```
## Parameters

<b>modType: <span style="color:#008017">Int</span></b>
<b>modIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">String</span></b>