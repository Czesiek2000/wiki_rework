# Vehicle::getEngineHealth

|Engine Health|Status|
|--- |--- |
|1000|Perfect - Undamaged|
|< 1000|Damaged|
|< 400|Engine is smoking and losing functionality|
|0|Engine stops working and car is not drivable|
|< 0|Engine catches fire, health rapidly declines and blows up seconds later|

Maximum: 1000

Minimum: -4000

**NOTE: Returns 1000.0 if the function is unable to get the address of the specified vehicle or if it's not a vehicle.**



## Syntax

```js
vehicle.getEngineHealth();
```

## Example

```js
function getEngineHealth(){

    let engineHealth = vehicle.getEngineHealth();

    mp.gui.chat.push(`engine health: ${engineHealth}`);

    return engineHealth; // returns the value of the engine health as a float
}

```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>