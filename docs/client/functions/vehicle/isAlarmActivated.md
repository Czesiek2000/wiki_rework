# Vehicle::isAlarmActivated


## Syntax

```js
vehicle.isAlarmActivated();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>