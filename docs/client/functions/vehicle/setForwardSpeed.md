# Vehicle::setForwardSpeed

SCALE: Setting the speed to 30 would result in a speed of roughly 60mph, according to speedometer.

Speed is in meters per second.

You can convert meters/s to mph here: [calculateme](http://www.calculateme.com/Speed/MetersperSecond/ToMilesperHour.htm)



## Syntax

```js
vehicle.setForwardSpeed(speed);
```

## Example

```js
// todo
```
## Parameters

<b>speed: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>