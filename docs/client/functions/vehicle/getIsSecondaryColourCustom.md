# Vehicle::getIsSecondaryColourCustom

Check if Vehicle Secondary is avaliable for customize

## Syntax

```js
vehicle.getIsSecondaryColourCustom();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>