# Vehicle::getLayoutHash


## Syntax

```js
vehicle.getLayoutHash();
```

## Example

```js
const vehicle = mp.players.local.vehicle;
mp.gui.chat.push(`hash: ${vehicle.getLayoutHash()}`);
```

## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Model hash or name</span></b>