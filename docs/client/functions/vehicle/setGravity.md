# Vehicle::setGravity

if you set this to false the vehicle will no longer move and will not have any gravity (vehicles with parachute will no longer go up when the parachute is open)



## Syntax

```js
vehicle.setGravity(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>