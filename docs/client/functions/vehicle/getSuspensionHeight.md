# Vehicle::getSuspensionHeight

Gets the height of the vehicle's suspension.

The higher the value the lower the suspension. 

Each 0.002 corresponds with one more level lowered.

0.000 is the stock suspension.0.008 is Ultra Suspension.



## Syntax

```js
vehicle.getSuspensionHeight();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>