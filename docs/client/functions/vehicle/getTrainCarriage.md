# Vehicle::getTrainCarriage

Corrected p1. it's basically the 'carriage/trailer number'. 

So if the train has 3 trailers you'd call the native once with a var or 3 times with 1, 2, 3.



## Syntax

```js
vehicle.getTrainCarriage(carriage);
```

## Example

```js
// todo
```

## Parameters

<b>carriage: <span style="color:#008017">Int</span></b>
No parameters here

<br />


## Return value

<b>Entity handle or object</b>