# Vehicle::setNumberPlateText

Sets a vehicle's license plate text. 8 chars maximum.

Example:
```cpp
Ped playerPed = PLAYER::PLAYER_Player_ID();
Vehicle veh = Player::GET_VEHICLE_Player_IS_USING(playerPed);
char *plateText = 'KING';
VEHICLE::SET_VEHICLE_NUMBER_PLATE_TEXT(veh, plateText);
```



## Syntax

```js
vehicle.setNumberPlateText(plateText);
```

## Example

```js
// todo
```
## Parameters

<b>plateText: <span style="color:#008017">String</span></b>

<br />


## Return value

<b>Undefined</b>