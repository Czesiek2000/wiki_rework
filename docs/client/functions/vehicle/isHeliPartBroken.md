# Vehicle::isHeliPartBroken

Returns if any of the heli parts selected as boolean arguments is broken (they pretty much work like bit flags).

**Note: If the tail is broken, the tail rotor is broken too!**

## Syntax

```js
vehicle.isHeliPartBroken(mainRotor, tailRotor, tail);
```

## Example

```js
const veh = mp.players.local.vehicle

if(veh.isHeliPartBroken(true, false, false)) { /* main rotor is broken */}
if(veh.isHeliPartBroken(false, true, false)) { /* tail rotor is broken */}
if(veh.isHeliPartBroken(false, false, true)) { /* the whole tail is ripped off */}

if(veh.isHeliPartBroken(true, true, false)) { /* any of the rotors are broken */}
if(veh.isHeliPartBroken(true, false, true)) { /* the main rotor or the whole tail is broken */}
if(veh.isHeliPartBroken(false, true, true)) { /* something related to the tail is broken */}

if(veh.isHeliPartBroken(true, true, true)) { /* something is broken, you just don't know what. */ }
```
## Parameters

<li><b>mainRotor: <span style="color:#008017">Boolean</span></b></li>
<li><b>tailRotor: <span style="color:#008017">Boolean</span></b></li>
<li><b>tail: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b> - if anything matching your selection is broken