# Vehicle::setRenderTrainAsDerailed

makes the train all jumbled up and derailed as it moves on the tracks (though that wont stop it from its normal operations)

## Syntax

```js
vehicle.setRenderTrainAsDerailed(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>