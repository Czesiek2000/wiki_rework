# Vehicle::getMod


Gets the mod currently applied on your vehicle in the targetted modType.

## Syntax

```js
vehicle.getMod(modType);
```

## Example

```js
// serverside code
mp.events.addCommand('spoiler', (player) => {
	if(!player.vehicle) return player.outputChatBox("You need to be in a vehicle to use this command.");
	let modIndex = player.vehicle.getMod(0);
	player.outputChatBox(`The mod index ID for your spoiler is ${modIndex}`);
});

```
## Parameters

<b>modType: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>modIndex: <span style="color:#008017">Int</span></b>