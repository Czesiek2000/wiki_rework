# Vehicle::setModColor1

### paintType:
* 0:Normal
* 1:Metallic
* 2:Pearl
* 3:Matte
* 4:Metal
* 5:Chrome

color: number of the color.

p3 seems to always be 0.



## Syntax

```js
vehicle.setModColor1(paintType, color, pearlescentColor);
```

## Example

```js
// todo
```
## Parameters

<li><b>paintType: <span style="color:#008017">Int</span></b></li>
<li><b>color: <span style="color:#008017">Int</span></b></li>
<li><b>pearlescentColor: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>