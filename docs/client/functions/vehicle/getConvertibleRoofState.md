# Vehicle::getConvertibleRoofState


Gets the current state of the roof of the convertible.


States
<ul><li>0 - Up</li>
<li>1 - Lowering Down</li>
<li>2 - Down</li>
<li>3 - Raising Up</li></ul>





## Syntax

```js
vehicle.getConvertibleRoofState();
```

## Example

Pressing the *F2* key will raise/lower your convertable roof

```js
mp.keys.bind(0x71, true, function() {
    let roofState =  mp.players.local.vehicle.getConvertibleRoofState();
    if(roofState === 0){
        mp.players.local.vehicle.lowerConvertibleRoof(false);
    } else if (roofState === 2){
        mp.players.local.vehicle.raiseConvertibleRoof(false);
    } else {
        mp.gui.chat.push("Please wait for the roof to stop changing.");
    }
});

```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>