# Vehicle::setTyreSmokeColor

Sets the tire smoke's color of this vehicle.

vehicle: The vehicle that is the target of this method.

r: The red level in the RGB color code.

g: The green level in the RGB color code.

b: The blue level in the RGB color code.

**Note:setting r,g,b to 0 will give the car independance day tyre smoke**



## Syntax

```js
vehicle.setTyreSmokeColor(r, g, b);
```

## Example

```js
// todo
```
## Parameters

<li><b>r: <span style="color:#008017">Int</span></b></li>
<li><b>g: <span style="color:#008017">Int</span></b></li>
<li><b>b: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>