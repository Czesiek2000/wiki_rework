# Vehicle::attachToTrailer


## Syntax

```js
vehicle.attachToTrailer(trailer, radius);
```

## Example

```js
// todo
```
## Parameters


<ul><li><b>trailer:</b> Vehicle handle or object</li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li></ul>

<br />


## Return value

<b>Undefined</b>