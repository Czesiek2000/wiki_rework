# Vehicle::removeWindow

windowIndex:
* 0: Front Right Window
* 1: Front Left Window
* 2: Back Right Window
* 3: Back Left Window-
* 1: Front Glass
* 7: Behind Glass 

the missing ones might be sun roof glass or other types of glassess, tried in a regular 4 door vehicle



## Syntax

```js
vehicle.removeWindow(windowIndex);
```

## Example

```js
// todo
```
## Parameters

<b>windowIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>