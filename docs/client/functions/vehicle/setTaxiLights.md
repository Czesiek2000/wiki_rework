# Vehicle::setTaxiLights

This is not tested - it's just an assumption.*Nac*

Doesn't seem to work. I'll try with an int instead. *JT*

Read the scripts, im dumpass.  
```cpp
if (!VEHICLE::IS_TAXI_LIGHT_ON(l_115)) {               
    VEHICLE::SET_TAXI_LIGHTS(l_115, 1); 
}
```


## Syntax

```js
vehicle.setTaxiLights(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>