# Vehicle::detachFromTrailer

Public Sub detatchTrailer(vh1 As Vehicle) 
```csharp
Native.Function.Call(Hash.DETACH_VEHICLE_FROM_TRAILER, vh1) 
```
End Sub



## Syntax

```js
vehicle.detachFromTrailer();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>