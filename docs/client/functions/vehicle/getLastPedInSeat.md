# Vehicle::getLastPedInSeat


## Syntax

```js
vehicle.getLastPedInSeat(seatIndex);
```

## Example

```js
// todo
```
## Parameters

<b>seatIndex<span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Ped handle or object</b>