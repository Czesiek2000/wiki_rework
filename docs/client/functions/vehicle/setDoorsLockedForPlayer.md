# Vehicle::setDoorsLockedForPlayer


## Syntax

```js
vehicle.setDoorsLockedForPlayer(player, toggle);
```

## Example

```js
// todo
```
## Parameters

<li><b>player: <span style="color:#008017">Player</span></b></li>
<li><b>toggle: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>