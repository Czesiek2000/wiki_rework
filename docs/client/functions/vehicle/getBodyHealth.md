# Vehicle::getBodyHealth

Seems related to vehicle health, like the one in IV.

**Max 1000, min 0**.

Vehicle does not necessarily explode or become undrivable at 0.



## Syntax

```js
vehicle.getBodyHealth();
```

## Example

```js
// todo
```
## Parameters

No parameters here

## Return value

<b><span style="color:#008017">Float</span></b>