# Vehicle::setBodyHealth

p2 often set to *1000.0* in the decompiled scripts.

## Syntax

```js
vehicle.setBodyHealth(value);
```

## Example

```js
// todo
```
## Parameters

<b>value: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>