# Vehicle::getNumberOfPassengers

Gets the number of passengers, NOT including the driver. 

Use `IS_VEHICLE_SEAT_FREE(Vehicle, -1)` to also check for the driver.



## Syntax

```js
vehicle.getNumberOfPassengers();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>