# Vehicle::rollDownWindow

Windows :

<ul><li>0 = Front Left Window</li>
<li>1 = Front Right Window</li>
<li>2 = Back Left Window</li>
<li>3 = Back Right Window</li></ul>


## Syntax

```js
vehicle.rollDownWindow(windowIndex);
```

## Example

```js
// todo
```
## Parameters

<b>windowIndex: <span style="color:#008017">Int(Windows)</span></b>

<br />


## Return value

No return value