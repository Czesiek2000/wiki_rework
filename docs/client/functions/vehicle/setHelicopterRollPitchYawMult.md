# Vehicle::setHelicopterRollPitchYawMult

value between *0.0* and *1.0*



## Syntax

```js
vehicle.setHelicopterRollPitchYawMult(multiplier);
```

## Example

```js
// todo
```
## Parameters

<b>multiplayer: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>