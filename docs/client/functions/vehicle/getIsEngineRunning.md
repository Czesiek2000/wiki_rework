# Vehicle::getIsEngineRunning

Returns `true` if the engine is on, returns `false` if the engine is off.

Returns null while entering/exiting vehicle.



## Syntax

```js
vehicle.getIsEngineRunning();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Number</span>0 or 1</b>