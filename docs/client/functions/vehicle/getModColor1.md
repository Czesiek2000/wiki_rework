# Vehicle::getModColor1


## Syntax

```js
vehicle.getModColor1(paintType, color, pearlescentColor);
```

## Example

```js
// todo
```
## Parameters

<b>paintType: <span style="color:#008017">int</span></b>
<b>color: <span style="color:#008017">int</span></b>
<b>pearlescentColor: <span style="color:#008017">int</span></b>

<br />


## Return value

<b>paintType, color, pearlescentColor<span style="color:#008017">object</span></b>