# Vehicle::setDeformationFixed

If the vehicle has been hit and it has some deformations this function does some smoothing on vehicle's body



## Syntax

```js
vehicle.setDeformationFixed();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>