# Vehicle::getCauseOfDestruction

```cpp
iVar3 = get_vehicle_cause_of_destruction(uLocal_248[iVar2]);
if (iVar3 == joaat('weapon_stickybomb')){ 
    func_171(726); iLocal_260 = 1;
}
```


## Syntax

```js
vehicle.getCauseOfDestruction();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b>Model hash or name</b>