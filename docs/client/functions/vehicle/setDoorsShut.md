# Vehicle::setDoorsShut

Closes all doors of a vehicle:



## Syntax

```js
vehicle.setDoorsShut(closeInstantly);
```

## Example

```js
// todo
```
## Parameters

<b>closeInstantly: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>