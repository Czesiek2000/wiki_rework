# Vehicle::trackVisibility

in script hook .net 

```csharp
Vehicle v = ...;
Function.Call(Hash.TRACK_VEHICLE_VISIBILITY, v.Handle);
```

## Syntax

```js
vehicle.trackVisibility();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>