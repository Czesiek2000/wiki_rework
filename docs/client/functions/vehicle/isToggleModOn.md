# Vehicle::isToggleModOn


## Syntax

```js
vehicle.isToggleModOn(modType);
```

## Example

```js
// todo
```
## Parameters

<b>modType: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>