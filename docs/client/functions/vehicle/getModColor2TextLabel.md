# Vehicle::getModColor2TextLabel

returns a string which is the codename of the vehicle's currently selected secondary color



## Syntax

```js
vehicle.getModColor2TextLabel();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">String</span></b>