# Vehicle::getPetrolTankHealth

1000 is max health.

Begins leaking gas at around 650 health-999.

90002441406 appears to be minimum health, although nothing special occurs



## Syntax

```js
vehicle.getPetrolTankHealth();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>