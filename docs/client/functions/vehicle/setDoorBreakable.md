# Vehicle::setDoorBreakable

Keeps Vehicle Doors/Hood/Trunk from breaking off



## Syntax

```js
vehicle.setDoorBreakable(doorIndex, isBreakable);
```

## Example

```js
// todo
```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>
<b>isBreakable: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>