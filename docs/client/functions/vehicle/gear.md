# Vehicle::gear

Property-only


This property is used to get the vehicle's gear



## Syntax

```js
let gear = vehicle.gear;
```

## Example

No example here

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>