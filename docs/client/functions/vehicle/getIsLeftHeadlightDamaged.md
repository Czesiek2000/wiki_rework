# Vehicle::getIsLeftHeadlightDamaged

From the driver's perspective, is the left headlight broken.



## Syntax

```js
vehicle.getIsLeftHeadlightDamaged();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>