# Vehicle::setIsWanted

Sets the wanted state of this vehicle.

## Syntax

```js
vehicle.setIsWanted(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>