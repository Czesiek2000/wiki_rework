# Vehicle::wasCounterActivated

Hash collision



## Syntax

```js
vehicle.wasCounterActivated(p1);
```

## Example

```js
// todo
```
## Parameters

No parameters here
<b>p1: </b> unknown (to be checked)

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>