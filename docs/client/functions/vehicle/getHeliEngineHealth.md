# Vehicle::getHeliEngineHealth

Max 1000.

At -100 both helicopter rotors will stall.



## Syntax

```js
vehicle.getHeliEngineHealth();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>