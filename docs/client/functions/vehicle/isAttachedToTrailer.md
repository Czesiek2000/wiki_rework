# Vehicle::isAttachedToTrailer

Public Function isVehicleAttachedToTrailer(vh As Vehicle) As Boolean 

```csharp
Return Native.Function.Call(Of Boolean)(Hash.IS_VEHICLE_ATTACHED_TO_TRAILER, vh)
```
End Function


## Syntax

```js
vehicle.isAttachedToTrailer();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value
<b><span style="color:#008017">Boolean</span></b>