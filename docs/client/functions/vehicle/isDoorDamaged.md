# Vehicle::isDoorDamaged

doorID starts at 0, not seeming to skip any numbers. 

Four door vehicles intuitively range from 0 to 3.



## Syntax

```js
vehicle.isDoorDamaged(doorID);
```

## Example

```js
// todo
```
## Parameters

<b>doorId: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>