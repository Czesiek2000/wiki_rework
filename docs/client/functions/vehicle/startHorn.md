# Vehicle::startHorn

Sounds the horn for the specified vehicle.

vehicle: The vehicle to activate the horn for.

mode: The hash of 'NORMAL' or 'HELDDOWN'. Can be 0.

duration: The duration to sound the horn, in milliseconds.

**Note: If a player is in the vehicle, it will only sound briefly.**



## Syntax

```js
vehicle.startHorn(duration, mode, forever);
```

## Example

```js
// todo
```
## Parameters

<li>duration: <b><span style="color:#008017">Int</span></b></li>
<li>mode: <b>Model hash or name</li>
<li>forever: <b><span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>