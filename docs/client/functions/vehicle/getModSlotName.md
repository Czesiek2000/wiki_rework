# Vehicle::getModSlotName

Returns the name for the type of vehicle mod(Armour, engine etc)

Seems like it returns undefined for upgrades (noBrain)



## Syntax

```js
vehicle.getModSlotName(modType);
```

## Example

```js
// todo
```
## Parameters

<b>modType: <span style="color:#008017">int</span></b>

<br />


## Return value

<b><span style="color:#008017">String</span></b>
