# Vehicle::isSirenOn

Return true if siren (or flashlights) are enabled, false if not



## Syntax

```js
vehicle.isSirenOn();
```

## Example

```js
var MyCarSiren = false

setInterval(() => {
    if(mp.players.local.vehicle.isSirenOn() != MyCarSiren){
        MyCarSiren = mp.players.local.vehicle.isSirenOn()

        // Here you can do an action when user toggle ON/OFF sirens

        // You can use this to sync siren between users

    }
}, 1000)
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>