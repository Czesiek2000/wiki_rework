# Vehicle::setDoorControl

doorIndex:
* 0: Front Left Door (driver door)
* 1: Front Right Door
* 2: Back Left Door
* 3: Back Right Door
* 4: Hood
* 5: Trunk
* 6: Trunk2

p2:mostly use 0 and 1, very rare using 3 and 5

p3:It seems it is an angle

Example in VB:  
```vb
Public Shared Sub Set_Vehicle_Door_Angle(Vehicle As Vehicle, Door As VehicleDoor, Angle As Single) 
Native.Function.Call(Hash.SET_VEHICLE_DOOR_CONTROL, Vehicle.Handle, Door, 1, Angle) 
End Sub
```
I'm Not MentaLsfink: p2 is speed, 5 is fast, 1 is slow 3 is medium


## Syntax

```js
vehicle.setDoorControl(doorIndex, p2, angle);
```

## Example

```js
// todo
```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>
<b>p2: <span style="color:#008017">Int</span></b>
<b>angle: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>