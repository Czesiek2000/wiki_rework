# Vehicle::setPaintFade

formerly known as `_SET_VEHICLE_PAINT_FADE`

The parameter fade is a value from 0-1, where 0 is fresh paint.

The actual value isn't stored as a float but as an unsigned char (BYTE). More info [here](https://pastebin.com/r0h6EM5s)



## Syntax

```js
vehicle.setPaintFade(fade);
```

## Example

```js
// todo
```
## Parameters

<b>fade: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>