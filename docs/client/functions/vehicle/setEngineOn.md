# Vehicle::setEngineOn





Toggles the vehicle engine






## Syntax

```js
vehicle.setEngineOn(toggle, instantly, otherwise);
```

## Example

```js
mp.events.add('playerStartEnterVehicle', (vehicle, seat) => { 
    if (!vehicle.getIsEngineRunning()) {
        vehicle.setEngineOn(false, false, true); // Turns Engine off once enter the vehicle to prevent player from toggling the engine manually
    }
});

```
## Parameters

<ul><li><span style="font-weight:bold; color:red;">*</span><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><span style="font-weight:bold; color:red;">*</span><b>instantly:</b> <b><span style="color:#008017">Boolean</span></b> (Turns the engine instantly)</li>
<li><span style="font-weight:bold; color:red;">*</span><b>otherwise:</b> <b><span style="color:#008017">Boolean</span></b> (Prevents player from manually toggling the engine)</li></ul>

<br />


## Return value

<b>void</b>