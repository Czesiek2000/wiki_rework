# Vehicle::setFullbeam

It switch to highbeam when p1 is set to true.



## Syntax

```js
vehicle.setFullbeam(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>