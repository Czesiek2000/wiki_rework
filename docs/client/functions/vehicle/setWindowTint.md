# Vehicle::setWindowTint


### Window Tints:
<ul><li>WINDOWTINT_NONE(0)</li>
<li>WINDOWTINT_PURE_BLACK(1)</li>
<li>WINDOWTINT_DARKSMOKE(2)</li>
<li>WINDOWTINT_LIGHTSMOKE(3)</li>
<li>WINDOWTINT_LIMO(4?)</li>
<li>WINDOWTINT_STOCK(4?)</li>
<li>WINDOWTINT_GREEN(5)</li></ul>

0 and 4 are different but only have a **VERY** slight change, barely noticable.



## Syntax

```js
vehicle.setWindowTint(tint);
```

## Example

```js
// todo
```
## Parameters

<b>tint: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>