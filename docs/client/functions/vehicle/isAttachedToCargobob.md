# Vehicle::isAttachedToCargobob


## Syntax

```js
vehicle.isAttachedToCargobob(vehicleAttached);
```

## Example

```js
// todo
```
## Parameters

<li><b>vehicleAttached:</b> Vehicle handle or object</li>
<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>