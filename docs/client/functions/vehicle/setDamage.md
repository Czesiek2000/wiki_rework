# Vehicle::setDamage

Apply damage to vehicle at a location. Location is relative to vehicle model (not world).

Radius of effect damage applied in a sphere at impact location.

It is enough to use x,y,z values from -1 to 1.

p6 should be true, with false it does not taking in account offset values and all damage is applied to rear left part of the vehicle.

Good values for radius could be from 25 to 200, for damage 50-200.

You need to apply delay if you want to call setDamage couple times in a row. Typically 10ms is enough.



## Syntax

```js
vehicle.setDamage(xOffset, yOffset, zOffset, damage, radius, p6);
```

## Example

```js
// todo
```
## Parameters

<li><b>xOffset<span style="color:#008017">Float</span></b></li>
<li><b>yOffset<span style="color:#008017">Float</span></b></li>
<li><b>zOffset<span style="color:#008017">Float</span></b></li>
<li><b>damage<span style="color:#008017">Float</span></b></li>
<li><b>radius<span style="color:#008017">Float</span></b></li>
<li><b>p6: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>