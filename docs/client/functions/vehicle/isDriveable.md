# Vehicle::isDriveable

p1 is always 0 in the scripts.



## Syntax

```js
vehicle.isDriveable(p1);
```

## Example

```js
// todo
```
## Parameters

<b>p1: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>