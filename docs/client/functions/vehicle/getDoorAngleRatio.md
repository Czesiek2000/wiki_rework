# Vehicle::getDoorAngleRatio


## Syntax

```js
vehicle.getDoorAngleRatio(door);
```

## Example

```js
// todo
```
## Parameters

<b>door: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Float</span></b>