# Vehicle::getOwner

The resulting entity can be a Vehicle or player.

The native is stored between `GET_VEHICLE_LIVERY_COUNT` and `GET_VEHICLE_MAX_BRAKING` so the actual name is either `GET_VEHICLE_L*` or `GET_VEHICLE_M*`



## Syntax

```js
vehicle.getOwner(entity);
```

## Example

```js
// todo
```
## Parameters

<li><b>entity:</b> Entity handle or object</li>
<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>