# Vehicle::getCustomPrimaryColour


## Syntax

```js
vehicle.getCustomPrimaryColour(r, g, b);
```

## Example

```js
mp.events.add("playerEnterVehicle", (vehicle, seat) => { // Getting RED Color of RGB when player Enter vehicle
  let rgb = vehicle.getCustomPrimaryColour(0, 0, 0); // 
  mp.gui.chat.push("Red color is: " + rgb.r); // rgb.r = from 0 to 255 depends on vehicle's color
});
```
## Parameters

<li>r: <b><span style="color:#008017">Int</span></b></li>
<li>g: <b><span style="color:#008017">Int</span></b></li>
<li>b: <b><span style="color:#008017">Int</span></b></li>

<br />


## Return value

<li>r,g,b: <b><span style="color:#008017">object</span></b></li>