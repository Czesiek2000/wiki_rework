# Vehicle::setHeliBladesSpeed

Sets the speed of the helicopter blades in percentage of the full speed.

vehicleHandle: The helicopter.

speed: The speed in percentage, 0.0f being 0% and 1.0f being 100%.



## Syntax

```js
vehicle.setHeliBladesSpeed(speed);
```

## Example

```js
// todo
```
## Parameters

<b>speed: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>