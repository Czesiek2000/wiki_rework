# Vehicle::isDoorFullyOpen

### Doors
<ul><li>0 = Front Left Door</li>
<li>1 = Front Right Door</li>
<li>2 = Back Left Door</li>
<li>3 = Back Right Door</li>
<li>4 = Hood</li>
<li>5 = Trunk</li>
<li>6 = Trunk2</li></ul>

## Syntax

```js
vehicle.isDoorFullyOpen(doorIndex);
```

## Example

```js
let vehicle = mp.players.local.vehicle;

if (vehicle.isDoorFullyOpen(1)) { // If the Front left door is fully open
vehicle.setDoorShut(1, false); // Close it gently.
}
```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>