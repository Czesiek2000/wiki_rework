# Vehicle::isStuckTimerUp

p1 can be anywhere from 0 to 3 in the scripts. 

p2 is generally somewhere in the 1000 to 10000 range.



## Syntax

```js
vehicle.isStuckTimerUp(p1, p2);
```

## Example

```js
// todo
```
## Parameters

<b>p1<span style="color:#008017">Int</span></b>
<b>p2<span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>