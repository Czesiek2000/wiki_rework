# Vehicle::setIndicatorLights

Sets the turn signal enabled for a vehicle.

Set turnSignal to 1 for left light, 0 for right light.

## Syntax

```js
vehicle.setIndicatorLights(turnSignal, toggle);
```

## Example

```js
// todo
```
## Parameters

<b>turnSignal: <span style="color:#008017">Int</span></b>
<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>