# Vehicle::setIsConsideredByPlayer

Setting this to false, makes the specified vehicle to where if you press Y your character doesn't even attempt the animation to enter the vehicle. 

Hence it's not considered aka ignored.



## Syntax

```js
vehicle.setIsConsideredByPlayer(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>