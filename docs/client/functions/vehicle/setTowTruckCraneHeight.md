# Vehicle::setTowTruckCraneHeight

Sets how much the crane on the tow truck is raised, where 0.0 is fully lowered and 1.0 is fully raised.

## Syntax

```js
vehicle.setTowTruckCraneHeight(height);
```

## Example

```js
// todo
```
## Parameters

<b>height: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>