# Vehicle::getCargobobHookPosition

Gets the position of the cargobob hook, in world coords.



## Syntax

```js
vehicle.getCargobobHookPosition();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Vector3</span></b>