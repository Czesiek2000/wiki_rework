# Vehicle::retractCargobobHook

Retracts the hook on the cargobob.

**Note: after you retract it the natives for dropping the hook no longer work**



## Syntax

```js
vehicle.retractCargobobHook();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>