# Vehicle::isDamaged

Appears to return true if the vehicle has any damage, including cosmetically.



## Syntax

```js
vehicle.isDamaged();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>