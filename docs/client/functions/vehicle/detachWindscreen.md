# Vehicle::detachWindscreen

Detaches the vehicle's windscreen.For further information, see : [gtaforums](htps://gtaforums.com/topic/859570-glass/#entry1068894566)



## Syntax

```js
vehicle.detachWindscreen();
```

## Example

```js
// todo
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>