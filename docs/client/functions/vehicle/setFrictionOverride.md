# Vehicle::setFrictionOverride

In the decompiled scripts are 2 occurencies of this method. 

One is in the 'country_race', setting the friction to 2, whereas in the 'trevor1'-file there are 4 values: `0.3`, `0.35`, `0.4` and `0.65`.

I couldn't really figure out any difference, but you might succeed. 

In that case, please edit this.

*JulioNIB*: Seems to be related to the metal parts, not tyres (like i was expecting lol)



## Syntax

```js
vehicle.setFrictionOverride(friction);
```

## Example

```js
// todo
```
## Parameters

<b>friction: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>