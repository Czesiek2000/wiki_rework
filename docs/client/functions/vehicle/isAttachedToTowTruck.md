# Vehicle::isAttachedToTowTruck

**MulleDK19**: First two parameters swapplayer. Scripts verify that towTruck is the first parameter, not the second.



## Syntax

```js
vehicle.isAttachedToTowTruck(vehicle);
```

## Example

```js
// todo
```
## Parameters

<li><b>vehicleAttached:</b> Vehicle handle or object</li>
<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>