# Vehicle::setHandbrake

does this work while in air?

## Syntax

```js
vehicle.setHandbrake(toggle);
```

## Example

```js
mp.players.local.vehicle.setHandbrake(true); // brakes the vehicle the player is sitting in
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>