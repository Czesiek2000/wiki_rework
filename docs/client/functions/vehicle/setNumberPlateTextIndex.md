# Vehicle::setNumberPlateTextIndex

Plates:
* Blue/White - 0
* Yellow/black - 1
* Yellow/Blue - 2
* Blue/White2 - 3
* Blue/White3 - 4
* Yankton - 5



## Syntax

```js
vehicle.setNumberPlateTextIndex(plateIndex);
```

## Example

```js
// todo
```
## Parameters

<b>plateIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>