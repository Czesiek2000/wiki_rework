# Vehicle::rollDownWindows

Roll down all the windows of the vehicle passed through the first parameter.-UNBOUND-



## Syntax

```js
vehicle.rollDownWindows();
```

## Example

```js
// todo
```
<b><span style="color:#008017">Float</span></b>
## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>