# Vehicle::setColours

colorPrimary & colorSecondary are the paint index for the vehicle.
For a list of valid paint indexes, view: [pastebin](pastebin.com/pwHci0xK)

Use this to get the number of color indices: [pastebin](pastebin.com/RQEeqTSM)

**Note: minimum color index is 0, maximum color index is (numColorIndices - 1)**



## Syntax

```js
vehicle.setColours(colorPrimary, colorSecondary);
```

## Example

```js
// todo
```
## Parameters

<b>colorPrimary: <span style="color:#008017">Int</span></b>
<b>colorSecondary<span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>