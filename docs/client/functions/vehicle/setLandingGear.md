# Vehicle::setLandingGear

Works for vehicles with a retractable landing gear

landing gear states:
* 0: Deployed
* 1: Closing
* 2: Opening
* 3: Retracted



## Syntax

```js
vehicle.setLandingGear(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>