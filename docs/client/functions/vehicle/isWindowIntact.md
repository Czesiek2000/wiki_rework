# Vehicle::isWindowIntact


## Syntax

```js
vehicle.isWindowIntact(windowIndex);
```

## Example

```js
// todo
```
## Parameters

<b>windowIndex: <span style="color:#008017">int</span></b>

<br />


## Return value

<b><span style="color:#008017">Float</span></b>