# Vehicle::setCustomPrimaryColour

p1, p2, p3 are RGB values for color (255,0,0 for Red, ect)



## Syntax

```js
vehicle.setCustomPrimaryColour(r, g, b);
```

## Example

```js
// todo
```
## Parameters

<li><b>r: <span style="color:#008017">Int</span></b></li>
<li><b>g: <span style="color:#008017">Int</span></b></li>
<li><b>b: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>