# Vehicle::isModel


## Syntax

```js
vehicle.isModel(model);
```

## Example

```js
// todo
```
## Parameters

<b>model: Model hash or name</b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>