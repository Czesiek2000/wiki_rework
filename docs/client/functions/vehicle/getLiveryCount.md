# Vehicle::getLiveryCount

Returns -1 if the vehicle has no livery



## Syntax

```js
vehicle.getLiveryCount();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Int</span></b>