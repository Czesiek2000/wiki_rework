# Vehicle::setExclusiveDriver


## Syntax

```js
vehicle.setExclusiveDriver(ped, p2);
```

## Example

```js
// todo
```
## Parameters

<li><b>ped: <span style="color:#008017">Ped handle or object</span></b></li>
<li><b>p2: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>