# Vehicle::setDisablePetrolTankDamage


## Syntax

```js
vehicle.setDisablePetrolTankDamage(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>