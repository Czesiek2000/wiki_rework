# Vehicle::isCargobobHookActive

Returns true only when the hook is active, will return false if the magnet is active



## Syntax

```js
vehicle.isCargobobHookActive();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>