# Vehicle::isOnAllWheels

```vb
Public Function isVehicleOnAllWheels(vh As Vehicle) As Boolean        

Return Native.Function.Call(Of Boolean)(Hash.IS_VEHICLE_ON_ALL_WHEELS, vh)

End Function
```


## Syntax

```js
vehicle.isOnAllWheels();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>