# Vehicle::setDoorLatched


## Syntax

```js
vehicle.setDoorLatched(doorIndex, p2, p3, p4);
```

## Example

```js
// todo
```
## Parameters

<li><b>doorIndex: <span style="color:#008017">Int</span></b></li>
<li><b>p2: <span style="color:#008017">Boolean</span></b></li>
<li><b>p3: <span style="color:#008017">Boolean</span></b></li>
<li><b>p4: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>