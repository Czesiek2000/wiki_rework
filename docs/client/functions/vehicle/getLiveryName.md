# Vehicle::getLiveryName

`Second Param = LiveryIndex`
example: 
```cpp
int count = VEHICLE::GET_VEHICLE_LIVERY_COUNT(veh);
for (int i = 0; i < count; i++)  { 
    char* LiveryName = VEHICLE::GET_LIVERY_NAME(veh, i); 
}
```
this example will work fine to fetch all names 

for example for Sanchez we get 
* SANC_LV1
* SANC_LV2
* SANC_LV3
* SANC_LV4
* SANC_LV5

*Edited By KiLLerBoy_001*

Using `_GET_LABEL_TEXT`, you can get the localized livery name.



## Syntax

```js
vehicle.getLiveryName(liveryIndex);
```

## Example

```js
// todo
```
## Parameters

<b>livery: <span style="color:#008017">Int</span></b>
<br />


## Return value

<b><span style="color:#008017">String</span></b>