# Vehicle::toggleMod

Toggles:

* UNK17 - 17
* Turbo - 18
* UNK19 - 19
* Tire Smoke - 20
* UNK21 - 21
* Xenon Headlights - 22



## Syntax

```js
vehicle.toggleMod(modType, toggle);
```

## Example

```js
// todo
```
## Parameters

<li>modType: <b><span style="color:#008017">Int</span></b></li>
<li>toggle: <b><span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>