# Vehicle::getModColor2


## Syntax

```js
vehicle.getModColor2(paintType, color);
```

## Example

```js
// todo
```
## Parameters

<li>paintType: <b><span style="color:#008017">Int</span></b></li>
<li>color: <b><span style="color:#008017">Int</span></b></li>

<br />


## Return value

<li>paintType, color: <b><span style="color:#008017">object</span></b></li>