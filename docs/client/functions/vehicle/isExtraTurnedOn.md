# Vehicle::isExtraTurnedOn


## Syntax

```js
vehicle.isExtraTurnedOn(extraId);
```

## Example

```js
// todo
```
<b>extraId: <span style="color:#008017">Int</span></b>
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>