# Vehicle::steerUnlockBias

This seems like a hash collision?

p1 (toggle) was always 1 (true) except in one case in the *b678* scripts.



## Syntax

```js
vehicle.steerUnlockBias(toggle);
```

## Example

```js
// todo
```
## Parameters

toggle: <b><span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>