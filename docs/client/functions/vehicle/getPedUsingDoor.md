# Vehicle::getPedUsingDoor

*MulleDK19*: Quick disassembly and test seems to indicate that this native gets the Ped currently using the specified door.



## Syntax

```js
vehicle.getPedUsingDoor(doorIndex);
```

## Example

```js
// todo
```
## Parameters

<b>doorIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Ped handle or object</b>