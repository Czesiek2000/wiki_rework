# Vehicle::enableCargobobHook

Drops the Hook/Magnet on a cargobobstate
```cpp
enum eCargobobHook { 
    CARGOBOB_HOOK = 0,
    CARGOBOB_MAGNET = 1
};
```


## Syntax

```js
vehicle.enableCargobobHook(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>