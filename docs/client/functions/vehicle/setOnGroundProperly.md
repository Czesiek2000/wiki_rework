# Vehicle::setOnGroundProperly

Sets a vehicle on the ground on all wheels. 

Returns whether or not the operation was successful.

*sfink*: This has an additional param `(Vehicle vehicle, float p1)` which is always set to `5.0f` in the b944 scripts.



## Syntax

```js
vehicle.setOnGroundProperly();
```

## Example

```js
mp.events.add('setVehiclePosition', (x, y, z, a, b, c) => {
    mp.players.local.vehicle.position = new mp.Vector3(x, y, z);
    mp.players.local.vehicle.rotation = new mp.Vector3(a, b, c);
    mp.players.local.vehicle.setOnGroundProperly();
});
```

## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>