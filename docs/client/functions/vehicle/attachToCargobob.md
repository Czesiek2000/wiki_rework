# Vehicle::attachToCargobob


## Syntax

```js
vehicle.attachToCargobob(cargobob, p2, x, y, z);
```

## Example

```js
// todo
```

## Parameters

<ul><li><b>cargobob:</b> Vehicle handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li></ul>

<br />


## Return value

<b>Undefined</b>