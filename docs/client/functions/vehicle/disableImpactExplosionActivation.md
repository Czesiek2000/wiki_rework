# Vehicle::disableImpactExplosionActivation

if set to true, prevents vehicle sirens from having sound, leaving only the lights.

HASH COLLISION !!! Please change to _SET_VEHICLE_SIREN_SOUND

In update function name was changed to:

```js
vehicle.setSirenSound(toggle);
```


## Syntax

```js
vehicle.disableImpactExplosionActivation(toggle);
```

## Example
```js
// todo
```

## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>