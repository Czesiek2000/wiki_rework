# Vehicle::doesExtraExist

Checks via *CVehicleModelInfo*



## Syntax

```js
vehicle.doesExtraExist(extraId);
```

## Example

```js
// todo
```

## Parameters

<b>extraId: <span style="color:#008017">Int</span></b>

## Return value

<b>Undefined</b>