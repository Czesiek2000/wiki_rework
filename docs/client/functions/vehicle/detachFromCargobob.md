# Vehicle::detachFromCargobob


## Syntax

```js
vehicle.detachFromCargobob(cargobob);
```

## Example

```js
// todo
```

## Parameters

<li><b>cargobob:</b> Vehicle handle or object</li>

<br />


## Return value

<b>Undefined</b>