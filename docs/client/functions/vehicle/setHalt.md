# Vehicle::setHalt

This native makes the vehicle stop immediately, as happens when we enter a MP garage.

distance defines how far it will travel until stopping. Garage doors use 3.0.

killEngine is the time in seconds that the engine will be off for, it must be set to at least 1 for the vehicle to stop. 

This looks like is a bitmapped integer.



## Syntax

```js
vehicle.setHalt(distance, killEngine, unknown);
```

## Example

```js
// todo
```
## Parameters

<b>distance: <span style="color:#008017">Float</span></b>
<b>killEngine: <span style="color:#008017">Int</span></b>
<b>unkown: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>