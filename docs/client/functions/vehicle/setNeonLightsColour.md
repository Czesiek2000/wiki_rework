# Vehicle::setNeonLightsColour

Sets the color of the neon lights of the specified vehicle.
[More info](https://pastebin.com/G49gqy8b)



## Syntax

```js
vehicle.setNeonLightsColour(r, g, b);
```

## Example

```js
// todo
```
## Parameters

No parameters here
<li><b>r: <span style="color:#008017">Int</span></b></li>
<li><b>g: <span style="color:#008017">Int</span></b></li>
<li><b>b: <span style="color:#008017">Int</span></b></li>

<br />


## Return value

<b>Undefined</b>