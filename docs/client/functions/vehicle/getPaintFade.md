# Vehicle::getPaintFade

Formerly known as `_GET_VEHICLE_PAINT_FADE`.

The result is a value from 0-1, where 0 is fresh paint.

The actual value isn't stored as a float but as an unsigned char (BYTE).



## Syntax

```js
vehicle.getPaintFade();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>