# Vehicle::lowerConvertibleRoof

Lowers the roof of a convertable, can be done instantly or with the animation.





## Syntax

```js
vehicle.lowerConvertibleRoof(instantlyLower);
```

## Example

Pressing the *F2* key will raise/lower your convertable roof

```js
mp.keys.bind(0x71, true, function() {
    let roofState =  mp.players.local.vehicle.getConvertibleRoofState();
    if(roofState === 0){
        mp.players.local.vehicle.lowerConvertibleRoof(false);
    } else if (roofState === 2){
        mp.players.local.vehicle.raiseConvertibleRoof(false);
    } else {
        mp.gui.chat.push("Please wait for the roof to stop changing.");
    }
});

```
## Parameters

<b>instantlyLower<span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>