# Vehicle::setTyresCanBurst

Allows you to toggle bulletproof tires.

## Syntax

```js
vehicle.setTyresCanBurst(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>