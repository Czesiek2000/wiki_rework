# Vehicle::setHandling

You can find all list of the types [here](https://forums.gta5-mods.com/topic/3842/tutorial-handling-meta)

**Note**
setHandling does not use the exact values from handling.meta for certain fields, these are as follows:



## Syntax

```js
vehicle.setHandling(type, value);
```

## Example

```js
if (mp.players.local.vehicle) {
  mp.players.local.vehicle.setHandling("fMass", 3500);
  mp.players.local.vehicle.setHandling("fInitialDragCoeff", 4.77);
}
```

## Parameters

<ul><li><b>fInitialDriveMaxFlatVel</b> - handling.meta value / 3.6</li>
<li><b>fBrakeBiasFront</b> - handling.meta value * 2</li>
<li><b>fSteeringLock</b> - handling.meta value * 0.017453292</li>
<li><b>fTractionCurveLateral</b> - handling.meta value * 0.017453292</li>
<li><b>fTractionBiasFront</b> - handling.meta value * 2</li>
<li><b>fSuspensionCompDamp</b> - handling.meta value / 10</li>
<li><b>fSuspensionReboundDamp</b> - handling.meta value / 10</li>
<li><b>fSuspensionBiasFront</b> - handling.meta value * 2</li>
<li><b>fAntiRollBarBiasFront</b> - handling.meta value * 2</li></ul>

<br />


## Return value

<b>Undefined</b>