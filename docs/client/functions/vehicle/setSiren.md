# Vehicle::setSiren

Activate siren on vehicle (Only works if the vehicle has a siren).

## Syntax

```js
vehicle.setSiren(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>