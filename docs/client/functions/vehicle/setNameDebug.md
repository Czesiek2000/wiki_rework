# Vehicle::setNameDebug

**NOTE: Debugging functions are not present in the retail version of the game.**



## Syntax

```js
vehicle.setNameDebug(name);
```

## Example

```js
// todo
```
## Parameters

<b>name: <span style="color:#008017">String</span></b>

<br />


## Return value

<b>Undefined</b>