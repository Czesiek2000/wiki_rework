# Vehicle::setDirtLevel

You can't use values greater than 15.0


(R* does (float)(rand() % 15) to get a random dirt level when generating a vehicle)



## Syntax

```js
vehicle.setDirtLevel(dirtLevel);
```

## Example

```js
// This is an example for sync dirt level.
mp.events.add('entityStreamIn', (entity) => {
    if (entity.type !== 'vehicle' || !entity.hasVariable('dirtLevel')) {
        return;
    }

    const dirtLevel = entity.getVariable('dirtLevel') || 0;
    entity.setDirtLevel(parseFloat(dirtLevel));
});
```
## Parameters

<b>dirtLevel: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>