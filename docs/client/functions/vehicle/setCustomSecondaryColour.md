# Vehicle::setCustomSecondaryColour

p1, p2, p3 are RGB values for color (255,0,0 for Red, ect)



## Syntax

```js
vehicle.setCustomSecondaryColour(r, g, b);
```

## Example

```js
// todo
```
## Parameters

<b>r: <span style="color:#008017">Int</span></b>
<b>g: <span style="color:#008017">Int</span></b>
<b>b: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>Undefined</b>