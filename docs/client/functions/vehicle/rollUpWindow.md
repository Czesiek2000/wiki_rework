# Vehicle::rollUpWindow


* 0: Front Left Window
* 1: Front Right Window
* 2: Back Left Window
* 3: Back Right Window



## Syntax

```js
vehicle.rollUpWindow(windowIndex);
```

## Example

```js
// todo
```
## Parameters

<b>windowIndex: <span style="color:#008017">Int</span></b>

<br />


## Return value

<b>unknown(to be checked)</b>