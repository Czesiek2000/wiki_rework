# Vehicle::setMod


Applies the specified mod onto the vehicle.












## Syntax

```js
vehicle.setMod(modType, modIndex);

```

## Example

```js
mp.events.addCommand('mod', (player, _, modType , modIndex) => {
	if(!player.vehicle) return player.outputChatBox("You need to be in a vehicle to use this command.");
	player.vehicle.setMod(parseInt(modType), parseInt(modIndex));
	player.outputChatBox(`Mod Type ${modType} with Mod Index ${modIndex} applied.`);
});

```
<b><span style="color:#008017">Float</span></b>
## Parameters

No parameters here

<br />


## Return value

<b>Undefined</b>