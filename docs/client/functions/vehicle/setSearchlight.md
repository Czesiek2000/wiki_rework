# Vehicle::setSearchlight

Only works during nighttime.

## Syntax

```js
vehicle.setSearchlight(toggle, canBeUsedByAI);
```

## Example

```js
// todo
```
## Parameters

<li><b>toggle: <span style="color:#008017">Boolean</span></b></li>
<li><b>canBeUsedByAI: <span style="color:#008017">Boolean</span></b></li>

<br />


## Return value

<b>Undefined</b>