# Vehicle::setLightMultiplier

multiplier = brightness of head lights.

this value isn't capped afaik.

multiplier = 0.0 no lights

multiplier = 1.0 default game value



## Syntax

```js
vehicle.setLightMultiplier(multiplier);
```

## Example

```js
// todo
```
## Parameters

<b>multiplayer: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>