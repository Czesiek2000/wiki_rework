# Vehicle::getHeliMainRotorHealth

Max 1000.

At 0 the main rotor will stall.

## Syntax

```js
vehicle.getHeliMainRotorHealth();
```

## Example

```js
// todo
```
## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Float</span></b>