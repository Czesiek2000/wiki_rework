# Vehicle::detachFromTowTruck

First two parameters swapplayer. Scripts verify that towTruck is the first parameter, not the second.

## Syntax

```js
vehicle.detachFromTowTruck(vehicle);
```

## Example

```js
// todo
```

## Parameters

<li><b>cargobob:</b> Vehicle handle or object</li>

<br />


## Return value

<b>Undefined</b>