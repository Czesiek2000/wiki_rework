# Vehicle::setEnginePowerMultiplier

Vehicle power multiplier.

Does not have to be looped each frame. Can be set once.

Values lower than 1f don't work.

**Note: If the value is set with GET_RANDOM_FLOAT_IN_RANGE, the vehicle will have an absurdly high ammount of power, and will become almost undrivable for the player or NPCs.** 

The range doesn't seem to matter.An high value like 10000000000f will visually remove the wheels that apply the power (front wheels for FWD, rear wheels for RWD), but the power multiplier will still apply, and the wheels still work.



## Syntax

```js
vehicle.setEnginePowerMultiplier(value);
```

## Example

```js

//Boosting all emergency vehicles
mp.events.add('render', () => {
    if (mp.players.local.vehicle) {
        if (mp.players.local.vehicle.getClass() === 18) {
            mp.players.local.vehicle.setEnginePowerMultiplier(12)
        }
    }
})

```
## Parameters

<b>value: <span style="color:#008017">Float</span></b>

<br />


## Return value

<b>Undefined</b>