# Vehicle::isAConvertible

p1 is false almost always.

However, in `launcher_carwash/carwash1/carwash2 scripts`, p1 is `true` and is accompanied by `DOES_VEHICLE_HAVE_ROOF`



## Syntax

```js
vehicle.isAConvertible(p1);
```

## Example

```js
// todo
```
## Parameters

<b>p1: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>