# Vehicle::isNeonLightEnabled

indices:
* 0 = Left
* 1 = Right
* 2 = Front
* 3 = Back



## Syntax

```js
vehicle.isNeonLightEnabled(index);
```

## Example

```js
// todo
```
## Parameters

<b><span style="color:#008017">Int</span></b>

<br />


## Return value

<b><span style="color:#008017">Boolean</span></b>