# Vehicle::setCanRespray

Hardcoded to not work in multiplayer.

## Syntax

```js
vehicle.setCanRespray(state);
```

## Example

```js
// todo
```
## Parameters

<b>state: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>