# Vehicle::setJetEngineOn

`VEHICLE::SET_VEHICLE_ENGINE_ON` is not enough to start jet engines when not inside the vehicle. But with this native set to true it works: 

[video](https://youtu.be/OK0ps2fDpxs) [imgur](i.imgur.com/7XA14pX.png)

Certain planes got jet engines.



## Syntax

```js
vehicle.setJetEngineOn(toggle);
```

## Example

```js
// todo
```
## Parameters

<b>toggle: <span style="color:#008017">Boolean</span></b>

<br />


## Return value

<b>Undefined</b>