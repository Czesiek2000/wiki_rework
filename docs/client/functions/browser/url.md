# Browser::url

This function set or get the url of a browser 


# Syntax

```js
browser.url;
```

# Example

Setter: 

```js
let browser = mp.browsers.new("package://index.html");
browser.url = "package://home.html";
```

Getter: 

```js
let url = browser.url;
```
# Parameters

<li><b>URL</b>: <b><span style="color:#008017">String</span></b></li>