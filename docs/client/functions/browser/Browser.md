# Browser::Browser
Function: Creates a browser

See [Package Protocol](../../../shared/package/protocol.md) for more info on the `package:// url`.

U can use browser events: [BrowserCreated](../../events/browser/BrowserCreated.md), [BrowserLoadingFailed](../../events/browser/BrowserLoadingFailed.md), [BrowserDomReady](../../events/browser/BrowserDomReady.md)

# Syntax

```js
mp.browsers.new(url);
```

# Example

```js
mp.events.add('playerReady', () => {
  let browser = mp.browsers.new("package://index.html");
});
```

# Parameters

<li><b>URL</b>: <b><span style="color:#008017">String</span></b></li>
