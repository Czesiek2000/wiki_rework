# Browser::destroy

Destroys browser instance.

# Syntax

```js
browser.destroy();
```

# Example

```js
let browser = mp.browsers.at(0);
if (browser) {
    browser.destroy();
}
```

# Parameters

This function has no parameters