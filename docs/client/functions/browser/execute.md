# Browser::execute

Calls JavaScript code inside the browser.


# Syntax

```js
browser.execute(executedCode);
```

# Example

Create a popup alert in the browser

```js
browser.execute(`alert(`It's a test.`);`);
```
