# Browser::reload

Reloads current page.

# Syntax

```js
browser.reload(ignoreCache);
```

# Example

```js
let browser = mp.browsers.at(0);
if (browser) {
    browser.reload(false);
}
```

# Parameters

<li><b>ignoreCache: </b> true to ignore cache</li>

<br />

# Return value

<li><b>Undefined</b></li>