# Trigger

Sends data / trigger events from html to your clientside JS file.



# Syntax

```js
mp.trigger('event', params);
```

# Example

```html
<script>
   mp.trigger('CEFDoneUsingBrowser'); 
</script>
```

```js
// client-side
let activeBrowser = null;
mp.events.add('playerReady', () => {
    activeBrowser = mp.browsers.new('package://menu/modes/race.html');
});

mp.events.add('CEFDoneUsingBrowser', () => {
   activeBrowser.destroy();
});
```

# Parameters

 <li><b> event </b>: <b><font color="green">String</font> </b></li> 

 <li><b> params </b>: <b><font color="red">int</font> </b> or <b><font color="green">String</font> </b></li > 

# Return value

<li><b>Unknown</b></li>