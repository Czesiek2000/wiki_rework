# Browser::active

This function gets or sets if the browser is active 


# Syntax

```js
browser.active;
```

# Example

Setter: 

```js
let browser = mp.browsers.new("package://index.html");
browser.active = false;
```

Getter: 

```js
let active = browser.active;
```

# Parameters
<li><b>Toggle</b>: <b><span style="color:#008017">Boolean</span></b></li>
