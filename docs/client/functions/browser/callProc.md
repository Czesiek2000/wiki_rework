# Browser::callProc

This function calls registered Remote Prodecure Call (RPC) from browser to client-side and expects a callback.


# Syntax

```js
mp.events.callProc('eventProcName', args);
```

# Example

```js
// calls 'test_proc' from client-side
mp.events.callProc('test_proc', 'test');
```

# Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>eventProcName</b>: <b><span style="color:#008017">String</span></b></li>

<li><b>args</b>: <b><span style="color:#008017">Any</span></b></li>
