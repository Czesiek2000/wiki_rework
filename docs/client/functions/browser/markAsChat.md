# Browser::markAsChat

This marks the browser as the chat for the server.


# Syntax

```js
browser.markAsChat();
```

# Example

This sets the browser named 'chatbox' to become the chatbox for the server.

```js
mp.gui.chat.show(false); //Disables default RageMP Chat

let chatbox = mp.browsers.new('package://chat/index.html');
chatbox.markAsChat();
```

# Parameters


# Return value

<li><b>Undefined</b></li>