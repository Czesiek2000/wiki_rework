# Camera::doesExist

Returns whether or not the passed camera handle exists.


# Syntax

```js
camera.doesExist();
```

# Example

```js
// todo
```

# Parameters
No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
