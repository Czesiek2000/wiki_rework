# Camera::getSplinePhase

Can use this with SET_Camera_SPLINE_PHASE to set the float it this native returns.

(returns 1.0f when no nodes has been added, reached end of non existing spline)


# Syntax

```js
camera.getSplinePhase();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>