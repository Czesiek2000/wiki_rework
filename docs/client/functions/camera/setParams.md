# Camera::setParams

# Syntax

```js
camera.setParams(x, y, z, rx, ry, rz, fov, duration, p9, p10, p11);
```

# Example

```js
let gameplayCam = mp.cameras.new ( "gameplay" );
let pos = gameplayCam.getCoord();
let rot = gameplayCam.getRot ( 2 );
let fov = gameplayCam.getFov();

testCam.setParams ( pos.x, pos.y, pos.z, rot.x, rot.y, rot.z, fov, 5000, 1, 1, 2 ); 

```

# Parameters

<li><b>x: <span style="color:#008017">Float</span></b></li>


<li><b>y: <span style="color:#008017">Float</span></b></li>


<li><b>z: <span style="color:#008017">Float</span></b></li>


<li><b>rx: <span style="color:#008017">Float</span></b></li>


<li><b>ry: <span style="color:#008017">Float</span></b></li>


<li><b>rz: <span style="color:#008017">Float</span></b></li>


<li><b>fov: <span style="color:#008017">Float</span></b></li>


<li><b>duration: <span style="color:#008017">Number</span></b></li>


<li><b>p9: <span style="color:#008017">Number</span></b></li>


<li><b>p10: <span style="color:#008017">Number</span></b></li>


<li><b>p11: <span style="color:#008017">Number</span></b></li>

<br />

# Return value

<li><b>Undefined</b></li>