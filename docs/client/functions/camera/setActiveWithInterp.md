# Camera::setActiveWithInterp

# Syntax

```js
camera.setActiveWithInterp(camFrom, duration, easeLocation, easeRotation);
```

# Example

```js
var camera1 = mp.cameras.new('default', new mp.Vector3(111.1, 222.2, 12.37), new mp.Vector3(0, 0, 0), 40);
var camera2 = mp.cameras.new('default', new mp.Vector3(222.2, 333.3, 1.37), new mp.Vector3(0, 0, 0), 40);
camera2.setActiveWithInterp(camera1.handle, 2000, 0, 0); // 2000ms = 2secs, 0, 0 - idk
```

# Parameters

<li><b>camFrom: Camera.Handle</b></li>


<li><b>duration: <span style="color:#008017">Int</span></b></li>


<li><b>easeLocation: <span style="color:#008017">Int</span></b></li>


<li><b>easeRotation: <span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>