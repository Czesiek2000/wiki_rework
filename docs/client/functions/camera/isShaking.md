# Camera::isShaking
# Syntax

```js
camera.isShaking();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>