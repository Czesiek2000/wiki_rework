# Camera::setFov

Sets the field of view of the camera.

---------------------------------------------

* Min: 1.0f

* Max: 130.0f


# Syntax

```js
camera.setFov(fieldOfView);
```

# Example

```js
let camera = mp.cameras.new('default', new mp.Vector3(-485, 1095.75, 323.85), new mp.Vector3(0,0,0), 40); // Creates the camera
camera.setActive(true);

camera.setFov(15); // Sets the Field of View of the camera to 15
```

# Parameters

<li><b>fieldOfView: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>