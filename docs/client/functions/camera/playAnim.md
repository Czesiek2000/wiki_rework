# Camera::playAnim
Atleast one time in a script for the zRot Rockstar uses GET_ENTITY_HEADING to help fill the parameter.

p9 is unknown at this time.

p10 throughout all the X360 Scripts is always 2.

Animations


# Syntax

```js
camera.playAnim(animName, animDictionary, x, y, z, xRot, yRot, zRot, p9, p10);
```

# Example

```js
// todo
```

# Parameters

<li><b>animName: <span style="color:#008017">String</span></b></li>

<li><b>animDictionary: <span style="color:#008017">String</span></b></li>

<li><b>x: <span style="color:#008017">Float</span></b></li>

<li><b>y: <span style="color:#008017">Float</span></b></li>

<li><b>z: <span style="color:#008017">Float</span></b></li>

<li><b>xRot: <span style="color:#008017">Float</span></b></li>

<li><b>yRot: <span style="color:#008017">Float</span></b></li>

<li><b>zRot: <span style="color:#008017">Float</span></b></li>

<li><b>p9: <span style="color:#008017">Boolean</span></b></li>

<li><b>p10: <span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>