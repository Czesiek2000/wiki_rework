# Camera::setInheritRollVehicle

The native seems to only be called once.

The native is used as so,

```cpp
Camera::SET_Camera_INHERIT_ROLL_VEHICLE(l_544, getElem(2, &l_525, 4));
```

In the exile1 script.


# Syntax

```js
camera.setInheritRollVehicle(p1);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1: <span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>