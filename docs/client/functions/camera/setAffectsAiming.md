# Camera::setAffectsAiming

Allows you to aim and shoot at the direction the camera is facing.


# Syntax

```js
camera.setAffectsAiming(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle: <span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>