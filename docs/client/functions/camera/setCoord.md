# Camera::setCoord

Sets the position of the camera.


# Syntax

```js
camera.setCoord(posX, posY, posZ);
```

# Example

```js
// todo
```

# Parameters

<li><b>posX: <span style="color:#008017">Float</span></b></li>


<li><b>posY: <span style="color:#008017">Float</span></b></li>


<li><b>posZ: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>