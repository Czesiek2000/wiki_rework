# Camera::getRot

The last parameter, as in other 'ROT' methods, is usually 2.

# Syntax

```js
camera.getRot(p1);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> unknown (to be checked)</li>

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>