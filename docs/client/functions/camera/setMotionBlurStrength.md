# Camera::setMotionBlurStrength

# Syntax

```js
camera.setMotionBlurStrength(strength);
```

# Example

```js
// todo
```

# Parameters

<li><b>strength: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>