# Camera::attachToPedBone

Attaches a camera to the bone specified.

# Syntax

```js
camera.attachToPedBone(ped, boneIndex, x, y, z, heading);

```

# Example

This creates a camera and then sets it active and attaches it to the player's hand so it stays focused. The camera will not move with the character.

```js
let handCamera = mp.cameras.new('default', new mp.Vector3(0,  0,  0), new mp.Vector3(0,0,0), 40);

mp.events.add("handCam", () => {
    let playerPosition = mp.players.local.position

    handCamera.setActive(true);
    handCamera.attachToPedBone(mp.players.local.handle, 57005, 0, 0, 0, true);
    handCamera.setCoord(playerPosition.x + 1, playerPosition.y + 1, playerPosition.z);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

```

# Parameters
<ul><li><b>ped:</b> Ped handle or object</li>
<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b> (<a href="../../../tables/bonesId.md" title="Bones">Bone IDs</a>)</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Boolean</span></b></li></ul>

# Return value

<li><b>Undefined</b></li>