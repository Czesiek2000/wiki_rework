# Camera::getFov


# Syntax

```js
camera.getFov();
```

# Example

This example creates a camera and if the current Field of View of it is less than or equals to 10 destroys the camera.

```js
let camera = mp.cameras.new('default', new mp.Vector3(-485, 1095.75, 323.85), new mp.Vector3(0,0,0), 40); // Creates the camera
camera.setActive(true);

let currFov = camera.getFov();

if (currFov <= 10) {
  camera.destroy();
}
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>
