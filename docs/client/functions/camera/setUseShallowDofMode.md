# Camera::setUseShallowDofMode

# Syntax

```js
camera.setUseShallowDofMode(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle: <span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>