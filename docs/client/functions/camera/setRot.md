# Camera::setRot

Sets the rotation of the camera.

Last parameter unknown.

Last parameter seems to always be set to 2.

# Syntax

```js
camera.setRot(rotX, rotY, rotZ, p4);
```

# Example

```js
// todo
```

# Parameters

<li><b>rotX: <span style="color:#008017">Float</span></b></li>


<li><b>rotY: <span style="color:#008017">Float</span></b></li>


<li><b>rotZ: <span style="color:#008017">Float</span></b></li>


<li><b>p4: <span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>