# Camera::isActive

Returns whether or not the passed camera handle is active.




# Syntax

```js
camera.isActive();
```

# Example

This example create a camera and then checks if the camera is active, if it's not active it should active it.

```js
let camera = mp.cameras.new('default', new mp.Vector3(-485, 1095.75, 323.85), new mp.Vector3(0,0,0), 40); // Creates the camera

if (camera.isActive()) { // checks if camera is active, if not then it should set the camera active
  camera.setFov(15);
} else {
  camera.setActive(true);
}
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>