# Camera::setNearClip

# Syntax

```js
camera.setNearClip(nearClip);
```

# Example

```js
// todo
```

# Parameters

<li><b>nearClip: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>