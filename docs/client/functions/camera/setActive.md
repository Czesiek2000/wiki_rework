# Camera::setActive

Set camera as active/inactive.




# Syntax

```js
camera.setActive(active);
```

# Example

Creates a camera with the stated positions and rendering it.

```js
let cam = mp.cameras.new('default', new mp.Vector3(0,0,0), new mp.Vector3(0,0,0), false);
cam.setActive(true);

mp.game.cam.renderScriptCams(true, false, 0, true, false);

```

# Parameters

<li><b>active:<span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>