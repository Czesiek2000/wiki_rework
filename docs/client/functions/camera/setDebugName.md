# Camera::setDebugName

**NOTE: Debugging functions are not present in the retail version of the game.**


# Syntax

```js
camera.setDebugName(name);
```

# Example

```js
// todo
```

# Parameters

<li><b>name: <span style="color:#008017">String</span></b></li>

# Return value

<li><b>Undefined</b></li>