# Camera::stopShaking

# Syntax

```js
camera.stopShaking(p1);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1: <span style="color:#008017">Boolean</span></b></li>

<br />

# Return value

<li><b>Undefined</b></li>