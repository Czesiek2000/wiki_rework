# Camera::animatedShake
Example from michael2 `script.Camera::ANIMATED_SHAKE_CAM(l_5069, '[email protected]', 'light', , 1f);`


# Syntax

```js
camera.animatedShake(p1, p2, p3, amplitude);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> <span style="color:#008017">String</span></li>


<li><b>p2:</b> <span style="color:#008017">String</span></li>


<li><b>p3:</b> <span style="color:#008017">String</span></li>


<li><b>amplitude:</b> <span style="color:#008017">Float</span></li>

# Return value

<li><b>Undefined</b></li>