# Camera::pointAtCoord

# Syntax

```js
camera.pointAtCoord(x, y, z);
```

# Example

```js
// todo
```

# Parameters

<li><b>x: <span style="color:#008017">Float</span></b></li>


<li><b>y: <span style="color:#008017">Float</span></b></li>


<li><b>z: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>