# Camera::setShakeAmplitude

# Syntax

```js
camera.setShakeAmplitude(amplitude);
```

# Example

```js
// todo
```

# Parameters

<li><b>amplitude: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>