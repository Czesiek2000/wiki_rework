# Camera::Camera
Creates a camera.




# Syntax

```js
mp.cameras.new(name, position, rotation, fov);

```

# Example

Creates a camera and sets it to look towards a location.

Version 0.3.7:

```js
let sceneryCamera = mp.cameras.new('default', new mp.Vector3(-485, 1095.75, 323.85), new mp.Vector3(0,0,0), 40);

sceneryCamera.pointAtCoord(402.8664, -996.4108, -98.5); // Changes the rotation of the camera to point towards a location
sceneryCamera.setActive(true);
mp.game.cam.renderScriptCams(true, false, 0, true, false);
```

Version 1.1:

```js
sceneryCamera.pointAtCoord(402.8664, -996.4108, -98.5); // PointAtCoord can't use Vector3 position in version 1.1. Use position.x, position.y, position.z instead.
```

# Parameters
<ul><li><b>name</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>rotation</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>fov</b>: <b><span style="color:#008017">Int</span></b></li></ul>

# Return value

<li><b>Undefined</b></li>