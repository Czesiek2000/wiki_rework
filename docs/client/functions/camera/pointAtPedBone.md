# Camera::pointAtPedBone

Sets the camera so it's pointing towards a ped's bone.


Bones List: [Bones](../../../tables/bonesId.md)


This event locks the camera onto the players hand.


# Syntax

```js
camera.pointAtPedBone(ped, boneIndex, x, y, z, p6);
```

# Example

```js
let handCamera = mp.cameras.new('default', new mp.Vector3(0,  0,  0), new mp.Vector3(0,0,0), 40);

mp.events.add("handCam", () => {
    let playerPosition = mp.players.local.position
    
    handCamera.setActive(true);
    handCamera.pointAtPedBone(mp.players.local.handle, 57005, 0, 0, 0, true);
    handCamera.setCoord(playerPosition.x + 1, playerPosition.y + 1, playerPosition.z);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

```

# Parameters

<li><b>ped: <span style="color:#008017">Int</span></b></li>


<li><b>boneIndex: <span style="color:#008017">Int</span></b></li>


<li><b>x: <span style="color:#008017">Float</span></b></li>


<li><b>y: <span style="color:#008017">Float</span></b></li>


<li><b>z: <span style="color:#008017">Float</span></b></li>


<li><b>p6: <span style="color:#008017">Boolean</span> (The bool p6 is unknown, but through every X360 script it's always 1.)</b></li>

# Return value

<li><b>Undefined</b></li>