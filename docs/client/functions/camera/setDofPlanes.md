# Camera::setDofPlanes
# Syntax

```js
camera.setDofPlanes(p1, p2, p3, p4);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1: <span style="color:#008017">Float</span></b></li>


<li><b>p2: <span style="color:#008017">Float</span></b></li>


<li><b>p3: <span style="color:#008017">Float</span></b></li>


<li><b>p4: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>