# Camera::attachTo

Attaches your camera to an object.

JulioNIB:
Last param determines if its relative to the Entity


# Syntax

```js
camera.attachTo(entity, xOffset, yOffset, zOffset, isRelative);
```

# Example

Attaches a camera to a vehicle and sets it as your active camera.

```js
let mainCar = mp.vehicles.new(mp.game.joaat("turismor"), new mp.Vector3(-421.88, 1136.86, 326));

mp.events.add("vehCam", () => {
    vehicleCamera.attachTo(mainCar.handle, 0, 0, 2.0, false);
    vehicleCamera.setActive(true);
    mp.game.cam.renderScriptCams(true, false, 0, true, false);
});

```

# Parameters

<ul><li><b>entity:</b> Entity handle or object</li>
<li><b>xOffset:</b> <span style="color:#008017">Float</span></li>
<li><b>yOffset:</b> <span style="color:#008017">Float</span></li>
<li><b>zOffset:</b> <span style="color:#008017">Float</span></li>
<li><b>isRelative:</b> <span style="color:#008017">Boolean</span></li></ul>

# Return value

<li><b>Undefined</b></li>