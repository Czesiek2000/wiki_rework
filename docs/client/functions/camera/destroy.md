# Camera::destroy

Removes the camera.

# Syntax

```js
camera.destroy(destroy);
```

# Example

This example creates a camera and then destroys it.

```js
let camera = mp.cameras.new('default', new mp.Vector3(-485, 1095.75, 323.85), new mp.Vector3(0,0,0), 40); // Creates the camera
camera.setActive(true);

// After 5 seconds destroys the camera.
setTimeout(function (){
  camera.destroy();
}, 5000)

```

# Parameters

<li><b>destroy:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>