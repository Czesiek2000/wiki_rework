# Camera::isPlayingAnim

# Syntax

```js
camera.isPlayingAnim(animName, animDictionary);
```

# Example

```js
// todo
```

# Parameters

<li><b>animName: <span style="color:#008017">String</span></b></li>

<li><b>animDictionary: <span style="color:#008017">String</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>