# Camera::setDofMaxNearInFocusDistance

This native has a name defined inside its code ~Zorg93


# Syntax

```js
camera.setDofMaxNearInFocusDistance(p1);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>