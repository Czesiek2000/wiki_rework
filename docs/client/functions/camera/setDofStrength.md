# Camera::setDofStrength

# Syntax

```js
camera.setDofStrength(dofStrength);
```

# Example

```js
// todo
```

# Parameters

<li><b>dofStrength: <span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>