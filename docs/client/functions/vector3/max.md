# Vector3::max

This function returns the maximum partial of a Vector3.


## Syntax

```js
vector.max();
```

## Example

```js
const vec1 = new mp.Vector3(10, 30, 100);

const maximum = vec1.max(); // maximum = 100
```

## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">number</span></b> The maximum.</li>