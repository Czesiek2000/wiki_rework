# Vector3::angleTo

This function returns the angle (in radians) between two vectors.

## Syntax

```js
vector.angleTo(Vector3 otherVec);
```

## Example

No example here

<br />


## Parameters

<li><b>otherVec:</b> Vector3: The other vector to calcuate the angle to.</li>

## Return value

<b><span style="color:#008017">number</span></b> The angle in radians