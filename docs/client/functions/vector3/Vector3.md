# Vector3::Vector3

This function create a vector object.



## Syntax

```js
let vector = new mp.Vector3(Number x, Number y, Number z)
```

## Example

No example here

<br />


## Parameters

No parameters here

<br />


## Return value

<li><b>Undefined</b></li>