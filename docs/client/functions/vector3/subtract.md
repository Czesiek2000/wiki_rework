# Vector3::subtract

This function is used to subtract a Vector3 or scalar from another Vector3.

## Syntax

```js
vector.subtract(Vector3 otherVec);
vector.subtract(number scalar);
```

## Example

```js
const vec1 = new mp.Vector3(50, 40, 30);
const vec2 = new mp.Vector3(10, 20, 15);

const difference = vec1.subtract(vec2); // difference = {x: 40, y: 20, z: 15}
```

```js
const vec1 = new mp.Vector3(50, 40, 30);
const scalar = 30;

const difference = vec1.subtract(scalar); // difference = {x: 20, y: 10, z: 0}
```

## Parameters

<li><b>otherVec:</b> Vector3: The vector or scalar to be subtracted from the callee.</li>

## Return value

<li><b><span style="color:#008017">Vector3</span></b> The difference.</li>