# Vector3::unit

This function returns a normalized copy of a Vector3- one that has the same direction but with a magnitude of 1.



## Syntax

```js
vector.unit();
```

## Example

No example here

<br />


## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">Vector3</span></b> The normalized vector.</li>