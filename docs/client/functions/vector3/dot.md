# Vector3::dot

This function is used to calculate the dot product of two vectors.


The dot product is a number calculated by multiplying the magnitudes of both vectors together, then multiplying that number by cosine of the angle between them.


For normalized vectors, the dot product will be:

* -1 - If the vectors point in the exact opposite direction
* 0 - If the vectors are perpendicular
* 1 - If the vectors point the same direction


## Syntax

```js
vector.dot(Vector3 otherVec);
```

## Example

```js
// todo
```

## Parameters

<li><b>otherVec:</b> Vector3: The other vector.</li>

## Return value

<li><b><span style="color:#008017">number</span></b> The dot product.</li>