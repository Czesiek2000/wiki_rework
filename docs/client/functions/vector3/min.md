# Vector3::min

This function returns the minimum partial of a Vector3.

## Syntax

```js
vector.min();
```

## Example

```js
const vec1 = new mp.Vector3(10, 30, 100);

const minimum = vec1.min(); // maximum = 10
```

## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">number</span></b> The minimum.</li>