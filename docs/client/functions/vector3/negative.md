# Vector3::negative

This function returns the opposite of a Vector3 by flipping the sign of each partial.


The same affect can be achieved by multiplying a vector by -1.

## Syntax

```js
vector.negative();
```

## Example

```js
const vec1 = new mp.Vector3(10, 30, 100);
const vec2 = new mp.Vector3(-50, -30, 40);

const opposite1 = vec1.negative(); // { x: -10, y: -30, z: -100 }
const opposite2 = vec2.negative(); // { x: 50, y: 30, z: -40 };
```

## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">Vector3Mp</span></b> The opposite vector.</li>