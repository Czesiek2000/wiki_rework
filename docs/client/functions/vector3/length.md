# Vector3::length

This function returns the magnitude of a Vector3.


It's calculated by square rooting the result of *x * x + y * y + z * z*.


This example calculates the distance between two players.

## Syntax

```js
vector.length();
```

## Example

```js
const vec1 = mp.players.at(0).position;
const vec2 = mp.players.at(1).position;

const distance = vec1.subtract(vec2).length();

// distance is the distance between the two players
```

## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">number</span></b> The vector's magnitude.</li>