# Vector3::clone

This function returns a copy of a Vector3.



## Syntax

```js
vector.clone();
```

## Example

No example here

<br />

## Parameters

No parameters here

<br />


## Return value

<b><span style="color:#008017">Vector3</span></b> new vector with the same values