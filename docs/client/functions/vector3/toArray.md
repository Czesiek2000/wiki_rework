# Vector3::toArray

This function returns an array of the partials of a Vector3.

## Syntax

```js
vector.toArray();
```

## Example

```js
const vec1 = new mp.Vector3(10, 30, 100);

const array = vec1.toArray(); // [10, 30, 100]
```

## Parameters

No parameters here

<br />


## Return value

<li><b><span style="color:#008017">number[]</span></b> An array of [x, y, z].</li>