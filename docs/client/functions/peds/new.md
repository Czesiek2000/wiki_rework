# Peds::new

## Syntax
```js
mp.peds.new(model, position, heading, dimension);
```

## Example
```js
let ped = mp.peds.new(
    mp.game.joaat('MP_F_Freemode_01'), 
    new mp.Vector3(100.0, -100.0, 25.0),
    270.0,
    mp.players.local.dimension
);
```

### Legacy syntax version

The previous syntax for this function allowed you to specify a "streamedIn" callback. 

This has been removed in favor of the EntityStreamIn event, but you can add this polyfill to your client script to enable the previous syntax:

```js
mp.peds.newLegacy = (hash, position, heading, streamIn, dimension) => {
    let ped = mp.peds.new(hash, position, heading, dimension);
    ped.streamInHandler = streamIn;
    return ped;
};

mp.events.add("entityStreamIn", entity => {
   if (entity.streamInHandler) {
       entity.streamInHandler(entity);
   }
});
```

#### Example
```js
let ped = mp.peds.newLegacy(mp.game.joaat('mp_m_freemode_01'), new mp.Vector3(1000, 100, 10), 0, ped => {
    // Called when the ped is streamed in
    ped.setAlpha(255);
    ped.freezePosition(false);
    ped.setInvincible(false);
    ped.setProofs(false, false, false, false, false, false, false, false); 
}, 0);
```

## Parameters
<ul><li><b>modelHash:</b> Model hash</li>
<li><b>position:</b> <b><span style="color:#008017">Vector3</span></b> position</li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dimension:</b> <b><span style="color:#008017">Int</span></b></li></ul>

## Return value
<li><b>Ped object</b></li>