# Entity::getForwardX

Gets the X-component of the entity's forward vector.


# Syntax

```js
entity.getForwardX();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>