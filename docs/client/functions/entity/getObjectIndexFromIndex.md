# Entity::getObjectIndexFromIndex

Simply returns whatever is passed to it (Regardless of whether the handle is valid or not).


# Syntax

```js
entity.getObjectIndexFromIndex();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Object handle or object</b></li>