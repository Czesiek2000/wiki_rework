# Entity::setQuaternion

w is the correct parameter name!


# Syntax

```js
entity.setQuaternion(x, y, z, w);
```

# Example

```js
// todo
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>w:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>