# Entity::getForwardY

Gets the Y-component of the entity's forward vector.


# Syntax

```js
entity.getForwardY();
```

# Example

```js
// todo
```

# Parameters


# Return value

<li><b><span style="color:#008017">Float</span></b></li>