# Entity::getCoords

Gets the coordinates for the given entity.


# Syntax

```js
entity.getCoords(alive);
```

# Example

```js
// todo
```

# Parameters

<li><b>alive :</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<b><span style="color:#008017">Vector3</span></b>