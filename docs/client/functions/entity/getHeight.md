# Entity::getHeight
# Syntax

```js
entity.getHeight(X, Y, Z, atTop, inWorldCoords);
```

# Example

```js
// todo
```

# Parameters

<li><b>X:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>atTop:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>inWorldCoords:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b><span style="color:#008017">Float</span></b></li>