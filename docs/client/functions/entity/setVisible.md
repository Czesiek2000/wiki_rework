# Entity::setVisible

unk was always false.


# Syntax

```js
entity.setVisible(toggle, unk);
```

# Example

```js
entity.setVisible(toggle, false);
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unk:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>