# Entity::isAnEntity


# Syntax

```js
mp.game.entity.isAnEntity(handle);
```

# Example

```js
// todo
```

# Parameters

<li><b>handle:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>