# Entity::setMaxSpeed

# Syntax

```js
entity.setMaxSpeed(speed);
```

# Example

```js
let vehicle = mp.players.local.vehicle

let speed = vehicle.getSpeed(); // Getting vehicle speed.

vehicle.setMaxSpeed(speed) // Sets the vehicle maximum speed the speed that we got.
```

# Parameters

<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>