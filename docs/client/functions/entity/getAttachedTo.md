# Entity::getAttachedTo

# Syntax

```js
entity.getAttachedTo();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Entity handle or object</b></li>