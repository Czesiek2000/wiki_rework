# Entity::setAnimCurrentTime

# Syntax

```js
entity.setAnimCurrentTime(animDictionary, animName, time);
```

# Example

```js
// todo
```

# Parameters

<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>time:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>