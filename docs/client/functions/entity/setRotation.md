# Entity::setRotation
rotationOrder refers to the order yaw pitch.

roll is appliedvalue ranges from 0 to 5. What you use for rotation. 

Order when setting must be the same as rotationOrder when getting the rotation. 

Unsure what value corresponds to what rotation order, more testing will be needed for that.

For the most part R* uses 1 or 2 as the order.

p5 is usually set as true


# Syntax

```js
entity.setRotation(pitch, roll, yaw, rotationOrder, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>pitch:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>roll:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yaw:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotationOrder:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>