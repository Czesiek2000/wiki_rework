# Entity::isInAngledArea
Creates a spherical cone at origin that extends to surface with the angle specified. Then returns true if the entity is inside the spherical cone.

Angle is measured in degrees.

These values are constant, most likely bogus:
* p8: 0
* p9: 1
* p10: 0

This method can also take two float<3> instead of 6 floats.


# Syntax

```js
entity.isInAngledArea(originX, originY, originZ, edgeX, edgeY, edgeZ, angle, p8, p9, p10);
```

# Example

```js
// todo
```

# Parameters

<li><b>originX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>originY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>originZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>edgeX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>edgeY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>edgeZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p10:</b> unknown (to be checked)</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>