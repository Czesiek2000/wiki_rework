# Entity::detach
p1 and p2 have no effect maybe a quick disassembly will tell us what they do

if p2 is set to true, the both entities won't collide with the other until the distance between them is above 4 meters.

p1?


# Syntax

```js
entity.detach(p1, collision);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>collision:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>