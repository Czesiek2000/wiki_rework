# Entity::setAsMission
Makes the specified entity (ped, vehicle or object) persistent. 

Persistent entities will not automatically be removed by the engine.

p1 has no effect when either its on or off maybe a quick disassembly will tell us what it does

p2 has no effect when either its on or off maybe a quick disassembly will tell us what it does


# Syntax

```js
entity.setAsMission(p1, byThisScript);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>byThisScript:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>