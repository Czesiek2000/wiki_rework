# Entity::getSubmergedLevel

Get how much of the entity is submerged. 1.0f is whole entity.


# Syntax

```js
entity.getSubmergedLevel();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>