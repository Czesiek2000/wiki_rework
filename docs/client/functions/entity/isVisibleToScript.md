# Entity::isVisibleToScript

# Syntax

```js
entity.isVisibleToScript();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>