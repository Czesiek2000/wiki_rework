# Entity::setMaxHealth
For instance: `ENTITY::SET_ENTITY_MAX_HEALTH(PLAYER::PLAYER_Player_ID(), 200);` // director_mode.c4: 67849


# Syntax

```js
entity.setMaxHealth(value);
```

# Example

```js
// todo
```

# Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>