# Entity::getMatrix

# Syntax

```js
entity.getMatrix(rightVector, forwardVector, upVector, position);
```

# Example

```js
// todo
```

# Parameters

<li><b>rightVector:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>forwardVector:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>upVector:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>position:</b> <b><span style="color:#008017">Vector3</span></b></li>

# Return value

<li><b>object:</b> rightVector, forwardVector, upVector, position</li>