# Entity::createModelSwap
Only works with objects!Network players do not see changes done with this.- Did ya try modifying p6 lol


# Syntax

```js
mp.game.entity.createModelSwap(x, y, z, radius, originalModel, newModel, p6);
```

# Example

```js
// todo
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>originalModel:</b> Model hash or name</li>
<li><b>newModel:</b> Model hash or name</li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>