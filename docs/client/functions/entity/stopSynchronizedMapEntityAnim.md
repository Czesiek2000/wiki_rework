# Entity::stopSynchronizedMapEntityAnim


# Syntax

```js
mp.game.entity.stopSynchronizedMapEntityAnim(p0, p1, p2, p3, p4, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>