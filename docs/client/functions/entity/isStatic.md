# Entity::isStatic
a static ped will not react to natives like `APPLY_FORCE_TO_ENTITY` or `SET_ENTITY_VELOCITY` and oftentimes will not react to task-natives like `Player::TASK_COMBAT_PED`. 

The only way I know of to make one of these peds react is to ragdoll them (or sometimes to use `CLEAR_Player_TASKS_IMMEDIATELY()`. 

Static peds include almost all far-away peds, beach-combers, peds in certain scenarios, peds crossing a crosswalk, peds walking to get back into their cars, and others. 

*If anyone knows how to make a ped non-static without ragdolling them, please edit this with the solution.*

how to i meik static entyti??


# Syntax

```js
entity.isStatic();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>