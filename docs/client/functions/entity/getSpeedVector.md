# Entity::getSpeedVector

Relative can be used for getting speed relative to the frame of the vehicle, to determine for example, if you are going in reverse (-y speed) or not (+y speed).


# Syntax

```js
entity.getSpeedVector(relative);
```

# Example

```js
// todo
```

# Parameters

<li><b>relative:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>