# Entity::setVelocity
Note that the third parameter(denoted as z) is 'up and down' with positive numbers encouraging upwards movement.


# Syntax

```js
entity.setVelocity(x, y, z);
```

# Example

```js
let vehicle = mp.players.local.vehicle

let velocity = vehicle.getVelocity(); // returns a object with x, y , z

vehicle.setVelocity(velocity.x, velocity.y, velocity.z) // setting the velocity which we got.
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>