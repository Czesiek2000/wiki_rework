# Entity::getLodDist

Returns the LOD distance of an entity.


# Syntax

```js
entity.getLodDist();
```

# Example

```js
// todo
```

# Parameters


# Return value

<b><span style="color:#008017">String</span></b>