# Entity::createForcedObject


# Syntax

```js
mp.game.entity.createForcedObject(x, y, z, p3, modelHash, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>