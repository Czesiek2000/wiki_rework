# Entity::setAnimSpeed

# Syntax

```js
entity.setAnimSpeed(animDictionary, animName, speedMultiplier);
```

# Example

```js
// todo
```

# Parameters

<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>speedMultiplier:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>