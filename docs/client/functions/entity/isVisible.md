# Entity::isVisible

# Syntax

```js
entity.isVisible();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>