# Entity::getOffsetFromGivenWorldCoords

Converts world coords (posX - Z) to coords relative to the entity

Example:
posX is given as 50

entity's x coord is 40

the returned x coord will then be 10 or -10, not sure haven't used this in a while (think it is 10 though).


# Syntax

```js
entity.getOffsetFromGivenWorldCoords(posX, posY, posZ);
```

# Example

```js
// todo
```

# Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>