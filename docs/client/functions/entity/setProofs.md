# Entity::setProofs
Enable / disable each type of damage.

Can't get drownProof to work.

--------------

p7 is to to '1' in **am_mp_property_ext**/int: `entity::set_entity_proofs(uParam0->f_19, true, true, true, true, true, true, 1, true);`


# Syntax

```js
entity.setProofs(bulletProof, fireProof, explosionProof, collisionProof, meleeProof, p6, p7, drownProof);
```

# Example

```js
// todo
```

# Parameters

<li><b>bulletProof:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>fireProof:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>explosionProof:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>collisionProof:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>meleeProof:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>drownProof:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>