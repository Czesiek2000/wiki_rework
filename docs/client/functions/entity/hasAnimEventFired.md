# Entity::hasAnimEventFired
```
if (ENTITY::HAS_ANIM_EVENT_FIRED(PLAYER::PLAYER_Player_ID(), GAMEPLAY::GET_HASH_KEY('CreateObject')))
```

# Syntax

```js
entity.hasAnimEventFired(actionHash);
```

# Example

```js
// todo
```

# Parameters

<li><b>actionHash:</b> Model hash or name</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>