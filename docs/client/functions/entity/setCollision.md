# Entity::setCollision

# Syntax

```js
entity.setCollision(toggle, keepPhysics);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b>
<li><b>keepPhysics:</b> <b><span style="color:#008017">Boolean</span></b></li>

*Setting 'toggle' to false will disable all collisions and you will fall through the world.*

# Return value

<li><b>Undefined</b></li>