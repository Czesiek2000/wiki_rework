# Entity::isInArea

# Syntax

```js
entity.isInArea(x1, y1, z1, x2, y2, z2, p7, p8, p9);
```

# Example

```js
// todo
```

# Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>

# Return value

<li><b>Undefined</b></li>