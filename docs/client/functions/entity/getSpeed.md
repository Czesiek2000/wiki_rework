# Entity::getSpeed

# Syntax

```js
entity.getSpeed();
```

# Example

```js
function getspeed() {

    let vehicle = mp.players.local.vehicle
    let speed = vehicle.getSpeed();

    speed = speed * 3.6; // Transform the speed into KM/H
    // If you want a realistic calculation for the vehicle speed use: speed = Math.ceil(speed * (speed / 20) * 2); <- this will raise up to 300 km/h for a T20, but you can still easily cruise around with 60 km/h

    return speed; // returns the speed in KM/H
}
```

# Parameters

<li><b>Entity:</b> <b><font color="red">vehicle</font></b></li>

# Return value

<li><b>MP/s(Meters per second)</b></li>

## Speed calculation
<li><b>KM/H(Kilometers per hour):</b> speed ( 3.6 )</li>
<li><b>MP/H(Miles per hour)</b> speed * 2.236936</li>