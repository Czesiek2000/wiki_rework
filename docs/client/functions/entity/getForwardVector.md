# Entity::getForwardVector

Gets the entity's forward vector.

(Similar to `Camera.getDirection`)


# Syntax

```js
entity.getForwardVector();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>