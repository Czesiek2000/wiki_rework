# Entity::setHealth
Sets entity health to a specified value.

<b>Player</b>: For some reason health is calculated relying on 100 + actual player health. Setting value to 100 will result in player having 0 health but not dead, values &lt;100 will kill the player.

<b>Vehicle</b>: Setting health of a vehicle doesn't seem to have any technical results, vehicle will not be destroyed at 0 mark even after any damage.

# Syntax

```js
entity.setHealth(health);
```

# Example

```js
mp.players.local.setHealth(150); // Setting player health to 50
mp.players.local.setHealth(200); // Setting player health to 100
```

# Parameters

<li><b>health:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>