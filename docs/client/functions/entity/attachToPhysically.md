# Entity::attachToPhysically
* breakForce is the amount of force required to break the bond.

* p14 - is always 1 in scriptsp15 - is 1 or 0 in scripts - unknoun what it does

* p16 - controls collision between the two entities (FALSE disables collision).

* p17 - do not teleport entity to be attached to the position of the bone Index of the target entity (if 1, entity will not be teleported to target bone)

* p18 - is always 2 in scripts.


# Syntax

```js
entity.attachToPhysically(entity2, boneIndex1, boneIndex2, xPos1, yPos1, zPos1, xPos2, yPos2, zPos2, xRot, yRot, zRot, breakForce, fixedRot, p15, collision, p17, p18);
```

# Example

```js
// todo
```

# Parameters

<li><b>entity2:</b> Entity handle or object</li>
<li><b>boneIndex1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>boneIndex2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>xPos1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xPos2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>breakForce:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fixedRot:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p15:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>collision:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p17:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p18:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>