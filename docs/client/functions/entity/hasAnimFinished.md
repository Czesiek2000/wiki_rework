# Entity::hasAnimFinished

P3 is always 3


# Syntax

```js
entity.hasAnimFinished(animDict, animName, p3);
```

# Example

```js
// todo
```

# Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>