# Entity::setLights

# Syntax

```js
entity.setLights(toggle);
```

# Example

```js
player.vehicle.setLights(0); // Turn off lights
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>