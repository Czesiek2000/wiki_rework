# Entity::processAttachments

Called to update entity attachments.


# Syntax

```js
entity.processAttachments();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Undefined</b></li>