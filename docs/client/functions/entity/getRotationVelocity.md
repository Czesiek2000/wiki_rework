# Entity::getRotationVelocity

# Syntax

```js
entity.getRotationVelocity();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>