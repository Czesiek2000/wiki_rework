# Entity::playAnim
delta and bitset are guessed fields. They are based on the fact that most of the calls have 0 or nil field types passed in.The only time bitset has a value is 0x4000 and the only time delta has a value is during stealth with usually <1.0f values.


# Syntax

```js
entity.playAnim(animName, propName, p3, p4, p5, p6, delta, bitset);
```

# Example

```js
// todo
```

# Parameters

<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>propName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>delta:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>bitset:</b> unknown (to be checked)</li>

# Return value

<li><b>Undefined</b></li>