# Entity::isTouching

# Syntax

```js
entity.isTouching(targetEntity);
```

# Example

```js
// todo
```

# Parameters

<li><b>targetEntity:</b> Entity handle or object</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>