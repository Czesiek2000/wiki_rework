# Entity::hasCollidedWithAnything
Called on tick.

Tested with vehicles, returns true whenever the vehicle is touching any entity.

**Note: for vehicles, the wheels can touch the ground and it will still return false, but if the body of the vehicle touches the ground, it will return true.**


# Syntax

```js
entity.hasCollidedWithAnything();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>