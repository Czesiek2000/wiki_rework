# Entity::getHeading
Returns the heading of the entity in degrees. Also know as the 'Yaw' of an entity.


# Syntax

```js
entity.getHeading();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>