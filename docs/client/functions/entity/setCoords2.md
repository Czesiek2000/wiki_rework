# Entity::setCoords2

does the same as `SET_ENTITY_COORDS`.

**Console Hash: 0x749B282E**


# Syntax

```js
entity.setCoords2(xPos, yPos, zPos, xAxis, yAxis, zAxis, clearArea);
```

# Example

```js
// todo
```

# Parameters

<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xAxis:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yAxis:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zAxis:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>clearArea:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>