# Entity::getType

Returns:

* 0: no entity
* 1: ped
* 2: vehicle
* 3: object
* 4: player


# Syntax

```js
entity.getType();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">int</span></b></li>