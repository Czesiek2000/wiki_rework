# Entity::isAtCoord

Checks if entity is within x/y/zSize distance of x/y/z. 

Last three are unknown ints, almost always p7 = 0, p8 = 1, p9 = 0


# Syntax

```js
entity.isAtCoord(xPos, yPos, zPos, xSize, ySize, zSize, p7, p8, p9);
```

# Example

```js
// todo
```

# Parameters

<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xSize:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ySize:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zSize:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>