# Entity::setCoordsNoOffset

Axis - Invert Axis Flags


# Syntax

```js
entity.setCoordsNoOffset(xPos, yPos, zPos, xAxis, yAxis, zAxis);
```

# Example

```js
// todo
```

# Parameters

<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>yAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>zAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>