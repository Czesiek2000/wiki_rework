# Entity::isPlayingAnim
See also 
```cpp
Player::IS_SCRIPTED_SCENARIO_Player_USING_CONDITIONAL_ANIM 0x6EC47A344923E1ED 0x3C30B447
```
Taken from 
```cpp
ENTITY::IS_ENTITY_PLAYING_ANIM(PLAYER::PLAYER_Player_ID(), 'creatures@shark@move', 'attack_player', 3)
```

p4 is always 3 in the scripts.

# Syntax

```js
entity.isPlayingAnim(animDict, animName, p4);
```

# Example

```js
// todo
```

# Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b>Undefined</b></li>