# Entity::getAnimTotalTime
Returns a float value representing animation's total playtime in milliseconds.

*Example:*
```cpp
GET_ENTITY_ANIM_TOTAL_TIME(PLAYER_ID(),'amb@world_human_yoga@female@base','base_b') return 20800.000000
```

# Syntax

```js
entity.getAnimTotalTime(animDict, animName);
```

# Example

```js
// todo```

# Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>

# Return value

<li><b>Undefined</b></li>