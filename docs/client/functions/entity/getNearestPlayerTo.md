# Entity::getNearestPlayerTo

# Syntax

```js
entity.getNearestPlayerTo();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Player</span></b></li>