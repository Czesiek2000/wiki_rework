# Entity::applyForceToCenterOfMass

* p6/relative - makes the xyz force not relative to world coords, but to something else
* p7/highForce - setting false will make the force really low


# Syntax

```js
entity.applyForceToCenterOfMass(forceType, x, y, z, p5, isRel, highForce, p8);
```

# Example

```js
// todo
```

# Parameters

<li><b>forceType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>isRel:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>highForce:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>