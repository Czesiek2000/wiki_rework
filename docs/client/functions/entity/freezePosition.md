# Entity::freezePosition

# Syntax

```js
entity.freezePosition(toggle);
```

# Example

```js
// Freeze player position on pressing F2 key
let freeze = false;
mp.keys.bind(0x71, true, function() {
    freeze = !freeze;
    mp.players.local.freezePosition(freeze);
    mp.gui.chat.push(`FreezePosition status: ${freeze}`);
});
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>