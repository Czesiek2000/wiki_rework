# Entity::setHeading
Set heading of your entity in degrees

min 0.0 - max 360.0


# Syntax

```js
entity.setHeading(heading);
```

# Example

```js
// todo
```

# Parameters

<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>