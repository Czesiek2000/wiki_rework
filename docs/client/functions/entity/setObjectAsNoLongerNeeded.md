# Entity::setObjectAsNoLongerNeeded
This is an alias of `SET_ENTITY_AS_NO_LONGER_NEEDED`.


# Syntax

```js
mp.game.entity.setObjectAsNoLongerNeeded(object);
```

# Example

```js
// todo
```

# Parameters

<li><b>object:</b> Object</li>

# Return value

<li><b>Object</b></li>