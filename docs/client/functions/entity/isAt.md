# Entity::isAt
Checks if entity1 is within the box defined by x/y/zSize of entity2.Last three parameters are almost alwasy p5 = 0, p6 = 1, p7 = 0


# Syntax

```js
entity.isAt(entity2, xSize, ySize, zSize, p5, p6, p7);
```

# Example

```js
// todo```

# Parameters

<li><b>entity2:</b> Entity handle or object</li>
<li><b>xSize:</b> float</li>
<li><b>ySize:</b> float</li>
<li><b>zSize:</b> float</li>
<li><b>p5:</b> Boolean</li>
<li><b>p6:</b> Boolean</li>
<li><b>p7:</b> int</li>

# Return value

<li><b>Undefined</b></li>