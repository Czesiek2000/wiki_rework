# Entity::getHeightAboveGround
Return height (z-dimension) above ground. 

Example: The pilot in a titan plane is 1.844176 above ground.

How can i convert it to meters?

Everything seems to be in meters, probably this too.


# Syntax

```js
entity.getHeightAboveGround();
```

# Example

```js
// todo
```

# Parameters


# Return value

<li><b><span style="color:#008017">Float</span></b></li>