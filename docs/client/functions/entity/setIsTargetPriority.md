# Entity::setIsTargetPriority

# Syntax

```js
entity.setIsTargetPriority(p1, p2);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>