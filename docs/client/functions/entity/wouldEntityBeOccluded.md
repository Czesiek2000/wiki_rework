# Entity::wouldEntityBeOccluded

First parameter was previously an Entity but after further research it is definitely a hash.


# Syntax

```js
mp.game.entity.wouldEntityBeOccluded(hash, x, y, z, p4);
```

# Example

```js
// todo
```

# Parameters

<li><b>hash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>