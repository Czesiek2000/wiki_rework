# Entity::getRoll

Displays the current ROLL axis of the entity [-180.0000/180.0000+](Sideways Roll) such as a vehicle tipped on its side


# Syntax

```js
entity.getRoll();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Float</span></b></li>