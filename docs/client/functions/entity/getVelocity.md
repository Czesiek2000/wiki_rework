# Entity::getVelocity

Get the velocity of the Entity


# Syntax

```js
entity.getVelocity();
```

# Example

```js
let vehicle = mp.players.local.vehicle

let velocity = vehicle.getVelocity(); // returns a object with x, y , z

vehicle.setVelocity(velocity.x, velocity.y, velocity.z) // setting the velocity which we got.
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Object Vector3</span></b></li>