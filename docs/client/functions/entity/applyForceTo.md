# Entity::applyForceTo
forceType - 0 to 5 (any number greater then 5 force does nothing)
* p8 - no effect (a quick disassembly will tell us what it does)

* isRel - specifies if the force direction is relative to direction entity is facing (true), or static world direction (false).

* p11/highForce - setting false will make the force really low
```cpp
enum ForceTypes { 
    MIN_FORCE = 0,
    MAX_FORCE_ROT = 1,
    MIN_FORCE_2 = 2,
    MAX_FORCE_ROT_2 = 3,
    //stable, good for custom handling FORCE_NO_ROT = 4, FORCE_ROT_PLUS_FORCE = 5};
```

# Syntax

```js
entity.applyForceTo(forceType, x, y, z, xRot, yRot, zRot, boneIndex, isRel, p10, highForce, p12, p13);
```

# Example

```js
// todo
```

# Parameters

<li><b>forceType:</b> : <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> : <b><span style="color:#008017">Float</span></b></li>
<li><b>boneIndex:</b> : <b><span style="color:#008017">Int</span></b></li>
<li><b>isRel:</b> : <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p10:</b> : <b><span style="color:#008017">Boolean</span></b></li>
<li><b>highForce:</b> : <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p12:</b> : <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p13:</b> : <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>