# Entity::createModelHideExcludingScriptObjects


# Syntax

```js
mp.game.entity.createModelHideExcludingScriptObjects(x, y, z, radius, model, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>model:</b> Model hash or name</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>