# Entity::isInZone

# Syntax

```js
entity.isInZone(zone);
```

# Example

```js
// todo
```

# Parameters

<li><b>zone:</b> <b><span style="color:#008017">String</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>