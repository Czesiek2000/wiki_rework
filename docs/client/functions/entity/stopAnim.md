# Entity::stopAnim

# Syntax

```js
entity.stopAnim(animation, animGroup, p3);
```

# Example

```js
// todo
```

# Parameters

<li><b>animation:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animGroup:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Unknown (to be checked)</b></li>