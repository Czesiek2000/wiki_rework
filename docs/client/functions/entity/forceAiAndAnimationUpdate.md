# Entity::forceAiAndAnimationUpdate

Based on **carmod_shop** script decompile this takes a vehicle parameter. It is called when repair is done on initial enter.


# Syntax

```js
entity.forceAiAndAnimationUpdate();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Undefined</b></li>