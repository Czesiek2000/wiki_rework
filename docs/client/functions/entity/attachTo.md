# Entity::attachTo
Attaches *entity1* to bone (boneIndex) of *entity2*.

* boneIndex - this is different to boneID, use `GET_Player_BONE_INDEX` to get the index from the **ID**. 

* use the index for attaching to specific bones. 

* entity1 will be attached to entity2's centre if bone index given doesn't correspond to bone indexes for that entity type.

* useSoftPinning - if set to false attached entity will not detach when fixedcollision - controls collision between the two entities (FALSE disables collision).

* isPed - pitch doesnt work when false and roll will only work on negative numbers (only peds)

* vertexIndex - position of vertexfixed

* Rot - if false it ignores entity vector 


# Syntax

```js
entity.attachTo(entity2, boneIndex, xPosOffset, yPosOffset, zPosOffset, xRot, yRot, zRot, p9, useSoftPinning, collision, isPed, vertexIndex, fixedRot);
```

# Example

```js
mp.events.add('arrestAttach', (cop, criminal) => {
	criminal.attachTo(cop.handle, 0, 0, 0, 0, 0, 0, 0, true, false, false, false, 0, false);
});
```

# Parameters

<li><b>entity2 :</b> Entity handle ( so use for example player.handle )</li>
<li><b>boneIndex :</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>xPosOffset :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPosOffset :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPosOffset :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot :</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9 :</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>useSoftPinning :</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>collision :</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>isPed :</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>vertexIndex :</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>fixedRot :</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>