# Entity::createModelHide
p5 = sets as true in scriptsSame as the comment for `CREATE_MODEL_SWAP` unless for some reason p5 affects it this only works with objects as well.

Network players do not see changes done with this.


# Syntax

```js
mp.game.entity.createModelHide(x, y, z, radius, model, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>model:</b> Model hash or name</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>