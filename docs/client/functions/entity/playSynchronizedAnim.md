# Entity::playSynchronizedAnim

p4 and p7 are usually 1000.0f.


# Syntax

```js
entity.playSynchronizedAnim(syncedScene, animation, propName, p4, p5, p6, p7);
```

# Example

```js
// todo
```

# Parameters

<li><b>syncedScene:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> <b><span style="color:#008017">String</span></b></li>

# Return value

<li><b>Undefined</b></li>