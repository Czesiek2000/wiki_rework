# Entity::isTouchingModel

# Syntax

```js
entity.isTouchingModel(modelHash);
```

# Example

```js
// todo
```

# Parameters

<li><b>modelHash:</b> Model hash or name</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>