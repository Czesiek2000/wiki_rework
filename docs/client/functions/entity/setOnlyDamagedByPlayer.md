# Entity::setOnlyDamagedByPlayer

# Syntax

```js
entity.setOnlyDamagedByPlayer(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>