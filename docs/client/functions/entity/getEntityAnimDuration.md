# Entity::getEntityAnimDuration
Console Hash: `0x8B5E3E3D = GET_ANIM_DURATION`




# Syntax

```js
mp.game.entity.getEntityAnimDuration(animDict, animName);
```

# Example

```js
// todo
```

# Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>

# Return value

<li><b>Undefined</b></li>