# Entity::setOnlyDamagedByRelationshipGroup

# Syntax

```js
entity.setOnlyDamagedByRelationshipGroup(p1, p2);
```

# Example

```js
// todo
```

# Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>

# Return value

<li><b>Undefined</b></li>