# Entity::setNoCollision
Calling this function, regardless of the 'unknown' value, disabled collision between two entities.

Importance of **entity1** and **2** order is unclear.


# Syntax

```js
entity.setNoCollision(entity2, collision);
```

# Example

```js
mp.events.add('enableCollisions', () => {
	mp.players.forEach(player => {
		mp.players.local.vehicle.setNoCollision(player.vehicle.handle, true);
		player.vehicle.setAlpha(255);
		player.setAlpha(255);
	});
});

mp.events.add('disableCollisions', () => {
	mp.players.forEach(player => {
		mp.players.local.vehicle.setNoCollision(player.vehicle.handle, false);
		player.vehicle.setAlpha(102);
		player.setAlpha(255);
	});
});
```

# Parameters

<li><b>entity2:</b> <b>Entity handle</b></li>
<li><b>collision:</b> <b><span style="color:#008017">Boolean</span></b>
<ul><li><b>false</b> = no collison, <b>true</b> = enabled collisions</li></ul></li>

# Return value

<li><b>Undefined</b></li>