# Entity::getVehicleIndexFromIndex

Simply returns whatever is passed to it (Regardless of whether the handle is valid or not).


# Syntax

```js
entity.getVehicleIndexFromIndex();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Vehicle handle or object</b></li>