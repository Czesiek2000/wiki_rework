# Entity::setCoords
p7 is always 1 in the scripts. 

Set to 1, an area around the destination coords for the moved entity is cleared from other entities.  

Often ends with 1, 0, 0, 1); in the scripts. It works. 

Axis - Invert Axis Flags


# Syntax

```js
entity.setCoords(xPos, yPos, zPos, xAxis, yAxis, zAxis, clearArea);
```

# Example

```js
//Set your ped off in to the sky
mp.players.local.setCoords(mp.players.local.getCoords(true).x, mp.players.local.getCoords(true).y, mp.players.local.getCoords(true).z + 500.0, true, false, false, false);
```

# Parameters

<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>yAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>zAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>clearArea:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>