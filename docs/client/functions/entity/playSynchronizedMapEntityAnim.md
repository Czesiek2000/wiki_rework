# Entity::playSynchronizedMapEntityAnim


# Syntax

```js
mp.game.entity.playSynchronizedMapEntityAnim(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11);
```

# Example

```js
// todo
```

# Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> unknown (to be checked)</li>
<li><b>p11:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b>Undefined</b></li>