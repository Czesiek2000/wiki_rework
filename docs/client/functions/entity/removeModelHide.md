# Entity::removeModelHide


# Syntax

```js
mp.game.entity.removeModelHide(p0, p1, p2, p3, p4, p5);
```

# Example

```js
// todo
```

# Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>

# Return value

<li><b>Undefined</b></li>