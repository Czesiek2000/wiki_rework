# Entity::getAnimCurrentTime

Returns a float value representing animation's current playtime with respect to its total playtime. This value increasing in a range from **[0 to 1]** and wrap back to *0* when it reach *1*.

Example:
* 0.000000 - mark the starting of animation.
* 0.500000 - mark the midpoint of the animation.
* 1.000000 - mark the end of animation.


# Syntax

```js
entity.getAnimCurrentTime(animDict, animName);
```

# Example

```js
// Example of stopping animation at the end and prevent loop.

let player = mp.players.local;

setInterval(() => {
	if(player.getAnimCurrentTime(path, name) > 0.95){
		player.stopAnimTask(path, name, 3.0);
		clearInterval(this);
	}
}, 1);
```

# Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>

# Return value

<li><b>Undefined</b></li>