# Entity::hasClearLosToInFront
Has the **entity1** got a clear line of sight to the other **entity2** from the direction **entity1** is facing.

This is one of the most CPU demanding `BOOL` natives in the game; avoid calling this in things like nested for-loops.


# Syntax

```js
entity.hasClearLosToInFront(entity2);
```

# Example

```js
// todo
```

# Parameters

<li><b>entity2:</b> Entity handle or object</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>