# Entity::getPedIndexFromIndex

Simply returns whatever is passed to it (Regardless of whether the handle is valid or not).


# Syntax

```js
entity.getPedIndexFromIndex();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Ped handle or object</b></li>