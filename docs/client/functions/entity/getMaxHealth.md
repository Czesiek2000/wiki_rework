# Entity::getMaxHealth
Return an integer value of entity's maximum health.
Example:
- Player: 200
- Ped: 150


# Syntax

```js
entity.getMaxHealth();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Int</span></b></li>