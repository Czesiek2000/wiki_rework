# Entity::getLastMaterialHitBy

# Syntax

```js
entity.getLastMaterialHitBy();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b>Model hash or name</b></li>