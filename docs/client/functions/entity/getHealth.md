# Entity::getHealth
Returns an integer value of entity's current health.Example of range for ped:- Player [0 to 200]- Ped [100 to 200]- Vehicle [0 to 1000]- Object [0 to 1000]Health is actually a float value but this native casts it to int.In order to get the actual value, do:float health = *(float *)(entityAddress + 0x280);


# Syntax

```js
entity.getHealth();
```

# Example

```js
var currentHealth = mp.players.local.getHealth();
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Int</span></b></li>