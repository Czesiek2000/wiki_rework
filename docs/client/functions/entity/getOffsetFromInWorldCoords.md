# Entity::getOffsetFromInWorldCoords

Offset values are relative to the entity.

x = left/right

y = forward/backward

z = up/down


# Syntax

```js
entity.getOffsetFromInWorldCoords(offsetX, offsetY, offsetZ);
```

# Example

```js
// todo
```

# Parameters

<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>