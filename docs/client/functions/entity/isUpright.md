# Entity::isUpright
# Syntax

```js
entity.isUpright(angle);
```

# Example

```js
// todo
```

# Parameters

<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>