# Entity::isCollisonDisabled

**console hash: 0xE8C0C629**


# Syntax

```js
entity.isCollisonDisabled();
```

# Example

```js
// todo
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>