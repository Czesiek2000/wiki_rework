# Entity::hasClearLosTo
traceType is always 17 in the scripts.

*JulioNIB: There is other codes used for traceType:

19 - in jewelry_prep1a

126 - in am_hunt_the_beast

256 & 287 - in fm_mission_controller


# Syntax

```js
entity.hasClearLosTo(entity2, traceType);
```

# Example

```js
// todo
```

# Parameters

<li><b>entity2:</b> Entity handle or object</li>
<li><b>traceType:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>