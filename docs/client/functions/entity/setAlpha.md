# Entity::setAlpha
skin - everything alpha except skinSet entity alpha level. 

Ranging from **0** to **255** but chnages occur after every **20** percent (after every 51).


# Syntax

```js
entity.setAlpha(alphaLevel);
```

# Example

```js
mp.events.add('enableCollisions', () => {
	mp.players.forEach(player => {
		mp.players.local.vehicle.setNoCollision(player.vehicle.handle, true);
		player.vehicle.setAlpha(255);
		player.setAlpha(255);
	});
});

mp.events.add('disableCollisions', () => {
	mp.players.forEach(player => {
		mp.players.local.vehicle.setNoCollision(player.vehicle.handle, false);
		player.vehicle.setAlpha(102);
		player.setAlpha(255);
	});
});
```

# Parameters

<li><b>alphaLevel:</b> <b><span style="color:#008017">Int</span></b></li>

### Old arguments

<ul><li><b>alphaLevel:</b> int</li>
<li><b>skin:</b> Boolean</li>
<li>The "skin" parameter is used only for peds, otherwise it will cause argument count exceptions if used.</li>
</ul>

# Return value

<li><b>Undefined</b></li>