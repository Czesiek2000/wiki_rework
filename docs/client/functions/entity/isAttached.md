# Entity::isAttached

It appears that, once you are driving a vehicle... you are attached to it.


# Syntax

```js
entity.isAttached();
```

# Example

```js
mp.keys.bind(0x32, false, () => { // '2'
	mp.game.graphics.notify(`~r~${player.isAttached()}.`);
	if(player.isAttached()) {
		// player is attached to something
	}
});
```

# Parameters

No parameters here

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>