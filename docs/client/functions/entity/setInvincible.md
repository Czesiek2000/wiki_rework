# Entity::setInvincible
Sets a ped or an object totally invincible. It doesn't take any kind of damage. Peds will not ragdoll on explosions and the tazer animation won't apply either.

If you use this for a ped and you want Ragdoll to stay enabled, then do:
```cpp
*(DWORD *)(pedAddress + 0x188) |= (1 << 9);
```
Use this if you want to get the invincibility status: 

```cpp
bool IsPedInvincible(Ped ped) { auto addr = getScriptHandleBaseAddress(ped);	 
    if (addr) { 
        DWORD flag = *(DWORD *)(addr + 0x188); 
        return ((flag & (1 << 8)) != 0) || ((flag & (1 << 9)) != 0); 
    }
    return false; 
}
```

# Syntax

```js
entity.setInvincible(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>