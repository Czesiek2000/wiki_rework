# Entity::isAttachedTo

# Syntax

```js
entity.isAttachedTo(to);
```

# Example

```js
// todo
```

# Parameters

<li><b>to:</b> Entity handle or object</li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>