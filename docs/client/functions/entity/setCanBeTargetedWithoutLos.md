# Entity::setCanBeTargetedWithoutLos

Sets whether the entity can be targeted without being in line-of-sight.


# Syntax

```js
entity.setCanBeTargetedWithoutLos(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b>Undefined</b></li>