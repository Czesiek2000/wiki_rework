# Entity::doesExist

Checks if the Entity exists


# Syntax

```js
entity.doesExist();
```

# Example

```js
// todo
```

# Parameters

No parameters

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>