# Entity::getModel

Returns the model hash from the entity


# Syntax

```js
entity.getModel();
```

# Example

```js
mp.events.add('render', () => {

    //Get first spawned vehicle
    var vehicleObject = mp.vehicles.atRemoteId(0);
    //Check if the object is valid
    if(vehicleObject)
    {
        //Get vehicle distant and if under 4 show the model name
        var vehicleDistant = LastVehicleDistant =  mp.game.gameplay.getDistanceBetweenCoords(mp.players.local.position.x, mp.players.local.position.y, mp.players.local.position.z, vehicleObject.position.x, vehicleObject.position.y, vehicleObject.position.z, false);
        if(vehicleDistant < 4.0)
        {
            //Display it's name
            var vehicleModel = vehicleObject.getModel(); // this is same as vehicle.model
            mp.game.graphics.drawText(`Interact With ~b~[${mp.game.vehicle.getDisplayNameFromVehicleModel(vehicleModel)}]`, [vehicleObject.position.x, vehicleObject.position.y, vehicleObject.position.z], { 
                font: 0,
                scale: [0.4, 0.4],
                color: [255, 255, 255, 185],
                outline: false
                });
        }
    }

})
```

# Parameters

No parameters here

# Return value

<li><b>Model hash or name</b></li>