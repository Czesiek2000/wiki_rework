# Entity::getNearestPlayerToOnTeam

# Syntax

```js
entity.getNearestPlayerToOnTeam(team);
```

# Example

```js
// todo
```

# Parameters

<li><b>team:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Player</span></b></li>