# Entity::getWorldPositionOfBone

Returns the coordinates of an entity-bone.


# Syntax

```js
entity.getWorldPositionOfBone(boneIndex);
```

# Example

```js
// todo
```

# Parameters

<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b></li>

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>