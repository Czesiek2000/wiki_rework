# Entity::hasBeenDamagedBy

Entity 1 = Victim

Entity 2 = Attacker

p2 seems to always be 1


# Syntax

```js
entity.hasBeenDamagedBy(entity2, p2);
```

# Example

```js
// todo
```

# Parameters

<li><b>entity2:</b> Entity handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>