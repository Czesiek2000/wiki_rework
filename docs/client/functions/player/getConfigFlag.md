# Player::getConfigFlag


Gets player config flag.

See available flags here: [Player Config Flags](../../../tables/configFlags.md)

## Syntax

```js
player.getConfigFlag(flagId, true);
```

## Example

```js
// todo
```

## Parameters

<li><span style="font-weight:bold; color:red;">*</span><b>flagId:</b> <b><span style="color:#008017">Int</span></b> (<a href="/index.php?title=Player_Config_Flags" title="Player Config Flags">Flag IDs</a>)</li>

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>