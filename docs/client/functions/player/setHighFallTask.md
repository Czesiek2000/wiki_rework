# Player::setHighFallTask

Makes the ped ragdoll like when falling from a great height


## Syntax

```js
player.setHighFallTask(p1, p2, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>