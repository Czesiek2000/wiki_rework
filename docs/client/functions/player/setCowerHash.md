# Player::setCowerHash

p1: Only `CODE_HUMAN_STAND_COWER` found in the b617d scripts.


## Syntax

```js
player.setCowerHash(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>