# Player::setCanBeKnockedOffVehicle

* 0 = can (bike)

* 1 = can't (bike)

* 2 = unk 

* 3 = unk


## Syntax

```js
player.setCanBeKnockedOffVehicle(state);
```

## Example

```js
// todo
```

## Parameters

<li><b>state:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>