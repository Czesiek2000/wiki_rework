# Player::taskArrest

Example from 'me_amanda1.ysc.c4':

```cpp
Player::TASK_ARREST_PED(l_19F /* This is a Ped */ , PLAYER::PLAYER_Player_ID());
//Example from 'armenian1.ysc.c4':

if (!Player::IS_Player_INJURED(l_B18[0/*1*/])) { 
    Player::TASK_ARREST_PED(l_B18[0/*1*/], PLAYER::PLAYER_Player_ID());
}
```

I would love to have time to experiment to see if a player Ped can arrest another player. Might make for a good cop mod.

Looks like only the player can be arrested this way. Peds react and try to arrest you if you task them, but the player charater doesn't do anything if tasked to arrest another player.


## Syntax

```js
player.taskArrest(target);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>

## Return value

<b>Undefined</b>