# Player::giveHelmet
* PoliceMotorcycleHelmet: 1024
* RegularMotorcycleHelmet: 4096
* FiremanHelmet: 16384
* PilotHeadset: 32768
* PilotHelmet: 65536

p2 is generally 4096 or 16384 in the scripts. 

p1 varies between 1 and 0.


## Syntax

```js
player.giveHelmet(cannotRemove, helmetFlag, textureIndex);
```

## Example

```js
// todo
```

## Parameters

<li><b>cannotRemove:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>helmetFlag:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>textureIndex:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>