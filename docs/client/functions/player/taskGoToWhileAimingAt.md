# Player::taskGoToWhileAimingAt

shootatEntity: If `true`, peds will shoot at Entity till it is dead. 
If `false`, peds will just walk till they reach the entity and will cease shooting.


## Syntax

```js
player.taskGoToWhileAimingAtEntity(entityToWalkTo, entityToAimAt, speed, shootatEntity, p5, p6, p7, p8, firingPattern);
```

## Example

```js
// todo
```

## Parameters

<li><b>entityToWalkTo:</b> Entity handle or object</li>
<li><b>entityToAimAt:</b> Entity handle or object</li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>shootatEntity:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>firingPattern:</b> Model hash or name</li>

## Return value

<b>Undefined</b>