# Player::controlMountedWeapon

Forces the ped to use the mounted weapon.

Returns false if task is not possible.


## Syntax

```js
player.controlMountedWeapon();
```

## Example

```js
// todo
```

## Parameters

<li><b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>