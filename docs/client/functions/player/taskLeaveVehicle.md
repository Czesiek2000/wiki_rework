# Player::taskLeaveVehicle

Flags from decompiled scripts:

0 = normal exit and closes door.

1 = normal exit and closes door.

16 = teleports outside, door kept closed.

64 = normal exit and closes door, maybe a bit slower animation than 0.

256 = normal exit but does not close the door.

4160 = ped is throwing himself out, even when the vehicle is still.

262144 = ped moves to passenger seat first, then exits normally

Others to be tried out: 320, 512, 131072.

Forces the local player to exit his current vehicle


## Syntax

```js
player.taskLeaveVehicle(vehicle, flags);
```

## Example

```js
let localVeh = mp.players.local.vehicle;
if (localVeh) {
    mp.players.local.taskLeaveVehicle(localVeh.handle, 0);
}
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>