# Player::getHeadOverlayValue

Likely a char, if that overlay is not set, e.i. 'None' option, returns 255;


## Syntax

```js
player.getHeadOverlayValue(overlayID);
```

## Example

```js
// todo
```

## Parameters

<li><b>overlayID:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>