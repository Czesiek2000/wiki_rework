# Player::getBoneCoords

Gets the position of the specified bone of the specified player.

ped: The ped to get the position of a bone from.

boneId: The ID of the bone to get the position from. This is NOT the index.

offsetX: The X-component of the offset to add to the position relative to the bone's rotation.

offsetY: The Y-component of the offset to add to the position relative to the bone's rotation.

offsetZ: The Z-component of the offset to add to the position relative to the bone's rotation.

[List of bones](../../../tables/bonesId.md)


## Syntax

```js
player.getBoneCoords(boneId, offsetX, offsetY, offsetZ);
```

## Example

```js
// todo
```

## Parameters

<li><b>boneId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>