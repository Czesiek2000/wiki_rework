# Player::addArmourTo

Same as `SET_Player_ARMOUR`, but ADDS **amount** to the armor the Ped already has.


## Syntax

```js
player.addArmourTo(amount);
```

## Example

```js
// todo
```

## Parameters

<li><b>amount:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>