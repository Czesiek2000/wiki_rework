# Player::getNumberOfPropDrawableVariations
[List of component/props ID](gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.getNumberOfPropDrawableVariations(propId);
```

## Example

```js
// todo
```

## Parameters

<li><b>propId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>