# Player::isDeadOrDying

Seems to consistently return true if the ped is dead.

p1 is always passed 1 in the scripts.

I suggest to remove `OR_DYING` part, because it does not detect dying phase.

That's what the devs call it, cry about it.lol


## Syntax

```js
player.isDeadOrDying(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>