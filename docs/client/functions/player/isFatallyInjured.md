# Player::isFatallyInjured

MulleDK19: Gets a value indicating whether this ped's health is below its fatally injured threshold. The default threshold is 100.

If the handle is invalid, the function returns true.


## Syntax

```js
player.isFatallyInjured();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>