# Player::setSweat

Sweat is set to 100.0 or 0.0 in the decompiled scripts.


## Syntax

```js
player.setSweat(sweat);
```

## Example

```js
// todo
```

## Parameters

<li><b>sweat:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>