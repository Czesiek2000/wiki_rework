# Player::isConversationDead

## Syntax

```js
player.isConversationDead();
```

## Example

```js
// todo
```

## Parameters

<li><b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b><b><span style="color:#008017">Boolean</span></b></b>