# Player::taskVehiclePark

Modes:

0 - ignore heading

1 - park forward

2 - park backwardsDepending on the angle of approach, the vehicle can park at the specified heading or at its exact opposite (-180) angle.

Radius seems to define how close the vehicle has to be -after parking- to the position for this task considered completed. 

If the value is too small, the vehicle will try to park again until it's exactly where it should be. 

20.0 Works well but lower values don't, like the radius is measured in centimeters or something.


## Syntax

```js
player.taskVehiclePark(vehicle, x, y, z, heading, mode, radius, keepEngineOn);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>mode:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>keepEngineOn:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>