# Player::setCanBeTargettedByPlayer

## Syntax

```js
player.setCanBeTargettedByPlayer(player, toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>player:</b> <b><span style="color:#008017">Player</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>