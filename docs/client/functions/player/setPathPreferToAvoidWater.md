# Player::setPathPreferToAvoidWater

## Syntax

```js
player.setPathPreferToAvoidWater(avoidWater);
```

## Example

```js
// todo
```

## Parameters

<li><b>avoidWater:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>