# Player::taskShuffleToNextVehicleSeat

*MulleDK19*: Makes the specified ped shuffle to the next vehicle seat.

The ped MUST be in a vehicle and the vehicle parameter MUST be the ped's current vehicle.


## Syntax

```js
player.taskShuffleToNextVehicleSeat(vehicle);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>

## Return value

<b>Undefined</b>