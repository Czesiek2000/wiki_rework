# Player::setCombatAbility

**100** would equal attackless then 50ish would mean run away.

Only the values **0**, **1** and **2** occur in the decompiled scripts. 

Most likely refers directly to the values also described in combatbehaviour.meta:

* 0: CA_Poor
* 1: CA_Average
* 2: CA_Professional

Tested this and got the same results as the first explanation here. Could not find any difference between **0**, **1** and **2**.


## Syntax

```js
player.setCombatAbility(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>