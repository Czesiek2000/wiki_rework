# Player::forceMotionState
Some motionstate hashes are

* 0xec17e58 (standing idle), 
* 0xbac0f10b (nothing?), 
* 0x3f67c6af (aiming with pistol 2-h),
* 0x422d7a25 (stealth),
* 0xbd8817db,
* 0x916e828c

and those for the strings
* 'motionstate_idle', 
* 'motionstate_walk', 
* 'motionstate_run',
* 'motionstate_actionmode_idle',
* 'motionstate_actionmode_walk'.

Regarding p2, p3 and p4: 

Most common is 0, 0, 0); followed by 0, 1, 0); and 1, 1, 0); in the scripts. 

p4 is very rarely something other than 0.

## Syntax

```js
player.forceMotionState(motionStateHash, p2, p3, p4);
```

## Example

```js
// todo
```

## Parameters

<li><b>motionStateHash:</b> Model hash or name</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>