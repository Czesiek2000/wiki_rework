# Player::getResetFlag

## Syntax

```js
player.getResetFlag(flagId);
```

## Example

```js
// todo
```

## Parameters

<li><b>flagId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>