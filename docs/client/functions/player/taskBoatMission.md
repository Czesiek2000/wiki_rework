# Player::taskBoatMission

You need to call 

`Player::SET_BLOCKING_OF_NON_TEMPORARY_EVENTS` after `TASK_BOAT_MISSION` in order for the task to execute.

Working example 

```cpp
float vehicleMaxSpeed = VEHICLE::_GET_VEHICLE_MAX_SPEED(ENTITY::GET_ENTITY_MODEL(pedVehicle));
Player::TASK_BOAT_MISSION(pedDriver, pedVehicle, 0, 0, waypointCoord.x, waypointCoord.y, waypointCoord.z, 4, vehicleMaxSpeed, 786469, -1.0, 7);Player::SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, 1);
```

P8 appears to be driving style flag - see [gtaforum](gtaforums.com/topic/822314-guide-driving-styles/ for documentation)


## Syntax

```js
player.taskBoatMission(boat, p2, p3, x, y, z, p7, maxSpeed, p9, p10, p11);
```

## Example

```js
// todo
```

## Parameters

<li><b>boat:</b> Vehicle handle or object</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>maxSpeed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>
<li><b>p10:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p11:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>