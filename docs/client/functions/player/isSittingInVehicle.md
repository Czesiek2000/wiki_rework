# Player::isSittingInVehicle

Detect if ped is sitting in the specified vehicle

[True/False]


## Syntax

```js
player.isSittingInVehicle(vehicle);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>