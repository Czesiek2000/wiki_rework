# Player::clearAllProps

[List of component/props ID](gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.clearAllProps();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<b>Undefined</b>