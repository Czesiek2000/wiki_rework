# Player::getParachuteLandingType
-1: no landing

0: landing on both feet

1: stumbling

2: rolling

3: ragdoll


## Syntax

```js
player.getParachuteLandingType();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>