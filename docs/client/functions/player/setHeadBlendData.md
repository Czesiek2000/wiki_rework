# Player::setHeadBlendData

The 'shape' parameters control the shape of the ped's face. The 'skin' parameters control the skin tone. ShapeMix and skinMix control how much the first and second IDs contribute,(typically mother and father.) 

ThirdMix overrides the others in favor of the third IDs. IsParent is set for 'children' of the player character's grandparents during old-gen character creation. It has unknown effect otherwise.

The IDs start at zero and go Male Non-DLC, Female Non-DLC, Male DLC, and Female DLC.

**THEAETIK**: For more info please refer to this [topic](https://gtaforums.com/topic/858970-all-gtao-face-ids-pedset-ped-head-blend-data-explained).

The first ids from 0 to 45 changing mother shape (`shapeFirstID`) or mother skin color (`skinFirstID`)

The second ids from 0 to 45 changing father shape (`shapeSecondID`) and father skin color (`skinSecondID`)

The third ids does not change anything, you can leave this with 0

You should leave `thirdMix` to the 0, in other way father's skin color will be white everytimeSeems like isParent do not change anything (true or false)

Shape and skin mixing values variable between 0.0f and 1.0f


See also [Player::setFaceFeature](./setFaceFeature.md).


## Syntax

```js
mp.players.local.setHeadBlendData(shapeFirstID, shapeSecondID, shapeThirdID, skinFirstID, skinSecondID, skinThirdID, shapeMix, skinMix, thirdMix, isParent);
```

## Example

```js
// todo
```

## Parameters

<li><b><span style="font-weight:bold; color:red;">*</span>shapeFirstID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>shapeSecondID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>shapeThirdID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>skinFirstID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>skinSecondID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>skinThirdID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>shapeMix:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>skinMix:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>thirdMix:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b><span style="font-weight:bold; color:red;">*</span>isParent:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>