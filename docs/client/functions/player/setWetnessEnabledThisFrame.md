# Player::setWetnessEnabledThisFrame

combined with `Player::SET_Player_WETNESS_HEIGHT()`, this native makes the ped drenched in water up to the height specified in the other function


## Syntax

```js
player.setWetnessEnabledThisFrame();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Undefined</b></li>