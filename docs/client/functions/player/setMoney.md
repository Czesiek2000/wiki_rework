# Player::setMoney

Maximum possible amount of money on MP is 2000. 

*~JX*

Maximum amount that a ped can theoretically have is **65535 (0xFFFF)** since the amount is stored as an unsigned short (uint16_t) value.


## Syntax

```js
mp.players.local.setMoney(amount);
```

## Example

```js
// todo
```

## Parameters

<li><b>amount:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>