# Player::setCombatRange

Only the values 0, 1 and 2 occur in the decompiled scripts. 

Most likely refers directly to the values also described as AttackRange in combat behaviour.

meta:
* 0: CR_Near
* 1: CR_Medium
* 2: CR_Far


## Syntax

```js
player.setCombatRange(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>