# Player::getIsTaskActive

from docks_heistb.c4:
```cpp
Player::GET_IS_TASK_ACTIVE(PLAYER::PLAYER_Player_ID(), 2))
```

[Known Tasks](https://pastebin.com/2gFqJ3Px)


## Syntax

```js
player.getIsTaskActive(taskNumber);
```

## Example

```js
// todo
```

## Parameters

<li><b>taskNumber:</b> int</li>

## Return value

<b>Undefined</b>