# Player::setTargetLossResponse

Only 1 and 2 appear in the scripts. 

combatbehaviour.meta seems to only have TLR_SearchForTarget for all peds, but we don't know if that's 1 or 2.


## Syntax

```js
player.setTargetLossResponse(responseType);
```

## Example

```js
// todo
```

## Parameters

<li><b>responseType:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>