# Player::getGroupIndex

Returns the group id of which the specified ped is a member of.


## Syntax

```js
player.getGroupIndex();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b><span style="color:#008017">Int</span></b></li>