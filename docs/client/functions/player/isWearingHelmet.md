# Player::isWearingHelmet

Returns true if the ped passed through the parenthesis is wearing a helmet.

*-UNBOUND-*


## Syntax

```js
player.isWearingHelmet();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>
