# Player::taskVehicleFollowWaypointRecording

task_vehicle_follow_waypoint_recording (Ped p0, Vehicle p1, string p2, int p3, int p4, int p5, int p6, float.x p7, float.Y p8, float.Z p9, bool p10, int p11)

p2 = Waypoint recording string (found in` update/update.rpf/x64/levels/gta5/waypointrec.rpf`

p3 = 786468

p4 = 0

p5 = 16

p6 = -1 (angle?)

p7/8/9 = usually v3.zero

p10 = bool (repeat?)

p11 = 1073741824

*khorio*


## Syntax

```js
player.taskVehicleFollowWaypointRecording(vehicle, WPRecording, p3, p4, p5, p6, p7, p8, p9);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>WPRecording:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>