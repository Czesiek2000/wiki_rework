# Player::setLodMultiplier

## Syntax

```js
player.setLodMultiplier(multiplier);
```

## Example

```js
// todo
```

## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>