# Player::setMoveRateOverride

Min: 0.00Max: 10.00Can be used in combo with fast run cheat.

When value is set to 10.00:

- Sprinting without fast run cheat: 66 m/s
- Sprinting with fast run cheat: 77 m/s

Does not need to be looped!

**Note: According to IDA for the Xbox360 xex, when they check bgt they seem to have the min to 0.0f, but the max set to 1.15f not 10.0f.**


## Syntax

```js
player.setMoveRateOverride(value);
```

## Example

```js
// todo
```

## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>