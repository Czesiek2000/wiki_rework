# Player::taskShootAtCoord

## Syntax

```js
player.taskShootAtCoord(x, y, z, duration, firingPattern);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>firingPattern:</b> Model hash or name</li>

## Return value

<b>Undefined</b>