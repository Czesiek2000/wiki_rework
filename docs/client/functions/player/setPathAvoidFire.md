# Player::setPathAvoidFire

## Syntax

```js
player.setPathAvoidFire(avoidFire);
```

## Example

```js
// todo
```

## Parameters

<li><b>avoidFire:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>