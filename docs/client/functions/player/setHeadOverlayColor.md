# Player::setHeadOverlayColor

Used for freemode (online) characters.

ColorType is 1 for eyebrows, beards, and chest hair; 

2 for blush and lipstick; 

and 0 otherwise, though not called in those cases.

Called after `SET_Player_HEAD_OVERLAY()`.

| OverlayID | Part | 
| ---  | ---  | 
| 0 | Blemishes | 
| 1 | Facial Hair | 
| 2 | Eyebrows | 
| 3 | Ageing | 
| 4 | Makeup | 
| 5 | Blush | 
| 6 | Complexion | 
| 7 | Sun Damage | 
| 8 | Lipstick | 
| 9 | Moles/Freckles | 
| 10 | Chest Hair | 
| 11 | Body Blemishes | 
| 12 | Add Body Blemishes | 



## Syntax

```js
player.setHeadOverlayColor(overlayID, colorType, colorID, secondColorID);
```

## Example

```js
// todo
```

## Parameters

<li><b>overlayID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>secondColorID:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>