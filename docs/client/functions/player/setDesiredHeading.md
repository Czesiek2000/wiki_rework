# Player::setDesiredHeading

## Syntax

```js
player.setDesiredHeading(heading);
```

## Example

```js
// todo
```

## Parameters

<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>