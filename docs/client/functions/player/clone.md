# Player::clone
p3 - last parameter does not mean ped handle is returned

maybe a quick view in disassembly will tell us what is actually does

Example of Cloning Your Player:

```cpp
CLONE_PED(PLAYER_Player_ID(), GET_ENTITY_HEADING(PLAYER_Player_ID()), 0, 1);
```


## Syntax

```js
player.clone(heading, networkHandle, pedHandle);
```

## Example

```js
// todo
```

## Parameters

<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>networkHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>pedHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>