# Player::setStealthMovement

This function will disable stealth movement ability (ustually ctrl button)


p1 is usually 0 in the scripts. action is either 0 or a pointer to 'DEFAULT_ACTION'.


## Syntax

```js
player.setStealthMovement(p1, action);
```

## Example

```js
// Disable Stealth Movement
mp.players.local.setStealthMovement(false, '0');
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>action:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>