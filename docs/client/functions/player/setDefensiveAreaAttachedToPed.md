# Player::setDefensiveAreaAttachedToPed

## Syntax

```js
player.setDefensiveAreaAttachedToPed(attachPed, p2, p3, p4, p5, p6, p7, p8, p9, p10);
```

## Example

```js
// todo
```

## Parameters

<li><b>attachPed:</b> Ped handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>