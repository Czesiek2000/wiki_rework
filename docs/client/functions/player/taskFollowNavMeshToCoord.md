# Player::taskFollowNavMeshToCoord

If no timeout, set timeout to -1.


## Syntax

```js
player.taskFollowNavMeshToCoord(x, y, z, speed, timeout, stoppingRange, persistFollowing, unk);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>stoppingRange:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>persistFollowing:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unk:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>