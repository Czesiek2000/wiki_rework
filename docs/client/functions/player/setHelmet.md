# Player::setHelmet

## Syntax

```js
player.setHelmet(canWearHelmet);
```

## Example

```js
mp.players.local.setHelmet(false) // Disable helmet on bikes
```

## Parameters

<li><b>canWearHelmet:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>