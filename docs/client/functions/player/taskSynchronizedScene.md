# Player::taskSynchronizedScene

```cpp
Player::TASK_SYNCHRONIZED_SCENE(ped, scene, '[email protected]@[email protected]_car', 'get_in', 1000.0, -8.0, 4, 0, 0x447a0000, 0);
```

## Syntax

```js
player.taskSynchronizedScene(scene, animDictionary, animationName, speed, speedMultiplier, duration, flag, playbackRate, p9);
```

## Example

```js
// todo
```

## Parameters

<li><b>scene:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animationName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speedMultiplier:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>flag:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>playbackRate:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>