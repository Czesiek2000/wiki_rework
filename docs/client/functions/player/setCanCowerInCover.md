# Player::setCanCowerInCover

It simply makes the said ped to cower behind cover object(wall, desk, car)

Peds flee attributes must be set to not to flee, first. 

Else, most of the peds, will just flee from gunshot sounds or any other panic situations.

*-YCSM*


## Syntax

```js
player.setCanCowerInCover(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> Boolean</li>

## Return value

<b>Undefined</b>