# Player::setHairColor

Used for freemode (online) characters.


## Syntax

```js
player.setHairColor(colorID, highlightColorID);
```

## Example

```js
// todo
```

## Parameters

<li><b>colorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>highlightColorID:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>