# Player::taskPutDirectlyIntoMelee

**from armenian3.c4**

```cpp
Player::TASK_PUT_Player_DIRECTLY_INTO_MELEE(PlayerPed, armenianPed, 0.0, -1.0, 0.0, 0);
```

## Syntax

```js
player.taskPutDirectlyIntoMelee(meleeTarget, p2, p3, p4, p5);
```

## Example

```js
// todo
```

## Parameters

<li><b>meleeTarget:</b> Ped handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>