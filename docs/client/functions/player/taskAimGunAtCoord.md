# Player::taskAimGunAtCoord

## Syntax

```js
player.taskAimGunAtCoord(x, y, z, time, p5, p6);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>