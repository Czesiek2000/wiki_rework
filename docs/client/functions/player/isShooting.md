# Player::isShooting

Returns whether the specified ped is shooting.


## Syntax

```js
player.isShooting();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>
