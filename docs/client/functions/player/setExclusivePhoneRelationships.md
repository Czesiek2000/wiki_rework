# Player::setExclusivePhoneRelationships

In `appcamera.c4`, Line *106*:
```cpp
if (VEHICLE::IS_VEHICLE_DRIVEABLE(Player::SET_EXCLUSIVE_PHONE_RELATIONSHIPS(PLAYER::PLAYER_Player_ID()), 0))
```
So return type could be a vehicle?!

Hash collision - gets the vehicle handle from ped which is about entering the vehicle!

sfink: agreed, 100%

Proper name is `GET_VEHICLE_Player_IS_ENTERING`


## Syntax

```js
player.setExclusivePhoneRelationships();
```

## Example

```js
// todo
```

## Parameters

<li><b>Vehicle handle or object</b></li>

## Return value

<b>Undefined</b>