# Player::hideBloodDamageByZone

## Syntax

```js
player.hideBloodDamageByZone(p1, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>