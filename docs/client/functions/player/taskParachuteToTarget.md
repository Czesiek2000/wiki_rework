# Player::taskParachuteToTarget

makes ped parachute to coords x y z. 

Works well with `PATHFIND::GET_SAFE_COORD_FOR_PED`


## Syntax

```js
player.taskParachuteToTarget(x, y, z);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>