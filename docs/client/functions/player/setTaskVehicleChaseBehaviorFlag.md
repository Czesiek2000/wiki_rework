# Player::setTaskVehicleChaseBehaviorFlag

## Syntax

```js
player.setTaskVehicleChaseBehaviorFlag(flag, set);
```

## Example

```js
// todo
```

## Parameters

<li><b>flag:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>set:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>