# Player::canRagdoll

Prevents the ped from going limp.

Example: Can prevent peds from falling when standing on moving vehicles.

*Edited by: Enumerator*


## Syntax

```js
player.canRagdoll();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>