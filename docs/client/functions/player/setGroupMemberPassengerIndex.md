# Player::setGroupMemberPassengerIndex

## Syntax

```js
player.setGroupMemberPassengerIndex(index);
```

## Example

```js
// todo
```

## Parameters

<li><b>index:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>