# Player::setHelmetPropIndex

[List of component/props ID](https://gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.setHelmetPropIndex(propIndex);
```

## Example

```js
// todo
```

## Parameters

<li><b>propIndex:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>