# Player::setCanTeleportToGroupLeader

This only will teleport the ped to the group leader if the group leader teleports (sets coords).

Only works in singleplayer


## Syntax

```js
player.setCanTeleportToGroupLeader(groupHandle, toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>groupHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>