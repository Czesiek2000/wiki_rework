# Player::setResetRagdollFlag

There seem to be **26** flags


## Syntax

```js
player.setResetRagdollFlag(flag);
```

## Example

```js
// todo
```

## Parameters

<li><b>flag:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>