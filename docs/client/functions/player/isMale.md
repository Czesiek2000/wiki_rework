# Player::isMale

Returns `true` / `false` if the ped is / isn't male.

*Edited by: Enumerator*


## Syntax

```js
player.isMale();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>