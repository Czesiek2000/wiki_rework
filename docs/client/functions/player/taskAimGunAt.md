# Player::taskAimGunAt

duration: the amount of time in milliseconds to do the task. -1 will keep the task going until either another task is applied, or `CLEAR_ALL_TASKS()` is called with the ped


## Syntax

```js
player.taskAimGunAt(entity, duration, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>entity:</b> Entity handle or object</li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>