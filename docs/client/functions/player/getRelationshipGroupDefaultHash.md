# Player::getRelationshipGroupDefaultHash

## Syntax

```js
player.getRelationshipGroupDefaultHash();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Model hash or name</b></li>