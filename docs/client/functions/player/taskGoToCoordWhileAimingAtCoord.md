# Player::taskGoToCoordWhileAimingAtCoord

movement_speed: mostly 2f, but also 1/1.2f, etc.

p8: always false

p9: 2f

p10: 0.5f

p11: true

p12: 0 / 512 / 513, etc.

p13: 0 

firing_pattern: ${firing_pattern_full_auto}, 

*0xC6EE6B4C*


## Syntax

```js
player.taskGoToCoordWhileAimingAtCoord(x, y, z, aimAtX, aimAtY, aimAtZ, moveSpeed, p8, p9, p10, p11, flags, p13, firingPattern);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimAtX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimAtY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimAtZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>moveSpeed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p11:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>flags:</b> unknown (to be checked)</li>
<li><b>p13:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>firingPattern:</b> Model hash or name</li>

## Return value

<b>Undefined</b>