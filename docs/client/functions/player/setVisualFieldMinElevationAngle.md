# Player::setVisualFieldMinElevationAngle

This native refers to the field of vision the ped has below them, starting at 0 degrees. 

The angle value should be negative. 

-90f should let the ped see 90 degrees below them, for example.


## Syntax

```js
player.setVisualFieldMinElevationAngle(angle);
```

## Example

```js
// todo
```

## Parameters

<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>