# Player::taskSeekCoverFrom

## Syntax

```js
player.taskSeekCoverFrom(target, duration, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>