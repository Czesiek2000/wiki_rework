# Player::taskFollowPointRoute

*MulleKD19: Makes the ped go on the created point route.*

ped: The ped to give the task to.speed: The speed to move at in m/s.int: Unknown. Can be 0, 1, 2 or 3.

Example:

```cpp
TASK_FLUSH_ROUTE();
TASK_EXTEND_ROUTE(0f, 0f, 70f);
TASK_EXTEND_ROUTE(10f, 0f, 70f);
TASK_EXTEND_ROUTE(10f, 10f, 70f);
TASK_FOLLOW_POINT_ROUTE(GET_PLAYER_PED(), 1f, 0);
```

## Syntax

```js
player.taskFollowPointRoute(speed, unknown);
```

## Example

```js
// todo
```

## Parameters

<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>