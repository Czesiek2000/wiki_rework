# Player::setResetFlag

```cpp
Player::SET_Player_RESET_FLAG(PLAYER::PLAYER_Player_ID(), 240, 1);
```

## Syntax

```js
player.setResetFlag(flagId, doReset);
```

## Example

```js
// todo
```

## Parameters

<li><b>flagId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>doReset:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>