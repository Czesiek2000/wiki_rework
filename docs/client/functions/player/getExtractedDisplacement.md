# Player::getExtractedDisplacement

Gets the offset the specified ped has moved since the previous tick.

If worldSpace is false, the returned offset is relative to the player. That is, if the ped has moved 1 meter right and 5 meters forward, it'll return 1,5,0. 

If worldSpace is true, the returned offset is relative to the world. That is, if the ped has moved 1 meter on the X axis and 5 meters on the Y axis, it'll return 1,5,0.


## Syntax

```js
player.getExtractedDisplacement(worldSpace);
```

## Example

```js
// todo
```

## Parameters

<li><b>worldSpace:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b><span style="color:#008017">Vector3</span></b>