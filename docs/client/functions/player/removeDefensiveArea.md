# Player::removeDefensiveArea

Ped will no longer get angry when you stay near him.


## Syntax

```js
player.removeDefensiveArea(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>