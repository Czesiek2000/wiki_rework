# Player::taskVehicleMissionTarget

Modes:

8: flees

1: drives around the ped

4: drives and stops near

7: follows

10: follows to the left

11: follows to the right

12: follows behind

13: follows ahead

14: follows, stop when near


## Syntax

```js
player.taskVehicleMissionTarget(vehicle, pedTarget, mode, maxSpeed, drivingStyle, minDistance, p7, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>pedTarget:</b> Ped handle or object</li>
<li><b>mode:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>maxSpeed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>drivingStyle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minDistance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>