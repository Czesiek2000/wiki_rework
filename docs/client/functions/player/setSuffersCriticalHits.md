# Player::setSuffersCriticalHits

ped cannot be headshot if this is set to false


## Syntax

```js
player.setSuffersCriticalHits(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>