# Player::isFalling

## Syntax

```js
player.isFalling();
```

## Example

```js
mp.events.add('render', () => {
    if (mp.players.local.isFalling()) {
        // your code
    }
});
```

## Parameters


## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>