# Player::registerHatedTargetsAround

Based on `TASK_COMBAT_HATED_TARGETS_AROUND_PED`, the parameters are likely similar (PedHandle, and area to attack in).


## Syntax

```js
player.registerHatedTargetsAround(radius);
```

## Example

```js
// todo
```

## Parameters

<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>