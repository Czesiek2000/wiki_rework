# Player::taskClearLookAt

Not clear what it actually does, but here's how script uses it - 

```cpp
if (OBJECT::HAS_PICKUP_BEEN_COLLECTED(...) { 
    if(ENTITY::DOES_ENTITY_EXIST(PLAYER::PLAYER_Player_ID())) { 
        Player::TASK_CLEAR_LOOK_AT(PLAYER::PLAYER_Player_ID()); 
    } 
    ...
}
//Another one where it doesn't 'look' at current player 
Player::TASK_PLAY_ANIM(l_3ED, 'missheist_agency2aig_2', 'look_at_phone_a', 1000.0, -2.0, -1, 48, v_2, 0, 0, 0);
Player::_2208438012482A1A(l_3ED, 0, 0);
Player::TASK_CLEAR_LOOK_AT(l_3ED);
```

## Syntax

```js
player.taskClearLookAt();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Undefined</b></li>