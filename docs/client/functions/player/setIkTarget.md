# Player::setIkTarget

## Syntax

```js
player.setIkTarget(p1, targetPed, boneLookAt, x, y, z, p7, duration, duration1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>targetPed:</b> Ped handle or object</li>
<li><b>boneLookAt:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>duration1:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>