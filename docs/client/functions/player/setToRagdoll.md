# Player::setToRagdoll

- time1 - Time Ped is in ragdoll mode(ms)
- time2 - time2 same as time1 but in mircoseconds (us)

ragdollType
* 0 : Normal ragdoll
* 1 : Falls with stiff legs/body
* 2 : Narrow leg stumble(may not fall)
* 3 : Wide leg stumble(may not fall)

p4, p5, p6- No idea

**EDIT 3/11/16**: unclear what 'mircoseconds' mean-- a microsecond is 1000x a ms, so time2 must be 1000x time1? more testing needed.

*sob*

time2 seems to be 2x time1 - worked in our tests with time1 = 5000 and time2 = 10000 and all booleans as false. - Enorion


## Syntax

```js
player.setToRagdoll(time1, time2, ragdollType, p4, p5, p6);
```

## Example

```js
mp.players.local.setToRagdoll(5000, 5000, 0, true, true, true);
// Sets the player in a ragdoll state for 5 seconds.
```

## Parameters

<li><b>time1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>time2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>ragdollType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>