# Player::taskWanderStandard

Makes ped walk around the area.set p1 to 10.0f and p2 to 10 if you want the ped to walk anywhere without a duration.


## Syntax

```js
player.taskWanderStandard(p1, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>