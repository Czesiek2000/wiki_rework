# Player::taskForceMotionState

p2 always false


## Syntax

```js
player.taskForceMotionState(state, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>state:</b> Model hash or name</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>