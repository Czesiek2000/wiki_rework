# Player::setAsCop

Turns the desired ped into a cop. 

If you use this on the player ped, you will become almost invisible to cops dispatched for you. 

You will also report your own crimes, get a generic cop voice, get a cop-vision-cone on the radar, and you will be unable to shoot at other cops. 

SWAT and Army will still shoot at you. 

Toggling ped as `false` has no effect; you must change p0's ped model to disable the effect.


## Syntax

```js
player.setAsCop(toggle);
```

## Example

```js
mp.events.add({
    "entityStreamIn" : (entity) => {
	if(entity.type === 'player') {	// check if it's a player or not
		const isOnDuty = entity.getVariable('isOnDuty'); // setVariable when player on duty, handle it on server-side
		if(isOnDuty) entity.setAsCop(true); // if this player on duty, set this player as a cop
	}
},
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>