# Player::taskPutDirectlyIntoCover

## Syntax

```js
player.taskPutDirectlyIntoCover(x, y, z, timeout, p5, p6, p7, p8, p9, p10);
```

## Example

```js
var localPlayer = mp.players.local;
localPlayer.taskPutDirectlyIntoCover(localPlayer.position.x, localPlayer.position.y, localPlayer.position.z, 5000, false, 0, true, false, 1, false);
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> time in milliseconds</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> number(needs to be checked but doesn't argue with a number.)</li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>