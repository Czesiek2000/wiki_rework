# Player::taskSetDecisionMaker

p1 is always `GET_HASH_KEY('empty')` in scripts, for the rare times this is used


## Syntax

```js
player.taskSetDecisionMaker(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> Model hash or name</li>

## Return value

<b>Undefined</b>