# Player::taskMoveNetwork

Example:
```cpp
Player::_2D537BA194896636(PLAYER::PLAYER_Player_ID(), 'arm_wrestling_sweep_paired_a_rev3', 0.0, 1, '_wrestling', 0);
```

## Syntax

```js
player.taskMoveNetwork(task, multiplier, p3, animDict, flags);
```

## Example

```js
// todo
```

## Parameters

<li><b>task:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>