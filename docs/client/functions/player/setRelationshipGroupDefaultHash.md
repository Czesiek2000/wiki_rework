# Player::setRelationshipGroupDefaultHash

## Syntax

```js
player.setRelationshipGroupDefaultHash(hash);
```

## Example

```js
// todo
```

## Parameters

<li><b>hash:</b> Model hash or name</li>

## Return value

<b>Undefined</b>