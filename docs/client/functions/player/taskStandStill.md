# Player::taskStandStill

Makes the specified ped stand still for (time) milliseconds.


## Syntax

```js
player.taskStandStill(time);
```

## Example

```js
// todo
```

## Parameters

<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>