# Player::taskGoToCoordAnyMeans

example from fm_mission_controller

```cpp
Player::TASK_GO_TO_COORD_ANY_MEANS(l_649, sub_f7e86(-1, 0), 1.0, 0, 0, 786603, 0xbf800000);
```

## Syntax

```js
player.taskGoToCoordAnyMeans(x, y, z, speed, p5, p6, walkingStyle, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>walkingStyle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>