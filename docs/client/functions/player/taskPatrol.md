# Player::taskPatrol

After looking at some scripts the second parameter seems to be an id of some kind. Here are some I found from some R* scripts:

* 'miss_Tower_01' (this went from 01 - 10)
* 'miss_Ass0' (0, 4, 6, 3)
* 'MISS_PATROL_8' 

I think they're patrol routes, but I'm not sure. And I believe the 3rd parameter is a BOOL, but I can't confirm other than only seeing 0 and 1 being passed.

*Shawn* (/u/shawn_of_the_reddit)

As far as I can see the patrol routes names such as 'miss_Ass0' have been defined earlier in the scripts. This leads me to believe we can defined our own new patrol routes by following the same approach. 

From the scripts 
```cpp
Player::OPEN_PATROL_ROUTE('miss_Ass0'); 
Player::ADD_PATROL_ROUTE_NODE(0, 'WORLD_HUMAN_GUARD_STAND', l_738[0/*3*/], -139.4076690673828, -993.4732055664062, 26.2754, GAMEPLAY::GET_RANDOM_INT_IN_RANGE(5000, 10000));
Player::ADD_PATROL_ROUTE_NODE(1, 'WORLD_HUMAN_GUARD_STAND', l_738[1/*3*/], -116.1391830444336, -987.4984130859375, 26.38541030883789, GAMEPLAY::GET_RANDOM_INT_IN_RANGE(5000, 10000));
Player::ADD_PATROL_ROUTE_NODE(2, 'WORLD_HUMAN_GUARD_STAND', l_738[2/*3*/], -128.46847534179688, -979.0340576171875, 26.2754, GAMEPLAY::GET_RANDOM_INT_IN_RANGE(5000, 10000)); 
Player::ADD_PATROL_ROUTE_LINK(0, 1); 
Player::ADD_PATROL_ROUTE_LINK(1, 2); 
Player::ADD_PATROL_ROUTE_LINK(2, 0); 
Player::CLOSE_PATROL_ROUTE(); 
Player::CREATE_PATROL_ROUTE();
```

## Syntax

```js
player.taskPatrol(p1, p2, p3, p4);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> String</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> Boolean</li>
<li><b>p4:</b> Boolean</li>

## Return value

<b>Undefined</b>