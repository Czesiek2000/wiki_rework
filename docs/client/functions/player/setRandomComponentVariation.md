# Player::setRandomComponentVariation

p1 is always false in R* scripts.

Quick disassembly seems to indicate that p1 is unused.

[List of component/props ID](https://gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.setRandomComponentVariation(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> Boolean</li>

## Return value

<b>Undefined</b>