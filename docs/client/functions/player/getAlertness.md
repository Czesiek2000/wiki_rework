# Player::getAlertness

Returns the ped's alertness (0-3).

Values :
* 0 : Neutral
* 1 : Heard something (gun shot, hit, etc)
* 2 : Knows (the origin of the event)
* 3 : Fully alerted (is facing the event?)

MulleDK19: If the Ped does not exist, returns -1.


## Syntax

```js
player.getAlertness();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>