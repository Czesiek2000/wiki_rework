# Player::isAPlayer

Returns true if the given ped has a valid pointer to CPlayerInfo in its CPed class. That's all.


## Syntax

```js
player.isAPlayer();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>