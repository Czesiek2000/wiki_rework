# Player::taskWanderInArea

## Syntax

```js
player.taskWanderInArea(x, y, z, radius, minimalLength, timeBetweenWalks);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>minimalLength:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeBetweenWalks:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>