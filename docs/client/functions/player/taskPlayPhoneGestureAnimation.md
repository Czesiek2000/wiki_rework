# Player::taskPlayPhoneGestureAnimation

Example from the scripts:

```cpp
Player::TASK_PLAY_PHONE_GESTURE_ANIMATION(PLAYER::PLAYER_Player_ID(), v_3, v_2, v_4, 0.25, 0.25, 0, 0);
```
--------

^^ No offense, but Idk how that would really help anyone. As for the animDict & animation, they're both store in a global in all 5 scripts. So if anyone would be so kind as to read that global and comment what strings they use. Thanks.

Known boneMaskTypes

BONEMASK_HEADONLY

BONEMASK_HEAD_NECK_AND_ARMS

BONEMASK_HEAD_NECK_AND_L_ARM

BONEMASK_HEAD_NECK_AND_R_ARM

p4 known args - 0.0f, 0.5f, 0.25f

p5 known args - 0.0f, 0.25f

p6 known args - 1 if a global if check is passed.

p7 known args - 1 if a global if check is passed. 

The values found above, I found within the 5 scripts this is ever called in. (fmmc_launcher, fm_deathmatch_controller, fm_impromptu_dm_controller, fm_mission_controller, and freemode).

--------


## Syntax

```js
player.taskPlayPhoneGestureAnimation(p1, p2, p3, p4, p5, p6, p7);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>