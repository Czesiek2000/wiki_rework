# Player::clearAlternateMovementAnim

## Syntax

```js
player.clearAlternateMovementAnim(stance, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>stance:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>