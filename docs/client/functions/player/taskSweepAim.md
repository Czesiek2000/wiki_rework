# Player::taskSweepAim

This function is called on peds in vehicles.

anim: animation namep2, p3, p4: 'sweep_low', 'sweep_med' or 'sweep_high'

p5: no idea what it does but is usually -1

## Syntax

```js
player.taskSweepAim(anim, p2, p3, p4, p5, vehicle, p7, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>anim:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>