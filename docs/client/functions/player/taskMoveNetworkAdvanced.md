# Player::taskMoveNetworkAdvanced

Example:

```cpp
Player::_D5B35BEA41919ACB(PLAYER::PLAYER_Player_ID(), 'minigame_tattoo_michael_parts', 324.13, 181.29, 102.6, 0.0, 0.0, 22.32, 2, 0, 0, 0, 0);
```

## Syntax

```js
player.taskMoveNetworkAdvanced(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, animDict, flags);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> unknown (to be checked)</li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>flags:</b> int</li>

## Return value

<b>Undefined</b>