# Player::taskVehicleAimAt

## Syntax

```js
player.taskVehicleAimAt(target);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>

## Return value

<b>Undefined</b>