# Player::isHuman

Returns `true` / `false` if the ped is/isn't humanoid.

Edited by: Enumerator


## Syntax

```js
player.isHuman();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>