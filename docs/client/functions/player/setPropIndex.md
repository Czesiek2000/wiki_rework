# Player::setPropIndex

ComponentId can be set to various things based on what category you're wanting to set.


[List of component/props ID](http://gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.setPropIndex(componentId, drawableId, TextureId, attach);
```

## Example

```js
// todo
```

## Parameters

<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>drawableId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>TextureId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>attach:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>