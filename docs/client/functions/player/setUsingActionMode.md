# Player::setUsingActionMode

p2 is usually -1 in the scripts. action is either 0 or 'DEFAULT_ACTION'.

## Syntax

```js
player.setUsingActionMode(state, timeout, action);
```

## Example

This will clear melee stance almost instantly when attacking.

```js
if (player.isUsingActionMode()) {
   player.setUsingActionMode(-1, -1, 1)
}
```

## Parameters

<li><b>state:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>action:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>