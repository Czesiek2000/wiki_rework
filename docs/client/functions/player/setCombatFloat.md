# Player::setCombatFloat

combatType can be between **0-14**. 

See `GET_COMBAT_FLOAT` below for a list of possible parameters.


## Syntax

```js
player.setCombatFloat(combatType, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>combatType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>