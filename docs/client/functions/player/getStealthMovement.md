# Player::getStealthMovement

Returns whether the entity is in stealth mode


## Syntax

```js
player.getStealthMovement();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>