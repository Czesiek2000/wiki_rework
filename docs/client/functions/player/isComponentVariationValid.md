# Player::isComponentVariationValid

Checks if the component variation is valid, this works great for randomizing components using loops.

[List of component/props ID](gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.isComponentVariationValid(componentId, drawableId, textureId);
```

## Example

```js
// todo
```

## Parameters

<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>drawableId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>textureId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>