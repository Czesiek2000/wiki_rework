# Player::taskGoToCoordAndAimAtHatedEntitiesNearCoord

The ped will walk or run towards goToLocation, aiming towards goToLocation or focusLocation (depending on the aimingFlag) and shooting if shootAtEnemies = true to any enemy in his path.

If the ped is closer than noRoadsDistance, the ped will ignore pathing/navmesh and go towards goToLocation directly. This could cause the ped to get stuck behind tall walls if the goToLocation is on the other side. To avoid this, use 0.0f and the ped will always use pathing/navmesh to reach his destination. 

If the speed is set to 0.0f, the ped will just stand there while aiming, if set to 1.0f he will walk while aiming, 2.0f will run while aiming.

The ped will stop aiming when he is closer than distanceToStopAt to goToLocation. 

I still can't figure out what unkTrue is used for. I don't notice any difference if I set it to false but in the decompiled scripts is always true. 

I think that unkFlag, like the driving styles, could be a flag that 'work as a list of 32 bits converted to a decimal integer. Each bit acts as a flag, and enables or disables a function'. What leads me to this conclusion is the fact that in the decompiled scripts, unkFlag takes values like: **0, 1, 5 (101 in binary) and 4097 (4096 + 1 or 1000000000001 in binary**). For now, I don't know what behavior enable or disable this possible flag so I leave it at 0.

**Note:** After some testing, using unkFlag = 16 (0x10) enables the use of sidewalks while moving towards goToLocation. The aimingFlag takes 2 values: 0 to aim at the focusLocation, 1 to aim at where the ped is heading (goToLocation).

Example:

```cpp
enum AimFlag { 
    AimAtFocusLocation,
    AimAtGoToLocation
};

Vector3 goToLocation1 = { 996.2867f, 0, -2143.044f, 0, 28.4763f, 0 }; // remember the padding.
Vector3 goToLocation2 = { 990.2867f, 0, -2140.044f, 0, 28.4763f, 0 }; // remember the padding.
Vector3 focusLocation = { 994.3478f, 0, -2136.118f, 0, 29.2463f, 0 }; // the coord z should be a little higher, around +1.0f to avoid aiming at the ground
// 1st example
Player::TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(pedHandle, goToLocation1.x, goToLocation1.y, goToLocation1.z, focusLocation.x, focusLocation.y, focusLocation.z, 2.0f /*run*/, true /*shoot*/, 3.0f /*stop at*/, 0.0f /*noRoadsDistance*/, true /*always true*/, 0 /*possible flag*/, AimFlag::AimAtGoToLocation, -957453492 /*FullAuto pattern*/);
// 2nd example
Player::TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(pedHandle, goToLocation2.x, goToLocation2.y, goToLocation2.z, focusLocation.x, focusLocation.y, focusLocation.z, 1.0f /*walk*/, false /*don't shoot*/, 3.0f /*stop at*/, 0.0f /*noRoadsDistance*/, true /*always true*/, 0 /*possible flag*/, AimFlag::AimAtFocusLocation, -957453492 /*FullAuto pattern*/);
```
1st example: The ped (pedhandle) will run towards goToLocation1. While running and aiming towards goToLocation1, the ped will shoot on sight to any enemy in his path, using 'FullAuto' firing pattern. The ped will stop once he is closer than distanceToStopAt to goToLocation1.

2nd example: The ped will walk towards goToLocation2. This time, while walking towards goToLocation2 and aiming at focusLocation, the ped will point his weapon on sight to any enemy in his path without shooting. The ped will stop once he is closer than distanceToStopAt to goToLocation2.

*Skru*


## Syntax

```js
player.taskGoToCoordAndAimAtHatedEntitiesNearCoord(gotoX, gotoY, gotoZ, aimNearX, aimNearY, aimNearZ, speed, shoot, unknown1, unknown2, unkTrue, unknown3, heading, firingPattern);
```

## Example

```js
// todo
```

## Parameters

<li><b>gotoX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>gotoY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>gotoZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimNearX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimNearY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>aimNearZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>shoot:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unknown1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unkTrue:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unknown3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>firingPattern:</b> Model hash or name</li>

## Return value

<b>Undefined</b>