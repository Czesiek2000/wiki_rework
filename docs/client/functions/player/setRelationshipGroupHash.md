# Player::setRelationshipGroupHash

## Syntax

```js
player.setRelationshipGroupHash(hash);
```

## Example

```js
// todo
```

## Parameters

<li><b>hash:</b> Model hash or name</li>

## Return value

<b>Undefined</b>