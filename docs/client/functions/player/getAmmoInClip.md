# Player::getAmmoInClip

## Syntax

```js
player.getAmmoInClip(weapon_hash);
```

## Example

```js
let weapon_hash = mp.players.local.weapon;
let ammoClip = mp.players.local.getAmmoInClip(weapon_hash);
```

## Parameters

<li><b>weapon_hash</b> - Weapon hash</li>

## Return value

<b><span style="color:#008017">Int</span></b>