# Player::isStopped

Returns true if the ped doesn't do any movement. 

If the ped is being pushed forwards by using `APPLY_FORCE_TO_ENTITY` for example, 
the function returns `false`.


## Syntax

```js
player.isStopped();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>
