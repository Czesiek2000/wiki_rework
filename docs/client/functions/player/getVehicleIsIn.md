# Player::getVehicleIsIn

Gets the vehicle the specified Ped is / was in depending on bool value.

False = CurrentVehicle, 

True = LastVehicle


## Syntax

```js
player.getVehicleIsIn(getLastVehicle);
```

## Example

```js
// todo
```

## Parameters

<li><b>getLastVehicle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Vehicle handle or object</b>