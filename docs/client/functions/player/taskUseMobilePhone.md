# Player::taskUseMobilePhone

Actually has 3 params, not 2.p0: Pedp1: int (or bool?)p2: int


## Syntax

```js
player.taskUseMobilePhone(duration, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>