# Player::getCauseOfDeath

Returns the hash of the weapon/model/object that killed the player.Edited by: Enumerator


## Syntax

```js
player.getCauseOfDeath();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Model hash or name</b></li>