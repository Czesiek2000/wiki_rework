# Player::applyDamageTo

## Syntax

```js
player.applyDamageTo(damageAmount, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>damageAmount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>