# Player::addVehicleSubtaskAttackCoord

x, y, z: offset in world coords from some entity.


## Syntax

```js
player.addVehicleSubtaskAttackCoord(x, y, z);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>