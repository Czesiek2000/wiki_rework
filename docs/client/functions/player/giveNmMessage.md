# Player::giveNmMessage

MulleDK19: Sends the message that was created by a call to `CREATE_NM_MESSAGE` to the specified player.

If a message hasn't been created already, this function does nothing.

If the Ped is not ragdolled with Euphoria enabled, this function does nothing.

The following call can be used to ragdoll the Ped with Euphoria enabled: `SET_Player_TO_RAGDOLL(ped, 4000, 5000, 1, 1, 1, 0);`

Call order:
```cpp
SET_Player_TO_RAGDOLL
CREATE_NM_MESSAGE
GIVE_Player_NM_MESSAGE
```
Multiple messages can be chained. Eg. to make the ped stagger and swing his arms around, the following calls can be made:
```cpp
SET_Player_TO_RAGDOLL(ped, 4000, 5000, 1, 1, 1, 0);
CREATE_NM_MESSAGE(true, 0); // stopAllBehaviours - Stop all other behaviours, in case the Ped is already doing some Euphoria stuff.
GIVE_Player_NM_MESSAGE(ped); // Dispatch message to player.
CREATE_NM_MESSAGE(true, 1151); // staggerFall - Attempt to walk while falling.
GIVE_Player_NM_MESSAGE(ped); // Dispatch message to player.
CREATE_NM_MESSAGE(true, 372); // armsWindmill - Swing arms around.
GIVE_Player_NM_MESSAGE(ped); // Dispatch message to player.
```

## Syntax

```js
player.giveNmMessage();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<b>Undefined</b>