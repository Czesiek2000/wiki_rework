# Player::getsJacker


## Syntax

```js
player.getsJacker();
```

## Example

This example will use the returned value from Player::getsJacker to add 25 armour to the specified entity (client-side). - 2019-5-16

```js
/* Player::getsJacker */
let localPlayerJacker = mp.players.local.getsJacker();

/* CLIENT-SIDE ADDARMOURTO localPlayerJacker ADDS 25 ARMOUR TO PLAYER. */
mp.players.at(localPlayerJacker).addArmourTo(25);
```

## Parameters

No parameters here

## Return value

<li><b>Ped handle or object</b></li>