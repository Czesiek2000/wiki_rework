# Player::resetMovementClipset

If p1 is **0.0**, I believe you are back to normal. 

If p1 is **1.0**, it looks like you can only rotate the ped, not walk.

Using the following code to reset back to normal `Player::RESET_Player_MOVEMENT_CLIPSET(PLAYER::PLAYER_Player_ID(), 0.0);`


## Syntax

```js
player.resetMovementClipset(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>