# Player::setMaxHealth

## Syntax

```js
player.setMaxHealth(value);
```

## Example

```js
// todo
```

## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>