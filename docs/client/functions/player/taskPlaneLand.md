# Player::taskPlaneLand

Using this you can task the ped to land (This will only task the ped to land the plane if already in air)

You need to turn the engines on manually because the ped will not


```js
Function.Call(Hash.TASK_PLANE_LAND, pilot, selectedAirplane, runwayStartPoint.X, runwayStartPoint.Y, runwayStartPoint.Z, runwayEndPoint.X, runwayEndPoint.Y, runwayEndPoint.Z); 
```

## Syntax

```js
player.taskPlaneLand(plane, runwayStartX, runwayStartY, runwayStartZ, runwayEndX, runwayEndY, runwayEndZ);
```

## Example

```js
const player = mp.players.local;

// Sandy Shore Airfield
player.taskPlaneLand(player.vehicle.handle, 1060.688232421875, 3077.2314453125, 40.273826599121094, 1667.779541015625, 3240.7724609375, 39.652400970458984);

// Runway 30R Los Santos International
player.taskPlaneLand(player.vehicle.handle, -944.08721923828, -3175.4035644531, 13.951797485352, -1211.2023925781, -3021.1779785156, 13.988622665405);

// Runway 30L Los Santos International
player.taskPlaneLand(player.vehicle.handle, -1015.9371948242, -3328.49609375, 13.987154960632, -1308.6403808594, -3159.6342773438, 13.944442749023);

// Runway 12R Los Santos International
player.taskPlaneLand(player.vehicle.handle, -1621.0546875, -2979.1828613281, 13.987594604492, -1300.5179443359, -3164.3552246094, 13.987669944763);

// Runway 12L Los Santos International
player.taskPlaneLand(player.vehicle.handle, -1553.873046875, -2823.3256835938, 14.001617431641, -1322.3289794922, -2956.9755859375, 13.989463806152);

// Runway 21 Los Santos International
player.taskPlaneLand(player.vehicle.handle, -1357.6005859375, -2247.2133789063, 13.968578338623, -1496.3876953125, -2487.9694824219, 13.97553062439);

// Runway at Zandudo
player.taskPlaneLand(player.vehicle.handle, -2035.1810302734, 2874.1303710938, 32.828636169434, -2425.7612304688, 3099.9943847656, 32.926719665527);
```

## Parameters

<li><b>plane:</b> Vehicle handle or object</li>
<li><b>runwayStartX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>runwayStartY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>runwayStartZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>runwayEndX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>runwayEndY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>runwayEndZ:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>