# Player::setPinnedDown

i could be time. Only example in the decompiled scripts uses it as -1.


## Syntax

```js
player.setPinnedDown(pinned, i);
```

## Example

```js
// todo
```

## Parameters

<li><b>pinned:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>i:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>