# Player::setRagdollFlag

Works for both player and peds, but some flags don't seem to work for the player (1, for example)

- 1 Blocks ragdolling when shot.

- 2 Blocks ragdolling when hit by a vehicle. The ped still might play a falling animation.

- 4 Blocks ragdolling when set on fire.

-----------------------------------------------------------------------

There seem to be **26** flags


## Syntax

```js
player.setRagdollFlag(flag);
```

## Example

```js
// todo
```

## Parameters

<li><b>flag:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>