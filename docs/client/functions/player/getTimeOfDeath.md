# Player::getTimeOfDeath

*Console Hash: 0xDF6D5D54*

There is no way this is the correct name. The only time this is called it's compared with the game timer and I used this to test something and noticed when I was killed by no matter what it was my 'Time Of Death' via this native was always 0, but my friends was some random big number like so, 147591.

Retreives [CPed + 15CC] (as of 944)


## Syntax

```js
player.getTimeOfDeath();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>