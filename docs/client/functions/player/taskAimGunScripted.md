# Player::taskAimGunScripted

## Syntax

```js
player.taskAimGunScripted(scriptTask, p2, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>scriptTask:</b> Model hash or name</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>