# Player::getRelationshipGroupHash

## Syntax

```js
player.getRelationshipGroupHash();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Model hash or name</b></li>