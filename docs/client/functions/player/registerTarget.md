# Player::registerTarget

`Player::REGISTER_TARGET(l_216, PLAYER::PLAYER_Player_ID());` 

from re_prisonbreak.txt.

l_216 = RECSBRobber1


## Syntax

```js
player.registerTarget(target);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>

## Return value

<b>Undefined</b>