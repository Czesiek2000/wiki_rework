# Player::setMovementClipset

*p2 is usually 1.0f*

EDIT 12/24/16: p2 does absolutely nothing no matter what the value is. - sollaholla

Edit: p2 is transition to the clipSet, 1.0 means 1 second (tested with crouch clipSet) 
*noBrain*


## Syntax

```js
player.setMovementClipset(clipSet, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>clipSet:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>