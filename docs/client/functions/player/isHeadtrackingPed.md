# Player::isHeadtrackingPed

## Syntax

```js
player.isHeadtrackingPed(ped2);
```

## Example

```js
// todo
```

## Parameters

<li><b>ped2:</b> Ped handle or object</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>