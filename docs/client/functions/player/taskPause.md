# Player::taskPause

Stand still (?)


## Syntax

```js
player.taskPause(ms);
```

## Example

```js
// todo
```

## Parameters

<li><b>ms:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>