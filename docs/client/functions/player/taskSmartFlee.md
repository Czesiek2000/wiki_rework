# Player::taskSmartFlee

Makes a ped run away from another ped (fleeTarget).

distance = ped will flee this distance.

fleeTime = ped will flee for this amount of time, set to '-1' to flee forever


## Syntax

```js
player.taskSmartFlee(fleeTarget, distance, fleeTime, p4, p5);
```

## Example

```js
// todo
```

## Parameters

<li><b>fleeTarget:</b> Ped handle or object</li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fleeTime:</b> unknown (to be checked)</li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>