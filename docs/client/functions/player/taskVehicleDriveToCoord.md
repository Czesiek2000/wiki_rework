# Player::taskVehicleDriveToCoord

[info about driving modes](http://gtaforums.com/topic/822314-guide-driving-styles/)

---------------------------------------------------------------

Passing P6 value as floating value didn't throw any errors, though unsure what is it exactly, looks like radius or something.

P10 though, it is mentioned as float, however, I used bool and set it to true, that too worked.

Here the e.g. code I used

```csharp
Function.Call(Hash.TASK_VEHICLE_DRIVE_TO_COORD, Ped, Vehicle, Cor X, Cor Y, Cor Z, 30f, 1f, Vehicle.GetHashCode(), 16777216, 1f, true);
```

## Syntax

```js
player.taskVehicleDriveToCoord(vehicle, x, y, z, speed, p6, vehicleModel, drivingMode, stopRange, p10);
```

## Example

```js
/* Create a Cop Car (x,y,z = arg1/arg2/arg3) and a Cop, Enter Vehicle, Drive to 0,0,72 Coords. */

let testPed = mp.peds.new(mp.game.joaat(arg0), new mp.Vector3(arg1, arg2, arg3), {dynamic:true}); //synced ped
testPed.controller = mp.players.at(0);

	setTimeout(function () {
	
		testPed.freezePosition(false);
		testPed.setCanBeDamaged(true);
		testPed.setInvincible(false);
		testPed.CanRagdoll = true;
		testPed.setOnlyDamagedByPlayer(true);
		testPed.setCanRagdollFromPlayerImpact(true);
		testPed.setSweat(100);
		testPed.setRagdollOnCollision(true);

		testPed.setProofs(false, false, false, false, false, false, false, false); 
		testPed.giveWeapon(487013001, 50, true);

		let Veh = mp.vehicles.new(mp.game.joaat("police"), new mp.Vector3(arg1, arg2, arg3),
		{
			numberPlate: "ADMIN",
			locked: false,
			engine: true,
			color: [[255, 255, 255],[0,0,0]]
		});

		setTimeout(function () {
			testPed.taskEnterVehicle(Veh.handle, 10000, -1, 1, 1, 0);
		}, 1500);

		//player.taskVehicleDriveToCoord(vehicle, x, y, z, speed, p6, vehicleModel, drivingMode, stopRange, p10);
		
		setTimeout(function () {
			testPed.taskVehicleDriveToCoord(Veh.handle, 0.52, 0.38, 72.1, 40, 1, 2046537925, 2, 3, true);
			mp.gui.chat.push("StartDriveTo: " + new Date());
		}, 10500);
	}, 300);
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>vehicleModel:</b> Model hash or name</li>
<li><b>drivingMode:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>stopRange:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>