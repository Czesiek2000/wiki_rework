# Player::setBoundsOrientation

## Syntax

```js
player.setBoundsOrientation(p1, p2, p3, p4, p5);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>