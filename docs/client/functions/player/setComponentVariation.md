# Player::setComponentVariation

[List of component/props ID](https://gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)

See player clothes: https://wiki.rage.mp/index.php?title=Clothes
paletteId/palletColor - 0 to 3: https://wiki.rage.mp/index.php?title=Player::getPaletteVariation
.


## Syntax

```js
player.setComponentVariation(componentId, drawableId, textureId, paletteId);
```

## Example

```js
// set shirt on player
player.setComponentVariation(11, 4, 0, 2);
```

## Parameters

<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>drawableId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>textureId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>paletteId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>