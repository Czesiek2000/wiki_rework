# Player::setAsGroupMember

## Syntax

```js
player.setAsGroupMember(groupId);
```

## Example

```js
// todo
```

## Parameters

<li><b>groupId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>