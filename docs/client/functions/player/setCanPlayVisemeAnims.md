# Player::setCanPlayVisemeAnims

## Syntax

```js
player.setCanPlayVisemeAnims(p1, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>