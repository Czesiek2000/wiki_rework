# Player::setCombatAttributes

These combat attributes seem to be the same as the BehaviourFlags from combatbehaviour.meta.

So far, these are the equivalents found:
```cpp
enum CombatAttributes { 
    BF_CanUseCover = 0,
    BF_CanUseVehicles = 1, 
    BF_CanDoDrivebys = 2,
    BF_CanLeaveVehicle = 3,
    BF_CanFightArmedPedsWhenNotArmed = 5,
    BF_CanTauntInVehicle = 20,
    BF_AlwaysFight = 46,
    BF_IgnoreTrafficWhenDriving = 52,
    BF_FreezeMovement = 292,
    BF_PlayerCanUseFireingWeapons = 1424
};
```

[Research thread](https://gtaforums.com/topic/833391-researchguide-combat-behaviour-flags/)


## Syntax

```js
player.setCombatAttributes(attributeIndex, enabled);
```

## Example

```js
// todo
```

## Parameters

<li><b>attributeIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>enabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>