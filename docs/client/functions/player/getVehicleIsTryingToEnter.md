# Player::getVehicleIsTryingToEnter

## Syntax

```js
player.getVehicleIsTryingToEnter();
```

## Example

```js
const vehHandle = mp.players.local.getVehicleIsTryingToEnter();
if (vehHandle) {
    const veh = mp.vehicles.atHandle(vehHandle);
    if (veh) mp.gui.chat.push(`You are trying to enter vehicle model: ${veh.model}, with remote id: ${veh.remoteId}.`);
}
```

## Parameters

No parameters here

## Return value

<li><b>Vehicle handle or object</b></li>