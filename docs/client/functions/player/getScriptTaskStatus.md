# Player::getScriptTaskStatus

Gets the status of a script-assigned task. The hash does not seem to match the actual native name, but is assigned hardcoded from the executable during task creation.

Statuses are specific to tasks, in addition '7' means the specified task is not assigned to the player.

A few hashes found in the executable (although not a complete list) can be found at [pastebin](https://pastebin.com/R9iK6M9W) as it was too long for this wiki.


## Syntax

```js
player.getScriptTaskStatus(taskHash);
```

## Example

```js
// todo
```

## Parameters

<li><b>taskHash:</b> Model hash or name</li>

## Return value

<b>Undefined</b>