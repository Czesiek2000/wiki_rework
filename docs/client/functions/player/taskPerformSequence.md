# Player::taskPerformSequence

## Syntax

```js
player.taskPerformSequence(taskSequence);
```

## Example

```js
// todo
```

## Parameters

<li><b>taskSequence:</b> Object handle or object</li>

## Return value

<b>Undefined</b>