# Player::clearTasksImmediately

Immediately stops the pedestrian from whatever it's doing. They stop fighting, animations, etc. they forget what they were doing.


## Syntax

```js
player.clearTasksImmediately();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Undefined</b></li>