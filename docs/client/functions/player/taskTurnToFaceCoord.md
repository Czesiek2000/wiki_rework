# Player::taskTurnToFaceCoord

duration in milliseconds


## Syntax

```js
player.taskTurnToFaceCoord(x, y, z, duration);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>