# Player::taskPlayAnim
Plays an animation on the targetted ped.


You must always preload the dictionary (use [Streaming::requestAnimDict](../streaming/requestAnimDict.md) to preload an animation), serverside will have some loaded but not all.


## Arguments
**Animation Dictionary, Animation Name**
<ul><li>A few possible values can be found on the <a href="https://wiki.rage.mp/index.php?title=Animations">Animations</a> page.</li></ul>
<p><b>Blend speed</b>:
</p>
<ul><li>Defines the blend time before/after the animation. Most common value is 8.</li></ul>
<dl><dd><pre>1 / [time in seconds] = [blend speed]</pre></dd></dl>
<p><b>Duration (ms)</b>:
</p>
<ul><li>How long the animation plays. -1 can be used to play the whole animation (or loop indefinitely, if flag 1 is used)</li></ul>
<p><b>Animation Flags </b>:
</p>
<ul><li>0: Normal</li>
<li>1: Repeat/loop</li>
<li>2: Stop animation on last frame</li>
<li>16: Animate upper body only</li>
<li>32: Enable player control (animation is played as a secondary task - can be used to mix animations)</li>
<li>128: Cancellable</li>
<li>256: Additive animation</li>
<li>512: Disables collision and physics on ped</li>
<li>1024: Disables collision and physics on ped, applies animation initial offset (see <a href="https://wiki.rage.mp/index.php?title=Ped::getAnimInitialOffsetPosition" title="Ped::getAnimInitialOffsetPosition">Ped::getAnimInitialOffsetPosition</a> and <a href="https://wiki.rage.mp/index.php?title=Ped::getAnimInitialOffsetRotation" title="Ped::getAnimInitialOffsetRotation">Ped::getAnimInitialOffsetRotation</a>)</li>
<li>2048: Disables collision and physics on ped</li></ul>
<dl><dd><i>Note:</i> You can sum flags together. For example, if you want an upper body controllable animation with last frame you can do 48+2 which is flag 50.</dd></dl>
<p><b>Start Offset</b>:
Can be used to skip a part of the animation. Values are between 0.0 and 1.0
</p>


## Syntax

```js
1 / [time in seconds] = [blend speed]
```

## Example

```js
player.taskPlayAnim(animDictionary, animationName, blendInSpeed, blendOutSpeed, duration, flag, startOffset, lockX, lockY, lockZ);
```
```js
mp.game.streaming.requestAnimDict("[email protected]_robbery");//preload the animation
mp.players.local.taskPlayAnim("[email protected]_robbery", "robbery_action_f", 8.0, 1.0, -1, 1, 1.0, false, false, false);
```

## Parameters

<li>A few possible values can be found on the <a href="/index.php?title=Animations" title="Animations">Animations</a> page.</li>

## Return value

<b>Undefined</b>