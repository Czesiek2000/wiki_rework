# Player::setEnableBoundAnkles

## Syntax

```js
player.setEnableBoundAnkles(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>