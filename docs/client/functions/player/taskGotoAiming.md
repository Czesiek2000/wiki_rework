# Player::taskGotoAiming

eg 

```cpp
Player::TASK_GOTO_ENTITY_AIMING(v_2, PLAYER::PLAYER_Player_ID(), 5.0, 25.0);
```
ped = Ped you want to perform this task.

target = the Entity they should aim at.

distanceToStopAt = distance from the target, where the ped should stop to aim.

StartAimingDist = distance where the ped should start to aim.

## Syntax

```js
player.taskGotoAiming(target, distanceToStopAt, StartAimingDist);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Entity handle or object</li>
<li><b>distanceToStopAt:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>StartAimingDist:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>