# Player::isOnVehicle

Gets a value indicating whether the specified ped is on top of any vehicle.

Return 1 when ped is on vehicle.

Return 0 when ped is not on a vehicle.

## Syntax

```js
player.isOnVehicle();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>