# Player::taskGoToCoordAnyMeansExtraParamsWithCruiseSpeed

## Syntax

```js
player.taskGoToCoordAnyMeansExtraParamsWithCruiseSpeed(x, y, z, speed, p5, p6, walkingStyle, p8, p9, p10, p11, p12);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> Boolean</li>
<li><b>walkingStyle:</b> int</li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>
<li><b>p10:</b> unknown (to be checked)</li>
<li><b>p11:</b> unknown (to be checked)</li>
<li><b>p12:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>