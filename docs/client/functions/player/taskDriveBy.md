# Player::taskDriveBy

Example:

```cpp
Player::TASK_DRIVE_BY(l_467[1/*22*/], PLAYER::PLAYER_Player_ID(), 0, 0.0, 0.0, 2.0, 300.0, 100, 0, ${firing_pattern_burst_fire_driveby});
```
Needs working example. Doesn't seem to do anything.

I marked p2 as targetVehicle as all these shooting related tasks seem to have that in common. I marked p6 as distanceToShoot as if you think of GTA's Logic with the native `SET_VEHICLE_SHOOT` natives, it won't shoot till it gets within a certain distance of the target.

I marked p7 as pedAccuracy as it seems it's mostly 100 (Completely Accurate), 75, 90, etc. Although this could be the ammo count within the gun, but I highly doubt it. 

I will change this comment once I find out if it's ammo count or not.


## Syntax

```js
player.taskDriveBy(targetPed, p2, targetX, targetY, targetZ, p6, p7, p8, firingPattern);
```

## Example

```js
// todo
```

## Parameters

<li><b>targetPed:</b> Ped handle or object</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>targetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>targetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>targetZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>p8:</b> Boolean</li>
<li><b>firingPattern:</b> Model hash or name</li>

## Return value

<b>Undefined</b>