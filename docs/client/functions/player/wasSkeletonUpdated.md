# Player::wasSkeletonUpdated

*MulleDK19*: Despite this function's name, it simply returns whether the specified handle is a player.


## Syntax

```js
player.wasSkeletonUpdated();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>