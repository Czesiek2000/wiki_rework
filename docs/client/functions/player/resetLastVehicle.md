# Player::resetLastVehicle

Resets the value for the last vehicle driven by the player.


## Syntax

```js
player.resetLastVehicle();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b>Undefined</b></li>
