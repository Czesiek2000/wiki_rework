# Player::taskVehicleFollow

Makes a ped in a vehicle follow an entity (ped, vehicle, etc.)

drivingStyle:

0 = Rushed

1 = Ignore Traffic Lights

2 = Fast

3 = Normal (Stop in Traffic)

4 = Fast avoid traffic

5 = Fast, stops in traffic but overtakes sometimes

6 = Fast avoids traffic extremely

`Console Hash: 0xA8B917D7`

```cpp
Player::_TASK_VEHICLE_FOLLOW(l_244[3/*1*/], l_268[3/*1*/], l_278, 40.0, 262144, 10);
```

What is this known as in the decompiled scripts ffs. I need more examples. 

I've searched in all scripts for keywords suchas, `TASK_VEHICLE_FOLLOW`, FC545A9F0626E3B6, 0xFC545A9F0626E3B6, all the parameters in the above example even just search the last few params '40.0, 262144, 10' and couldnt find where this native is used in scripts at all unless whoever decompiled the scripts gave it a whack a.. name.


## Syntax

```js
player.taskVehicleFollow(vehicle, targetEntity, drivingStyle, speed, minDistance);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>targetEntity:</b> Entity handle or object</li>
<li><b>drivingStyle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>minDistance:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>