# Player::getRagdollBoneIndex

## Syntax

```js
player.getRagdollBoneIndex(bone);
```

## Example

```js
// todo
```

## Parameters

<li><b>bone:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>