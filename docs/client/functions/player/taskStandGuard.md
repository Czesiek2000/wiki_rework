# Player::taskStandGuard

scenarioName example: 'WORLD_HUMAN_GUARD_STAND'


## Syntax

```js
player.taskStandGuard(x, y, z, heading, scenarioName);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scenarioName:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>