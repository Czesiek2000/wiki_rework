# Player::setPathsWidthPlant

**Hash collision!!!** 

Actual name: `SET_Player_PATH_MAY_ENTER_WATER`


## Syntax

```js
player.setPathsWidthPlant(mayEnterWater);
```

## Example

```js
// todo
```

## Parameters

<li><b>mayEnterWater:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>