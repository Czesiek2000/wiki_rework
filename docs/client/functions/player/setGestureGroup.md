# Player::setGestureGroup

From the scripts:
```cpp
Player::SET_Player_GESTURE_GROUP(PLAYER::PLAYER_Player_ID(),'ANIM_GROUP_GESTURE_MISS_FRA0');
Player::SET_Player_GESTURE_GROUP(PLAYER::PLAYER_Player_ID(),'ANIM_GROUP_GESTURE_MISS_DocksSetup1');
```

## Syntax

```js
player.setGestureGroup(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>