# Player::knockOffProp

[List of component/props ID](gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.knockOffProp(p1, p2, p3, p4);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>