# Player::isSittingInAnyVehicle

Detect if ped is in any vehicle

Return True / False

*Edited by: Enumerator*


## Syntax

```js
player.isSittingInAnyVehicle();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>
