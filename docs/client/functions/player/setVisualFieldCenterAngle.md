# Player::setVisualFieldCenterAngle

## Syntax

```js
player.setVisualFieldCenterAngle(angle);
```

## Example

```js
// todo
```

## Parameters

<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>