# Player::taskOpenVehicleDoor

## Syntax

```js
player.taskOpenVehicleDoor(vehicle, timeOut, doorIndex, speed);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>timeOut:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>doorIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>