# Player::taskSeekCoverToCoords

**from michael2**:

```cpp
Player::TASK_SEEK_COVER_TO_COORDS(ped, 967.5164794921875, -2121.603515625, 30.479299545288086, 978.94677734375, -2125.84130859375, 29.4752, -1, 1);
// appears to be shorter variationfrom michael3: 
Player::TASK_SEEK_COVER_TO_COORDS(ped, -2231.011474609375, 263.6326599121094, 173.60195922851562, -1, 0);
```

## Syntax

```js
player.taskSeekCoverToCoords(x1, y1, z1, x2, y2, z2, p7, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>