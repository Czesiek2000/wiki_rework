# Player::stopWeaponFiringWhenDropped

## Syntax

```js
player.stopWeaponFiringWhenDropped();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Undefined</b></li>