# Player::updateTaskHandsUpDuration

## Syntax

```js
player.updateTaskHandsUpDuration(duration);
```

## Example

```js
// todo
```

## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>