# Player::setRagdollOnCollision

Causes Ped to ragdoll on collision with any object (e.g Running into trashcan). 

If applied to player you will sometimes trip on the sidewalk.


## Syntax

```js
player.setRagdollOnCollision(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>