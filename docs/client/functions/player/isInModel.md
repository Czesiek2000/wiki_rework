# Player::isInModel

## Syntax

```js
player.isInModel(modelHash);
```

## Example

```js
// todo
```

## Parameters

<li><b>modelHash:</b> Model hash or name</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>