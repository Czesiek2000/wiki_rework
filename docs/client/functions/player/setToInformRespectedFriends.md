# Player::setToInformRespectedFriends

## Syntax

```js
player.setToInformRespectedFriends(radius, maxFriends);
```

## Example

```js
// todo
```

## Parameters

<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>maxFriends:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>