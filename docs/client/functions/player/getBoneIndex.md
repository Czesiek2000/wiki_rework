# Player::getBoneIndex

no bone = -1

## Syntax

```js
player.getBoneIndex(boneId);
```

## Example

```js
// todo
```

## Parameters

<li><b>boneId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>