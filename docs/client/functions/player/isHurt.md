# Player::isHurt

Returns whether the specified ped is hurt.


## Syntax

```js
player.isHurt();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>