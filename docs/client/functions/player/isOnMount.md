# Player::isOnMount

Same function call as `Player::GET_MOUNT`, aka just returns 0


## Syntax

```js
player.isOnMount();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>
