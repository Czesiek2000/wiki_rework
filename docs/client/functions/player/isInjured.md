# Player::isInjured

Gets a value indicating whether this ped's health is below its injured threshold.

The default threshold is 100.


## Syntax

```js
player.isInjured();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>