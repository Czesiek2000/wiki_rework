# Player::setLegIkMode

'IK' stands for 'Inverse kinematics.' I assume this has something to do with how the ped uses his legs to balance. 

In the scripts, the second parameter is always an int with a value of 2, 0, or sometimes 1


## Syntax

```js
player.setLegIkMode(mode);
```

## Example

```js
// todo
```

## Parameters

<li><b>mode:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>