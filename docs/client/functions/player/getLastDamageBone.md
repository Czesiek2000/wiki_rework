# Player::getLastDamageBone

## Syntax

```js
player.getLastDamageBone(outBone);
```

## Example

```js
// todo
```

## Parameters

<li><b>outBone:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>