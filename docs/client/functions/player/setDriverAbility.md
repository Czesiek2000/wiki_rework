# Player::setDriverAbility

Decompiled scripts seem to use a range between **0.0** and **1.0**, but tests confirm that **1.0** is not the limit.

*Both 10.0 and 100.0 allow the driver to avoid other vehicles better and have faster reaction times, but I don't know if 10.0 is the limit or is it 100.0. Didn't really notice differences.* 

(wrong) MulleDK19: The above comment is either outdated, or just wrong from the start. 

The function specifically verifies the value is equal to, or less than `1.0f`. 

If it is greater than `1.0f`, the function does nothing at all.


## Syntax

```js
player.setDriverAbility(ability);
```

## Example

```js
// todo
```

## Parameters

<li><b>ability:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>