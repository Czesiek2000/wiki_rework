# Player::playFacialAnim

## Syntax

```js
player.playFacialAnim(animName, animDict);
```

## Example

```js
mp.events.add('startTalking', (player) => {
	player.playFacialAnim("mic_chatter", "mp_facial");
});

mp.events.add('stopTalking', (player) => {
	player.playFacialAnim("mood_normal_1", "facials@gen_male@variations@normal");
});
```

## Parameters

<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>