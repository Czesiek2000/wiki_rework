# Player::isBeingStunned

p1 is always 0


## Syntax

```js
player.isBeingStunned(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><b><span style="color:#008017">Boolean</span></b></b>