# Player::call

FROM SERVER: This function calls a client-side event for the selected player.

FROM CLIENT: For Peer 2 Peer connections. The current client can call an event on another client's scriptside and that other client can handle that event.


## Syntax

```js
player.call(String eventName [, Array args]);
```

## Example

This example will call the client-side event 'disablePlayerRegeneration' for the player with the ID 1337.

```js
// clientside
let disableRegeneration = (currentHealth) => { //currentHealth - value, what we send from server.
	mp.game.player.setHealthRechargeMultiplier(0); //Disable regeneration
	mp.gui.chat.push(`Regeneration disabled. Current health: ${currentHealth}`); //Output text to default chatbox
};

mp.events.add('disablePlayerRegeneration', disableRegeneration);
```
```js
// serverside
let player = mp.player.at(1337); //Get player by ID
if (player) {
	let playerHealth = player.health;
	player.call(`disablePlayerRegeneration`, [playerHealth]);
};
```

## Parameters

<li><b>eventName:</b> The event name that will be called</li>
<li><b>args:</b> Any arguments, what should be sent to client. Supports entities, strings, numbers and booleans. (Objects and Arrays should be packed to JSON format.) Arguments has to be packed in an array.</li>

## Return value

<b>Undefined</b>