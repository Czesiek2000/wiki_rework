# Player::setFacialDecoration

*Console Hash: 0x8CD3E487*


## Syntax

```js
player.setFacialDecoration(collection, overlay);
```

## Example

```js
// todo
```

## Parameters

<li><b>collection:</b> Model hash or name</li>
<li><b>overlay:</b> Model hash or name</li>

## Return value

<b>Undefined</b>