# Player::taskPlaneChase

## Syntax

```js
player.taskPlaneChase(entityToFollow, x, y, z);
```

## Example

```js
// todo
```

## Parameters

<li><b>entityToFollow:</b> Entity handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>