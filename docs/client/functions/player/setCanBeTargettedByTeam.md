# Player::setCanBeTargettedByTeam

## Syntax

```js
player.setCanBeTargettedByTeam(team, toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>team:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>