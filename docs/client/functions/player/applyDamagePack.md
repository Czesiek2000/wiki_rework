# Player::applyDamagePack

Damage Packs:

* SCR_TrevorTreeBang
* HOSPITAL_0
* HOSPITAL_1
* HOSPITAL_2
* HOSPITAL_3
* HOSPITAL_4
* HOSPITAL_5
* HOSPITAL_6
* HOSPITAL_7
* HOSPITAL_8
* HOSPITAL_9
* SCR_Dumpster
* BigHitByVehicle
* SCR_Finale_Michael_Face
* SCR_Franklin_finb
* SCR_Finale_Michael
* SCR_Franklin_finb2
* Explosion_Med
* SCR_Torture
* SCR_TracySplash
* Skin_Melee_0

Additional damage packs: [on gist](gist.github.com/alexguirre/f3f47f75ddcf617f416f3c8a55ae2227)


Adds bloody wounds to the player


## Syntax

```js
player.applyDamagePack(damagePack, damage, mult);
```

## Example

```js
player.applyDamagePack("Explosion_Large", 100, 1);
```

## Parameters

<li><b>damagePack:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>damage:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>mult:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>