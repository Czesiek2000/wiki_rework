# Player::isScriptedScenarioUsingConditionalAnim

## Syntax

```js
player.isScriptedScenarioUsingConditionalAnim(animDict, anim);
```

## Example

```js
// todo
```

## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>anim:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>