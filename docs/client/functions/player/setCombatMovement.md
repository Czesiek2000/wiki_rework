# Player::setCombatMovement

* 0 - Stationary (Will just stand in place)

* 1 - Defensive (Will try to find cover and very likely to blind fire)

* 2 - Offensive (Will attempt to charge at enemy but take cover as well)

* 3 - Suicidal Offensive (Will try to flank enemy in a suicidal attack)


## Syntax

```js
player.setCombatMovement(combatMovement);
```

## Example

```js
// todo
```

## Parameters

<li><b>combatMovement:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>