# Player::taskFollowNavMeshToCoordAdvanced

## Syntax

```js
player.taskFollowNavMeshToCoordAdvanced(x, y, z, speed, timeout, unkFloat, unkInt, unkX, unkY, unkZ, unk_40000f);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unkFloat:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unkInt:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unkX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unkY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unkZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unk_40000f:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>