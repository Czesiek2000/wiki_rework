# Player::setNameDebug

*NOTE: Debugging functions are not present in the retail version of the game.*

* untested but char 
* name could also be a hash for a localized string


## Syntax

```js
player.setNameDebug(name);
```

## Example

```js
// todo
```

## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>