# Player::isUsingScenario

## Syntax

```js
player.isUsingScenario(scenario);
```

## Example

```js
// todo
```

## Parameters

<li><b>scenario:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>