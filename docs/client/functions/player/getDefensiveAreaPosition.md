# Player::getDefensiveAreaPosition

## Syntax

```js
player.getDefensiveAreaPosition(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b><span style="color:#008017">Vector3</span></b>