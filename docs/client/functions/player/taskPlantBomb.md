# Player::taskPlantBomb

This function plays a planting bomb animation and plants a bomb in heading target location, you need to have the bomb equiped in your hands to work, otherwise nothing happens.

## Syntax

```js
player.taskPlantBomb(x, y, z, heading);
```

## Example

In the example below the player will plant when the X key is pressed:

```js
mp.keys.bind(88, false, () => {
  if (mp.players.local.weapon != mp.game.joaat('weapon_stickybomb'))
    return mp.gui.chat.push(`You need a sticky bomb to use planting feature!`);

  const { x, y, z } = mp.players.local.position;

  mp.players.local.taskPlantBomb(x, y, z, mp.players.local.heading);

});
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>