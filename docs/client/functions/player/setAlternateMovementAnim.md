## Player::setAlternateMovementAnim

stance:

0 = idle

1 = walk

2 = running

p5 = usually set to true


## Syntax

```js
player.setAlternateMovementAnim(stance, animDictionary, animationName, p4, p5);
```

## Example

```js
// todo
```

## Parameters

<li><b>stance:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animationName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>