# Player::getMount
Function just returns 0
```cpp
void __fastcall Player__get_mount(NativeContext *a1) {
    NativeContext *v1; // rbx@1
    v1 = a1; 
    GetAddressOfPedFromScriptHandle(a1->Args->Arg1); 
    v1->Returns->Item1= 0;
}
```

## Syntax

```js
player.getMount();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Ped handle or object</b></li>