# Player::setDucking

MulleDK19: Does nothing. Characters cannot duck in GTA V.

^^Wrong this is for ducking in the vehicle.

*- jedijosh920*

MulleDK19: ^ Wrong. This is the `SET_CHAR_DUCKING` from GTA IV, that makes Peds duck. 

This function does nothing in GTA V. It cannot set the ped as ducking in vehicles, and `IS_Player_DUCKING` will always return false.


## Syntax

```js
player.setDucking(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>