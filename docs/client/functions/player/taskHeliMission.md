# Player::taskHeliMission


## Syntax

```js
player.taskHeliMission(vehicle, p2, pedToFollow, posX, posY, posZ, mode, speed, radius, angle, p11, height, p13, landingFlag);
```

### Modes

* <b>4</b> (Heads to the destination)
* <b>8</b> (Flee from player)
* <b>9</b> (Circle around the destination)
* <b>10</b> (Take same target's heading)
* <b>20</b> (Lands the heli. on the destination)
* <b>21</b> (Crash on the destination)

## Example

```js
// Makes the helicopter pilot land on destination (X: 2113.1481, Y: 4799.7558, Z: 41.1516)
heliPed.taskHeliMission(heli.handle, 0, 0, 2113.1481, 4799.7558, 41.1516, 20, 1.0, 5, -1.0, 10, 0, 5.0, 32);
```

## Parameters

<ul><li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>p2:</b> unk</li>
<li><b>pedToFollow:</b> Ped handle or object</li>
<li><b>posX:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>mode:</b> <b><span style="color:#008017">int</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>angle:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>p11:</b> <b><span style="color:#008017">int</span></b></li>
<li><b>height:</b> <b><span style="color:#008017">int</span></b></li>
<li><b>p13:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>landingFlag:</b> <b><span style="color:#008017">int</span></b> (<b>Flags</b>: <code>0</code> Hover over destination, <code>32</code> Land on destination, <code>1024</code> Crash into destination, <code>4096</code> Rush and Hover to destination)</li></ul>


## Return value

<li><b>Undefined</b></li>