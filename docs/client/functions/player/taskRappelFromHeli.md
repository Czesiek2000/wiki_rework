# Player::taskRappelFromHeli

Only appears twice in the scripts.

```cpp
Player::TASK_RAPPEL_FROM_HELI(PLAYER::PLAYER_Player_ID(), 0x41200000);Player::TASK_RAPPEL_FROM_HELI(a_0, 0x41200000);
```

Fixed, definitely not a float and since it's such a big number obviously not a bool. All though note when I thought it was a bool and set it to 1 it seemed to work that same as `int 0x41200000`.


## Syntax

```js
player.taskRappelFromHeli(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>