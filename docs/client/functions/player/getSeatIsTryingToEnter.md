# Player::getSeatIsTryingToEnter

## Syntax

```js
player.getSeatIsTryingToEnter();
```

## Example

```js
const vehHandle = mp.players.local.getVehicleIsTryingToEnter();
if (vehHandle) {
    const seat = mp.players.local.getSeatIsTryingToEnter(); // usually returns -3 if ped is not entering any seat
    if (seat >= -1) mp.gui.chat.push(`You are trying to enter vehicle seat: ${seat}.`);
}
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>