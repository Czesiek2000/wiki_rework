# Player::getMaxHealth

## Syntax

```js
player.getMaxHealth();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>