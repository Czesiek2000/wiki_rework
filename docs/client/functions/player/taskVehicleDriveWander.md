# Player::taskVehicleDriveWander

## Syntax

```js
player.taskVehicleDriveWander(vehicle, speed, drivingStyle);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>drivingStyle:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>