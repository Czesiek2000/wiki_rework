# Player::taskFollowToOffsetOf

p6 always -1

p7 always 10.0

p8 always 1


## Syntax

```js
player.taskFollowToOffsetOf(entity, offsetX, offsetY, offsetZ, movementSpeed, timeout, stoppingRange, persistFollowing);
```

## Example

```js
// todo
```

## Parameters

<li><b>entity:</b> Entity handle or object</li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>movementSpeed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> int</li>
<li><b>stoppingRange:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>persistFollowing:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>