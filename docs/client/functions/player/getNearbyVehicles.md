# Player::getNearbyVehicles
Returns size of array, passed into the second variable.

See below for usage information.

This function actually requires a struct, where the first value is the maximum number of elements to return. Here is a sample of how I was able to get it to work correctly, without yet knowing the struct format.

```cpp
//Setup the array 
const int numElements = 10; 
const int arrSize = numElements * 2 + 2; 
Any veh[arrSize]; //0 index is the size of the array 
veh[0] = numElements; 
int count = Player::GET_Player_NEARBY_VEHICLES(PLAYER::PLAYER_Player_ID(), veh); 
if (veh != NULL) { 
    //Simple loop to go through results 
    for (int i = 0; i < count; i++) { 
        int offsettedID = i * 2 + 2; 
        //Make sure it exists 
        if (veh[offsettedID] != NULL && ENTITY::DOES_ENTITY_EXIST(veh[offsettedID])) { 
            //Do something 
        } 
    } 
}
```

## Syntax

```js
player.getNearbyVehicles(sizeAndVehs);
```

## Example

```js
// todo
```

## Parameters

<li><b>sizeAndVehs:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>