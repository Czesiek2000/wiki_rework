# Player::setAlertness

value ranges from 0 to 3.


## Syntax

```js
player.setAlertness(value);
```

## Example

```js
// todo
```

## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>