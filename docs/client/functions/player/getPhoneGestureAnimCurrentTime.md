# Player::getPhoneGestureAnimCurrentTime

## Syntax

```js
player.getPhoneGestureAnimCurrentTime();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Float</span></b></li>