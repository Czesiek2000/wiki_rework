# Player::taskVehicleMissionCoorsTarget

Example from `fm_mission_controller.c4`:

```cpp
Player::TASK_VEHICLE_MISSION_COORS_TARGET(l_65E1, l_65E2, 324.84588623046875, 325.09619140625, 104.3525, 4, 15.0, 802987, 5.0, 5.0, 0);
```

## Syntax

```js
player.taskVehicleMissionCoorsTarget(vehicle, x, Y, Z, p5, p6, p7, p8, p9, p10);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>