# Player::isJumping

This function returns 1 or 0 of player jumping state.


**Note: this property is read-only.**


## Syntax


## Example

```js
let playerIsJumping = player.isJumping()
if (playerIsJumping === 1)
  player.outputChatBox('You are jumping right now!');
else
  player.outputChatBox('You are not jumping right now!');
```

## Parameters


## Return value
