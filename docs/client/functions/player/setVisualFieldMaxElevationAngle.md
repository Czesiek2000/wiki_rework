# Player::setVisualFieldMaxElevationAngle

This native refers to the field of vision the ped has above them, starting at 0 degrees. 

90f would let the ped see enemies directly above of them.


## Syntax

```js
player.setVisualFieldMaxElevationAngle(angle);
```

## Example

```js
// todo
```

## Parameters

<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>