# Player::taskChatTo

p2 tend to be 16, 17 or 1

p3 to p7 tend to be 0.0


## Syntax

```js
player.taskChatTo(target, p2, p3, p4, p5, p6, p7);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>