# Player::applyBloodSpecific

## Syntax

```js
player.applyBloodSpecific(p1, p2, p3, p4, p5, p6, p7, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> unknown (to be checked)</li>

<br />

## Return value

<b>Undefined</b>