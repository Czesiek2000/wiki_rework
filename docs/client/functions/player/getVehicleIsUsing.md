# Player::getVehicleIsUsing

Gets ID of vehicle player using. It means it can get ID at any interaction with vehicle. Enter / exit for example. 

And that means it is faster than `GET_VEHICLE_Player_IS_IN` but less safe.


## Syntax

```js
player.getVehicleIsUsing();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Vehicle handle or object</b></li>