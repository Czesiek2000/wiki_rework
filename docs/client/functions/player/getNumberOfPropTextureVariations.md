# Player::getNumberOfPropTextureVariations
Need to check behavior when drawableId = -1 

Doofy.Ass

Why this function doesn't work and return nill value?
```cpp
GET_NUMBER_OF_Player_PROP_TEXTURE_VARIATIONS(PLAYER.PLAYER_Player_ID(), 0, 5)
```
tick: `scripts/addins/menu_execute.lua:51`: attempt to call field `GET_NUMBER_OF_Player_PROP_TEXTURE_VARIATIONS` (a nil value)

[List of component/props ID](gtaxscripting.blogspot.com/2016/04/gta-v-peds-component-and-props.html)


## Syntax

```js
player.getNumberOfPropTextureVariations(propId, drawableId);
```

## Example

```js
// todo
```

## Parameters

<li><b>propId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>drawableId:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>