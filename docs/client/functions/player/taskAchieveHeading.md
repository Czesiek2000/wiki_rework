# Player::taskAchieveHeading

Makes the specified ped achieve the specified heading.

* pedHandle: The handle of the ped to assign the task to.heading: The desired heading.

* timeout: The time, in milliseconds, to allow the task to complete. 
If the task times out, it is cancelled, and the ped will stay at the heading it managed to reach in the time.


## Syntax

```js
player.taskAchieveHeading(heading, timeout);
```

## Example

```js
// todo
```

## Parameters

<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>