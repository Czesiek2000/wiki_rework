# Player::taskStealthKill

known 'killTypes' are: 'AR_stealth_kill_knife' and 'AR_stealth_kill_a'.


## Syntax

```js
player.taskStealthKill(target, killType, p3, p4);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>
<li><b>killType:</b> Model hash or name</li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>