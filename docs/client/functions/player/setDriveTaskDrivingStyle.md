# Player::setDriveTaskDrivingStyle

This native is used to set the driving style for specific player.
Driving styles id seems to be:

786468

262144

786469

More on [gtaforums thread](http://gtaforums.com/topic/822314-guide-driving-styles/)


## Syntax

```js
player.setDriveTaskDrivingStyle(drivingStyle);
```

## Example

```js
// todo
```

## Parameters

<li><b>drivingStyle:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>