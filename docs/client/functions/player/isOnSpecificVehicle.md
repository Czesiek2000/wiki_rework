# Player::isOnSpecificVehicle

## Syntax

```js
player.isOnSpecificVehicle(vehicle);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>