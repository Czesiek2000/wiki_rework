# Player::getSourceOfDeath

Returns the **Entity (Ped, Vehicle, or ?Object?)** that killed the **ped**

Is best to check if the Ped is dead before asking for its killer.


## Syntax

```js
player.getSourceOfDeath();
```

## Example

```js
// todo
```

## Parameters

<li><b>Entity handle or object</b></li>

## Return value

<b>Undefined</b>