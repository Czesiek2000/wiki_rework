# Player::taskReloadWeapon

The 2nd param (unused) is not implemented.

-----------------------------------------------------------------------

The only occurrence I found in a R* script ('assassin_construction.ysc.c4'): 
```cpp
if (((v_3 < v_4) && (Player::GET_SCRIPT_TASK_STATUS(PLAYER::PLAYER_Player_ID(), 0x6a67a5cc) != 1)) && (v_5 > v_3)) { 
    Player::TASK_RELOAD_WEAPON(PLAYER::PLAYER_Player_ID(), 1); 
}
```

## Syntax

```js
player.taskReloadWeapon(doReload);
```

## Example

```js
// todo
```

## Parameters

<li><b>doReload:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>