# Player::taskVehicleShootAt

## Syntax

```js
player.taskVehicleShootAt(target, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>