# Player::taskStartScenarioInPlace

Plays a scenario on a player at their current location.

This will play the drilling scenario and constantly loop.

## Syntax

```js
player.taskStartScenarioInPlace(scenarioName, unkDelay, playEnterAnim);
```

## Example

```js
mp.events.add('playDrillScenario', () => {
    mp.players.local.taskStartScenarioInPlace('WORLD_HUMAN_CONST_DRILL', 0, false);
});
```

## Parameters

<li><b>scenarioName:</b> <b><span style="color:#008017">String</span></b> ( <a href="https://wiki.rage.mp/index.php?title=Scenarios" title="Scenarios">Scenario List</a> )</li>
<li><b>unkDelay:</b> <b><span style="color:#008017">Int</span></b> (0 or -1 loops infinitely, anything else will only make it run once)</li>
<li><b>playEnterAnim:</b> <b><span style="color:#008017">Boolean</span></b> (True = Play the 'enter' animation, False = Exit animation)</li>

## Return value

<b>Undefined</b>