# Player::taskClimb

Climbs or vaults the nearest thing.


## Syntax

```js
player.taskClimb(unused);
```

## Example

```js
// todo
```

## Parameters

<li><b>unused:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>