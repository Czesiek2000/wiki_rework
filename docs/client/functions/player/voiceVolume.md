# Player::voiceVolume

After voice is unmuted, this function sets other player's voice volume for the local player

Volume is between 0.0 - 1.0

## Syntax

```js
const local = mp.players.local;
mp.players.forEach(player => {
    var PlayerDist = mp.game.gameplay.getDistanceBetweenCoords(local.position.x, local.position.y, local.position.z, player.position.x, player.position.y, player.position.z, true);
    if(PlayerDist < 0.5)
    {
        player.voiceVolume = 1.0;
    }
    else
    {
        player.voiceVolume = 0.5;
    }
})
```

## Example

No example here

## Parameters

No parameters here

## Return value

No return value