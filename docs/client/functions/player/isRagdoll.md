# Player::isRagdoll

If the ped handle passed through the parenthesis is in a ragdoll state this will return true.

*-UNBOUND-*


## Syntax

```js
player.isRagdoll();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>