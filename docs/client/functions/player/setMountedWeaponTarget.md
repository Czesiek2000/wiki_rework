# Player::setMountedWeaponTarget

**Note: Look in decompiled scripts and the times that p1 and p2 aren't 0. They are filled with vars.**

If you look through out that script what other natives those vars are used in, you can tell p1 is a ped and p2 is a vehicle. 

Which most likely means if you want the mounted weapon to target a ped set targetVehicle to 0 or vice-versa.


## Syntax

```js
player.setMountedWeaponTarget(targetEntity, p2, x, y, z);
```

## Example

```js
// todo
```

## Parameters

<li><b>targetEntity:</b> Entity handle or object</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>