# Player::setDriveByClipsetOverride

## Syntax

```js
player.setDriveByClipsetOverride(clipset);
```

## Example

```js
// todo
```

## Parameters

<li><b>clipset:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>