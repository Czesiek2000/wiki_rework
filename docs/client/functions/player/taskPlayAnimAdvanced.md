# Player::taskPlayAnimAdvanced

It's similar to the one above, except the first 6 floats let you specify the initial position and rotation of the task. (Ped gets teleported to the position). 

animTime is a float from 0.0 -> 1.0, lets you start an animation from given point. The rest as in `Player::TASK_PLAY_ANIM`. 

Rotation information : rotX and rotY don't seem to have any effect, only rotZ works.


## Syntax

```js
player.taskPlayAnimAdvanced(animDict, animName, posX, posY, posZ, rotX, rotY, rotZ, speed, speedMultiplier, duration, flag, animTime, p14, p15);
```

## Example

```js
// todo
```

## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speedMultiplier:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> int</li>
<li><b>flag:</b> unknown (to be checked)</li>
<li><b>animTime:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p14:</b> unknown (to be checked)</li>
<li><b>p15:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>