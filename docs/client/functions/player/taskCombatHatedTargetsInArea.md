# Player::taskCombatHatedTargetsInArea

Despite its name, it only attacks ONE hated target. The one closest to the specified position.


## Syntax

```js
player.taskCombatHatedTargetsInArea(x, y, z, radius, p5);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> unknown (to be checked)</li>

## Return value

<b>Undefined</b>