# Player::setMinMoveBlendRatio

## Syntax

```js
player.setMinMoveBlendRatio(value);
```

## Example

```js
// todo
```

## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>