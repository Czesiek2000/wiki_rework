# Player::taskLookAt

param3: duration in ms, use -1 to look forever

param4: using 2048 is fine

param5: using 3 is fine


## Syntax

```js
player.taskLookAt(lookAt, duration, unknown1, unknown2);
```

## Example

```js
// todo
```

## Parameters

<li><b>lookAt:</b> Entity handle or object</li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unknown1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unknown2:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>