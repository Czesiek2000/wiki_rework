# Player::setPlaysHeadOnHornAnimWhenDiesInVehicle

Points to the same function as for example `GET_RANDOM_VEHICLE_MODEL_IN_MEMORY` and it does absolutely nothing.


## Syntax

```js
player.setPlaysHeadOnHornAnimWhenDiesInVehicle(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>