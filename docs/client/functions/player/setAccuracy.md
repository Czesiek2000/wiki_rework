# Player::setAccuracy

accuracy = 0-100, 100 being perfectly accurate


## Syntax

```js
player.setAccuracy(accuracy);
```

## Example

```js
// todo
```

## Parameters

<li><b>accuracy:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Unknown (to be checked)</b>