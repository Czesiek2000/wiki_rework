# Player::setShootRate

shootRate 0-1000


## Syntax

```js
player.setShootRate(shootRate);
```

## Example

```js
// todo
```

## Parameters

<li><b>shootRate:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>