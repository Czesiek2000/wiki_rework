# Player::getMeleeTargetFor

## Syntax

```js
player.getMeleeTargetFor();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>Ped handle or object</b></li>