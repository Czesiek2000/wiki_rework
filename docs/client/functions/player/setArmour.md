# Player::setArmour

Sets the armor of the specified player.

ped: The Ped to set the armor of.

amount: A value between **0** and **100** indicating the value to set the Ped's armor to.


## Syntax

```js
player.setArmour(amount);
```

## Example

```js
// todo
```

## Parameters

<li><b>amount:</b> int</li>

## Return value

<b>Undefined</b>