# Player::setCapsule

Overrides the ped's collision capsule radius for the current tick.Must be called every tick to be effective.

Setting this to **0.001** will allow warping through some objects.


## Syntax

```js
player.setCapsule(value);
```

## Example

```js
// todo
```

## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>