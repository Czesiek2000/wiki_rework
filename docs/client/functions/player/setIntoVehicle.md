# Player::setIntoVehicle

This function puts ped into vehicle.


Seats:
<ul><li>-1 - driver seat</li>
<li>0 - passenger seat 1</li>
<li>1 - passenger seat 2</li>
<li>2 - passenger seat 3</li></ul>

(note this is for normal vehicles with 4 seats)


## Syntax

```js
player.setIntoVehicle(vehicle, seatIndex);
```

## Example

```js
// todo
```

## Parameters



## Return value

<b>Undefined</b>