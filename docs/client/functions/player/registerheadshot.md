# Player::registerheadshot

## Syntax

```js
player.registerheadshot();
```

## Example

```js
let pedHeadShot;
if (pedHeadShot == null) {
    pedHeadShot = mp.players.local.registerheadshot();
    mp.gui.chat.push(`pedHeadShot: ${pedHeadShot}`);
}

mp.events.add('render', () => {
    if (pedHeadShot == null) {
        pedHeadShot = mp.players.local.registerheadshot();
        mp.gui.chat.push(`pedHeadShot: ${pedHeadShot}`);
    }
    if (mp.game.ped.isPedheadshotValid(pedHeadShot) && mp.game.ped.isPedheadshotReady(pedHeadShot)) {
        const headshotTexture = mp.game.ped.getPedheadshotTxdString(pedHeadShot);
	
        mp.game.graphics.drawSprite(headshotTexture, headshotTexture, 0.5, 0.5, 0.1, 0.1, 0, 255, 255, 255, 100);
    }
});
```

## Parameters


## Return value

<b><span style="color:#008017">Int</span></b>