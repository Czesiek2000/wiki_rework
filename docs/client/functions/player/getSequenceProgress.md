# Player::getSequenceProgress
returned values:
0 to 7 = task that's currently in progress, 0 meaning the first one.

-1 no task sequence in progress.


## Syntax

```js
player.getSequenceProgress();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b><span style="color:#008017">Int</span></b></li>