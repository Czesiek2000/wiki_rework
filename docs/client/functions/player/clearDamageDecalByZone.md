# Player::clearDamageDecalByZone
p1: from 0 to 5 in the **b617d scripts.**

p2: 'blushing' and 'ALL' found in the **b617d** scripts.


## Syntax

```js
player.clearDamageDecalByZone(p1, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>