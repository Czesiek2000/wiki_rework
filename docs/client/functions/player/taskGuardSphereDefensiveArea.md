# Player::taskGuardSphereDefensiveArea

p0 - Guessing PedID

p1, p2, p3 - XYZ?p4 - ???p5 - Maybe the size of sphere from XYZ?p6 - ???p7, p8, p9 - XYZ again?

p10 - Maybe the size of sphere from second XYZ?


## Syntax

```js
player.taskGuardSphereDefensiveArea(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>