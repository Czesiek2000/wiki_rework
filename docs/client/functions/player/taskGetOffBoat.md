# Player::taskGetOffBoat

*MulleDK19: Jenkins of this native is `0x4293601F`. This is the actual name.*


## Syntax

```js
player.taskGetOffBoat(boat);
```

## Example

```js
// todo
```

## Parameters

<li><b>boat:</b> Vehicle handle or object</li>

## Return value

<b>Undefined</b>