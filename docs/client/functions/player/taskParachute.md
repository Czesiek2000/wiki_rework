# Player::taskParachute

This function has a third parameter as well (bool).

Second parameter is unused.

seconds parameter was for jetpack in the early stages of gta and the hard coded code is now removed


## Syntax

```js
player.taskParachute(p1);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>