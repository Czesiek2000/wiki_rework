# Player::getHeadBlendData

The pointer is to a padded struct that matches the arguments to `SET_Player_HEAD_BLEND_DATA(...)`. 
There are 4 bytes of padding after each field.

(Edit) Console Hash: `0x44E1680C` 

pass this struct in the second parameter 
```cpp
typedef struct { 
    int shapeFirst, shapeSecond, shapeThird;  
    int skinFirst, skinSecond, skinThird;  
    float shapeMix, skinMix, thirdMix; 
} headBlendData;
```

## Syntax

```js
player.getHeadBlendData(headBlendData);
```

## Example

```js
// todo
```

## Parameters

<li><b>headBlendData:</b> unknown (to be checked)</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>