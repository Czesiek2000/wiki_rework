# Player::taskGoStraightToCoord

timeout (-1 works, others seem to break)

distanceToSlide (distance the player can slide if needed)


## Syntax

```js
player.taskGoStraightToCoord(x, y, z, speed, timeout, targetHeading, distanceToSlide);
```

## Example

```js
player.taskGoStraightToCoord(x, y, z, 6.2, -1, player.getHeading(), 2);
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>targetHeading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>distanceToSlide:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>