# Player::taskCombat

Makes the specified ped attack the target player.

p2 should be 0

p3 should be 16


## Syntax

```js
player.taskCombat(targetPed, p2, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>targetPed:</b> Ped handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>