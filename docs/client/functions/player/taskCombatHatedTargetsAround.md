# Player::taskCombatHatedTargetsAround

Despite its name, it only attacks ONE hated target. The one closest hated target.

p2 seems to be always 0


## Syntax

```js
player.taskCombatHatedTargetsAround(radius, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>