# Player::clearWetness

It clears the wetness of the selected Ped/Player. Clothes have to be wet to notice the difference.


## Syntax

```js
player.clearWetness();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b>unknown (to be checked)</b></li>