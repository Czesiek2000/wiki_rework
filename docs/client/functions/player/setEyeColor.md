# Player::setEyeColor

Set a pedestrians eye color based on the integers between 0 and 31.


## Syntax

```js
player.setEyeColor(index);
```

## Example

```js
mp.players.local.setEyeColor(1);
```

## Parameters

<li>index: <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>