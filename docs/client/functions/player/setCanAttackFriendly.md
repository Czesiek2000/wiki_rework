# Player::setCanAttackFriendly

Setting ped to true allows the ped to shoot 'friendlies'.

p2 set to true when toggle is also true seams to make peds permanently unable to aim at, even if you set p2 back to false.

p1 = false & p2 = false for unable to aim at.

p1 = true & p2 = false for able to aim at.


## Syntax

```js
player.setCanAttackFriendly(toggle, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>