# Player::setFleeAttributes

Bool probably has something to do with vehicles, maybe if the ped can use vehicle to flee?

Values used as attributes are those in sequence of powers of two, 1, 2, 4, 8, 16, 32, 64.... 65536.


## Syntax

```js
player.setFleeAttributes(attributes, p2);
```

## Example

```js
// todo
```

## Parameters

<li><b>attributes:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>