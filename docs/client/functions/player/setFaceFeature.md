# Player::setFaceFeature

Sets the various freemode face features, e.g. nose length, chin shape. Scale ranges from -1.0 to 1.0.

Index can be 0 - 19.

## Syntax

```js
player.setFaceFeature(index, scale);
```

## Example

```js
function updateAppearance(genderChanged) {

    if (genderChanged) {
        mp.players.local.model = characterData.gender ? mp.game.joaat("mp_m_freemode_01") : mp.game.joaat("mp_f_freemode_01");

        setTimeout(sUpdateAppearance, 100);
    }

    else
        sUpdateAppearance();
    }

    function sUpdateAppearance() {
        
        for (var i = 0; i < 20; i++)
            mp.players.local.setFaceFeature(i, characterData.faceFeatures[i]);
    }
}
```

## Parameters

<li><b>index:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>