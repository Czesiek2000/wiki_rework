# Player::setVisualFieldPeripheralRange

## Syntax

```js
player.setVisualFieldPeripheralRange(range);
```

## Example

```js
// todo
```

## Parameters

<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>