# Player::setHeadOverlay

OverlayID ranges from 0 to 12, index from 0 to `_GET_NUM_OVERLAY_VALUES(overlayID)-1`, and opacity from 0.0 to 1.0.

First and second color you can take in the list of hair colors.
[List of colors](https://wiki.gtanet.work/index.php?title=Hair_Colors)

To disable any overlay use 255 as index.

| OverlayID | Part | Index | 
| --------- | ---  | ----  | 
| 0 | Blemishes | 0-23 | 
| 1 | Facial Hair | 0-28 | 
| 2 | Eyebrows | 0-33 | 
| 3 | Ageing | 0-14 | 
| 4 | Makeup | 0-74 | 
| 5 | Blush | 0-32 | 
| 6 | Complexion | 0-11 | 
| 7 | Sun Damage | 0-10 | 
| 8 | Lipstick | 0-9 | 
| 9 | Moles/Freckles | 0-17 | 
| 10 | Chest Hair | 0-16 | 
| 11 | Body Blemishes | 0-11 | 
| 12 | Add Body Blemishes | 0-1 | 


## Syntax

```js
player.setHeadOverlay(overlayID, index, opacity, firstColor, secondColor);

// The variation of this function for peds (mp.peds) does not take values to set the overlay color. It uses the syntax used in the native.
ped.setHeadOverlay(overlayID, index, opacity);

```

## Example

```js
player.setHeadOverlay(overlayID, [index, opacity, firstColor, secondColor]);
```
```js
//C# Serverside Example

private static readonly string[] headOverlayNames = { "blemishes", "facialHair", "eyebrows", "ageing", "makeup", "blush", "complexion", "sunDamage", "lipstick", "molesFreckles", "chestHair", "bodyBlemishes", "addBodyBlemishes" };

private static HeadOverlay CreateHeadOverlay(Byte index, Byte color, Byte secondaryColor, float opacity){
    return new HeadOverlay
    {
       Index = index,
       Color = color,
       SecondaryColor = secondaryColor,
       Opacity = opacity
    };
}

[Command("beard")]
public void Beard(Client c, byte Index, byte Color, byte SeconardyColor, byte Opacity)
{
    Dictionary<int, HeadOverlay> headOverlays = new Dictionary<int, HeadOverlay>();
    headOverlays.Add(1, CreateHeadOverlay((byte)Index, (byte)Color, (byte)SeconardyColor, (byte)Opacity));
    c.SetHeadOverlay(1, headOverlays[1]);
}

Testexamples use Command:
beard 1 0 0 1 = Set beard 1 with PrimaryColor 0 SecondardyColor 0 and opacity 1 (use opacity 0 for invisible, 1 for full visible)
beard 2 22 0 1 = Set beard 1 with PrimaryColor 22(red) SecondardyColor 0 and opacity 1 (use opacity 0 for invisible, 1 for full visible)
Restriction: Command can just handle 0, 1 on opacity but it is a byte and can have a value from 0.0 - 1.0
```

## Parameters

<li><b>overlayID:</b> int</li>
<li><b>index:</b> int</li>
<li><b>opacity:</b> float</li>
<li><b>firstColor:</b> int</li>
<li><b>secondColor:</b> int</li>

## Return value

<b>Undefined</b>