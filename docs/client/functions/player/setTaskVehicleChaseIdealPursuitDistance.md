# Player::setTaskVehicleChaseIdealPursuitDistance

## Syntax

```js
player.setTaskVehicleChaseIdealPursuitDistance(distance);
```

## Example

```js
// todo
```

## Parameters

<li><b>distance:</b> float</li>

## Return value

<b>Undefined</b>