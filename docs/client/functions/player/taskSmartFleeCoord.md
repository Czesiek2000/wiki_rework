# Player::taskSmartFleeCoord

Makes the specified ped flee the specified distance from the specified position.


## Syntax

```js
player.taskSmartFleeCoord(x, y, z, distance, time, p6, p7);
```

## Example

```js
// todo
```

## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>