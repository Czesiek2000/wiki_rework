# Player::taskVehicleChase

chases targetEnt fast and aggressively

-- Makes ped (needs to be in vehicle) chase targetEnt.


## Syntax

```js
player.taskVehicleChase(targetEnt);
```

## Example

```js
/*arg0 = pedName, arg1 = Position.X, arg2 = Position.Y, arg3 = Position.Z */

const myplayer = mp.players.local;

mp.events.add('PedDriving3', (arg0, arg1, arg2, arg3) => {
let testPed = mp.peds.new(mp.game.joaat(arg0), new mp.Vector3(arg1, arg2, arg3), {dynamic:true}); //synced ped
testPed.controller = mp.players.at(0);

	setTimeout(function () {
	
		testPed.freezePosition(false);
		testPed.setCanBeDamaged(true);
		testPed.setInvincible(false);
		testPed.CanRagdoll = true;
		testPed.setOnlyDamagedByPlayer(true);
		testPed.setCanRagdollFromPlayerImpact(true);
		testPed.setSweat(100);
		testPed.setRagdollOnCollision(true);

		testPed.setProofs(false, false, false, false, false, false, false, false); 
		testPed.giveWeapon(487013001, 50, true);

		let Veh = mp.vehicles.new(mp.game.joaat("police"), new mp.Vector3(arg1, arg2, arg3),
		{
			numberPlate: "ADMIN",
			locked: false,
			engine: true,
			color: [[255, 255, 255],[0,0,0]]
		});

		setTimeout(function () {
			testPed.taskEnterVehicle(Veh.handle, 10000, -1, 1, 1, 0);
		}, 1500);

		//player.taskVehicleDriveToCoord(vehicle, x, y, z, speed, p6, vehicleModel, drivingMode, stopRange, p10);
		
		setTimeout(function () {
			testPed.taskVehicleChase(myplayer.handle);
			//mp.gui.chat.push("StartDriveTo: " + new Date());
		}, 10500);
	}, 300);
});
```

## Parameters

<li><b>targetEnt:</b> Entity handle or object</li>

## Return value

<b>Undefined</b>