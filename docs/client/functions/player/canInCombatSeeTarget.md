# Player::canInCombatSeeTarget

## Syntax

```js
player.canInCombatSeeTarget(target);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>

## Return value

<b>Undefined</b>