# Player::reviveInjured

It will revive/cure the injured player. The condition is ped must not be dead.Upon setting and converting the health int, found, if health falls below 5, the ped will lay on the ground in pain(Maximum default health is 100).

This function is well suited there.


## Syntax

```js
player.reviveInjured();
```

## Example

```js
// todo
```

## Parameters


## Return value

<li><b>Undefined</b></li>
