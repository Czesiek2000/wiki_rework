# Player::isInCombat

Checks to see if ped and target are in combat with eachother. Only goes one-way: 

if target is engaged in combat with ped but ped has not yet reacted, the function will return false until ped starts fighting back.

p1 is usually 0 in the scripts because it gets the ped id during the task sequence. 

For instance: 

```cpp
Player::IS_Player_IN_COMBAT(l_42E[4/*14*/], PLAYER::PLAYER_Player_ID()) // armenian2.ct4: 43794
```

## Syntax

```js
player.isInCombat(target);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>