# Player::taskVehicleDriveToCoordLongrange

## Syntax

```js
player.taskVehicleDriveToCoordLongrange(vehicle, x, y, z, speed, driveMode, stopRange);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>driveMode:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>stopRange:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>