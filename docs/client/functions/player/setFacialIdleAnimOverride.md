# Player::setFacialIdleAnimOverride

**WARNING:**
Works only via a native call! 

Also, animDict argument should be 0

```js
const SET_FACIAL_IDLE_ANIM_OVERRIDE = '0xFFC24B988B938B38';

const player = mp.players.local;
const mood = 'mood_happy_1'

mp.game.invoke(SET_FACIAL_IDLE_ANIM_OVERRIDE, player.handle, mood, 0);
```

See more in a real-world example: [link](https://rage.mp/files/file/108-moods/)


## Syntax

```js
player.setFacialIdleAnimOverride(animName, animDict);
```

## Example

```js
// todo
```

## Parameters

<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>