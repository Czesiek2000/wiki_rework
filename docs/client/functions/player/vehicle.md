# Player::vehicle

Returns the vehicle object the player is currently sitting in.

## Syntax

```js
player.vehicle
```

## Example

```js
if (player.vehicle)
  player.outputChatBox('You are in the vehicle right now!');
else
  player.outputChatBox('You are not in the vehicle right now!');
```

## Parameters

No parameters here

## Return value

<b>Undefined</b>