# Player::isTrackedVisible

returns whether or not a ped is visible within your FOV, not this check auto's to false after a certain distance.

*~ Lynxaa*

Target needs to be tracked.. won't work otherwise.

*-THEAETIK*


## Syntax

```js
player.isTrackedVisible();
```

## Example

```js
// todo
```

## Parameters

<li><b>Boolean</b></li>

## Return value

<b>Undefined</b>