# Player::setHelmetTextureIndex

## Syntax

```js
player.setHelmetTextureIndex(textureIndex);
```

## Example

```js
// todo
```

## Parameters

<li><b>textureIndex:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>