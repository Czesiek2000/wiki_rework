# Player::taskHandsUp

In the scripts, p3 was always -1.

p3 seems to be duration or timeout of turn animation.Also facingPed can be 0 or -1 so ped will just raise hands up. 

If p3 is 0 and there is no facingPed, the player will face north.


## Syntax

```js
player.taskHandsUp(duration, facingPed, p3, p4);
```

## Example

```js
// This will place hands up constantly
var localPlayer = mp.players.local;

localPlayer.taskHandsUp(-1, localPlayer.handle, 0, false);
```

## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>facingPed:</b> Ped handle or object</li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>