# Player::getNearbyPeds

sizeAndPeds - is a pointer to an array. The array is filled with peds found nearby the ped supplied to the first argument.

ignore - ped type to ignore.

Return value is the number of peds found and added to the array passed.

To make this work in most menu bases at least in C++ do it like so, 

[Formatted Example](https://pastebin.com/D8an9wwp)

[Example](gtaforums.com/topic/789788-function-args-to-pedget-ped-nearby-peds/?p=1067386687)


## Syntax

```js
player.getNearbyPeds(sizeAndPeds, ignore);
```

## Example

```js
// todo
```

## Parameters

<li><b>sizeAndPeds:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>ignore:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b><span style="color:#008017">Int</span></b>