# Player::isFacingPed

angle is ped's view cone


## Syntax

```js
player.isFacingPed(otherPed, angle);
```

## Example

```js
// todo
```

## Parameters

<li><b>otherPed:</b> Ped handle or object</li>
<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b><span style="color:#008017">Boolean</span></b>