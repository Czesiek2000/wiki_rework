# Player::playAnimOnRunningScenario

## Syntax

```js
player.playAnimOnRunningScenario(animDict, animName);
```

## Example

```js
// todo
```

## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>

## Return value

<b>Undefined</b>