# Player::setWetnessHeight

It adds the wetness level to the player clothing/outfit. 

As if player just got out from water surface.


## Syntax

```js
player.setWetnessHeight(height);
```

## Example

```js
// todo
```

## Parameters

<li><b>height:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<li><b>unknown (to be checked)</b></li>