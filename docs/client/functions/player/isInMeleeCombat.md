# Player::isInMeleeCombat

**Notes: The function only returns true while the ped is:** 

1. Swinging a random melee attack (including pistol-whipping)

2. Reacting to being hit by a melee attack (including pistol-whipping)

3. Is locked-on to an enemy (arms up, strafing/skipping in the default fighting-stance, ready to dodge+counter). 

You don't have to be holding the melee-targetting button to be in this stance; you stay in it by default for a few seconds after swinging at someone. 

If you do a sprinting punch, it returns true for the duration of the punch animation and then returns false again, even if you've punched and made-angry many peds


## Syntax

```js
player.isInMeleeCombat();
```

## Example

```js
// todo
```

## Parameters

<li><b>Boolean</b></li>

## Return value

<b>Undefined</b>