# Player::isReloading

Returns whether the specified ped is reloading.


## Syntax

```js
player.isReloading();
```

## Example

```js
// todo
```

## Parameters


## Return value

<b><span style="color:#008017">Boolean</span></b>