# Player::taskEnterVehicle

## Syntax

```js
player.taskEnterVehicle(vehicle, timeout, seat, speed, mode, p6);

```

## Example

```js
let Ped = mp.peds.new(mp.game.joaat('MP_F_Freemode_01'), new mp.Vector3( 100.0, -100.0, 25.0), 270.0, (streamPed) => {
    // Ped Streamed
    streamPed.setAlpha(0);
}, player.dimension);

let Veh = mp.vehicles.new(mp.game.joaat("turismor"), new mp.Vector3(-421.88, 1136.86, 326),  {
    numberPlate: "ADMIN",
    color: [[255, 0, 0],[255,0,0]]
});

Ped.taskEnterVehicle(Veh.handle, 10000, -1, 1, 1, 0);
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>timeout:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>seat:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b> (<b>Speeds</b>: <code>1.0</code> Walking, <code>2.0</code> Running)</li>
<li><b>mode:</b> <b><span style="color:#008017">Int</span></b> (<b>Modes</b>: <code>1</code> Moves to the vehicle, <code>3</code> Teleport to the vehicle, <code>16</code> Teleports directly intro vehicle, <code>8</code> Jacks the vehicle, <code>262144</code> Enters the vehicle from the opposite door, <code>524288</code> Opens door without entering vehicle)</li>
<li><b>p6:</b> int (Always 0)</li>

## Return value

<b>Undefined</b>