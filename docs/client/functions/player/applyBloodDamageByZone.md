# Player::applyBloodDamageByZone

## Syntax

```js
player.applyBloodDamageByZone(p1, p2, p3, p4);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>

<br />

## Return value

<b>Undefined</b>