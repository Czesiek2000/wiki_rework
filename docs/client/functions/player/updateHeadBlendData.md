# Player::updateHeadBlendData
See `SET_Player_HEAD_BLEND_DATA()`.


## Syntax

```js
player.updateHeadBlendData(shapeMix, skinMix, thirdMix);
```

## Example

```js
// todo
```

## Parameters

<li><b>shapeMix:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>skinMix:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>thirdMix:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>