# Player::taskWrithe

EX: `Function.Call(Ped1, Ped2, Time, 0);`

The last parameter is always 0 for some reason I do not know. 

The first parameter is the pedestrian who will writhe to the pedestrian in the other parameter. 

The third paremeter is how long until the Writhe task ends. When the task ends, the ped will die. 

If set to -1, he will not die automatically, and the task will continue until something causes it to end. 

This can be being touched by an entity, being shot, explosion, going into ragdoll, having task cleared. Anything that ends the current task will kill the ped at this point.

MulleDK19: Third parameter does not appear to be time. The last parameter is not implemented (It's not used, regardless of value).


## Syntax

```js
player.taskWrithe(target, time, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>target:</b> Ped handle or object</li>
<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>