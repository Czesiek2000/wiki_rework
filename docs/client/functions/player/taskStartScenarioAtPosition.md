# Player::taskStartScenarioAtPosition

Scenario List

----------------

going on the last 3 parameters, they appear to always be '0, 0, 1'

p6 -1 also used in scripsp7 used for sitting scenarios

p8 teleports ped to position


## Syntax

```js
player.taskStartScenarioAtPosition(scenarioName, x, y, z, heading, p6, p7, p8);
```

## Example

```js
// todo
```

## Parameters

<li><b>scenarioName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>