# Player::setConfigFlag


Toggles player config flag.

See available flags here: [Player Config Flags](../../../tables/configFlags.md)


## Syntax

```js
player.setConfigFlag(flagId, state);
```

## Example

```js
// todo
```

## Parameters

<ul><li><span style="font-weight:bold; color:red;">*</span><b>flagId:</b> <b><span style="color:#008017">Int</span></b> (<a href="../../../tables/configFlags.md">Flag IDs</a>)</li>
<li><span style="font-weight:bold; color:red;">*</span><b>state:</b> <b><span style="color:#008017">Boolean</span></b> (Toggle flag's state)</li></ul>
