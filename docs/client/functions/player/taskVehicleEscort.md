# Player::taskVehicleEscort

Makes a ped follow the targetVehicle with <minDistance> in between.

note: minDistance is ignored if drivingstyle is avoiding traffic, but Rushed is fine.

Mode: The mode defines the relative position to the targetVehicle. 

The ped will try to position its vehicle there.

* -1: behind

* 0: ahead

* 1: left

* 2: right

* 3: back left

* 4: back right

if the target is closer than noRoadsDistance, the driver will ignore pathing/roads and follow you directly.

[Driving Styles guide](https://gtaforums.com/topic/822314-guide-driving-styles/)


## Syntax

```js
player.taskVehicleEscort(vehicle, targetVehicle, mode, speed, drivingStyle, minDistance, p7, noRoadsDistance);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>targetVehicle:</b> Vehicle handle or object</li>
<li><b>mode:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>drivingStyle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minDistance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>noRoadsDistance:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>