# Player::stopAnimTask

## Syntax

```js
player.stopAnimTask(animDictionary, animationName, p3);
```

## Example

```js
mp.players.local.stopAnimTask("[email protected]_robbery", "robbery_action_f", 3.0);
```

## Parameters

<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animationName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>