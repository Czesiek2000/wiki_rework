# Player::setMinGroundTimeForStungun

Ped will stay on the ground after being stunned for at lest ms time. (in milliseconds)


## Syntax

```js
player.setMinGroundTimeForStungun(ms);
```

## Example

```js
// todo
```

## Parameters

<li><b>ms:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>