# Player::setDriverAggressiveness

range 0.0f - 1.0f


## Syntax

```js
player.setDriverAggressiveness(aggressiveness);
```

## Example

```js
// todo
```

## Parameters

<li><b>aggressiveness:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>