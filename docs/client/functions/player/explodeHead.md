# Player::explodeHead

Forces the ped to fall back and kills it.


## Syntax

```js
player.explodeHead(weaponHash);
```

## Example

```js
// todo
```

## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

## Return value

<b>Undefined</b>