# Player::setCoordsNoGang

## Syntax

```js
player.setCoordsNoGang(posX, posY, posZ);
```

## Example

```js
// todo
```

## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>