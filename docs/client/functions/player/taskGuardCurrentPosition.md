# Player::taskGuardCurrentPosition

From re_prisonvanbreak:

```cpp
Player::TASK_GUARD_CURRENT_POSITION(l_DD, 35.0, 35.0, 1);
```

## Syntax

```js
player.taskGuardCurrentPosition(p1, p2, p3);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>