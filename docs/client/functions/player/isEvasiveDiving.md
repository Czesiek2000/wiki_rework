# Player::isEvasiveDiving

Presumably returns the Entity that the Ped is currently diving out of the way of.

```csharp
var num3; 
if (Player::IS_Player_EVASIVE_DIVING(A_0, &num3) != 0) 
if (ENTITY::IS_ENTITY_A_VEHICLE(num3) != 0)
```


## Syntax

```js
player.isEvasiveDiving(evadingEntity);
```

## Example

```js
// todo
```

## Parameters

<li><b>evadingEntity:</b> Entity handle or object</li>

## Return value

<b><span style="color:#008017">Boolean</span></b>