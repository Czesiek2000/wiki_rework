# Player::applyBlood

Found one occurence in **re_crashrescue.c4** 

```cpp
Player::APPLY_Player_BLOOD(l_4B, 3, 0.0, 0.0, 0.0, 'wound_sheet');
```

winject


## Syntax

```js
player.applyBlood(boneIndex, xRot, yRot, zRot, woundType);
```

## Example

```js
// todo
```

## Parameters

<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>xRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>woundType:</b> <b><span style="color:#008017">String</span></b></li>

<br />

## Return value

<b>Undefined</b>