# Player::setBlockingOfNonTemporaryEvents

works with `Player::TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS` to make a ped completely oblivious to all events going on around him


## Syntax

```js
player.setBlockingOfNonTemporaryEvents(toggle);
```

## Example

```js
// todo
```

## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

## Return value

<b>Undefined</b>