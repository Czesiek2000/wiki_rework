# Player::applyDamageDecal

```cpp
APPLY_Player_DAMAGE_DECAL(ped, 1, 0.5f, 0.513f, 0f, 1f, unk, 0, 0, 'blushing');
```


## Syntax

```js
player.applyDamageDecal(p1, p2, p3, p4, p5, p6, p7, p8, p9);
```

## Example

```js
// todo
```

## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">String</span></b></li>

<br />

## Return value

<b>Undefined</b>