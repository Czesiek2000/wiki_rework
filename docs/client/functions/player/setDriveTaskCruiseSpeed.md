# Player::setDriveTaskCruiseSpeed

## Syntax

```js
player.setDriveTaskCruiseSpeed(cruiseSpeed);
```

## Example

```js
// todo
```

## Parameters

<li><b>cruiseSpeed:</b> <b><span style="color:#008017">Float</span></b></li>

## Return value

<b>Undefined</b>