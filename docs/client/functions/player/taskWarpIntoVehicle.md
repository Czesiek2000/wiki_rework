# Player::taskWarpIntoVehicle

Seat Numbers

-------------------------------

* Driver = -1
* Any = -2
* Left-Rear = 1
* Right-Front = 0
* Right-Rear = 2
* Extra seats = 3-14

(This may differ from vehicle type e.g. Firetruck Rear Stand, Ambulance Rear)-YCSM


## Syntax

```js
player.taskWarpIntoVehicle(vehicle, seat);
```

## Example

```js
// todo
```

## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>seat:</b> <b><span style="color:#008017">Int</span></b></li>

## Return value

<b>Undefined</b>