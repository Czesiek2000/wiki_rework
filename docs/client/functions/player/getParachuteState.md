# Player::getParachuteState

Returns:

-1: Standing on a ground

0: Freefall with parachute on back 

1: Parachute opening

2: Parachute open

3: Falling without parachute


## Syntax

```js
player.getParachuteState();
```

## Example

```js
// todo
```

## Parameters

No parameters here

## Return value

<li><b><span style="color:#008017">Int</span></b></li>