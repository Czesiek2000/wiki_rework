# Blip::setRotation

After some testing, looks like you need to use UI:CEIL() on the rotation (vehicle/ped heading) before using it there.

# Syntax

```js
blip.setRotation(rotation);
```

# Example

```js
// todo
```

# Parameters

<li><b>rotation:</b> int</li>

<br />

# Return value

<li><b>Undefined</b></li>