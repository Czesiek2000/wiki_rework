# Blip::getCoords
Function to return the coordinates of the blip.


# Syntax

```js
blip.getCoords();
```

# Example

Retrieving the coordinates value from the blip called policeBlip

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0), {
    color: 3,
    shortRange: true,
});

let getBlipCoords = policeBlip.getCoords();

mp.gui.chat.push("Coords: " + JSON.stringify(getBlipCoords)); //Output: "Coords: {"x":407.95001220703125,"y":-961.0499877929688,"z":0}"
```

# Return value

<li><b><span style="color:#008017">Vector3</span></b></li>