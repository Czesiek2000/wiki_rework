# Blip::setAsShortRange
Function to set a blip as short range (gone when it is not in minimap range)

# Syntax

```js
blip.setAsShortRange(toggle);
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0));
    policeBlip.setAsShortRange(true);
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />


# Return value

<li><b>Undefined</b></li>