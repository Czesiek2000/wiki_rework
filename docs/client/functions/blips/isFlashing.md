# Blip::isFlashing

Function to return the flashing status of blip.

# Syntax

```js
blip.isFlashing();
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0));
policeBlip.setFlashes(true);

let isBlipFlashing = policeBlip.isFlashing();
mp.gui.chat.push("Is Flashing: " + isBlipFlashing); //Output: "Is Flashing: true"
```


<br />

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>