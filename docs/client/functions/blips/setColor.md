# Blip::setColor

Function to set a blip color. Default to yellow if not used.

# Syntax

```js
blip.setColour(color);
```

# Example

```js
let blip = mp.blips.new(1, new mp.Vector3(407.95, -961.05, 0));

blip.setColour(1); // Set blip color to red
```

# Parameters

<li><b>color:</b> <b><span style="color:#008017">Number</span></b></li>

<br />

Color list can be found [here](../../../tables/blips.md#colors)


# Return value

<li><b>Undefined</b></li>