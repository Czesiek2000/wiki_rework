# Blip::setCategory

int index:
1. No Text on blip or Distance
2. Text on blip
3. No text, just distance
7. Other players %name% (%distance%)
10. Property
11. Occupied property
12. No Text on blip or distance

# Syntax

```js
blip.setCategory(index);
```

# Example

```js
// todo
```

# Parameters

<li><b>index:</b> int</li>

<br />


# Return value

<li><b>Undefined</b></li>