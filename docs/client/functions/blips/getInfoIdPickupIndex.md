# Blip::getInfoIdPickupIndex

MulleDK19: This function is hard-coded to always return 0.

# Syntax

```js
blip.getInfoIdPickupIndex();
```

# Example

```js
// todo
```


# Return value

<li><b>Pickup</b></li>