# Blip::getAlpha

Function to return the alpha value of the blip.


# Syntax

```js
blip.getAlpha();
```

# Example

Retrieving the alpha value from the blip called policeBlip

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0), {
    color: 3,
    alpha: 50,
    shortRange: true,
});

let getBlipAlpha = policeBlip.getAlpha();
mp.gui.chat.push("Alpha:" + getBlipAlpha); //Output: Alpha: 50"
```

<br />


# Return value

<li><b><span style="color:#008017">Number</span></b></li>