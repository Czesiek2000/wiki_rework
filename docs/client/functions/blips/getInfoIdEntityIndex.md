# Blip::getInfoIdEntityIndex

# Syntax

```js
blip.getInfoIdEntityIndex();
```

# Example

```js
// todo
```

# Return value

<li><b>Entity handle or object</b></li>