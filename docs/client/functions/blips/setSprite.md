# Blip::setSprite

Takes a blip object and adds a sprite to it on the map.You may have your own list, but since dev-c didn't show it I was bored and started looking through scripts and functions to get a presumable almost positive list of a majority of [blip IDs](
http://pastebin.com/Bpj9SfftBlipsImages) + [IDs](https://gtaxscripting.blogspot.com/2016/05/gta-v-blips-id-and-image.html)

# Syntax

```js
blip.setSprite(spriteId);
```

# Example

```js
// todo
```

# Parameters

<li><b>spriteId:</b> int</li>

<br />

# Return value

<li><b>Undefined</b></li>