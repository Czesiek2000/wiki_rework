# Blip::Blip

Creates a blip to display on your map & minimap.

*Blips created server-side seem to be impossible to attach to the map. If you create the same blip server-side and client-side, only the second will stay attached to the map (the scale won't change if you zoom in or out).*




# Syntax

```js
mp.blips.new(sprite, position, {
        name: name,
        scale: scale,
        color: color,
        alpha: alpha,
        drawDistance: drawDistance,
        shortRange: shortRange,
        rotation: rotation,
        dimension: dimension,
        radius: radius,
});
```

# Example

This creates a blip at the police station. The blip is assigned to the variable policeBlip.

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(427.95, -981.05, 0), {
        name: 'Los Santos Police Station',
        color: 3,
        shortRange: true,
});
```

<br />


# Parameters

<li><b><span style="font-weight:bold; color:red;">*</span>sprite</b>: <b><span style="color:#008017">Int</span></b> (<a href="/index.php?title=Blips#Blip_model" title="Blips">Blip sprites</a>)</li>
<li><b><span style="font-weight:bold; color:red;">*</span>position</b>: <b><span style="color:#008017">Vector3</span></b></li>
<li><b>name</b>: <b><span style="color:#008017">String</span></b></li>
<li><b>scale</b>: <b><span style="color:#008017">Float</span></b>: How big the blip shows on the map</li>
<li><b>color</b>: <b><span style="color:#008017">Color ID</span></b> (<a href="/index.php?title=Blips#Blip_colors" title="Blips">Blip colors</a>)</li>
<li><b>alpha</b>: <b><span style="color:#008017">Int</span></b> [0:255]</li>
<li><b>drawDistance</b>: <b><span style="color:#008017">Float</span></b></li>
<li><b>shortRange</b>: <b><span style="color:#008017">Boolean</span></b>: If it's hidden in the minimap until the player is close</li>
<li><b>rotation</b>: <b><span style="color:#008017">Float</span></b></li>
<li><b>dimension</b>: <b><span style="color:#008017">Int</span></b></li>
<li><b>radius</b>: <b><span style="color:#008017">Float</span></b></li>
