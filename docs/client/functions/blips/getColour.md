# Blip::getColour

Function to return the colour value of the blip.

# Syntax

```js
blip.getColour();
```

# Example

Retrieving the colour value from the blip called policeBlip

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0), {
    color: 3,
    shortRange: true,
});

let getBlipColour = policeBlip.getColour();
mp.gui.chat.push("Colour: " + getBlipColour); //Output: "Colour: 3"
```

<br />


# Return value

<li><b><span style="color:#008017">Number</span></b></li>