# Blip::isMissionCreator

Function to return whether the blip is set as mission creator.

# Syntax

```js
blip.isMissionCreator();
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0));
policeBlip.setAsMissionCreator(true);

let isBlipMissionCreator = policeBlip.isMissionCreator();

mp.gui.chat.push("Is mission creator? " + isBlipMissionCreator); // Output: "Is mission creator? true"
```

<br />

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>