# Blip::setAsFriendly

Function to set a blip as friendly.

# Syntax

```js
blip.setAsFriendly(toggle);
```

# Example

```js
let criminalBlip = mp.blips.new(1, new mp.Vector3(407.95, -961.05, 0));
criminalBlip.setAsFriendly(false);
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b> <code>true</code> to set as friendly (blue) or <code>false</code> for the opposite (red)</li>

<br />


# Return value

<li><b>Undefined</b></li>