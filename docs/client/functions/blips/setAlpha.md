# Blip::setAlpha

Function to set alpha-channel for blip color.

# Syntax

```js
blip.setAlpha(alpha);
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0));
policeBlip.setAlpha(64);
```

# Parameters

<li><b>alpha:</b> <b><span style="color:#008017">Number</span></b> alpha value in 0 to 255</li>

<br />


# Return value

<li><b>Undefined</b></li>