# Blip::setFlashes

Function to set a blip flashes (blinking).

# Syntax

```js
blip.setFlashes(toggle);
```

# Example

```js
let flagBlip = mp.blips.new(164, new mp.Vector3(407.95, -961.05, 0));

flagBlip.setFlashes(true);

mp.gui.chat.push("Go to flag marked in map");
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />

# Return value

<li><b>Undefined</b></li>