# Blip::isOnMinimap

Function to return whether the blip is visible on client's minimap.

# Syntax

```js
blip.isOnMinimap();
```

# Example

```js
let my = mp.players.local;
let policeBlip = mp.blips.new(60, my.position); // Create police blip on player location so it's visible on their map

let isBlipVisible = policeBlip.isOnMinimap();
mp.gui.chat.push("Is blip visible? " + isBlipVisible ); // Output: "Is blip visible? true"
```

<br />

# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>