# Blip::setSecondaryColour

# Syntax

```js
blip.setSecondaryColour(r, g, b);
```

# Example

```js
// todo
```

# Parameters

<li><b>r:</b> float</li>
<li><b>g:</b> float</li>
<li><b>b:</b> float</li>

<br />


# Return value

<li><b>Undefined</b></li>