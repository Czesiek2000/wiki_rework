# Blip::doesExist

A function to check if a blip exists.



# Syntax

```js
blip.doesExist();
```

# Example

An example of returning true of a blip exists.

```js
let policeBlip_Client = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0), {
    color: 3,
    shortRange: true,
});

let doesExist = policeBlip_Client.doesExist();
mp.gui.chat.push("Exist: " + doesExist); //Output: "Exist: 1"
```

<br />


# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>