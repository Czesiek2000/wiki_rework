# Blip::setFade

# Syntax

```js
blip.setFade(opacity, duration);
```

# Example

```js
// todo
```

# Parameters

<li><b>opacity:</b> int</li>
<li><b>duration:</b> int</li>

<br />

# Return value

<li><b>Undefined</b></li>