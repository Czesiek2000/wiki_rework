# Blip::setDisplay

# Syntax

```js
blip.setDisplay(displayID);
```

# Example

```js
let localPlayer = mp.players.local;

let blip = mp.blips.new(2, localPlayer.position, {
    name: localPlayer.name
});

blip.setDisplay(3); // You won't show on my big radar men.
```

# Parameters

<li><b>displayID:</b> int</li>

<br />


# Return value

<li><b>Undefined</b></li>


<br />


# Display id values
<ul><li><b>8</b>: Shows on both. (Not selectable on map)</li>
<li><b>5</b> or <b>9</b>: Shows on minimap only.</li>
<li><b>3</b> or <b>4</b>: Shows on main map only. (Selectable on map)</li>
<li><b>2</b> or <b>6</b>: Shows on both. (Selectable on map)</li>
<li><b>0</b>: Doesn't show up, ever, anywhere.</li></ul>