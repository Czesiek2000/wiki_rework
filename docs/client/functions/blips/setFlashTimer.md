# Blip::setFlashTimer

Adds up after viewing multiple R* scripts. I believe that the duration is in miliseconds.- Shawn (/u/shawn_of_the_reddit) (GTAF: thunder_)

# Syntax

```js
blip.setFlashTimer(duration);
```

# Example

```js
// todo
```

# Parameters

<li><b>duration:</b> int</li>

<br />

# Return value

<li><b>Undefined</b></li>