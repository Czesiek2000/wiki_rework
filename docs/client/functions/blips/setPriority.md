# Blip::setPriority

Function to set blip priority (i.e. orders in map legend)

# Syntax

```js
blip.setPriority(priority);
```

# Example

```js
// todo
```

# Parameters

<li><b>priority:</b> <b><span style="color:#008017">Number</span></b></li>

<br />

# Return value

<li><b>Undefined</b></li>