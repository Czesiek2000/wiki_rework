# Blip::addTextComponentSubstringName


# Syntax

```js
blip.addTextComponentSubstringName();
```

# Example

Only works with 'addTextComponentSubstringPlayerName'

```js
let blip = mp.blips.at(0);
mp.game.ui.beginTextCommandSetBlipName("STRING");
mp.game.ui.addTextComponentSubstringPlayerName('TEST');
blip.endTextCommandSetName();
```


<br />


# Return value

<li><b>Undefined</b></li>