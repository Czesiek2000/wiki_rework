# Blip::setShowHeadingIndicator

Adds the GTA:Online player heading indicator to a blip.

# Syntax

```js
blip.setShowHeadingIndicator(toggle);
```

# Example

```js
// todo
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />


# Return value

<li><b>Undefined</b></li>