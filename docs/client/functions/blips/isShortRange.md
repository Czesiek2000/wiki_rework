# Blip::isShortRange

Function to find out whether blip is set as short range.

# Syntax

```js
blip.isShortRange();
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0), {
    color: 3,
    shortRange: true,
});

let isBlipShortRange = policeBlip.isShortRange();
mp.gui.chat.push("Is blip short range? " + isBlipShortRange ); // Output: "Is blip short range? true"
```

<br />


# Return value

<li><b><span style="color:#008017">Boolean</span></b></li>