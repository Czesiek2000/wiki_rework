# Blip::setNameToPlayerName

Function to set blip name to [player social club gamer tag name](../../../server/functions/player/rgscId.md).

# Syntax

```js
blip.setNameToPlayerName(player);
```

# Example

```js
// todo
```

# Parameters

<li><b>player:</b> <b><span style="color:#008017">Number</span></b>player shared id</a></li>

<br />

# Return value

<li><b>Undefined</b></li>