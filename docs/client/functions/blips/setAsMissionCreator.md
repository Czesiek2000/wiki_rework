# Blip::setAsMissionCreator

Function to set a blip as Mission Creator.

# Syntax

```js
blip.setAsMissionCreator(toggle);
```

# Example

```js
let policeBlip = mp.blips.new(60, new mp.Vector3(407.95, -961.05, 0));
policeBlip.setAsMissionCreator(true);
```

# Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />


# Return value

<li><b>Undefined</b></li>