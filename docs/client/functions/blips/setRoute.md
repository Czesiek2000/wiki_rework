# Blip::setRoute

Enable / disable showing route for the Blip-object.

# Syntax

```js
blip.setRoute(enabled);
```

# Example

```js
var policeStation = mp.blips.new(526, new mp.Vector3(433.25567626953125,-981.8964233398438, 30.710012435913086), {
    color: 5,
    shortRange: false,
    dimension: mp.players.local.dimension
});

policeStation.setRoute(true) // SetRoute for police station
```

# Parameters

<li><b>enabled:</b> Boolean</li>

<br />


# Return value

<li><b>Undefined</b></li>