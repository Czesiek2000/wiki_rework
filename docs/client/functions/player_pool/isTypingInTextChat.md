# Players::isTypingInTextChat

Returns if the player is currently typing in the chat.

You can also invoke this inside CEF using mp.invoke("setTypingInChatState", state);


## Syntax

```js
mp.players.local.isTypingInTextChat
```

## Example

```js
// todo
```

## Parameters

<li>Typing state <b><span style="color:#008017">Bool</span></b></li>
