# Players::local

Returns the local player object of the given client-side environment.

## Syntax

```js
mp.players.local
```

## Example
```js
const localPlayer = mp.players.local;
localPlayer.freezePosition(true);
```