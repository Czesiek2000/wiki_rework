# Controls::disableAllControlActions


Disables all controls within a given Input Group.
 
 

## Syntax

```js
mp.game.controls.disableAllControlActions(inputGroup);
```

## Example

This disables all controls under the 'INPUTGROUP_WHEEL' input group, so any controls that fall under this input group will be disabled.

```js
mp.events.add('render', () => {
    if(condition) {
        mp.game.controls.disableAllControlActions(2);
    }
});
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>

[Input Groups](../../../../tables/inputGroups.md)

<br />
<br />

## Return value

<ul><li><b>Undefined</b></li></ul>