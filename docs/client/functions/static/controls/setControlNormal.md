# Controls::setControlNormal

This is for simulating player input.

amount is a float value from 0 - 10, 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.setControlNormal(inputGroup, control, amount);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>amount:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>