# Controls::enableControlAction


Enables a control so it can be used.
 

## Syntax

```js
mp.game.controls.enableControlAction(inputGroup, control, enable);
```

## Example

```js
// todo

```


<br />


## Parameters

<ul><li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>enable:</b> <b><span style="color:#008017">Boolean</span></b></li></ul>

[Input Groups](../../../../tables/inputGroups.md)
[Game Controls](../../../../tables/gameControls.md)

<br />
<br />

## Return value
<li><b>Undefined</b></li>