# Controls::isControlJustReleased

This works on the release of E

## Syntax

```js
mp.game.controls.isControlJustReleased(inputGroup, control);
```

## Example

```js
if (mp.game.controls.isControlJustReleased(0, 38)) {
	// Do Stuff Here
}
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>