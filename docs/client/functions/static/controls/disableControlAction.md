# Controls::disableControlAction

Disable a control so it cannot be used.

## Syntax

```js
mp.game.controls.disableControlAction(inputGroup, control, disable);

```

## Example

```js
// This will disable using the character wheel
mp.events.add('render', () => {
    mp.game.controls.disableControlAction(2, 19, true);
});

// Disable vehicle weapon on RMB. 
mp.events.add('render', () => {
    mp.game.controls.disableControlAction(32, 68, true); // Use inputGroup 32 in case you are not sure, it seems to have the whole collection of control actions. 
    mp.game.controls.disableControlAction(32, 70, true);
});

```


<br />


## Parameters

<ul><li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>disable:</b> <b><span style="color:#008017">Boolean</span></b></li></ul>

[Input Groups](../../../../tables/inputGroups.md)
[Game controls](../../../../tables/gameControls.md)
<br />
<br />

## Return value
<li><b><span style="color:#008017">Boolean</span></b></li>