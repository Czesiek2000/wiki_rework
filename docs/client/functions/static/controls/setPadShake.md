# Controls::setPadShake

p0 always seems to be 0duration in milliseconds frequency should range from about 10 (slow vibration) to 255 (very fast)appears to be a hash collision, though it does do what it says

example:
```cpp
SET_PAD_SHAKE(0, 100, 200);
```

## Syntax

```js
mp.game.controls.setPadShake(p0, duration, frequency);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>frequency:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>