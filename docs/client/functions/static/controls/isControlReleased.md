# Controls::isControlReleased
0, 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.isControlReleased(inputGroup, control);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>inputGroup:</b> int</li>
<li><b>control:</b> int</li>

<br />
<br />

## Return value

<li><b>Boolean</b></li>