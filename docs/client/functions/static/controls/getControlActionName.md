# Controls::getControlActionName

formerly called `_GET_CONTROL_ACTION_NAME` incorrectly

p2 appears to always be true. p2 is unused variable in function.

EG:
```cpp
_GET_CONTROL_ACTION_NAME(2, 201, 1) /*INPUT_FRONTEND_ACCEPT (e.g. Enter button)*/
_GET_CONTROL_ACTION_NAME(2, 202, 1) /*INPUT_FRONTEND_CANCEL (e.g. ESC button)*/
_GET_CONTROL_ACTION_NAME(2, 51, 1) /*INPUT_CONTEXT (e.g. E button)*/
```
[gta forums](https://gtaforums.com/topic/819070-c-draw-instructional-buttons-scaleform-movie/#entry10681973780), 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.getControlActionName(inputGroup, control, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>