# Controls::isInputDisabled

Seems to return true if the input is currently disabled. `_GET_LAST_INPUT_METHOD` didn't seem very accurate, but I've left the original description below.

index usually 2

returns true if the last input method was made with mouse + keyboard, false if it was made with a gamepad

0, 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.isInputDisabled(inputGroup);
```

## Example

```js
// test if the player is using a controller vs keyboard/mouse

function isUsingController(){
    return !mp.game.controls.isInputDisabled(0);
}
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>