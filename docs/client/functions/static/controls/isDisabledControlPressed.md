# Controls::isDisabledControlPressed

0, 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.isDisabledControlPressed(inputGroup, control);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>