# Controls::isControlPressed

index always is 2 for xbox 360 controller and razerblade

0, 1 and 2 used in the scripts. 0 is by far the most common of them.
 

## Syntax

```js
mp.game.controls.isControlPressed(inputGroup, control);
```

## Example

```js
if(mp.game.controls.isControlPressed(27, 71)) {
  // Vehicle Control: Pressed W
}
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>

[Input groups](../../../../tables/inputGroups.md)
[Controls](../../../../tables/gameControls.md)
<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>