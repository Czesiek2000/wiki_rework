# Controls::enableAllControlActions


Enables all controls within a given Input Group.
 

## Syntax

```js
mp.game.controls.enableAllControlActions(inputGroup);
```

## Example

```js
// todo

```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>

[Input Groups](../../../../tables/inputGroups.md)

<br />
<br />

## Return value

<li><b>Undefined</b></li>