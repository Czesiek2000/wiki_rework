# Controls::isControlEnabled

Checks if the control is enabled.

[Input Groups](../../../../tables/inputGroups.md)

[Game Controls](../../../../tables/gameControls.md)
 

## Syntax

```js
mp.game.controls.isControlEnabled(inputGroup, control);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : 

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>