# Controls::isControlJustPressed

[Input Groups](../../../../tables/inputGroups.md)

[Game Controls](../../../../tables/gameControls.md)

## Syntax

```js
mp.game.controls.isControlJustPressed(inputGroup, control);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>inputGroup:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>control:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>