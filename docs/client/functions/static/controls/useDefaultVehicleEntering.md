# Controls::useDefaultVehicleEntering

Enable/Disable RageMP's controls for entering a vehicle.

**False**: Holding F and pressing F both go to the driver's seat

**True**: Holding F will go to the passenger seat, pressing F will go to the driver's seat.
 

## Syntax

```js
Boolean mp.game.controls.useDefaultVehicleEntering
```

## Example

```js
mp.game.controls.useDefaultVehicleEntering = true;
```


<br />


## Parameters

<li><b>enabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>