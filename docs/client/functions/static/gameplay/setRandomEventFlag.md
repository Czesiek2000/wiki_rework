# Gameplay::setRandomEventFlag

*MulleDK19*: If the parameter is true, sets the random event flag to true, if the parameter is false, the function does nothing at all.

Does nothing if the mission flag is set.
 

## Syntax

```js
mp.game.gameplay.setRandomEventFlag(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>