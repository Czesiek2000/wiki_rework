# Gameplay::hasButtonCombinationJustBeenEntered

This native appears on the **cheat_controller** script and tracks a combination of buttons, which may be used to toggle cheats in-game. 

Credits to ThreeSocks for the info. 

The hash contains the combination, while the 'amount' represents the amount of buttons used in a combination. 

The following page can be used to make a button combination: [here](https://gta5offset.com/ts/hash/)
`INT_SCORES_SCORTED` was a hash collision
 

## Syntax

```js
mp.game.gameplay.hasButtonCombinationJustBeenEntered(hash, amount);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hash:</b> Model hash or name</li>
<li><b>amount:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>