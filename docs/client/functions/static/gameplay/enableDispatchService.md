# Gameplay::enableDispatchService

dispatchType:

* Police Automobile = 1
* Police Helicopter = 2
* Swat Helicopter = 3
* Fire Department = 4
* Swat Automobile = 5
* Ambulance Department = 6
* Gangs = 7
* Police Riders = 8
* Police Vehicle Request = 9
* Police Road Block = 10
* Police Boat = 11
* Army Vehicle = 12

By making toggle false it disables the dispatch (for 7, it will disable gangs.)

curious if this is what they used when you toggled on and off cops in a GTA IV freemode you hosted. Sad they got rid of the option to make a private session without cops.

Also on x360 seems with or without neverWanted on, setting these to all false in SP of course doesn't seem to work. I would understand getting stars, but cops are still dispatched and combat you.
 

## Syntax

```js
mp.game.gameplay.enableDispatchService(dispatchType, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dispatchType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>