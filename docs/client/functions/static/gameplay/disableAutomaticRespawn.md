# Gameplay::disableAutomaticRespawn

## Syntax

```js
mp.game.gameplay.disableAutomaticRespawn(disableRespawn);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>disableRespawn:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>