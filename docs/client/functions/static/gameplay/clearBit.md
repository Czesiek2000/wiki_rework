# Gameplay::clearBit

This sets bit [offset] of [address] to off.

Example:
```cpp
GAMEPLAY::CLEAR_BIT(&bitAddress, 1);
```

To check if this bit has been enabled:

```cpp
GAMEPLAY::IS_BIT_SET(bitAddress, 1); // will return 0 afterwards
```

## Syntax

```js
mp.game.gameplay.clearBit(address, offset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>address:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>offset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>