# Gameplay::clearAreaOfPeds

Example: 
```cpp
CLEAR_AREA_OF_PEDS(0, 0, 0, 10000, 1);
```
 

## Syntax

```js
mp.game.gameplay.clearAreaOfPeds(x, y, z, radius, flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>