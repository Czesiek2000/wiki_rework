# Gameplay::isNextWeatherType

## Syntax

```js
mp.game.gameplay.isNextWeatherType(weatherType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weatherType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>