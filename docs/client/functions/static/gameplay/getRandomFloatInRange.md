# Gameplay::getRandomFloatInRange

## Syntax

```js
mp.game.gameplay.getRandomFloatInRange(startRange, endRange);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>startRange:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>endRange:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>