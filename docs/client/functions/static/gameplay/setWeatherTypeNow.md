# Gameplay::setWeatherTypeNow

The following weatherTypes are used in the scripts:

* CLEAR
* EXTRASUNNY
* CLOUDS
* OVERCAST
* RAIN
* CLEARING
* THUNDER
* SMOG
* FOGGY
* XMAS
* SNOWLIGHT
* BLIZZARD
 

## Syntax

```js
mp.game.gameplay.setWeatherTypeNow(weatherType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weatherType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>