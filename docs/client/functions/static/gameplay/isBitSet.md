# Gameplay::isBitSet

Returns bit's boolean state from [offset] of [address].

Example:
```cpp
GAMEPLAY::IS_BIT_SET(bitAddress, 1);
```
To enable and disable bits, see:

```cpp
GAMEPLAY::SET_BIT(&bitAddress, 1); // enable 
GAMEPLAY::CLEAR_BIT(&bitAddress, 1); // disable
```
 

## Syntax

```js
mp.game.gameplay.isBitSet(address, offset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>address:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>offset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>