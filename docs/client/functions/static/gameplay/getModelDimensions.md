# Gameplay::getModelDimensions

Gets the dimensions of a model. 

Calculate (maximum - minimum) to get the size, in which case, Y will be how long the model is.
 

## Syntax

```js
mp.game.gameplay.getModelDimensions(modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>object:</b> min, max</li>