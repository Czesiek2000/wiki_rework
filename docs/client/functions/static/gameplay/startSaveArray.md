# Gameplay::startSaveArray

Second parameter might be length.
 

## Syntax

```js
mp.game.gameplay.startSaveArray(p0, p1, arrayName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>arrayName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>