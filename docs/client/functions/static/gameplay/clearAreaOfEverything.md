# Gameplay::clearAreaOfEverything

`GAMEPLAY::_0x957838AAF91BD12D(x, y, z, radius, false, false, false, false);` seem to make all objects go away, peds, vehicles etc. 

All booleans set to true doesn't seem to change anything.
 

## Syntax

```js
mp.game.gameplay.clearAreaOfEverything(x, y, z, radius, p4, p5, p6, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>