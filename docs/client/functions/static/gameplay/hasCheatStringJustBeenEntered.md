# Gameplay::hasCheatStringJustBeenEntered

Get inputted 'Cheat code', 

For example:

```cpp
while (TRUE){ 
    if (GAMEPLAY::_557E43C447E700A8(${fugitive})) {
        // Do something. 
    } 
    
    SYSTEM::WAIT(0);
}
```

Calling this will also set the last saved string hash to zero.
 

## Syntax

```js
mp.game.gameplay.hasCheatStringJustBeenEntered(hash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>