# Gameplay::displayOnscreenKeyboard

sfink: note, p0 is set to 6 for PC platform in at least 1 script, or to `unk::_get_ui_language_id() == 0` otherwise.

**NOTE:** windowTitle uses text labels, and an invalid value will display nothing. 
[gtaforums](www.gtaforums.com/topic/788343-vrel-script-hook-v/?p=1067380474)

windowTitle's: 
* CELL_EMAIL_BOD = 'Enter your Eyefind message
* CELL_EMAIL_BODE = 'Message too long. Try again
* CELL_EMAIL_BODF = 'Forbidden message. Try again
* CELL_EMAIL_SOD = 'Enter your Eyefind subject
* CELL_EMAIL_SODE = 'Subject too long. Try again
* CELL_EMAIL_SODF = 'Forbidden text. Try again
* CELL_EMASH_BOD = 'Enter your Eyefind message
* CELL_EMASH_BODE = 'Message too long. Try again
* CELL_EMASH_BODF = 'Forbidden message. Try again
* CELL_EMASH_SOD = 'Enter your Eyefind subject
* CELL_EMASH_SODE = 'Subject too long. Try again
* CELL_EMASH_SODF = 'Forbidden Text. Try again
* FMMC_KEY_TIP10 = 'Enter Synopsis
* FMMC_KEY_TIP12 = 'Enter Custom Team Name
* FMMC_KEY_TIP12F = 'Forbidden Text. Try again
* FMMC_KEY_TIP12N = 'Custom Team Name
* FMMC_KEY_TIP8 = 'Enter Message
* FMMC_KEY_TIP8F = 'Forbidden Text. Try again
* FMMC_KEY_TIP8FS = 'Invalid Message. Try again
* FMMC_KEY_TIP8S = 'Enter Message
* FMMC_KEY_TIP9 = 'Enter Outfit Name
* FMMC_KEY_TIP9F = 'Invalid Outfit Name. Try again
* FMMC_KEY_TIP9N = 'Outfit Name
* PM_NAME_CHALL = 'Enter Challenge Name
 

## Syntax

```js
mp.game.gameplay.displayOnscreenKeyboard(p0, windowTitle, p2, defaultText, defaultConcat1, defaultConcat2, defaultConcat3, maxInputLength);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>windowTitle:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>defaultText:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>defaultConcat1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>defaultConcat2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>defaultConcat3:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>maxInputLength:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>