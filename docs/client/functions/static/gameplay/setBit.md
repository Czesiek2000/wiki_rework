# Gameplay::setBit

This sets bit [offset] of [address] to on.

The offsets used are different bits to be toggled on and off, typically there is only one address used in a script.

Example:
```cpp
GAMEPLAY::SET_BIT(&bitAddress, 1);
```

To check if this bit has been enabled:

```cpp
GAMEPLAY::IS_BIT_SET(bitAddress, 1); // will return 1 afterwards Please note, this method may assign a value to [address] when used.
```
 

## Syntax

```js
mp.game.gameplay.setBit(address, offset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>address:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>offset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>