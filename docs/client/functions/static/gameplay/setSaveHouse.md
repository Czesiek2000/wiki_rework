# Gameplay::setSaveHouse

Both p1 & p2 were always true in scripts. 

*Kryptus*

I don't think this is correct. Both p1 & p2 were always true in scripts.

*Kryptus*
 

## Syntax

```js
mp.game.gameplay.setSaveHouse(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>