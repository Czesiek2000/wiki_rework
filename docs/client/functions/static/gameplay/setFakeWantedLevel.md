# Gameplay::setFakeWantedLevel

Sets a visually fake wanted level on the user interface. Used by Rockstar's scripts to 'override' regular wanted levels and make custom ones while the real wanted level and multipliers are ignored.

Max is 5, anything above this makes it just 5. Also the mini-map gets the red & blue flashing effect. 

I wish I could use this to fake I had 6 stars like a few of the old GTAs'
 
As of 1.1+ you can set 6 stars.
 

## Syntax

```js
mp.game.gameplay.setFakeWantedLevel(fakeWantedLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>fakeWantedLevel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>