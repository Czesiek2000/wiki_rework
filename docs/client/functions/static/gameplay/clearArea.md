# Gameplay::clearArea

Example: 
```cpp
CLEAR_AREA(0, 0, 0, 30, true, false, false, false);
```
 

## Syntax

```js
mp.game.gameplay.clearArea(X, Y, Z, radius, p4, ignoreCopCars, ignoreObjects, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>X:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>ignoreCopCars:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>ignoreObjects:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>