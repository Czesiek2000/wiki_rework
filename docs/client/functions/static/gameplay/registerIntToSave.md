# Gameplay::registerIntToSave

## Syntax

```js
mp.game.gameplay.registerIntToSave(p0, name);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>