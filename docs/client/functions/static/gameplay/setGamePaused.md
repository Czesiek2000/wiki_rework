# Gameplay::setGamePaused

Make sure to call this from the correct thread if you're using multiple threads because all other threads except the one which is calling `SET_GAME_PAUSED` will be paused which means you will lose control and the game remains in paused mode until you exit *GTA5.exe*
 

## Syntax

```js
mp.game.gameplay.setGamePaused(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>