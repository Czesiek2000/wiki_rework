# Gameplay::shootSingleBulletBetweenCoords

## Syntax

```js
mp.game.gameplay.shootSingleBulletBetweenCoords(x1, y1, z1, x2, y2, z2, damage, p7, weaponHash, ownerPed, isAudible, isInvisible, speed);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>damage:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>weaponHash:</b> Model hash or name</li>
<li><b>ownerPed:</b> Ped handle or object</li>
<li><b>isAudible:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>isInvisible:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>