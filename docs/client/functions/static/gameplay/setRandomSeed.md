# Gameplay::setRandomSeed

## Syntax

```js
mp.game.gameplay.setRandomSeed(time);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>