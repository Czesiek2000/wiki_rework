# Gameplay::absf

## Syntax

```js
mp.game.gameplay.absf(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>