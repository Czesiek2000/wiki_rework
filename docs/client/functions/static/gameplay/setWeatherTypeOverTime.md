# Gameplay::setWeatherTypeOverTime

This function transitions the weather from one weather type to another over the specified amount of time in seconds.
 
For example, it is now *EXTRASUNNY*. The weather is now transitioning to *THUNDER* over 60 minutes. Rain will slowly start and clouds will slowly form over the time of 1 hour, rather than it being instant like `World::weather`
 
You only have to specify the new weather.
 
This example will call the client-side event 'transitionWeather' for the every player, to synchronise the weather. This gradually transitions the weather over x seconds, not milliseconds.
 

 

## Syntax

```js
mp.game.gameplay.setWeatherTypeOverTime(weatherType, time);
```

## Example

```js
//newWeather - string, any of weather  /  timeTaken - time taken to transition from old weather to new weather in seconds

mp.events.add('transitionWeather', (newWeather, timeTaken) => {
    mp.game.gameplay.setWeatherTypeOverTime(newWeather, timeTaken);
});
```


<br />


## Parameters

<li><b>weatherType:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>time:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>