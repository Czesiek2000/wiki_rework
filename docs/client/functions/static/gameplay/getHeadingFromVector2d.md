# Gameplay::getHeadingFromVector2d

dx = x1 - x2

dy = y1 - y2
 

## Syntax

```js
mp.game.gameplay.getHeadingFromVector2d(dx, dy);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dx:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dy:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>