# Gameplay::setUnkMapFlag

Sets an unknown flag used by CScene in determining which entities from CMapData scene nodes to draw, similar to 9BAE5AD2508DF078.
 

## Syntax

```js
mp.game.gameplay.setUnkMapFlag(flag);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>flag:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>