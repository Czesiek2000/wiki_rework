# Gameplay::getHashKey

This native converts its past string to hash. It is hashed using jenkins one at a time method.
 

## Syntax

```js
mp.game.gameplay.getHashKey(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>