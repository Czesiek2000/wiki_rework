# Gameplay::setOverrideWeather

Appears to have an optional bool parameter that is unused in the scripts. If you pass true, something will be set to zero.
 

## Syntax

```js
mp.game.gameplay.setOverrideWeather(weatherType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weatherType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>