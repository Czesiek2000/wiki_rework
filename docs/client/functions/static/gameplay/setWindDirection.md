# Gameplay::setWindDirection

## Syntax

```js
mp.game.gameplay.setWindDirection(direction);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>direction:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>