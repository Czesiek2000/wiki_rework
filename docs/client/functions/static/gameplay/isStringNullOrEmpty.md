# Gameplay::isStringNullOrEmpty

## Syntax

```js
mp.game.gameplay.isStringNullOrEmpty(string);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>string:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>