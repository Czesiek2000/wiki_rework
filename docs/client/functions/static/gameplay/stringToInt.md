# Gameplay::stringToInt

Basically the same as std::stoi.
 

## Syntax

```js
mp.game.gameplay.stringToInt(string, outInteger);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>string:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>outInteger:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>