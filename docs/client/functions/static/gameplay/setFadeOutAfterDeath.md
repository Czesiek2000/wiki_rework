# Gameplay::setFadeOutAfterDeath

Sets whether the game should fade out after the player dies.
 

## Syntax

```js
mp.game.gameplay.setFadeOutAfterDeath(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>