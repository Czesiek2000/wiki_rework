# Gameplay::compareStrings

Compares two strings up to a specified number of characters.

Parameters:

* str1 - String to be compared.
* str2 - String to be compared.
* matchCase - Comparison will be case-sensitive.
* maxLength - Maximum number of characters to compare. 

A value of -1 indicates an infinite length.

Returns: A value indicating the relationship between the strings:

<0 - The first non-matching character in 'str1' is less than the one in 'str2'. (e.g. 'A' < 'B', so result = -1)

0 - The contents of both strings are equal.

<p>>0 - The first non-matching character in 'str1' is less than the one in 'str2'. (e.g. 'B' > 'A', so result = 1)</p>

Examples:
```cpp
GAMEPLAY::COMPARE_STRINGS('STRING', 'string', false, -1); // 0; equal
GAMEPLAY::COMPARE_STRINGS('TESTING', 'test', false, 4); // 0; equal
GAMEPLAY::COMPARE_STRINGS('R2D2', 'R2xx', false, 2); // 0; equal
GAMEPLAY::COMPARE_STRINGS('foo', 'bar', false, -1); // 4; 'f' > 'b'
GAMEPLAY::COMPARE_STRINGS('A', 'A', true, 1); // 0; equalWhen comparing case-sensitive strings, lower-case characters are greater than upper-case characters:
GAMEPLAY::COMPARE_STRINGS('A', 'a', true, 1); // -1; 'A' < 'a'
GAMEPLAY::COMPARE_STRINGS('a', 'A', true, 1); // 1; 'a' > 'A'
```
 

## Syntax

```js
mp.game.gameplay.compareStrings(str1, str2, matchCase, maxLength);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>str1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>str2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>matchCase:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>maxLength:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>