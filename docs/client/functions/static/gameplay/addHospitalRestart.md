# Gameplay::addHospitalRestart

Returns the index of the newly created hospital spawn point.p3 might be radius?- mlgthatsme
 

## Syntax

```js
mp.game.gameplay.addHospitalRestart(x, y, z, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>