# Gameplay::getProfileSetting

anyone have a settingid dump?
 

## Syntax

```js
mp.game.gameplay.getProfileSetting(profileSetting);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>profileSetting:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>