# Gameplay::isAreaOccupied

## Syntax

```js
mp.game.gameplay.isAreaOccupied(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p11:</b> unknown (to be checked)</li>
<li><b>p12:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>