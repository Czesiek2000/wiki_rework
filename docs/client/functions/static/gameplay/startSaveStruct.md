# Gameplay::startSaveStruct
Second parameter might be length.
 

## Syntax

```js
mp.game.gameplay.startSaveStruct(p0, p1, structName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> int</li>
<li><b>structName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>