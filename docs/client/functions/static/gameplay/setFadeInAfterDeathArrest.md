# Gameplay::setFadeInAfterDeathArrest

Sets whether the game should fade in after the player dies or is arrested.
 

## Syntax

```js
mp.game.gameplay.setFadeInAfterDeathArrest(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>