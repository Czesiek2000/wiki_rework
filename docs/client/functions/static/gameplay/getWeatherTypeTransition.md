# Gameplay::getWeatherTypeTransition

## Syntax

```js
mp.game.gameplay.getWeatherTypeTransition(p0, p1, progress_or_time);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>progress_or_time:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b>*<b>unknown (to be checked)</b>*<b>float</b></li>