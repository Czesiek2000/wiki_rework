# Gameplay::setTimeScale

Maximum value is 1. 

At a value of 0 the game will still run at a minimum time scale.

Slow Motion 1: 0.6

Slow Motion 2: 0.4

Slow Motion 3: 0.2
 

## Syntax

```js
mp.game.gameplay.setTimeScale(time);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>time:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>