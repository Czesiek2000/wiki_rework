# Gameplay::setWindSpeed

Using this native will clamp the wind speed value to a range of *0.0- 12.0*. 

Using `SET_WIND` sets the same value but without the restriction.
 

## Syntax

```js
mp.game.gameplay.setWindSpeed(speed);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>