# Gameplay::setBitsInRange

## Syntax

```js
mp.game.gameplay.setBitsInRange(var, rangeStart, rangeEnd, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>var:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>rangeStart:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>rangeEnd:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>