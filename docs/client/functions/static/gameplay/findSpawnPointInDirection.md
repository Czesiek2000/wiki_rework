# Gameplay::findSpawnPointInDirection

Finds a position ahead of the player by predicting the players next actions.The positions match path finding node positions.

When roads diverge, the position may rapidly change between two or more positions. This is due to the engine not being certain of which path the player will take.

I may sort this with alter research, but if someonealready knows please tell what the difference in X2, Y2, Z2 is. 

I doubt it's rotation. Is it like checkpoints where X1, Y1, Z1 is your/a position and X2, Y2, Z2 is a given position ahead of that position?
 

## Syntax

```js
mp.game.gameplay.findSpawnPointInDirection(x1, y1, z1, x2, y2, z2, distance, spawnPoint);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>spawnPoint:</b> <b><span style="color:#008017">Vector3</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>