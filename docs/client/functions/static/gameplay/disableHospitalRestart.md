# Gameplay::disableHospitalRestart

The game by default has 5 hospital respawn points. Disabling them all will cause the player to respawn at the last position they were.

*mlgthatsme*

Doesn't work....
 

## Syntax

```js
mp.game.gameplay.disableHospitalRestart(hospitalIndex, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hospitalIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>