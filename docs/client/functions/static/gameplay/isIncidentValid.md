# Gameplay::isIncidentValid

## Syntax

```js
mp.game.gameplay.isIncidentValid(incidentId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>incidentId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>