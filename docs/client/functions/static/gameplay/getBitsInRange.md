# Gameplay::getBitsInRange

## Syntax

```js
mp.game.gameplay.getBitsInRange(var, rangeStart, rangeEnd);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>var:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>rangeStart:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>rangeEnd:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>