# Gameplay::getRandomIntInRange

Another extremely useful native.

You can use it simply like: `if (GAMEPLAY::GET_RANDOM_INT_IN_RANGE(0, 2))` and the if-statement will count it as false only if the random int is 0. That means there is a one in three chance of it being false. 

Put a '!' in front and it means there is a one in three chance of it being true.
 

## Syntax

```js
mp.game.gameplay.getRandomIntInRange(startRange, endRange);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>startRange:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>endRange:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>