# Gameplay::setGravityLevel

## Syntax

```js
mp.game.gameplay.setGravityLevel(level);
```

## Example

```js
mp.game.gameplay.setGravityLevel(0)
```


<br />


## Parameters

<li><b>level</b> can be a <b><span style="color:#008017">Int</span></b> from 0 to 3<br/><b>0</b>: 9.8 - normal gravity<br/><b>1</b>: 2.4 - low gravity<br/><b>2</b>: 0.1 - very low gravity<br/><b>3</b>: 0.0 - no gravity<br/><br/></li>

<br />
<br />

## Return value

No return value here
