# Gameplay::setFadeOutAfterArrest

Sets whether the game should fade out after the player is arrested.
 

## Syntax

```js
mp.game.gameplay.setFadeOutAfterArrest(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>