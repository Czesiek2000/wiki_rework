# Gameplay::getGroundZFor3dCoord

Returns the first surface beneath the specified elevation (at the given location).

This can be a roof, an elevated road, or the ground.
(If no surface can be found beneath, then the returned boolean will be false, and the float will contain 0.)

For water positions the sea floor will be returned (negative value).
 
For the function to work reliably, the position tested should have been streamed already (i.e. close to the player); otherwise it might return 0.
 

## Syntax

```js
mp.game.gameplay.getGroundZFor3dCoord(x, y, z, unknowna, unknownb);
```

## Example

```js
const getGroundZ = mp.game.gameplay.getGroundZFor3dCoord(position.x, position.y, position.z, parseFloat(0), false);

mp.markers.new(1, new mp.Vector3(position.x, position.y,getGroundZ), 1.5, {
    color: [0, 255, 0, 100],
    visible: true
});
```


<br />


## Parameters

<li><b>x:</b> float - X-coordinate of location to test</li>
<li><b>y:</b> float - Y-coordinate of location to test</li>
<li><b>z:</b> float - Z-coordinate, beneath which to test</li>
<li><b>unknowna:</b> float - this is ignored</li>
<li><b>unknownb:</b> boolean - unknown (normally 0)</li>

<br />
<br />

## Return value

<li><b>float</b> - Elevation above the ground</li>