# Gameplay::setRainFxIntensity

puddles, rain fx on ground / buildings / puddles, rain sound
 

## Syntax

```js
mp.game.gameplay.setRainFxIntensity(intensity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>intensity:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>