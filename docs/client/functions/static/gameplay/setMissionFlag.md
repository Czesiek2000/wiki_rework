# Gameplay::setMissionFlag

If true, the player can't save the game. 

*MulleDK19*: If the parameter is true, sets the mission flag to true, if the parameter is false, the function does nothing at all.^ also, if the mission flag is already set, the function does nothing at all
 

## Syntax

```js
mp.game.gameplay.setMissionFlag(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>