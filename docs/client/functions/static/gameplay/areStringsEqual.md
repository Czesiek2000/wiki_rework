# Gameplay::areStringsEqual

is this like strcmp??
 

## Syntax

```js
mp.game.gameplay.areStringsEqual(string1, string2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>string1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>string2:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>