# Gameplay::getDistanceBetweenCoords

If useZ is false, only the 2D plane (X-Y) will be considered for calculating the distance.Consider using this faster native instead: `SYSTEM::VDIST - DVIST` always takes in consideration the 3D coordinates.
 

## Syntax

```js
mp.game.gameplay.getDistanceBetweenCoords(x1, y1, z1, x2, y2, z2, useZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>useZ:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>