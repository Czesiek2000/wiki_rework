# Gameplay::setDispatchIdealSpawnDistance

## Syntax

```js
mp.game.gameplay.setDispatchIdealSpawnDistance(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>