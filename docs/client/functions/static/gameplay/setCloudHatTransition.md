# Gameplay::setCloudHatTransition

## Syntax

```js
mp.game.gameplay.setCloudHatTransition(type, transitionTime);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>transitionTime:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>