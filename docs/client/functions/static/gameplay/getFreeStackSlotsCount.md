# Gameplay::getFreeStackSlotsCount

## Syntax

```js
mp.game.gameplay.getFreeStackSlotsCount(stackSize);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>stackSize:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>