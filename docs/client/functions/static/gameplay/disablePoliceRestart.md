# Gameplay::disablePoliceRestart

Disables the spawn point at the police house on the specified index.

policeIndex: The police house index.

toggle: true to enable the spawn point, false to disable.

*Nacorpio*
 

## Syntax

```js
mp.game.gameplay.disablePoliceRestart(policeIndex, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>policeIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>