# Gameplay::deleteIncident

Delete an incident with a given id.

Correction, I have change this to int, instead of int*as it doesn't use a pointer to the createdIncident.

If you try it you will crash (or) freeze.
 

## Syntax

```js
mp.game.gameplay.deleteIncident(incidentId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>incidentId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>