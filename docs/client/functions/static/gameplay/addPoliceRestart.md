# Gameplay::addPoliceRestart

## Syntax

```js
mp.game.gameplay.addPoliceRestart(p0, p1, p2, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>