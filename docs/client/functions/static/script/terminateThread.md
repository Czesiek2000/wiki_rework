# Script::terminateThread

## Syntax

```js
mp.game.script.terminateThread(threadId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>threadId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>