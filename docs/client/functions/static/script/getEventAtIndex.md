# Script::getEventAtIndex

## Syntax

```js
mp.game.script.getEventAtIndex(eventGroup, eventIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>eventGroup</b>: <b><span style="color:#008017">int</span></b> - event types:
<ul><li><b>0</b> - CEventGroupScriptAI</li>
<li><b>1</b> - CEventGroupScriptNetwork</li></ul></li>
<li><b>eventIndex:</b> <b><span style="color:#008017">int</span></b></li>

<br />
<br />

## Return value

<li><b>0</b> - CEventGroupScriptAI</li>