# Script::triggerScriptEvent

## Syntax

```js
mp.game.script.triggerScriptEvent(p0, argsStruct, argsStructSize, bitset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>argsStruct:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>argsStructSize:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>bitset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>