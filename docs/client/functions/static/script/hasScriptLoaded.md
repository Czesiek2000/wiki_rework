# Script::hasScriptLoaded

Returns if a script has been loaded into the game. Used to see if a script was loaded after requesting.For a full list, see here: pastebin.com/yLNWicUi


## Syntax

```js
mp.game.script.hasScriptLoaded(scriptName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>