# Script::getNumberOfInstancesOfStreamedScript

Gets the number of instances of the specified script is currently running.

Actually returns numInstances - 1.

```cpp
if (scriptPtr) 
    v3 = GetNumberOfInstancesOfScript(scriptPtr) - 1;
return v3;
```
 

## Syntax

```js
mp.game.script.getNumberOfInstancesOfStreamedScript(scriptHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>int</b></li>