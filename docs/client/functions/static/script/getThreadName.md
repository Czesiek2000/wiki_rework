# Script::getThreadName

The reversed code looks like this (Sasuke78200)

```cpp
char g_szScriptName[64];

char* _0xBE7ACD89(int a_iThreadID) {
    scrThread* l_pThread;
    
    // Get the script thread 
    l_pThread = GetThreadByID(a_iThreadID);
    
    if(l_pThread == 0 || l_pThread->m_iThreadState == 2) {
        strncpy(g_szScriptName, , 64); 
    } else { 
        strncpy(g_szScriptName, l_pThread->m_szScriptName, 64); 
    }
    return g_szScriptName;
}
```
 

## Syntax

```js
mp.game.script.getThreadName(threadId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>threadId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>