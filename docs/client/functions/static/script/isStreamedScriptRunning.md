# Script::isStreamedScriptRunning

formerly `_IS_STREAMED_SCRIPT_RUNNING`

Jenkins hash: `0x19EAE282`


## Syntax

```js
mp.game.script.isStreamedScriptRunning(scriptHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>