# Script::setStreamedScriptAsNoLongerNeeded

## Syntax

```js
mp.game.script.setStreamedScriptAsNoLongerNeeded(scriptHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>