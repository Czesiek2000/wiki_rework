# Script::doesScriptExist

For a full list, see [here](https://pastebin.com/yLNWicUi)


## Syntax

```js
mp.game.script.doesScriptExist(scriptName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>