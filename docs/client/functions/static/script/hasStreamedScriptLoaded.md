# Script::hasStreamedScriptLoaded

## Syntax

```js
mp.game.script.hasStreamedScriptLoaded(scriptHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>