# Script::isThreadActive


## Syntax

```js
mp.game.script.isThreadActive(threadId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>threadId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>