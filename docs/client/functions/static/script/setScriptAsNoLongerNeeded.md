# Script::setScriptAsNoLongerNeeded

For a full list, see [here](https://pastebin.com/yLNWicUi)


## Syntax

```js
mp.game.script.setScriptAsNoLongerNeeded(scriptName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>