# Script::requestStreamedScript

formerly `_REQUEST_STREAMED_SCRIPT`


## Syntax

```js
mp.game.script.requestStreamedScript(scriptHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>