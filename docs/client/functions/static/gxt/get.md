# GXT::get

This function is used to get the value of a GXT entry.
 

 
 

 

## Syntax

```js
mp.game.gxt.get(labelNameOrHash);

```

## Example

Get the value of PM_PAUSE_HDR:

```js
mp.game.gxt.set("PM_PAUSE_HDR", "My Server Name");
mp.gui.chat.push(`Header: ${mp.game.gxt.get("PM_PAUSE_HDR")}`); // "Header: My Server Name"

```


<br />


## Parameters

<ul><li><b>labelNameOrHash</b>: <b><span style="color:#008017">String</span></b>/<b><span style="color:#008017">Number</span></b>
<ul><li>Name (string) or hash (number) of the GXT entry.</li></ul></li></ul>
<ul><li><b>newLabelValue</b>: <b><span style="color:#008017">String</span></b>
<ul><li>New value of the GXT entry.</li></ul></li></ul>

<br />
<br />

## Return value

No return value here