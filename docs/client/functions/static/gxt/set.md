# GXT::set

This function is used to add new GXT entries or replace existing ones.


## Syntax

```js
mp.game.gxt.set(labelNameOrHash, newLabelValue);
```

## Example

Set pause menu title to "My Server Name":

```js
mp.game.gxt.set("PM_PAUSE_HDR", "My Server Name");
```

<br />


## Parameters

<ul><li><b>labelNameOrHash</b>: <b><span style="color:#008017">String</span></b>/<b><span style="color:#008017">Number</span></b>
<ul><li>Name (string) or hash (number) of the GXT entry.</li></ul></li></ul>
<ul><li><b>newLabelValue</b>: <b><span style="color:#008017">String</span></b>
<ul><li>New value of the GXT entry.</li></ul></li></ul>

<br />
<br />

## Return value

No return value here
