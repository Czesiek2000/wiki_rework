# GXT::getDefault

This function is used to get the default value of a GXT entry.


## Syntax

```js
mp.game.gxt.getDefault(labelNameOrHash);

```

## Example

Get the default value of PM_PAUSE_HDR:

```js
mp.gui.chat.push(`Header Default: ${mp.game.gxt.getDefault("PM_PAUSE_HDR")}`); // "Header Default: Grand Theft Auto V"
```


<br />


## Parameters

<ul><li><b>labelNameOrHash</b>: <b><span style="color:#008017">String</span></b>/<b><span style="color:#008017">Number</span></b>
<ul><li>Name (string) or hash (number) of the GXT entry.</li></ul></li></ul>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>
