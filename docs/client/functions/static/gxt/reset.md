# GXT::reset

This function is used to reset all changes done to GXT entries.


## Syntax

```js
mp.game.gxt.reset();

```

## Example

```js
mp.game.gxt.set("PM_PAUSE_HDR", "My Server Name");
mp.game.gxt.reset();

mp.gui.chat.push(`Header: ${mp.game.gxt.get("PM_PAUSE_HDR")}`); // "Header: RAGE Multiplayer"
mp.gui.chat.push(`Header Default: ${mp.game.gxt.getDefault("PM_PAUSE_HDR")}`); // "Header Default: Grand Theft Auto V"
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

No return values here