# Cam::setGameplayCamShakeAmplitude

Sets the amplitude for the gameplay (i.e. 3rd or 1st) camera to shake. Used in script 'drunk_controller.ysc.c4' to simulate making the player drunk.- Shawn
 

## Syntax

```js
mp.game.cam.setGameplayCamShakeAmplitude(amplitude);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>amplitude:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>