# Cam::setFollowVehicleCamZoomLevel

## Syntax

```js
mp.game.cam.setFollowVehicleCamZoomLevel(zoomLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>zoomLevel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>