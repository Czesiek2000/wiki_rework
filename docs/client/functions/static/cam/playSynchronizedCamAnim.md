# Cam::playSynchronizedCamAnim

Examples:
```cpp
CAM::PLAY_SYNCHRONIZED_CAM_ANIM(l_2734, NETWORK::_02C40BF885C567B6(l_2739), 'PLAYER_EXIT_L_CAM', 'mp_doorbell');
CAM::PLAY_SYNCHRONIZED_CAM_ANIM(l_F0D[7/*1*/], l_F4D[15/*1*/], 'ah3b_attackheli_cam2', 'missheistfbi3b_helicrash');
```

## Syntax

```js
mp.game.cam.playSynchronizedCamAnim(p0, p1, animName, animDictionary);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>animName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>animDictionary:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>