# Cam::setGameplayCamRelativeHeading

Sets the camera position relative to heading in float from -360 to +360.

Heading is alwyas 0 in aiming camera.
 

## Syntax

```js
mp.game.cam.setGameplayCamRelativeHeading(heading);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>