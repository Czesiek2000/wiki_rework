# Cam::setWidescreenBorders

## Syntax

```js
mp.game.cam.setWidescreenBorders(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>