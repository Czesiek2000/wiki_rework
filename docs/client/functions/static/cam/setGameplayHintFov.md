# Cam::setGameplayHintFov

## Syntax

```js
mp.game.cam.setGameplayHintFov(FOV);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>FOV:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>