# Cam::setGameplayCamRelativePitch

Sets the camera pitch.Parameters:

x: pitches the camera on the x axis.

Value2: always seems to be hex 0x3F800000 (1.000000 float).
 

## Syntax

```js
mp.game.cam.setGameplayCamRelativePitch(x, Value2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Value2:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>