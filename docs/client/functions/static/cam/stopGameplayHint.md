# Cam::stopGameplayHint

## Syntax

```js
mp.game.cam.stopGameplayHint(force);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>force:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>