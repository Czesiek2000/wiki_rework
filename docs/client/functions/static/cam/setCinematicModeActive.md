# Cam::setCinematicModeActive

p0 = *0/1* or **true/false**

It doesn't seems to work
 

## Syntax

```js
mp.game.cam.setCinematicModeActive(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>