# Cam::doScreenFadeOut

Fades the screen out.

duration: The time the fade should take, in milliseconds.
 

## Syntax

```js
mp.game.cam.doScreenFadeOut(duration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>