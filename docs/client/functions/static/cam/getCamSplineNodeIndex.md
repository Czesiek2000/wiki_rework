# Cam::getCamSplineNodeIndex

I named the beginning from Any to BOOL as this native is used in an if statement as well.
 

## Syntax

```js
mp.game.cam.getCamSplineNodeIndex(cam);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cam:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>