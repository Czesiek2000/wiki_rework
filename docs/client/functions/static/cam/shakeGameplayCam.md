# Cam::shakeGameplayCam

Possible shake types (updated b617d):

* DEATH_FAIL_IN_EFFECT_SHAKE
* DRUNK_SHAKE
* FAMILY5_DRUG_TRIP_SHAKE
* HAND_SHAKE
* JOLT_SHAKE
* LARGE_EXPLOSION_SHAKE
* MEDIUM_EXPLOSION_SHAKE
* SMALL_EXPLOSION_SHAKE
* ROAD_VIBRATION_SHAKE
* SKY_DIVING_SHAKE
* VIBRATE_SHAKE
 

## Syntax

```js
mp.game.cam.shakeGameplayCam(shakeName, intensity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>shakeName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>intensity:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>