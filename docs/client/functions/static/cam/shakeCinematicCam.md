# Cam::shakeCinematicCam

p0 argument found in the **b617d** scripts: 'DRUNK_SHAKE'
 

## Syntax

```js
mp.game.cam.shakeCinematicCam(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>