# Cam::createCam

* DEFAULT_SCRIPTED_CAMERA
* DEFAULT_ANIMATED_CAMERA
* DEFAULT_SPLINE_CAMERA
* DEFAULT_SCRIPTED_FLY_CAMERA
* TIMED_SPLINE_CAMERA
 

## Syntax

```js
mp.game.cam.createCam(camName, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>camName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Cam</b></li>