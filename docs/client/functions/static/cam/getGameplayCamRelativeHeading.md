# Cam::getGameplayCamRelativeHeading

## Syntax

```js
var heading = mp.game.getGameplayCamRelativeHeading();
```

## Example

```js
// PLEASE MAKE ME
```


<br />


## Parameters


<li><b>heading:</b> number</li>
<br />
<br />

## Return value

No return value here
