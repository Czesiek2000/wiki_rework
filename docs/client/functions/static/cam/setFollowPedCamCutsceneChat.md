# Cam::setFollowPedCamCutsceneChat
From the **b617d** scripts:
```cpp
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_ATTACHED_TO_ROPE_CAMERA', 0); 
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_ON_EXILE1_LADDER_CAMERA', 1500);
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_SKY_DIVING_CAMERA', 0); 
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_SKY_DIVING_CAMERA', 3000); 
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_SKY_DIVING_FAMILY5_CAMERA', 0);
CAM::SET_FOLLOW_PED_CAM_CUTSCENE_CHAT('FOLLOW_PED_SKY_DIVING_CAMERA', 0);
``` 

## Syntax

```js
mp.game.cam.setFollowPedCamCutsceneChat(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>