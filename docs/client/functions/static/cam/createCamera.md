# Cam::createCamera

## Syntax

```js
mp.game.cam.createCamera(camHash, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>camHash:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Cam</b></li>