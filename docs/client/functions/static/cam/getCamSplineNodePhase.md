# Cam::getCamSplineNodePhase

I'm pretty sure the parameter is the camera as usual, but I am not certain so 

I'm going to leave it as is.
 

## Syntax

```js
mp.game.cam.getCamSplineNodePhase(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>