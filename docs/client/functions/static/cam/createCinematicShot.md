# Cam::createCinematicShot

## Syntax

```js
mp.game.cam.createCinematicShot(p0, p1, p2, entity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>entity:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>