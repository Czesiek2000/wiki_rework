# Cam::renderScriptCams

ease - smooth transition between the camera's positionsease

Time - Time in milliseconds for the transition to happen

If you have created a script (rendering) camera, and want to go back to the character (gameplay) camera, call this native with render set to false.

Setting ease to true will smooth the transition.
 
p3 freezes the previous/gameplay camera when easing is turned on.
 

## Syntax

```js
mp.game.cam.renderScriptCams(render, ease, easeTime, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>render:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>ease:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>easeTime:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>