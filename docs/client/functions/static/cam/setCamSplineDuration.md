# Cam::setCamSplineDuration

I named p1 as timeDuration as it is obvious. I'm assuming tho it is ran in ms(Milliseconds) as usual.
 

## Syntax

```js
mp.game.cam.setCamSplineDuration(cam, timeDuration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cam:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>timeDuration:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>