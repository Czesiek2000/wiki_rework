# Cam::clampGameplayCamYaw
minimum: Degrees between -180f and 180f.

maximum: Degrees between -180f and 180f.

Clamps the gameplay camera's current yaw.

Eg. `_CLAMP_GAMEPLAY_CAM_YAW(0.0f, 0.0f)` will set the horizontal angle directly behind the player.
 

## Syntax

```js
mp.game.cam.clampGameplayCamYaw(minimum, maximum);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>minimum:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>maximum:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>