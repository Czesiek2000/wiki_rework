# Cam::destroyAllCams

## Syntax

```js
mp.game.cam.destroyAllCams(destroy);
```

## Example

```js
mp.game.cam.destroyAllCams(false);
```


<br />


## Parameters

<li><b>destroy:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>