# Cam::setFollowVehicleCamViewMode

Sets the type of Player camera in vehicles:

* 0 Third Person Close
* 1 Third Person Mid
* 2 Third Person Far
* 4 First Person
 

## Syntax

```js
mp.game.cam.setFollowVehicleCamViewMode(viewMode);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>viewMode:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>