# Cam::setGameplayPedHint

## Syntax

```js
mp.game.cam.setGameplayPedHint(p0, x1, y1, z1, p4, p5, p6, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> Ped handle or object</li>
<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>