# Cam::clampGameplayCamPitch

minimum: Degrees between -90f and 90f.

maximum: Degrees between -90f and 90f.

Clamps the gameplay camera's current pitch.

Eg. `_CLAMP_GAMEPLAY_CAM_PITCH(0.0f, 0.0f)` will set the vertical angle directly behind the player.
 

## Syntax

```js
mp.game.cam.clampGameplayCamPitch(minimum, maximum);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>minimum:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>maximum:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>