# Cam::getGameplayCamRot

p0 dosen't seem to change much, I tried it with 0, 1, 2:

* 0-Pitch(X): -70.000092
* 0-Roll(Y): -0.000001
* 0-Yaw(Z): -43.886459
* 1-Pitch(X): -70.000092
* 1-Roll(Y): -0.000001
* 1-Yaw(Z): -43.886463
* 2-Pitch(X): -70.000092
* 2-Roll(Y): -0.000002
* 2-Yaw(Z): -43.886467
 

The X component is stored as  

Same for Y and Z (keep in mind that when we get a component from a Vector3 we use small letters)
 

## Syntax

```js
mp.game.cam.getGameplayCamRot(p0);
```

## Example

```js
//Store the values:
var cam_rot = mp.game.cam.getGameplayCamRot(0);
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>