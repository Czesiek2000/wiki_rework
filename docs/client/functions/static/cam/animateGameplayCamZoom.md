# Cam::animateGameplayCamZoom
Seems to animate the gameplay camera zoom.

Eg. `_ANIMATE_GAMEPLAY_CAM_ZOOM(1f, 1000f);`

will animate the camera zooming in from 1000 meters away.Game scripts use it like this:// Setting this to 1 prevents V key from changing zoom
```cpp
PLAYER::SET_PLAYER_FORCED_ZOOM(PLAYER::PLAYER_ID(), 1);// These restrict how far you can move cam up/down left/right
CAM::_CLAMP_GAMEPLAY_CAM_YAW(-20f, 50f);
CAM::_CLAMP_GAMEPLAY_CAM_PITCH(-60f, 0f);
CAM::_ANIMATE_GAMEPLAY_CAM_ZOOM(1f, 1f);
```

## Syntax

```js
mp.game.cam.animateGameplayCamZoom(p0, distance);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>