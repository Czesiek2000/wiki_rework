# Cam::setCamSplinePhase

## Syntax

```js
mp.game.cam.setCamSplinePhase(cam, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cam:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>