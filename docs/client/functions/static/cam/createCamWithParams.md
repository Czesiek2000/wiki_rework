# Cam::createCamWithParams

camName is always set to 'DEFAULT_SCRIPTED_CAMERA' in Rockstar's scripts.

Camera names found in the *b617d* scripts:
* DEFAULT_ANIMATED_CAMERA
* DEFAULT_SCRIPTED_CAMERA
* DEFAULT_SCRIPTED_FLY_CAMERA
* DEFAULT_SPLINE_CAMERA

**Side Note**: It seems p8 is basically to represent what would be the bool p1 within CREATE_CAM native. As well as the p9 since it's always 2 in scripts seems to represent what would be the last param within SET_CAM_ROT native which normally would be 2.
 

## Syntax

```js
mp.game.cam.createCamWithParams(camName, posX, posY, posZ, rotX, rotY, rotZ, fov, p8, p9);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>camName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fov:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Cam</b></li>