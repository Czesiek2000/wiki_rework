# Cam::setGameplayCamRawYaw

Does nothing
 

## Syntax

```js
mp.game.cam.setGameplayCamRawYaw(yaw);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>yaw:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>