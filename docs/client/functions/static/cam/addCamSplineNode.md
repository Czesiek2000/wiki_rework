# Cam::addCamSplineNode

I filled *p1-p6* (the floats) as they are as other natives with 6 floats in a row are similar and I see no other method. So if a test from anyone proves them wrong please correct.

p7 (length) determines the length of the spline, affects camera path and duration of transition between previous node and this one

p8 big values ~100 will slow down the camera movement before reaching this node

p9 != 0 seems to override the rotation/pitch (bool?)
 

## Syntax

```js
mp.game.cam.addCamSplineNode(camera, x, y, z, xRot, yRot, zRot, length, p8, p9);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>camera:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>length:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>