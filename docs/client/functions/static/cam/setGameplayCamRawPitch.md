# Cam::setGameplayCamRawPitch

## Syntax

```js
mp.game.cam.setGameplayCamRawPitch(pitch);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pitch:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>