# Cam::doScreenFadeIn

Fades the screen in.

duration: The time the fade should take, in milliseconds.
 

## Syntax

```js
mp.game.cam.doScreenFadeIn(duration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>