# Cam::createCameraWithParams

`CAM::_GET_GAMEPLAY_CAM_COORDS` can be used instead of posX,Y,Z

`CAM::_GET_GAMEPLAY_CAM_ROT` can be used instead of rotX,Y,Z

`CAM::_80EC114669DAEFF4()` can be used instead of p7 (Possible p7 is FOV parameter. )

p8 ???

p9 uses 2 by default
 

## Syntax

```js
mp.game.cam.createCameraWithParams(camHash, posX, posY, posZ, rotX, rotY, rotZ, fov, p8, p9);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>camHash:</b> Model hash or name</li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fov:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Cam</b></li>