# Dlc1::getShopPedQueryComponent

## Syntax

```js
mp.game.dlc1.getShopPedQueryComponent(componentId, outComponent);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outComponent:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>