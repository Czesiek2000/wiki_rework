# Dlc1::getDlcVehicleModel
dlcVehicle Index is **0** to `GET_NUM_DLC_VEHICLS() - 1`
 

## Syntax

```js
mp.game.dlc1.getDlcVehicleModel(dlcVehicleIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dlcVehicleIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>