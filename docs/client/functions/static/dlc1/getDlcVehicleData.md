# Dlc1::getDlcVehicleData

dlcVehicle

Index takes a number from 0 - `GET_NUM_DLC_VEHICLES()` - 1.

outData is a struct of 3 8-byte items.

The Second item in the struct `*(Hash *)(outData + 1)` is the vehicle hash.
 

## Syntax

```js
mp.game.dlc1.getDlcVehicleData(dlcVehicleIndex, outData);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dlcVehicleIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outData:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>