# Dlc1::getNumDlcWeaponComponents

Allowed Values from **0** - `DLC1::GET_NUM_DLC_WEAPONS() - 1`

## Syntax

```js
mp.game.dlc1.getNumDlcWeaponComponents(dlcWeaponIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dlcWeaponIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>