# Dlc1::getDlcVehicleFlags

## Syntax

```js
mp.game.dlc1.getDlcVehicleFlags(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>