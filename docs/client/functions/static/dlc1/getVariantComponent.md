# Dlc1::getVariantComponent

## Syntax

```js
mp.game.dlc1.getVariantComponent(componentHash, componentId, p2, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentHash:</b> Model hash or name</li>
<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>