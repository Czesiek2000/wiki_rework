# Dlc1::getNumPropsFromOutfit

character is **0** for Michael, **1** for Franklin, **2** for Trevor, **3** for freemode male, and **4** for freemode female.

componentId is between 0 and 11 and corresponds to the usual component slots.

p1 could be the outfit number; unsure.

p2 is usually -1; unknown function.

p3 appears to be a boolean flag; unknown function.

p4 is usually -1; unknown function.
 

## Syntax

```js
mp.game.dlc1.getNumPropsFromOutfit(character, p1, p2, p3, p4, componentId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>character:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>componentId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>