# Dlc1::getDlcWeaponData

dlcWeapon Index takes a number from 0 - `GET_NUM_DLC_WEAPONS() - 1`.

```cpp
struct DlcWeaponData{
    int emptyCheck;
    //use DLC1::_IS_DLC_DATA_EMPTY on this
    int padding1;
    int weaponHash;
    int padding2
	int unk
	int padding3
	int weaponCost
	int padding4
	int ammoCost
	int padding5
	int ammoType
	int padding6
	int defaultClipSize
	int padding7
	int nameLabel[64]
	int descLabel[64]
	int desc2Label[64]; // usually 'the' + namechar upperCaseNameLabel[64];
};
```
 

## Syntax

```js
mp.game.dlc1.getDlcWeaponData(dlcWeaponIndex, outData);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dlcWeaponIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outData:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>