# Dlc1::getShopPedQueryOutfit
```cpp
struct Outfit_s {
    int mask, torso, pants, parachute, shoes, misc1, tops1, armour, crew, tops2, hat, glasses, earpiece; 
    int maskTexture, torsoTexture, pantsTexture, parachuteTexture, shoesTexture, misc1Texture, tops1Texture,  armourTexture, crewTexture, tops2Texture, hatTexture, glassesTexture, earpieceTexture; 
};
```
 

## Syntax

```js
mp.game.dlc1.getShopPedQueryOutfit(p0, outfit);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>outfit:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>