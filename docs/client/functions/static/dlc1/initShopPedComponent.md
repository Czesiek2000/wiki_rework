# Dlc1::initShopPedComponent

## Syntax

```js
mp.game.dlc1.initShopPedComponent(outComponent);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>outComponent:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>