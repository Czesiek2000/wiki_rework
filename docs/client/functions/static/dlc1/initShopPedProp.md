# Dlc1::initShopPedProp

## Syntax

```js
mp.game.dlc1.initShopPedProp(outProp);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>outProp:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>