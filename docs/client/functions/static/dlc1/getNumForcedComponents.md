# Dlc1::getNumForcedComponents

Returns number of possible values of the component

Id argument of `GET_FORCED_COMPONENT`.

## Syntax

```js
mp.game.dlc1.getNumForcedComponents(componentHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>