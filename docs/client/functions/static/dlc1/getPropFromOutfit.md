# Dlc1::getPropFromOutfit

outfit = a structure passing though it - see `GET_SHOP_PED_QUERY_OUTFITslot` - outfit slotitem - hold 3 ints in a struct, you can use `Vector3 structureGET_SHOP_PED_` ???
 

## Syntax

```js
mp.game.dlc1.getPropFromOutfit(outfit, slot, item);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>outfit:</b> unknown (to be checked)</li>
<li><b>slot:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>item:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>