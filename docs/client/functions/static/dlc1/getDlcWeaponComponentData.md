# Dlc1::getDlcWeaponComponentData

p0 seems to be the weapon index

p1 seems to be the weapon component index

```cpp
struct DlcComponentData {
    int attachBone;
    int padding1;
    int bActiveByDefault;
    int padding2;
    int unk;
    int padding3;
    int componentHash;
    int padding4;
    int unk2;
    int padding5;
    int componentCost;
    int padding6;
    char nameLabel[64];
    char descLabel[64];
};
```

## Syntax

```js
mp.game.dlc1.getDlcWeaponComponentData(p0, p1, ComponentDataPtr);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>ComponentDataPtr:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>