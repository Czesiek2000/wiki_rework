# Dlc1::isDlcDataEmpty

## Syntax

```js
mp.game.dlc1.isDlcDataEmpty(dlcData);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>dlcData:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>