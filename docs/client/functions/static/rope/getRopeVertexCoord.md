# Rope::getRopeVertexCoord

## Syntax

```js
mp.game.rope.getRopeVertexCoord(rope, vertex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>vertex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>