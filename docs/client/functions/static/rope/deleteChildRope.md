# Rope::deleteChildRope

## Syntax

```js
mp.game.rope.deleteChildRope(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>