# Rope::loadRopeData

Rope presets can be found in the gamefiles. 

One example is 'ropeFamily3', it is NOT a hash but rather a string.


## Syntax

```js
mp.game.rope.loadRopeData(rope, rope_preset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>rope_preset:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>