# Rope::setDisableFragDamage

sometimes used used with NET_TO_OBJ

hash collision last 2 words


## Syntax

```js
mp.game.rope.setDisableFragDamage(object, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>object:</b> Object handle or object</li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>