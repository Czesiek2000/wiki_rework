# Rope::addRope

Creates a rope at the specific position, that extends in the specified direction when not attached to any entities.

`__Add_Rope(pos.x,pos.y,pos.z,0.0,0.0,0.0,20.0,4,20.0,1.0,0.0,false,false,false,5.0,false,NULL)`

When attached, Position<vector> does not matterWhen attached, Angle<vector> does not matter

Rope Type: 
* 4: and bellow is a thick rope
* 5: and up are small metal wires
* 0: crashes the game

Max_length - Rope is forced to this length, generally best to keep this the same as your rope length.

Rigid - If max length is zero, and this is set to false the rope will become rigid (it will force a specific distance, what ever length is, between the objects).

breakable - Whether or not shooting the rope will break it.

unkPtr - unknown ptr, always 0 in orig scripts__Lengths can be calculated like so:
```cpp
float distance = abs(x1 - x2) + abs(y1 - y2) + abs(z1 - z2); // Rope length
```

**NOTES:**

Rope does NOT interact with anything you attach it to, in some cases it make interact with the world AFTER it breaks (seems to occur if you set the type to -1).

Rope will sometimes contract and fall to the ground like you'd expect it to, but since it doesn't interact with the world the effect is just jaring.
 

## Syntax

```js
mp.game.rope.addRope(x, y, z, rotX, rotY, rotZ, length, ropeType, maxLength, minLength, p10, p11, p12, rigid, p14, breakWhenShot, unkPtr);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>length:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ropeType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>maxLength:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>minLength:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Float</span></b> (0.5 makes the rope twist when you fold it and there's excess (more realistic))</li>
<li><b>p11:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p12:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>rigid:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p14:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>breakWhenShot:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unkPtr:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Object handle or object</b></li>