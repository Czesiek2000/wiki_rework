# Rope::ropeForceLength

Forces a rope to a certain length.


## Syntax

```js
mp.game.rope.ropeForceLength(rope, length);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>length:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>