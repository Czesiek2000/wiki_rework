# Rope::attachEntitiesToRope

Attaches entity 1 to entity 2.


## Syntax

```js
mp.game.rope.attachEntitiesToRope(rope, ent1, ent2, ent1_x, ent1_y, ent1_z, ent2_x, ent2_y, ent2_z, length, p10, p11, p12, p13);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>ent1:</b> Entity handle or object</li>
<li><b>ent2:</b> Entity handle or object</li>
<li><b>ent1_x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ent1_y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ent1_z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ent2_x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ent2_y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ent2_z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>length:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p11:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p12:</b> unknown (to be checked)</li>
<li><b>p13:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>