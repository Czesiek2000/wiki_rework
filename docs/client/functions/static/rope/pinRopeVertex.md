# Rope::pinRopeVertex

## Syntax

```js
mp.game.rope.pinRopeVertex(rope, vertex, x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>rope:</b> Object handle or object</li>
<li><b>vertex:</b> int</li>
<li><b>x:</b> float</li>
<li><b>y:</b> float</li>
<li><b>z:</b> float</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>