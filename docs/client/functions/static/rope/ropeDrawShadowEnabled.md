# Rope::ropeDrawShadowEnabled

## Syntax

```js
mp.game.rope.ropeDrawShadowEnabled(rope, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> <b><span style="color:#008017">Object</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b></li>