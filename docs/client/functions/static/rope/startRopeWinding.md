# Rope::startRopeWinding

## Syntax

```js
mp.game.rope.startRopeWinding(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>