# Rope::stopRopeUnwindingFront

## Syntax

```js
mp.game.rope.stopRopeUnwindingFront(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>