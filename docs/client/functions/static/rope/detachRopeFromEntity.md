# Rope::detachRopeFromEntity

## Syntax

```js
mp.game.rope.detachRopeFromEntity(rope, entity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>entity:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>