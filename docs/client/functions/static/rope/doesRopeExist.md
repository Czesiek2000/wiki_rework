# Rope::doesRopeExist

Ptr is correct


## Syntax

```js
mp.game.rope.doesRopeExist(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> <b><span style="color:#008017">Object</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b></li>