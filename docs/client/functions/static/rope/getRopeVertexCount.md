# Rope::getRopeVertexCount

## Syntax

```js
mp.game.rope.getRopeVertexCount(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>