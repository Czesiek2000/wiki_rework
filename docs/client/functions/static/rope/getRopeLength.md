# Rope::getRopeLength

Get a rope's length. Can be modified with `ROPE_FORCE_LENGTH`


## Syntax

```js
mp.game.rope.getRopeLength(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>