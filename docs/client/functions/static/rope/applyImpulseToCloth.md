# Rope::applyImpulseToCloth

## Syntax

```js
mp.game.rope.applyImpulseToCloth(posX, posY, posZ, vecX, vecY, vecZ, impulse);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>vecX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>vecY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>vecZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>impulse:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>