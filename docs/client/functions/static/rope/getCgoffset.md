# Rope::getCgoffset

## Syntax

```js
mp.game.rope.getCgoffset(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>