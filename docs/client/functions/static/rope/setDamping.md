# Rope::setDamping

## Syntax

```js
mp.game.rope.setDamping(rope, vertex, value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>vertex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>