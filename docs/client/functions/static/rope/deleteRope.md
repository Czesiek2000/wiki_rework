# Rope::deleteRope

## Syntax

```js
mp.game.rope.deleteRope(rope);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> <b><span style="color:#008017">Object</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b></li>