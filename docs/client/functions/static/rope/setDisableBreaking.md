# Rope::setDisableBreaking

## Syntax

```js
mp.game.rope.setDisableBreaking(rope, enabled);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>enabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>