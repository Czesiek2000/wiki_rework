# Rope::attachRopeToEntity

The position supplied can be anywhere, and the entity should anchor relative to that point from it's origin.


## Syntax

```js
mp.game.rope.attachRopeToEntity(rope, entity, x, y, z, p5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rope:</b> Object handle or object</li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>