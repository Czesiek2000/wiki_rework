# Globals::invokeString

Invokes a specified [Native](https://cdn.rage.mp/public/natives/) that returns a String value.
 

## Syntax

```js
mp.game.invokeString(hash, args);
```

## Example

```js
let stateString = mp.game.invokeString('0x717E4D1F2048376D', mp.players.local.handle);
```


<br />


## Parameters

<ul><li><b>hash: <span style="color:#008017">String</span></b></li>
<li><b>args: <span style="color:#008017">Any</span></b></li></ul>

<br />
<br />

## Return value

<li><b>String</b></li>
