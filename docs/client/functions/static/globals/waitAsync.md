# Globals::waitAsync

An asynchronous function that pauses execution of your current script.
 

## Syntax

```js
mp.game.waitAsync(ms);
```

## Example

Requests the specified scaleform, waits until it is loaded then returns the scaleform handle.

```js
async function requestScaleform(scaleformName) {
    const handle = mp.game.graphics.requestScaleformMovie(scaleformName);

    while (!mp.game.graphics.hasScaleformMovieLoaded(handle)) {
        await mp.game.waitAsync(0);
    }

    return handle;
}
```


<br />


## Parameters

<li><b>ms</b>: <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value


