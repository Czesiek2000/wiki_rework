# Globals::invokeVector3
Invokes a specified [Native](https://cdn.rage.mp/public/natives/) that returns a Vector3 object.
 

## Syntax

```js
mp.game.invokeVector3(hash, args);
```

## Example

```js
let bonePosVector = mp.game.invokeVector3('0x44A8FCB8ED227738', mp.players.local.handle, boneId);
```


<br />


## Parameters

<ul><li><b>hash: <span style="color:#008017">String</span></b></li>
<li><b>args: <span style="color:#008017">Any</span></b></li></ul>

<br />
<br />

## Return value

<li><b>Vector3</b></li>