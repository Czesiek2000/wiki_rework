# Globals::invoke

Invokes specified [native](https://cdn.rage.mp/public/natives/) function
 

## Syntax

```js
mp.game.invoke(hash, [, ...args])
```

## Example

```js
// Normal invoke void
mp.game.invoke('0x428CA6DBD1094446', mp.players.local.handle, true); // FREEZE_ENTITY_POSITION

// You should use arrays with one element of required type to pass variable as a reference
let posixTime = { year: [0], month: [0], day: [0], hour: [0], minute: [0], second: [0] };
mp.game.invoke('0xDA488F299A5B164E', posixTime.year, posixTime.month, posixTime.day, posixTime.hour, posixTime.minute, posixTime.second);

mp.gui.chat.push(`Year: ${posixTime.year[0]}`);

// that works for all types
let matrix = { r: [[0, 0, 0]], f: [[0, 0, 0]], u: [[0, 0, 0]], p: [[0, 0, 0]] };
mp.game.invoke('0xECB2FC7235A7D137', handle, matrix.r, matrix.f, matrix.u, matrix.p);

// ArrayBuffers could be used too
let outDataBuffer = [new ArrayBuffer(256)];
mp.game.invoke('0x79923CD21BECE14E', 0, outDataBuffer);

// If you are dealing with hashes, you might have some problems since JavaScript might try to convert it to float in some cases.
// To avoid that, always append >> 0 to the hash
mp.game.invoke('0x015A522136D7F951', mp.players.local.handle, mp.game.joaat('weapon_specialcarbine') >> 0) // GET_AMMO_IN_PED_WEAPON
// This example would not work otherwise
```


<br />


## Parameters

<ul><li><b>hash: <span style="color:#008017">String</span></b></li>
<li><b>args: <span style="color:#008017">Any</span></b></li></ul>

<br />
<br />

## Return value

No return value