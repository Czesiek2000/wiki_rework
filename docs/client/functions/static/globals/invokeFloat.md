# Globals::invokeFloat

Invokes a specified [Native](https://cdn.rage.mp/public/natives/) that returns a float value.
 

## Syntax

```js
mp.game.invokeFloat(hash, args);

```

## Example

```js
let phoneGestureAnimTime = mp.game.invokeFloat('0x47619ABE8B268C60', mp.players.local.handle);
```


<br />


## Parameters

<ul><li><b>hash: <span style="color:#008017">String</span></b></li>
<li><b>args: <span style="color:#008017">Any</span></b></li></ul>

<br />
<br />

## Return value

<li><b>Float</b></li>
