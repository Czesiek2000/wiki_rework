# Globals::joaat

This function generates hashes/arrays of hashes (hash is integer) using strings/arrays of strings. Those hashes could be used to set entity model.
 
The name "joaat" stands for <a href="https://en.wikipedia.org/wiki/en:Jenkins_hash_function#one_at_a_time" class="extiw" title="wikipedia:en:Jenkins hash function"><b>Jenkin's One At A Time</b></a> hashing function. 

## Syntax

```js
mp.joaat(String);
mp.joaat(String[]);
mp.game.joaat(String);
mp.game.joaat(String[]);
```

## Example

```js
mp.joaat("bati");
mp.joaat(["bati", "benson"]);
```


<br />


## Parameters

<li><b><span style="color:#008017">String</span></b> or <b><span style="color:#008017">String[]</span></b></li>

<br />
<br />

## Return value

No return value here
