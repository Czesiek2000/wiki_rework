# Dlc2::isDlcPresent

Example:

```cpp
DLC2::IS_DLC_PRESENT($/mpbusiness2/);
```

($ = gethashkey)

bruteforce these:
0x96F02EE6
0xB119F6D
 

## Syntax

```js
mp.game.dlc2.isDlcPresent(DlcHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>DlcHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>