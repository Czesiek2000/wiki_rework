# Dlc2::nullify

Sets the value of the specified variable to 0.Always returns true.
```cpp
bool _NULLIFY(void* variable, int unused){ 
    *variable = NULL; 
    return true;
}
```
 

## Syntax

```js
mp.game.dlc2.nullify(variable, unused);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>variable:</b> unknown (to be checked)</li>
<li><b>unused:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>