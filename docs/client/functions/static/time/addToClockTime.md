# Time::addToClockTime

## Syntax

```js
mp.game.time.addToClockTime(hours, minutes, seconds);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hours:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minutes:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>seconds:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>