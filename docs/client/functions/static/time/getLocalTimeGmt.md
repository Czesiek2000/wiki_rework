# Time::getLocalTimeGmt

console hash: `0xC589CD7D` = `GET_UTC_TIME`

gets current UTC time
 

## Syntax

```js
mp.game.time.getLocalTimeGmt(year, month, day, hour, minute, second);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>year:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>month:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>day:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>hour:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minute:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>second:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> year, month, day, hour, minute, second</li>