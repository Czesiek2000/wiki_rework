# Time::getLocalTime
Gets local system time as year, month, day, hour, minute and second.

Example usage:
```cpp
int year;
int month;
int day;
int hour;
int minute;
int second;
or use 
std::tm 
struct TIME::GET_LOCAL_TIME(&year, &month, &day, &hour, &minute, &second);
```
 

## Syntax

```js
mp.game.time.getLocalTime(year, month, day, hour, minute, second);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>year:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>month:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>day:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>hour:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minute:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>second:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> year, month, day, hour, minute, second</li>