# Time::advanceClockTimeTo

## Syntax

```js
mp.game.time.advanceClockTimeTo(hour, minute, second);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hour:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minute:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>second:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>