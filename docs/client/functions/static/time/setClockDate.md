# Time::setClockDate

## Syntax

```js
mp.game.time.setClockDate(day, month, year);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>day:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>month:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>year:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>