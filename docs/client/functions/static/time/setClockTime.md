# Time::setClockTime

`SET_CLOCK_TIME(12, 34, 56);`


## Syntax

```js
mp.game.time.setClockTime(hour, minute, second);
```

## Example

```js
//Realtime
const date = new Date();

function syncTime() {
    mp.game.time.setClockTime(date.getHours(), date.getMinutes(), date.getSeconds());
}
setInterval(syncTime, 2000);
```


<br />


## Parameters

<li><b>hour:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>minute:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>second:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>