# Discord::update

This function will let you set further details for the Discord Rich Presence field for a player if they have Discord running. Each argument represents one line under the 'playing a game' section.
 
Result:

![rich presence](../../../../images/discord/discord_rich_presence.png)

 

## Syntax

```js
mp.discord.update(detailedStatus, state)
```

## Example

```js
mp.discord.update('Playing on Freeroam', 'Playing as Ronald McDonald')
```


<br />


## Parameters

<li><b>detailedStatus:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>state:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>