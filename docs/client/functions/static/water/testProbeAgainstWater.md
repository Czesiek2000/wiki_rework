# Water::testProbeAgainstWater

## Syntax

```js
mp.game.water.testProbeAgainstWater(startX, startY, startZ, endX, endY, endZ, p6);
```

## Example

```js
// todo

```


<br />


## Parameters

<li><b>startX:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>startY:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>startZ:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>endX:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>endY:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>endZ:</b> <b><span style="color:#008017">Number</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b> (result?)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>