# Water::setWavesIntensity

Sets a value that determines how aggressive the ocean waves will be. Values of 2.0 or more make for very aggressive waves like you see during a thunderstorm.

Works only ~200 meters around the player.

Most likely `SET_CURRENT_*`


## Syntax

```js
mp.game.water.setWavesIntensity(intensity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>intensity:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>