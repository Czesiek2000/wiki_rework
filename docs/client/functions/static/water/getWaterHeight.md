# Water::getWaterHeight

This function set height to the value of z-axis of the water surface.

This function works with sea and lake. However it does not work with shallow rivers (e.g. raton canyon will return -100000.0f)

**note: seems to return true when you are in water**


## Syntax

```js
mp.game.water.getWaterHeight(x, y, z, height);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>height:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>