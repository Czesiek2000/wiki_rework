# Ui::getLengthOfStringWithThisTextLabel

Returns the string length of the string from the gxt string .


## Syntax

```js
mp.game.ui.getLengthOfStringWithThisTextLabel(gxt);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxt:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>