# Ui::setNotificationMessage

See [Notification Pictures](https://wiki.rage.mp/) for a list of notification pictures. Pictures from this link have the same textureDict and textureName.

Also some of them needs to be requested with [Graphics::requestStreamedTextureDict](../graphics/requestStreamedTextureDict.md).

## Syntax

```js
mp.game.ui.setNotificationMessage(textureDict, textureName, flash, iconType, sender, subject);
```

## Example

```js
const notifyWithPicture = () => {
	mp.game.ui.setNotificationTextEntry('STRING');
	mp.game.ui.setNotificationMessage('CHAR_RON', 'CHAR_RON', false, 2, 'New Message', 'Hello World!');
};

notifyWithPicture();
```

Output: ![notification1](../../../../images/notifications/Outputnative.png)


Values textureDict and textureName gives you ability to make custom notifcation pictures using: [Using_DLC_Packs_with_Custom_Textures](https://wiki.rage.mp/index.php?title=Using_DLC_Packs_with_Custom_Textures)
```js
// Before using your custom textures you must request them by this function
mp.game.graphics.requestStreamedTextureDict('notifications', true);

const notifyWithPicture = () => {
	mp.game.ui.setNotificationTextEntry('STRING');
	mp.game.ui.setNotificationMessage('notifications', 'rage', false, 2, 'New Message', 'Hi, now your notifications look ~y~beast');
};

notifyWithPicture();
```
Output: ![notification2](../../../../images/notifications/Outputcustom.png)

```js
// Just in case you should request textureDict
mp.game.graphics.requestStreamedTextureDict('commonmenu', true);

const notifyWithPicture = () => {
	mp.game.ui.setNotificationTextEntry('STRING');
	return mp.game.ui.setNotificationMessage('commonmenu', 'mp_specitem_cash', false, 9, 'New notification!', 'Your account has been charged');
};

notifyWithPicture();
```

Output: ![notification3](../../../../images/notifications/Outputcustom2.png)

<br />

### Icon types
<li><b>No Icon</b>: 0, 4, 5, 6</li>
<li><b>Speech Bubble</b>: 1</li>
<li><b>Message</b>: 2</li>
<li><b>Friend Request</b>: 3</li>
<li><b>Arrow</b>: 7</li>
<li><b>RP</b>: 8</li>
<li><b>Money</b>: 9</li>

<br/>

## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>textureName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>flash:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>iconType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>sender:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>subject:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>