# Ui::setTextJustification

Types 

* 0: Center-Justify
* 1: Left-Justify
* 2: Right-JustifyRight-Justify requires SET_TEXT_WRAP, otherwise it will draw to the far right of the screen


## Syntax

```js
mp.game.ui.setTextJustification(justifyType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>justifyType:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>