# Ui::addBlipForCoord

Creates an orange ( default ) Blip-object. Returns a Blip-object which can then be modified.
 

## Syntax

```js
mp.game.ui.addBlipForCoord(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Blip</span></b></li>