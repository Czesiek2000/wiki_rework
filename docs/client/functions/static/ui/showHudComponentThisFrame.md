# Ui::showHudComponentThisFrame

## Syntax

```js
mp.game.ui.showHudComponentThisFrame(componentIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>

[HUD Components full list](../../../../tables/hudCompoents.md)

<br />
<br />

## Return value

<li><b>Undefined</b></li>