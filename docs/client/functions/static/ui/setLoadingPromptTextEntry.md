# Ui::setLoadingPromptTextEntry

Initializes the text entry for the the text next to a loading prompt. 

All natives for for building UI texts can be used here

These are some localized strings used in the loading spinner.
* 'PM_WAIT' = Please Wait
* 'CELEB_WPLYRS' = Waiting For Players.
* 'CELL_SPINNER2' = Scanning storage.
* 'ERROR_CHECKYACHTNAME' = Registering your yacht's name. Please wait.
* 'ERROR_CHECKPROFANITY' = Checking your text for profanity. Please wait.
* 'FM_COR_AUTOD' = Just spinner no text
* 'FM_IHELP_WAT2' = Waiting for other players
* 'FM_JIP_WAITO' = Game options are being set
* 'FMMC_DOWNLOAD' = Downloading
* 'FMMC_PLYLOAD' = Loading
* 'FMMC_STARTTRAN' = Launching session
* 'HUD_QUITTING' = Quiting session
* 'KILL_STRIP_IDM' = Waiting for to accept
* 'MP_SPINLOADING' = Loading
 

## Syntax

```js
mp.game.ui.setLoadingPromptTextEntry(string);
```

## Example

```js
function startLoadingMessage (text) {
    mp.game.ui.setLoadingPromptTextEntry("STRING");
    mp.game.ui.addTextComponentSubstringPlayerName(text);
    mp.game.ui.showLoadingPrompt(1);

    setTimeout(() => {
        mp.game.invoke('0x10D373323E5B9C0D'); //stop after 15s
    }, 15000)
}

mp.events.add('render', () => {
    startLoadingMessage ("Hello world");
})

```


<br />


## Parameters

<li><b>string:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>