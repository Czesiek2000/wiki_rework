# Ui::restartFrontendMenu

Before using this native click the native above and look at the decription.

Example:

```csharp
int GetHash = Function.Call<int>(Hash.GET_HASH_KEY, 'fe_menu_version_corona_lobby');
Function.Call(Hash.ACTIVATE_FRONTEND_MENU, GetHash, 0, -1);
Function.Call(Hash.RESTART_FRONTEND_MENU(GetHash, -1);
```
This native refreshes the frontend menu.

p1 = Hash of Menu

p2 = Unknown but always works with -1.
 

## Syntax

```js
mp.game.ui.restartFrontendMenu(menuHash, p1);
```

## Example

```js
// todo
```

<br />


## Parameters

<li><b>menuHash:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>