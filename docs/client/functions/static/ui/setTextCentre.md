# Ui::setTextCentre

## Syntax

```js
mp.game.ui.setTextCentre(align);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>align:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>