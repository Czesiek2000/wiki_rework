# Ui::setTextFont

fonts that mess up your text where made for number values/misc stuff


## Syntax

```js
mp.game.ui.setTextFont(fontType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>fontType:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>