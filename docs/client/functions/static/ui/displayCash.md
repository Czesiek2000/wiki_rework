# Ui::displayCash
`DISPLAY_CASH(false);` makes the cash amount render on the screen when appropriate `DISPLAY_CASH(true);` disables cash amount rendering


## Syntax

```js
mp.game.ui.displayCash(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>