# Ui::setHeadDisplayWanted

displays wanted star above head


## Syntax

```js
mp.game.ui.setHeadDisplayWanted(headDisplayId, wantedlvl);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>headDisplayId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>wantedlvl:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>