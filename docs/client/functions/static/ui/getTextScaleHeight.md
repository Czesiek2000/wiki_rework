# Ui::getTextScaleHeight

## Syntax

```js
mp.game.ui.getTextScaleHeight(size, font);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>size:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>font:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>