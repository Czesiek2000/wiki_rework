# Ui::getStreetNameFromHashKey
This functions converts the hash of a street name into a readable string.For how to get the hashes, see PATHFIND::GET_STREET_NAME_AT_COORD.
 
To retrieve the street hashes go to: [wiki](https://wiki.rage.mp/index.php?title=Pathfind::getStreetNameAtCoord) or [this docs](../pathfind/getStreetNameAtCoord.md)


## Syntax

```js
mp.game.ui.getStreetNameFromHashKey(hash);
```

## Example

```js
// Clientside
const local = mp.players.local;
let getStreet = mp.game.pathfind.getStreetNameAtCoord(local.position.x, local.position.y, local.position.z, 0, 0);
// Returns obj {"streetName": hash, crossingRoad: hash}

let streetName = mp.game.ui.getStreetNameFromHashKey(getStreet.streetName); // Return string, if exist
let crossingRoad  = mp.game.ui.getStreetNameFromHashKey(getStreet.crossingRoad); // Return string, if exist
```


<br />


## Parameters

<li><b>hash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>