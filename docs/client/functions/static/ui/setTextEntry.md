# Ui::setTextEntry

The following were found in the decompiled script files:
* STRING,
* TWOSTRINGS,
* NUMBER,
* PERCENTAGE,
* FO_TWO_NUM,
* ESMINDOLLA,
* ESDOLLA,
* MTPHPER_XPNO,
* AHD_DIST,
* CMOD_STAT_0,
* CMOD_STAT_1,
* CMOD_STAT_2,
* CMOD_STAT_3,
* DFLT_MNU_OPT,
* F3A_TRAFDEST,
* ES_HELP_SOC3ESDOLLA - cash
* ESMINDOLLA - cash (negative)

Used to be known as `_SET_TEXT_ENTRY`


## Syntax

```js
mp.game.ui.setTextEntry(text);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>