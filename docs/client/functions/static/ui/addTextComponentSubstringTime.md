# Ui::addTextComponentSubstringTime

Adds a timer (e.g. '00:00:00:000'). The appearance of the timer depends on the flags, which needs more research.


## Syntax

```js
mp.game.ui.addTextComponentSubstringTime(timestamp, flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>timestamp:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>