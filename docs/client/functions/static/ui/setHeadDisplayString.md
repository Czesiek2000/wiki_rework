# Ui::setHeadDisplayString

## Syntax

```js
mp.game.ui.setHeadDisplayString(headDisplayId, string);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>headDisplayId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>string:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>