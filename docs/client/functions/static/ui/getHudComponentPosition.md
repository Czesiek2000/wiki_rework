# Ui::getHudComponentPosition
Use this to get subtitles position.
 

## Syntax

```js
mp.game.ui.getHudComponentPosition(componentIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : 

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />

Full list: [Hud componets](../../../../tables/hudCompoents.md)

<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>