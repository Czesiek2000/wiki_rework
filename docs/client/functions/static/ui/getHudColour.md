# Ui::getHudColour

HUD colors and their values [here](https://pastebin.com/d9aHPbXN)


## Syntax

```js
mp.game.ui.getHudColour(hudIndex, r, g, b, a);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hudIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>a:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> r, g, b, a</li>