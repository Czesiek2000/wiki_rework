# Ui::addBlipForPickup

## Syntax

```js
mp.game.ui.addBlipForPickup(pickup);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickup:</b> <b><span style="color:#008017">Pickup</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Blip</span></b></li>