# Ui::enableDeathbloodSeethrough

Please change back to `_0x4895BDEA16E7C080` (hash collision)


## Syntax

```js
mp.game.ui.enableDeathbloodSeethrough(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>