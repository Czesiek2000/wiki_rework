# Ui::displayHelpTextFromStringLabel

p0 is always 0.

duration when set to -1, is around 3s. Can not be more than 5000ms.

Example:

```cpp
void FloatingHelpText1(char* text) {
    BEGIN_TEXT_COMMAND_DISPLAY_HELP('STRING');
    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(text);
    END_TEXT_COMMAND_DISPLAY_HELP (0, 0, 1, -1);
}
```

[Image](https://imgbin.org/images/26209.jpg)

more inputs/icons: (scroll to line 377 and below)- [pastebin](https://pastebin.com/nqNYWMSB)

Used to be known as _DISPLAY_HELP_TEXT_FROM_STRING_LABEL
 

## Syntax

```js
mp.game.ui.displayHelpTextFromStringLabel(p0, loop, beep, duration);
```

## Example
```js
mp.game.ui.setTextComponentFormat('STRING');
mp.game.ui.addTextComponentSubstringPlayerName('Hit ~INPUT_CONTEXT~ to do something');
mp.game.ui.displayHelpTextFromStringLabel(0, true, true, -1);
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>loop:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>beep:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>