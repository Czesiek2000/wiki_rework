# Ui::addTextComponentSubstringPlayerName

� Description : Processes a string and removes the player name(max len 99) You can use this function to create notifications/subtitles

� Usage(Colors) : ~r~ = red ~y~ = yellow ~g~ = green ~b~ = light blue ~w~ = white ~p~ = purple ~n~ = new line

� Example (C++):
```cpp
void ShowNotification(char *text) { 
    UI::_SET_NOTIFICATION_TEXT_ENTRY('STRING');
    UI::ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(text);
    UI::_DRAW_NOTIFICATION(FALSE, FALSE); // if first param = 1, the message flashes 1 or 2 times
}
```
� Colors example : 

```cpp
string red = '~r~Red test';
string white_and_yellow = '~w~White and ~y~yellow';
string text_with_double_line = 'First line.~n~Second line';
```
You can also call fonts which are defined in: `common/data/ui/fontmap.xml`, along with 'html' style colours and font sizes, see example below

This native (along with `0x5F68520888E69014` and `0x94CF4AC034C9C986`) do not actually filter anything. 

They simply add the provided text (as of 944)
 

## Syntax

```js
mp.game.ui.addTextComponentSubstringPlayerName(text);
```

## Example

```js
mp.game.ui.addTextComponentSubstringPlayerName(`<font face="ChaletComprime-CologneSixty" size="16" color="rgb(123,213,123)">font</font> renders fine`);
```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>