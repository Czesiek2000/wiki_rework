# Ui::keyHudColour

hash collision


## Syntax

```js
mp.game.ui.keyHudColour(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p1:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>