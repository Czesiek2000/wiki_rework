# Ui::objectDecalToggle

Please change back to `_0x444D8CF241EC25C5` (hash collision)


## Syntax

```js
mp.game.ui.objectDecalToggle(hash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>