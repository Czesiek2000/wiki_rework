# Ui::setMinimapComponent

p2 appears to be always -1.

Map component IDs:

* 6 = "Vespucci Beach lifeguard building"
* 7 = "Beam Me Up (Grand Senora Desert)"
* 8 = "Paleto Bay fire station building"
* 9 = "Land Act Dam"
* 10 = "Paleto Forest cable car station"
* 11 = "Galileo Observatory"
* 12 = "Engine Rebuils building (Strawberry)"
* 13 = "Mansion pool (Richman)"
* 14 = "Beam Me Up (Grand Senora Desert) (2)"
* 15 = "Fort Zancudo"
 

## Syntax

```js
mp.game.ui.setMinimapComponent(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>