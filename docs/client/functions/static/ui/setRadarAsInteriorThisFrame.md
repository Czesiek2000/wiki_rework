# Ui::setRadarAsInteriorThisFrame

## Syntax

```js
mp.game.ui.setRadarAsInteriorThisFrame(interior, x, y, z, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interior:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>