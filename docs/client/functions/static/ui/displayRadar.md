# Ui::displayRadar

Function to set whether Minimap / Radar should be displayed.

## Syntax

```js
mp.game.ui.displayRadar(Toggle);
```

## Example

```js
mp.game.ui.displayRadar(true); // Display the Radar
mp.game.ui.displayRadar(false); // Hide the Radar
```


<br />


## Parameters

<li><b>Toggle:</b> <b><span style="color:#008017">bool</span></b> - <code>true</code> to display radar, otherwise <code>false</code>.</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Number</span></b></li>
