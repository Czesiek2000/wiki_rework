# Ui::setTextWrap

It sets the text in a specified box and wraps the text if it exceeds the boundries. Both values are for X axis. 

Useful when positioning text set to center or aligned to the right.

start - left boundry on screen position (0.0 - 1.0)

end - right boundry on screen position (0.0 - 1.0)


## Syntax

```js
mp.game.ui.setTextWrap(start, end);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>start:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>end:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>