# Ui::setNotificationTextEntry

Declares the entry type of a notification, for example 'STRING'.


## Syntax

```js
mp.game.ui.setNotificationTextEntry(type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>