# Ui::getTextSubstringSlice

Returns a substring that is between two specified positions. The length of the string will be calculated using (endPosition - startPosition).

Example:
```cpp
// Get 'STRING' text from 'MY_STRING'
subStr = UI::_GET_TEXT_SUBSTRING_SLICE('MY_STRING', 3, 9);
// Overflows are possibly replaced with underscores (needs verification)
subStr = UI::_GET_TEXT_SUBSTRING_SLICE('MY_STRING', 3, 10); // 'STRING_'?
```
 

## Syntax

```js
mp.game.ui.getTextSubstringSlice(text, startPosition, endPosition);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>startPosition:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>endPosition:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>