# Ui::addTextComponentSubstringWebsite

This native (along with `0x5F68520888E69014` and `0x6C188BE134E074AA`) do not actually filter anything. 

They simply add the provided text (as of 944)


## Syntax

```js
mp.game.ui.addTextComponentSubstringWebsite(website);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>website:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>