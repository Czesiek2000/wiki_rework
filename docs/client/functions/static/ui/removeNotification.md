# Ui::removeNotification

Removes a notification instantly instead of waiting for it to disappear


## Syntax

```js
mp.game.ui.removeNotification(notifactionId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>notifactionId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>