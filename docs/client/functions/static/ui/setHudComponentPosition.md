# Ui::setHudComponentPosition

Use this function to set subtitles position.

## Syntax

```js
mp.game.ui.setHudComponentPosition(componentIndex, x, y);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>

<br />

Full list: [Hud Components list](../../../../tables/hudCompoents.md)

<br />

## Return value

<li><b>Undefined</b></li>