# Ui::setRadarZoom

zoomLevel ranges from 0 to 200


## Syntax

```js
mp.game.ui.setRadarZoom(zoomLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>zoomLevel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>