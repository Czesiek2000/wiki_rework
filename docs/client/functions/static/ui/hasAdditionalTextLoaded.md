# Ui::hasAdditionalTextLoaded

## Syntax

```js
mp.game.ui.hasAdditionalTextLoaded(slot);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>slot:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>