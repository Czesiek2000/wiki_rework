# Ui::isPauseMenuActive

Determine if the front end pause menu is currently active. Ie. the options menu.


## Syntax

```js
mp.game.ui.isPauseMenuActive();
```

## Example

```js
let isPauseActive = mp.game.ui.isPauseMenuActive();

```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>true/false</b></li>
