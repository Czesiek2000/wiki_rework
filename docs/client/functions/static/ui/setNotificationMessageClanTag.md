# Ui::setNotificationMessageClanTag

picName1 & picName2 must match. 

Possibilities: 
* 'CHAR_DEFAULT', 
* 'CHAR_FACEBOOK', 
* 'CHAR_SOCIAL_CLUB'.

List of picNames [here](pastebin.com/XdpJVbHz)

flash is a bool for fading in.

iconTypes:
* 1: Chat Box
* 2: Email
* 3: Add Friend Request
* 4: Nothing
* 5: Nothing
* 6: Nothing
* 7: Right Jumping Arrow
* 8: RP Icon
* 9: $ Icon

'sender' is the very top header. This can be any old string.

'subject' is the header under the sender.

'duration' is a multiplier, so 1.0 is normal, 2.0 is twice as long (very slow), and 0.5 is half as long.

'clanTag' shows a crew tag in the 'sender' header, after the text. 

You need to use 3 underscores as padding. Maximum length of this field seems to be 7. (e.g. 'MK' becomes '___MK', 'ACE' becomes '___ACE', etc.)


## Syntax

```js
mp.game.ui.setNotificationMessageClanTag(picName1, picName2, flash, iconType, sender, subject, duration, clanTag);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>picName1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>picName2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>flash:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>iconType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>sender:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>subject:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>clanTag:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>