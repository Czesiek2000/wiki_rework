# Ui::setGpsFlags

*MulleDK19*: Only the script that originally called `SET_GPS_FLAGS` can set them again. 

Another script cannot set the flags, until the first script that called it has called `CLEAR_GPS_FLAGS`.

Doesn't seem like the flags are actually read by the game at all.


## Syntax

```js
mp.game.ui.setGpsFlags(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>