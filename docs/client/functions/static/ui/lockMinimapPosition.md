# Ui::lockMinimapPosition

Locks the minimap to the specified world position.


## Syntax

```js
mp.game.ui.lockMinimapPosition(x, y);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>