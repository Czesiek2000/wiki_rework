# Ui::setUseridsUihidden

Hash collision! Please change back to `_0xEF4CED81CEBEDC6D`


## Syntax

```js
mp.game.ui.setUseridsUihidden(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>