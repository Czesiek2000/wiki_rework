# Ui::lockMinimapAngle

Locks the minimap to the specified angle in integer degrees.

angle: The angle in whole degrees. 

If less than 0 or greater than 360, unlocks the angle.


## Syntax

```js
mp.game.ui.lockMinimapAngle(angle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>angle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>