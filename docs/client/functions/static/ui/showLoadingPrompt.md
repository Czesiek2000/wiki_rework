# Ui::showLoadingPrompt

This does NOT get called per frame. Call it once to show, then use `UI::_REMOVE_LOADING_PROMPT` to remove itChanges the the above native's (`UI::_SET_LOADING_PROMPT_TEXT_ENTRY`) spinning circle type.

Types:
```cpp
enum LoadingPromptTypes { 
    LOADING_PROMPT_LEFT, 
    LOADING_PROMPT_LEFT_2, 
    LOADING_PROMPT_LEFT_3, 
    SAVE_PROMPT_LEFT, 
    LOADING_PROMPT_RIGHT,
};
```
 

## Syntax

```js
mp.game.ui.showLoadingPrompt(busySpinnerType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>busySpinnerType:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>