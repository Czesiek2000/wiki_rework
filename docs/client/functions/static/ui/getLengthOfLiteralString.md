# Ui::getLengthOfLiteralString

Returns the length of the string passed (much like strlen).


## Syntax

```js
mp.game.ui.getLengthOfLiteralString(string);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>string:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>