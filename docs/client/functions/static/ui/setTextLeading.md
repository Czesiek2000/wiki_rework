# Ui::setTextLeading

from script am_mp_yacht.c 
```cpp
int?ui::set_text_leading(2);
```


## Syntax

```js
mp.game.ui.setTextLeading(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>