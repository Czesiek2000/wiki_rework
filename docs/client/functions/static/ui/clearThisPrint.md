# Ui::clearThisPrint

p0: found arguments in the b617d scripts: [pastebin](https://pastebin.com/X5akCN7z)


## Syntax

```js
mp.game.ui.clearThisPrint(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>