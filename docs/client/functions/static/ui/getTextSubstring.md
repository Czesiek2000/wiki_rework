# Ui::getTextSubstring

Returns a substring of a specified length starting at a specified position.

Example:
```cpp
// Get 'STRING' text from 'MY_STRING'
subStr = UI::_GET_TEXT_SUBSTRING('MY_STRING', 3, 6);
```


## Syntax

```js
mp.game.ui.getTextSubstring(text, position, length);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>position:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>length:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>