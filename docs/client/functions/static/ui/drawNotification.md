# Ui::drawNotification

Draws a notification above the map and returns the notifications handle

### Color syntax:
* ~r~ = Red
* ~b~ = Blue
* ~g~ = Green
* ~y~ = Yellow
* ~p~ = Purple
* ~o~ = Orange
* ~c~ = Grey
* ~m~ = Darker Grey
* ~u~ = Black
* ~n~ = New Line
* ~s~ = Default White
* ~w~ = White
* ~h~ = Bold Text
* ~nrt~ = ???

### Special characters:
* � = Rockstar Verified Icon (U+00A6:Broken Bar - Alt+0166)
* ? = Rockstar Icon (U+00F7:Division Sign - Alt+0247)
* ? = Rockstar Icon 2 (U+2211:N-Ary Summation)

Example C#: 
```csharp
Function.Call(Hash._ADD_TEXT_COMPONENT_STRING3, 'Now I need you to bring the ~b~vehicle~w~ back to me!');
```
 

## Syntax

```js
mp.game.ui.drawNotification(blink, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>blink:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>