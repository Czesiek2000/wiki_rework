# Ui::setTextDropshadow

distance - shadow distance in pixels, both horizontal and verticalr, g, b, a - color


## Syntax

```js
mp.game.ui.setTextDropshadow(distance, r, g, b, a);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>distance:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>a:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>