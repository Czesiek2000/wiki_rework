# Ui::setPlayerCashChange

Displays cash change notifications on HUD.


## Syntax

```js
mp.game.ui.setPlayerCashChange(cash, bank);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cash:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>bank:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>