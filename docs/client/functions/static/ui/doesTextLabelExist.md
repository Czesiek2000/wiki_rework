# Ui::doesTextLabelExist

Checks if the passed gxt name exists in the game files.


## Syntax

```js
mp.game.ui.doesTextLabelExist(gxt);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxt:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>