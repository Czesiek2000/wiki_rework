# Ui::setMinimapVisible

Toggles the minimap-map and the map(in the mainmenu) visibility, no map will be drawn anymore


## Syntax

```js
mp.game.ui.setMinimapVisible(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>