# Ui::setTextScale

## Syntax

```js
mp.game.ui.setTextScale(sizex, sizey);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sizex:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>sizey:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>