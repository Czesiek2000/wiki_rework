# Ui::addTrevorRandomModifier

Hash collision!

`_HAS_HEAD_DISPLAY_LOADED_2`


## Syntax

```js
mp.game.ui.addTrevorRandomModifier(headDisplayId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>headDisplayId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>