# Ui::setHeadDisplayFlag

```cpp

enum HeadDisplayFlag { 
    TextWithOutline = 0, 
    NoneEmpty, 
    HealthBar, 
    AudioSpeaker, 
    FlagOrPaused, 
    Flag, 
    PassiveMode, 
    WantedStar, 
    SteeringWheel, 
    Headset, 
    HighlightPlayer, 
    TextNoOutline, 
    ArrowDown, 
    BreifCase, 
    LittleUser, 
    RankNumber,
    VoiceIndicator
};
```


## Syntax

```js
mp.game.ui.setHeadDisplayFlag(headDisplayId, sprite, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>headDisplayId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>sprite:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>