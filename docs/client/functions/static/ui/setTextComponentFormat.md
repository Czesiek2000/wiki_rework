# Ui::setTextComponentFormat

Used to be known as `_SET_TEXT_COMPONENT_FORMAT`


## Syntax

```js
mp.game.ui.setTextComponentFormat(inputType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>inputType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>