# Ui::setWarningMessage

You can only use text entries. No custom text.

Example: 
```cpp
SET_WARNING_MESSAGE('t20', 3, 'adder', false, -1, 0, 0, true);
```


## Syntax

```js
mp.game.ui.setWarningMessage(entryLine1, instructionalKey, entryLine2, p3, p4, p5, p6, background);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>entryLine1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>instructionalKey:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>entryLine2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>background:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>