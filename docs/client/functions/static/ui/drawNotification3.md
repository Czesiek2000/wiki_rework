# Ui::drawNotification3

## Syntax

```js
mp.game.ui.drawNotification3(blink, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>blink:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>