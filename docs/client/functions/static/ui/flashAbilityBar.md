# Ui::flashAbilityBar

If set to true ability bar will flash


## Syntax

```js
mp.game.ui.flashAbilityBar(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>