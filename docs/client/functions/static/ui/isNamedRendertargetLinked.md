# Ui::isNamedRendertargetLinked

## Syntax

```js
mp.game.ui.isNamedRendertargetLinked(hash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>