# Ui::hideHudComponentThisFrame


 

 

## Syntax

```js
mp.game.ui.hideHudComponentThisFrame(componentIndex);
```

## Example

```js
//Disables the HUD
mp.game.ui.hideHudComponentThisFrame(0);
```


<br />


## Parameters

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>


<br />

## Return value
<li><b>Undefined</b></li>

<br />

### Components

<b>Full list: </b> [Hud Components list](../../../../tables/hudCompoents.md)
