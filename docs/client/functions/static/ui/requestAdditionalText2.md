# Ui::requestAdditionalText2

## Syntax

```js
mp.game.ui.requestAdditionalText2(gxt, slot);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxt:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>slot:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>