# Ui::setTextRenderId

## Syntax

```js
mp.game.ui.setTextRenderId(renderId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>renderId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>