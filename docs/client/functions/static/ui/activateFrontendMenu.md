# Ui::activateFrontendMenu

Does stuff like this [here](https://i.imgur.com/dPtwga3.png)

Example:
```cpp
int GetHash = GET_HASH_KEY('fe_menu_version_corona_lobby');
ACTIVATE_FRONTEND_MENU(GetHash, 0, -1);
```

BOOL p1 is a toggle to define the game in pause.

int p2 is unknown but -1 always works, not sure why though.
 

## Syntax

```js
mp.game.ui.activateFrontendMenu(menuhash, Toggle_Pause, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>menuhash:</b> Model hash or name</li>
<li><b>Toggle_Pause:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>