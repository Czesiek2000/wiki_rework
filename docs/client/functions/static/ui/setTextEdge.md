# Ui::setTextEdge

## Syntax

```js
mp.game.ui.setTextEdge(p0, r, g, b, a);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>a:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>