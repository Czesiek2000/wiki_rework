# Ui::hasThisAdditionalTextLoaded

Checks if the specified gxt has loaded into the passed slot.


## Syntax

```js
mp.game.ui.hasThisAdditionalTextLoaded(gxt, slot);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxt:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>slot:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>