# Ui::isNamedRendertargetRegistered

## Syntax

```js
mp.game.ui.isNamedRendertargetRegistered(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>