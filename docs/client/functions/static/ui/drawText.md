# Ui::drawText

After applying the properties to the text (See `UI::SET_TEXT_`), this will draw the text in the applied position. 

Also 0.0f < x, y < 1.0f, percentage of the axis.Used to be known as `_DRAW_TEXT`


## Syntax

```js
mp.game.ui.drawText(x, y);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>