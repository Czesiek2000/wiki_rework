# Ui::drawSubtitleTimed

Draws the subtitle at middle center of the screen.

`int duration = time` in milliseconds to show text on screen before disappearing

drawImmediately = If true, the text will be drawn immediately, if false, the text will be drawn after the previous subtitle has finishedUsed to be known as `_DRAW_SUBTITLE_TIMED`


## Syntax

```js
mp.game.ui.drawSubtitleTimed(time, drawImmediately);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>drawImmediately:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>