# Ui::setTextEntry2

Used to be known as `_SET_TEXT_ENTRY_2`


## Syntax

```js
mp.game.ui.setTextEntry2(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>