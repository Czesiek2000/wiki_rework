# Ui::addTextComponentItemString

## Syntax

```js
mp.game.ui.addTextComponentItemString(labelName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>labelName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>