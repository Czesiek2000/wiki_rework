# Ui::setRadarZoomLevelThisFrame

## Syntax

```js
mp.game.ui.setRadarZoomLevelThisFrame(zoomLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>zoomLevel:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>