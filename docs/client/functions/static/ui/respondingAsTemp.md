# Ui::respondingAsTemp

Please change back to `_0xBD12C5EEE184C33` (hash collision)

actual native starts with `SET_RADAR_ZOOM_`...


## Syntax

```js
mp.game.ui.respondingAsTemp(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>