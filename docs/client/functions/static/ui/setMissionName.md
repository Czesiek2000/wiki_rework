# Ui::setMissionName

## Syntax

```js
mp.game.ui.setMissionName(p0, name);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>