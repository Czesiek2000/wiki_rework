# Ui::showWeaponWheel

Forces the weapon wheel to appear on screen.


## Syntax

```js
mp.game.ui.showWeaponWheel(forcedShow);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>forcedShow:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>