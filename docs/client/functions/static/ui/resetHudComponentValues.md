# Ui::resetHudComponentValues

## Syntax

```js
mp.game.ui.resetHudComponentValues(componentIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />

Full list: [Hud Components list](../../../../tables/hudCompoents.md)

<br />

## Return value

<li><b>Undefined</b></li>
