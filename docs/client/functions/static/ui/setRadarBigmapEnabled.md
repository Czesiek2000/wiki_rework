# Ui::setRadarBigmapEnabled

Toggles the big minimap state like in GTA:Online.


## Syntax

```js
mp.game.ui.setRadarBigmapEnabled(toggleBigMap, showFullMap);
```

## Example

```js
mp.events.add('render', () => {
    if (mp.keys.isDown(0x47) === true) { // key 'G' is down
        mp.game.ui.setRadarBigmapEnabled(true, false);
    } else {
        mp.game.ui.setRadarBigmapEnabled(false, false);
    }
});
```


<br />


## Parameters

<li><b>toggleBigMap:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>showFullMap:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>