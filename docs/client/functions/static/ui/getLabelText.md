# Ui::getLabelText

Gets a string literal from a label name.


## Syntax

```js
mp.game.ui.getLabelText(labelName);
```

## Example

```js
function playerEnterVehicleHandler(vehicle) {{
    var vehicleName = mp.game.ui.getLabelText(mp.game.vehicle.getDisplayNameFromVehicleModel(vehicle.model));
    mp.gui.chat.push(`Name of your vehicle: ${vehicleName}`);
}

mp.events.add("playerEnterVehicle", playerEnterVehicleHandler);
```


<br />


## Parameters

<li><b>labelName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>