# Ui::setNotificationFlashColor

sets color for notification flash


## Syntax

```js
mp.game.ui.setNotificationFlashColor(red, green, blue, alpha);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>red:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>green:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>blue:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>