# Ui::setTextGxtEntry

get's line countint 
```cpp
GetLineCount(char *text, float x, float y) { 
    _SET_TEXT_ENTRY_FOR_LINE_COUNT('STRING'); 
    ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(text); 
    return _GET_TEXT_SCREEN_LINE_COUNT(x, y); 
}
```


## Syntax

```js
mp.game.ui.setTextGxtEntry(entry);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>entry:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>