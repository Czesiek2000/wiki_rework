# Ui::getTextScreenWidth

## Syntax

```js
mp.game.ui.getTextScreenWidth(p0);
```

## Example

```js
const getTextWidth = (text, font, scale) => {
    mp.game.ui.setTextEntryForWidth("STRING");
    mp.game.ui.addTextComponentSubstringPlayerName(text);
    mp.game.ui.setTextFont(font);
    mp.game.ui.setTextScale(scale * 1.25, scale);
    return mp.game.ui.getTextScreenWidth(true);
};
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>p0: <font color="blue">Boolean</font></b></li>

<br />
<br />

## Return value

<li><b><font color="green">float</font></b></li>