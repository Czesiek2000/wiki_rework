# Ui::setMinimapRevealed

*MulleDK19*: If true, the entire map will be revealed.


## Syntax

```js
mp.game.ui.setMinimapRevealed(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>