# Ui::displayAmmoThisFrame

## Syntax

```js
mp.game.ui.displayAmmoThisFrame(display);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>display:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>