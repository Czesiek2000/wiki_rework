# Ui::addBlipForRadius

## Syntax

```js
mp.game.ui.addBlipForRadius(posX, posY, posZ, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Blip</span></b></li>