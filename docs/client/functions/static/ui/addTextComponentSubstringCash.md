# Ui::addTextComponentSubstringCash

## Syntax

```js
mp.game.ui.addTextComponentSubstringCash(cashAmount, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cashAmount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>