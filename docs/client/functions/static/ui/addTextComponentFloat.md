# Ui::addTextComponentFloat

## Syntax

```js
mp.game.ui.addTextComponentFloat(value, decimalPlaces);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>decimalPlaces:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>