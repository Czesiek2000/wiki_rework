# Ui::linkNamedRendertarget

## Syntax

```js
mp.game.ui.linkNamedRendertarget(hash);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>hash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>