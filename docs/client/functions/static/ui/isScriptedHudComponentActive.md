# Ui::isScriptedHudComponentActive

## Syntax

```js
mp.game.ui.isScriptedHudComponentActive(componentIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentIndex:</b> <b><span style="color:#008017">Int</span></b></li>

Full list: [Hud Components list](../../../../tables/hudCompoents.md)

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>