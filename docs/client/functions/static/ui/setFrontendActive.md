# Ui::setFrontendActive

## Syntax

```js
mp.game.ui.setFrontendActive(active);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>active:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>