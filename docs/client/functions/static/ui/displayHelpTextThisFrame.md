# Ui::displayHelpTextThisFrame

The messages are localized strings.

Examples:
* 'No_bus_money'
* 'Enter_bus'
* 'Tour_help'
* 'LETTERS_HELP2'
* 'Dummy'

**The bool appears to always be false (if it even is a bool, as it's represented by a zero)**

p1 doesn't seem to make a difference, regardless of the state it's in.


## Syntax

```js
mp.game.ui.displayHelpTextThisFrame(message, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>message:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>