# Ui::setCursorSprite

Changes the mouse cursor's sprite.

* None = 0
* Normal = 1
* TransparentNormal = 2
* PreGrab = 3
* Grab = 4
* MiddleFinger = 5
* LeftArrow = 6
* RightArrow = 7
* UpArrow = 8
* DownArrow = 9
* HorizontalExpand = 10
* Add = 11
* Remove = 12,


## Syntax

```js
mp.game.ui.setCursorSprite(spriteId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>spriteId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>