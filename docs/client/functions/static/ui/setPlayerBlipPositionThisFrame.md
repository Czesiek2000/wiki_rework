# Ui::setPlayerBlipPositionThisFrame

Sets the position of the arrow icon representing the player on both the minimap and world map.

Too bad this wouldn't work over the network (obviously not). 

Could spoof where we would be.


## Syntax

```js
mp.game.ui.setPlayerBlipPositionThisFrame(x, y);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>