# Ui::beginTextCommandSetBlipName

example:
```cpp
UI::BEGIN_TEXT_COMMAND_SET_BLIP_NAME('STRING');
UI::_ADD_TEXT_COMPONENT_STRING('Name');
UI::END_TEXT_COMMAND_SET_BLIP_NAME(blip);
```


## Syntax

```js
mp.game.ui.beginTextCommandSetBlipName(gxtentry);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxtentry:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>