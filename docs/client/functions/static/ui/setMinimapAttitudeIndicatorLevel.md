# Ui::setMinimapAttitudeIndicatorLevel

Argument must be 0.0f or above 38.0f, or it will be ignored.


## Syntax

```js
mp.game.ui.setMinimapAttitudeIndicatorLevel(altitude, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>altitude:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>