# Ui::addTextComponentSubstringLocalized
Takes a Hash of an input (example : `INPUT_FRONTEND_RIGHT`) and displays it's name it when you display your notification above the map.^^ 

I don't know how somebody figured it prints the name of the input, when this native is clearly used for displaying Street Names in the scripts.

Can you give an example if it actually does print out the INPUTS?

*MulleDK19*: It does neither. It adds the localized text of the specified GXT entry name. Eg. if the argument is `GET_HASH_KEY('ES_HELP')`, adds 'Continue'. I've renamed it to `_ADD_TEXT_COMPONENT_SUBSTRING_LOCALIZED` to reflect this.

Zorg93: correct name found, just uses a text labels hash key


## Syntax

```js
mp.game.ui.addTextComponentSubstringLocalized(gxtEntryHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>gxtEntryHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>