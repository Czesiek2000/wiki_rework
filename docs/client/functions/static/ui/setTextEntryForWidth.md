# Ui::setTextEntryForWidth

Used for setting text entry and then getting the text width via `_GET_TEXT_SCREEN_WIDTH`.

Example:
```cpp
_SET_TEXT_ENTRY_FOR_WIDTH('NUMBER');
ADD_TEXT_COMPONENT_FLOAT(69.420f, 2);
FLOAT width = _GET_TEXT_SCREEN_WIDTH(1);// YOU CANNOT DRAW TEXT WITH THIS. USE _SET_TEXT_ENTRY FOR THAT
```


## Syntax

```js
mp.game.ui.setTextEntryForWidth(text);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>