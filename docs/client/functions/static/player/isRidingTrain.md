# Player::isRidingTrain

Returns true if the player is riding a train.
 

## Syntax

```js
player.isRidingTrain();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
