# Player::getGroup

Returns the group ID the player is member of.
 

## Syntax

```js
player.getGroup();
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value
