# Player::setWeaponDamageModifier

This modifies the damage value of your weapon. 

Whether it is a multiplier or base damage is unknown. 

Based on tests, it is unlikely to be a multiplier.
 

## Syntax

```js
player.setWeaponDamageModifier(damageAmount);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>damageAmount:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>