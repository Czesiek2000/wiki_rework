# Player::reserveEntityExplodesOnHighExplosionCombo

This was previously named as `RESERVE_ENTITY_EXPLODES_ON_HIGH_EXPLOSION_COMBO` which is obviously incorrect.

Seems to only appear in scripts used in Singleplayer. p1 ranges from 2 - 46.I assume this switches the crime type
 

## Syntax

```js
mp.game.player.reserveEntityExplodesOnHighExplosionCombo(p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>