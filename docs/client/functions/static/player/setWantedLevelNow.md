# Player::setWantedLevelNow

Forces any pending wanted level to be applied to the specified player immediately.

Call `SET_PLAYER_WANTED_LEVEL` with the desired wanted level, followed by `SET_PLAYER_WANTED_LEVEL_NOW`.

Second parameter is unknown (always false).
 

## Syntax

```js
player.setWantedLevelNow(p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>