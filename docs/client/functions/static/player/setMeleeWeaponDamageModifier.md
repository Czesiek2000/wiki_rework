# Player::setMeleeWeaponDamageModifier

## Syntax

```js
player.setMeleeWeaponDamageModifier(modifier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modifier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>