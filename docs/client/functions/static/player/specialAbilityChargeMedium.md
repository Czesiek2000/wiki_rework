# Player::specialAbilityChargeMedium

Only 1 match. Both p1 & p2 were true.
 

## Syntax

```js
mp.game.player.specialAbilityChargeMedium(p1p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>