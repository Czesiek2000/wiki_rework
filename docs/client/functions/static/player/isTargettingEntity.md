# Player::isTargettingEntity

## Syntax

```js
mp.game.player.isTargettingEntity(entity.handle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>entity:</b> Entity handle</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>