# Player::getUnderwaterTimeRemaining

## Syntax

```js
player.getUnderwaterTimeRemaining();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>
