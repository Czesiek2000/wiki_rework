# Player::setWantedLevelDifficulty

Max value is 1.0


## Syntax

```js
mp.game.player.setWantedLevelDifficulty(difficulty);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>difficulty:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>