# Player::setVehicleDamageModifier

## Syntax

```js
player.setVehicleDamageModifier(damageAmount);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>damageAmount:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>