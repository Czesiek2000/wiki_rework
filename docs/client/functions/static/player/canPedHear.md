# Player::canPedHear

## Syntax

```js
player.canPedHear(ped);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ped:</b> Ped handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>