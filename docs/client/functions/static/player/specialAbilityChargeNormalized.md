# Player::specialAbilityChargeNormalized

normalizedValue is from 0.0 - 1.0

p2 is always 1
 

## Syntax

```js
mp.game.player.specialAbilityChargeNormalized(normalizedValue, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>normalizedValue:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>