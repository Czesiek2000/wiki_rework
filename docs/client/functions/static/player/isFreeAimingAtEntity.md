# Player::isFreeAimingAtEntity

Gets a value indicating whether the specified player is currently aiming freely at the specified entity.
 

## Syntax

```js
mp.game.player.isFreeAimingAtEntity(entity.handle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>entity:</b> Entity handle</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
