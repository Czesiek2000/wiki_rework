# Player::setRunSprintMultiplierFor

Changes the speed a player runs with.

Multiplier goes up to 1.49 (looks like Sonic) any value above will be completely overruled by the game and the multiplier will not take effect, this can be edited in memory however.

Just call it one time, it is not required to be called once every tick.

**Note: At least the IDA method if you change the max float multiplier from 1.5 it will change it for both this and SWIM above. I say 1.5 as the function blrs if what you input is greater than or equal to 1.5 hence why it's 1.49 max default.**
 

## Syntax

```js
mp.game.player.setRunSprintMultiplierFor(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>
