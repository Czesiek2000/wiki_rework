# Player::giveAchievementToPlayer

Achievements from 0-57more achievements came with update 1.29 (freemode events update), I'd say that they now go to 60, but I'll need to check.
 

## Syntax

```js
mp.game.player.giveAchievementToPlayer(achievement);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>achievement:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>