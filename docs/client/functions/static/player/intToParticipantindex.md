# Player::intToParticipantindex

Simply returns whatever is passed to it (Regardless of whether the handle is valid or not).

```cpp
if (NETWORK::NETWORK_IS_PARTICIPANT_ACTIVE(PLAYER::INT_TO_PARTICIPANTINDEX(i)))
```
 

## Syntax

```js
mp.game.player.intToParticipantindex(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>