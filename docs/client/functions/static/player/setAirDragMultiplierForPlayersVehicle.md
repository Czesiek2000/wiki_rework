# Player::setAirDragMultiplierForPlayersVehicle

This can be between 1.0f - 14.9f You can change the max in IDA from 15.0. 

I say 15.0 as the function blrs if what you input is greater than or equal to 15.0 hence why it's 14.9 max default.
 

## Syntax

```js
mp.game.player.setAirDragMultiplierForPlayersVehicle(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>