# Player::getParachutePackTintIndex

## Syntax

```js
player.getParachutePackTintIndex(tintIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>tintIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>