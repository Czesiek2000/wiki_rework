# Player::hasLeftTheWorld

Gets the player's info and calls a function that checks the player's ped position.

Here's the decompiled function that checks the position [here](https://pastebin.com/ZdHG2E7n)
 

## Syntax

```js
player.hasLeftTheWorld();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
