# Player::getTimeSincePlayerDroveOnPavement

## Syntax

```js
mp.game.player.getTimeSincePlayerDroveOnPavement();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>
