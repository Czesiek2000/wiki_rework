# Player::restoreStamina

## Syntax

```js
player.restoreStamina(p1);
```

## Example

```js
mp.events.add('render', () => {
    mp.game.player.restoreStamina(100);
});
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>