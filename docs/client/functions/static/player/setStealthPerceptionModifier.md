# Player::setStealthPerceptionModifier

## Syntax

```js
player.setStealthPerceptionModifier(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>