# Player::setSneakingNoiseMultiplier

Values around 1.0f to 2.0f used in game scripts.


## Syntax

```js
player.setSneakingNoiseMultiplier(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>