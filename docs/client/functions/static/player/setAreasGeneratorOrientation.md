# Player::setAreasGeneratorOrientation

This is called after `SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME` hash collision
 

## Syntax

```js
mp.game.player.setAreasGeneratorOrientation();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>Undefined</b></li>
