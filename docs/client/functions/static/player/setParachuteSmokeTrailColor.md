# Player::setParachuteSmokeTrailColor

## Syntax

```js
player.setParachuteSmokeTrailColor(r, g, b);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>