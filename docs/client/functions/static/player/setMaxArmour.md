# Player::setMaxArmour

Default is 100. Use player id and not ped id. 

For instance: 

```cpp
PLAYER::SET_PLAYER_MAX_ARMOUR(PLAYER::PLAYER_ID(), 100); // main_persistent.ct4
```
 

## Syntax

```js
player.setMaxArmour(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>