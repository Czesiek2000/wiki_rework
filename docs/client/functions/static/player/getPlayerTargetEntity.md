# Player::getPlayerTargetEntity

Assigns the handle of locked-on melee target to *entity that you pass it.

Returns false if no entity found.
 

## Syntax

```js
mp.game.player.getPlayerTargetEntity(entity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>entity:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>