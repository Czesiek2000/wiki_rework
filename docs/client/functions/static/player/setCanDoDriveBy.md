# Player::setCanDoDriveBy

Set whether this player should be able to do drive-bys.

A drive-by is when a ped is aiming/shooting from vehicle. This includes middle finger taunts. 

By setting this value to false I confirm the player is unable to do all that. Tested on tick.

*THEAETIK*
 

## Syntax

```js
player.setCanDoDriveBy(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>