# Player::isFreeAiming

Gets a value indicating whether the specified player is currently aiming freely.
 
Outputs the current "FreeAiming" state into the chatbox
 

## Syntax

```js
mp.game.player.isFreeAiming();
```

## Example

```js
let freeAiming = mp.game.player.isFreeAiming();
mp.gui.chat.push("Freeaiming is " + (freeAiming ? "active" : "inactive"))
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
