# Player::hasBeenSpottedInStolenVehicle

## Syntax

```js
player.hasBeenSpottedInStolenVehicle();
```

## Example

```js
// todo
```


<br />


## Parameters

No paramters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
