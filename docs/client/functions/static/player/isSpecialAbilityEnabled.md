# Player::isSpecialAbilityEnabled

## Syntax

```js
mp.game.player.isSpecialAbilityEnabled();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
