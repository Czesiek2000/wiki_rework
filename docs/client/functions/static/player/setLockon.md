# Player::setLockon

Example from fm_mission_controler.ysc.c4:

`PLAYER::SET_PLAYER_LOCKON(PLAYER::PLAYER_ID(), 1);`

All other decompiled scripts using this seem to be using the player id as the first parameter, so I feel the need to confirm it as so.
 

## Syntax

```js
player.setLockon(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>