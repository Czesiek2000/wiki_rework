# Player::setSpecialAbilityMultiplier

## Syntax

```js
mp.game.player.setSpecialAbilityMultiplier(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>