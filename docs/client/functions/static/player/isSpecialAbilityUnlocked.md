# Player::isSpecialAbilityUnlocked

## Syntax

```js
mp.game.player.isSpecialAbilityUnlocked(playerModel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>playerModel:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>