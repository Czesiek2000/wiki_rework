# Player::getSprintTimeRemaining

Returns the sprint time remaining. It steadily decreases during sprinting and refreshes when you're not sprinting.
 

## Syntax

```js
player.getSprintTimeRemaining();
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value
