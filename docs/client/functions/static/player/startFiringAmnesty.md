# Player::startFiringAmnesty

## Syntax

```js
mp.game.player.startFiringAmnesty(duration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>