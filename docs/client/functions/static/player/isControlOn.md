# Player::isControlOn

Can the player control himself, used to disable controls for player for things like a cutscene.

You can't disable controls with this, use `SET_PLAYER_CONTROL(...)` for this.
 

## Syntax

```js
player.isControlOn();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
