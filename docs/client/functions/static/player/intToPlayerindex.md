# Player::intToPlayerindex

Simply returns whatever is passed to it (Regardless of whether the handle is valid or not).
 

## Syntax

```js
mp.game.player.intToPlayerindex(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Player</span></b></li>