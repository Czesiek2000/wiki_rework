# Player::setCanBeHassledByGangs

Sets whether this player can be hassled by gangs.


## Syntax

```js
player.setCanBeHassledByGangs(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>