# Player::setPlayerClothPackageIndex

Every occurrence was either 0 or 2.
 

## Syntax

```js
mp.game.player.setPlayerClothPackageIndex(index);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>index:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>