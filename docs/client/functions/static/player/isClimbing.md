# Player::isClimbing

This property returns true or false of player climbing state.
 
**Note: this property is read-only.**
 

## Syntax

```js
let playerIsClimbing = player.isClimbing
if (playerIsClimbing)
  player.outputChatBox('You are climbing right now!');
else
  player.outputChatBox('You are not climbing right now!');
```

## Example

```js
undefined
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

No return value here
