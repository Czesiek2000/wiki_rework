# Player::forceCleanupForThreadWithThisId

## Syntax

```js
mp.game.player.forceCleanupForThreadWithThisId(id, cleanupFlags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>id:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>cleanupFlags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>