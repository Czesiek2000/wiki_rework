# Player::setMayOnlyEnterThisVehicle

## Syntax

```js
player.setMayOnlyEnterThisVehicle(vehicle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicle:</b> Vehicle handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>