# Player::setPlayerTargetingMode

I don't know which number is which mode I'm sure it can easily be found with testing, but scripts showed it's not player. 

They ask a function which returns this Global (Global_2404048) on in the Xbox360 scripts, then sets the param to either, (0, 1, 2, or 3).
 
0 = Traditional GTA

1 = Assisted Aiming

2 = Free Aim
 

## Syntax

```js
mp.game.player.setTargetingMode(targetMode);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>targetMode:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>