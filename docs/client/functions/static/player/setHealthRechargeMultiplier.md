# Player::setHealthRechargeMultiplier

*NOTICE: After changing the local model (mp.players.local.model), this function must be called again*

This function has to be called once per render.
 

## Syntax

```js
mp.game.player.setHealthRechargeMultiplier(regenRate);
```

## Example

```js
mp.events.add('render', () => {

    // remove health regeneration
    mp.game.player.setHealthRechargeMultiplier(0.0);

    // set health regeneration to default
    mp.game.player.setHealthRechargeMultiplier(1.0);

});
```


<br />


## Parameters

<li><b>regenRate:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>