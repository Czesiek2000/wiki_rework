# Player::setControl

Flags used in the scripts: 0,4,16,24,32,56,60,64,128,134,256,260,384,512,640,768,896,900,952,1024,1280,2048,2560

Note to people who needs this with camera mods, etc.: 

Flags(0, 4, 16, 24, 32, 56, 60, 64, 128, 134, 512, 640, 1024, 2048, 2560)

- Disables camera rotation as well. Flags(256, 260, 384, 768, 896, 900, 952, 1280)

- Allows camera rotation.
 

## Syntax

```js
mp.game.player.setControl(toggle, possiblyFlags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>possiblyFlags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>