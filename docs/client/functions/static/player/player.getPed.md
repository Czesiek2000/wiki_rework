# Mp.game.player.getPed

## Syntax

```js
mp.game.player.getPed();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>Ped handle</b></li>
