# Player::isWantedLevelGreater

## Syntax

```js
player.isWantedLevelGreater(wantedLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>wantedLevel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>