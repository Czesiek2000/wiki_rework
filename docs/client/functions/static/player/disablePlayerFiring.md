# Player::disablePlayerFiring

Inhibits the player from using any method of combat including melee and firearms.

**NOTE: Only disables the firing for one frame**
 

## Syntax

```js
mp.game.player.disableFiring(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>