# Player::simulatePlayerInputGait

This is to make the player walk without accepting input from INPUT.

gaitType is in increments of 100s. 2000, 500, 300, 200, etc.

p4 is always 1 and p5 is always 0.

C# Example:

```cpp
Function.Call(Hash.SIMULATE_PLAYER_INPUT_GAIT, Game.Player, 1.0f, 100, 1.0f, 1, 0); //Player will go forward for 100ms
```
 

## Syntax

```js
mp.game.player.simulatePlayerInputGait(speed, time, heading, headingRelativeToPed, notCancellable);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>time:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>headingRelativeToPed:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>notCancellable:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>