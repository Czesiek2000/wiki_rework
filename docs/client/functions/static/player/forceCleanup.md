# Player::forceCleanup

used with 1,2,8,64,128 in the scripts


## Syntax

```js
mp.game.player.forceCleanup(cleanupFlags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cleanupFlags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>