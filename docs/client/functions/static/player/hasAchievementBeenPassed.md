# Player::hasAchievementBeenPassed

## Syntax

```js
mp.game.player.hasAchievementBeenPassed(achievement);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>achievement:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>