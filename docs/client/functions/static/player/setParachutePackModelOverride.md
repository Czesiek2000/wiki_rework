# Player::setParachutePackModelOverride

## Syntax

```js
player.setParachutePackModelOverride(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>