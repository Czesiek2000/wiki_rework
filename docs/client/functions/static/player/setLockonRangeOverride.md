# Player::setLockonRangeOverride

Affects the range of auto aim target.


## Syntax

```js
player.setLockonRangeOverride(range);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>