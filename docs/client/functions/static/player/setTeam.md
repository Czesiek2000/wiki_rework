# Player::setTeam

Set player team on deathmatch and last team standing..


## Syntax

```js
player.setTeam(team);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>team:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>