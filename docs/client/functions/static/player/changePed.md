# Player::changePed

b2 and/or b3 maybe got something to do with keeping values from the last player. Both of them set to 1 works great. 

Examples from the decompiled scripts:
```cpp
PLAYER::CHANGE_PLAYER_PED(PLAYER::PLAYER_ID(), l_5C0[4/*14*/], 0, 1);
PLAYER::CHANGE_PLAYER_PED(PLAYER::PLAYER_ID(), a_0[a_0._f7/*1*/], a_2, 0);
```
The only way I ever got this to work in GTA Online once is by setting both to 0, 0. However, when you switch from your online character to whomever, your character will start walking away 'as if you left the game.' 

If from there you attempt to call this native once more to switch back to you online player. You will freeze or if you try changing to another player. 

I've tried all posibilities so far.1, 1 (Freeze), 0, 0(Works Once), 1, 0 & 0, 1 (Freeze). 

Note of course trying to call this on another online player will crash. Anyone have any idea if implementing a blr within the xex itself on a possible check if it would prevent this freezing?
 

## Syntax

```js
player.changePed(ped, b2, b3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ped:</b> Ped handle or object</li>
<li><b>b2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>b3:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>