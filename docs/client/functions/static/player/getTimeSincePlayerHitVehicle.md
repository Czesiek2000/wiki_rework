# Player::getTimeSincePlayerHitVehicle

## Syntax

```js
mp.game.player.getTimeSincePlayerHitVehicle();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>
