# Player::getWantedCentrePosition

## Syntax

```js
player.getWantedCentrePosition();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>
