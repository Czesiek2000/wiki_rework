# Player::setInvincible

Simply sets you as invincible (Health will not deplete).

Use `0x733A643B5B0C53C1` instead if you want Ragdoll enabled, which is equal to: `*(DWORD *)(playerPedAddress + 0x188) |= (1 << 9);`
 

## Syntax

```js
player.setInvincible(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>