# Player::getTimeSincePlayerDroveAgainstTraffic

## Syntax

```js
mp.game.player.getTimeSincePlayerDroveAgainstTraffic();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>
