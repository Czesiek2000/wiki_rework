# Player::setPlayerClothLockCounter

6 matches across 4 scripts.

5 occurrences were 240.

The other was 255.
 

## Syntax

```js
mp.game.player.setPlayerClothLockCounter(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>