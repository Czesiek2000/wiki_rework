# Player::specialAbilityChargeAbsolute

p1 appears as 5, 10, 15, 25, or 30. 

p2 is always true.
 

## Syntax

```js
mp.game.player.specialAbilityChargeAbsolute(p1p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>