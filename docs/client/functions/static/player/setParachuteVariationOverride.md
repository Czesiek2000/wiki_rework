# Player::setParachuteVariationOverride

p1 was always 5.

p4 was always false.
 

## Syntax

```js
player.setParachuteVariationOverride(p1, p2, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>