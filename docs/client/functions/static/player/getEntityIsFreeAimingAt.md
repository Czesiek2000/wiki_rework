# Player::getEntityIsFreeAimingAt

Returns RAGE:MP Object or World-Object Handle if it found an entity in your crosshair within range of your weapon.

Returns undefined if no entity found.


## Syntax

```js
mp.game.player.getEntityIsFreeAimingAt();
```

## Example

```js
// todo
```


<br />


## Parameters

<ul><li><b>entity: Entity object (RAGE:MP Objects)</b></li></ul>

<p>or</p>

<ul><li><b>int: Entity handle (World-Objects)</b></li></ul>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span>: Entity handle (World-Objects)</b></li>