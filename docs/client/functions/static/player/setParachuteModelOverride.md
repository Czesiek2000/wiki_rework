# Player::setParachuteModelOverride

example:

```cpp
PLAYER::SET_PLAYER_PARACHUTE_MODEL_OVERRIDE(PLAYER::PLAYER_ID(), 0x73268708);
```
 

## Syntax

```js
player.setParachuteModelOverride(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>