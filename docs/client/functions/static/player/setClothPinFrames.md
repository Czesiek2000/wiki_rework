# Player::setClothPinFrames

Every occurrence of p1 I found was true.

1.0.335.2, 1.0.350.1/2, 1.0.372.2, 1.0.393.2, 1.0.393.4, 1.0.463.1;
 

## Syntax

```js
player.setClothPinFrames(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>