# Player::getInvincible

Returns the Player's Invincible status.

This function will always return false if `0x733A643B5B0C53C1` is used to set the invincibility status. 

To always get the correct result, use this: 
```cpp
bool IsPlayerInvincible(Player player) {
    auto addr = getScriptHandleBaseAddress(GET_PLAYER_PED(player));	 
    if (addr) { 
        DWORD flag = *(DWORD *)(addr + 0x188); 
        return ((flag & (1 << 8)) != 0) || ((flag & (1 << 9)) != 0); 
    } 
    return false;
}
```
 

## Syntax

```js
player.getInvincible();
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value
