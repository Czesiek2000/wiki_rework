# Player::forceCleanupForAllThreadsWithThisName

```cpp
PLAYER::FORCE_CLEANUP_FOR_ALL_THREADS_WITH_THIS_NAME('pb_prostitute', 1); // Found in decompilation
```
 

## Syntax

```js
mp.game.player.forceCleanupForAllThreadsWithThisName(name, cleanupFlags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>cleanupFlags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>