# Player::getTeam

Gets the player's team.

Does nothing in singleplayer.


## Syntax

```js
mp.game.player.getTeam();
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value
