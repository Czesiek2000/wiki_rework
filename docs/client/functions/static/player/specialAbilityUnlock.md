# Player::specialAbilityUnlock

## Syntax

```js
mp.game.player.specialAbilityUnlock(playerModel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>playerModel:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>