# Player::getName

## Syntax

```js
player.getName();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>
