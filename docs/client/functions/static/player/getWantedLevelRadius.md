# Player::getWantedLevelRadius

Remnant from GTA IV. Does nothing in GTA V.
 

## Syntax

```js
mp.game.player.getWantedLevelRadius();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>
