# Player::specialAbilityDeactivate

## Syntax

```js
mp.game.player.specialAbilityDeactivate();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>Undefined</b></li>
