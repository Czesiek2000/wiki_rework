# Player::setCanLeaveParachuteSmokeTrail

## Syntax

```js
player.setCanLeaveParachuteSmokeTrail(enabled);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>enabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>