# Player::specialAbilityChargeContinuous

p1 appears to always be 1 (only comes up twice)


## Syntax

```js
mp.game.player.specialAbilityChargeContinuous(p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>