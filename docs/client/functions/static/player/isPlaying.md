# Player::isPlaying

Checks whether the specified player has a Ped, the Ped is not dead, is not injured and is not arrested.
 

## Syntax

```js
player.isPlaying();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>
