# Player::setReserveParachuteTintIndex

Tints:
* None = -1
* Rainbow = 0
* Red = 1
* SeasideStripes = 2
* WidowMaker = 3
* Patriot = 4
* Blue = 5
* Black = 6
* Hornet = 7
* AirFocce = 8
* Desert = 9
* Shadow = 10
* HighAltitude = 11
* Airbone = 12
* Sunrise = 13


## Syntax

```js
player.setReserveParachuteTintIndex(index);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>index:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>