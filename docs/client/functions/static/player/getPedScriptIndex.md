# Player::getPedScriptIndex

Same as GET_PLAYER_player.
 

## Syntax

```js
player.getPedScriptIndex();
```

## Example

```js
// todo
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>
