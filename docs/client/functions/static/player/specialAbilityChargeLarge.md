# Player::specialAbilityChargeLarge

2 matches. p1 was always true.


## Syntax

```js
mp.game.player.specialAbilityChargeLarge(p1p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>