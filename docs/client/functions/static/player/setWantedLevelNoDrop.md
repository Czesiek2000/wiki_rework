# Player::setWantedLevelNoDrop

p2 is always false in R* scripts


## Syntax

```js
player.setWantedLevelNoDrop(wantedLevel, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>wantedLevel:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>