# Player::setResetFlagPreferRearSeats

example:

flags: 0-6 `PLAYER::SET_PLAYER_RESET_FLAG_PREFER_REAR_SEATS(PLAYER::PLAYER_ID(), 6);` wouldn't the flag be the seatIndex?
 

## Syntax

```js
player.setResetFlagPreferRearSeats(flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>