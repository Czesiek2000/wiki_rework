# Player::enableSpecialAbility

## Syntax

```js
mp.game.player.enableSpecialAbility(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>