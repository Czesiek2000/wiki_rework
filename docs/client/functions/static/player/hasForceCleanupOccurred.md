# Player::hasForceCleanupOccurred

## Syntax

```js
mp.game.player.hasForceCleanupOccurred(cleanupFlags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cleanupFlags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>