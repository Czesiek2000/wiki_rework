# Player::setMaxWantedLevel

## Syntax

```js
mp.game.player.setMaxWantedLevel(maxWantedLevel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>maxWantedLevel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>