# Player::disableVehicleRewards

## Syntax

```js
player.disableVehicleRewards();
```

## Example

```js
mp.events.add('playerReady', () => {
    // Disable the rewards given when entering vehicles
    mp.game.player.disableVehicleRewards();
});
```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

<li><b>Undefined</b></li>
