# Brain::registerObjectScriptBrain
Registers a script for any object with a specific model hash.BRAIN::REGISTER_OBJECT_SCRIPT_BRAIN('ob_telescope', ${prop_telescope_01}, 100, 4.0, -1, 9);- Nacorpio
 

## Syntax

```js
mp.game.brain.registerObjectScriptBrain(scriptName, p1, p2, p3, p4, p5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> Model hash or name</li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>