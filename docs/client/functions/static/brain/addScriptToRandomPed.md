# Brain::addScriptToRandomPed
BRAIN::ADD_SCRIPT_TO_RANDOM_PED('pb_prostitute', ${s_f_y_hooker_01}, 100, 0);- Nacorpio
 

## Syntax

```js
mp.game.brain.addScriptToRandomPed(name, model, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>model:</b> Model hash or name</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>