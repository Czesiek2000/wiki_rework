# Brain::isObjectWithinBrainActivationRange

## Syntax

```js
mp.game.brain.isObjectWithinBrainActivationRange(object);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>object:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>