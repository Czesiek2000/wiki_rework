# Brain::disableScriptBrainSet

## Syntax

```js
mp.game.brain.disableScriptBrainSet(brainSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>brainSet:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>