# Brain::registerWorldPointScriptBrain

## Syntax

```js
mp.game.brain.registerWorldPointScriptBrain(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>