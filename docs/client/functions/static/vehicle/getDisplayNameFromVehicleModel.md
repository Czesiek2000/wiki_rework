# Vehicle::getDisplayNameFromVehicleModel
Returns model name of vehicle in all caps. Needs to be displayed through localizing text natives to get proper display name.

While often the case, this does not simply return the model name of the vehicle (which could be hashed to return the model hash). Variations of the same vehicle may also use the same display name.

Returns 'CARNOTFOUND' if the hash doesn't match a vehicle hash.

Using `UI::_GET_LABEL_TEXT`, you can get the localized name.


## Syntax

```js
mp.game.vehicle.getDisplayNameFromVehicleModel(modelHash);
```

## Example

```js
function playerEnterVehicleHandler(vehicle) {
    var vehicleName = mp.game.ui.getLabelText(mp.game.vehicle.getDisplayNameFromVehicleModel(vehicle.model));
    mp.gui.chat.push(`Name of your vehicle: ${vehicleName}`);
}

mp.events.add("playerEnterVehicle", playerEnterVehicleHandler);
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>