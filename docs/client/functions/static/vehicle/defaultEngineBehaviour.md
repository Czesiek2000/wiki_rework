# Vehicle::defaultEngineBehaviour

Lets you disable the default engine behaviour (when you enter a vehicle, the player automatically turns on the engine). Setting this to false will stop the player from turning the engine on.

<b>Note:</b><br>

It is best to set the config flag: 
[PED_FLAG_STOP_ENGINE_TURNING](../../../../tables/playerConfigFlags.md) - to ensure the player doesn't attempt to turn the engine resulting in a strange animation.

```js
mp.players.local.setConfigFlag(429, true);
```

## Syntax

```js
mp.game.vehicle.defaultEngineBehaviour = flag;
```

## Example

Simply disables people from turning on their engine within the server.

```js
mp.game.vehicle.defaultEngineBehaviour = false;
```


<br />


## Parameters

<li><b>flag</b>: <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

No return value
