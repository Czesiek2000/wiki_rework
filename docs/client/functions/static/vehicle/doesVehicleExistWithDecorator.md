# Vehicle::doesVehicleExistWithDecorator

## Syntax

```js
mp.game.vehicle.doesVehicleExistWithDecorator(decorator);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>decorator:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>