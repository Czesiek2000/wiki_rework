# Vehicle::displayDistantVehicles

Toggles to render distant vehicles. They may not be vehicles but images to look like vehicles.


## Syntax

```js
mp.game.vehicle.displayDistantVehicles(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>