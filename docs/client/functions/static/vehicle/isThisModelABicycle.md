# Vehicle::isThisModelABicycle

## Syntax

```js
mp.game.vehicle.isThisModelABicycle(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>