# Vehicle::createVehicle

p6 - last parameter does not mean vehicle handle is returned 

maybe a quick view in disassembly will tell us what is actually does

p6 seems to check for something with the script in the disassembly


## Syntax

```js
mp.game.vehicle.createVehicle(modelHash, x, y, z, heading, networkHandle, vehiclehandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>networkHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>vehiclehandle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>