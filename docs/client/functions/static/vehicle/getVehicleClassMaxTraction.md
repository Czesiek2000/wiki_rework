# Vehicle::getVehicleClassMaxTraction

## Syntax

```js
mp.game.vehicle.getVehicleClassMaxTraction(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>