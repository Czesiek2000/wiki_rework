# Vehicle::setScriptVehicleGenerator

Only called once in the decompiled scripts. Presumably activates the specified generator.


## Syntax

```js
mp.game.vehicle.setScriptVehicleGenerator(vehicleGenerator, enabled);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleGenerator:</b> unknown (to be checked)</li>
<li><b>enabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>