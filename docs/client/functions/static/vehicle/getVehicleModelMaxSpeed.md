# Vehicle::getVehicleModelMaxSpeed

## Syntax

```js
mp.game.vehicle.getVehicleModelMaxSpeed(modelHash);
```

## Example

```js
mp.events.add('render', () => {
    let maxs = mp.game.vehicle.getVehicleModelMaxSpeed(mp.players.local.vehicle.model); //max speed of vehicle 
    let currents = mp.players.local.vehicle.getSpeed(); // Current Speed
    mp.game.graphics.drawText(`Speed: ${(currents * 3.6).toFixed(0)}/ ${((maxs * 3.6).toFixed(0))} km/h`, [0.5, 0.005], {  
        font: 7, 
        color: [255, 255, 255, 185], 
        scale: [0.5, 0.5], 
        outline: false
 });
}); // Displays on the top of the screen Speed: CurrentSpeed/Max speed in km/h
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>