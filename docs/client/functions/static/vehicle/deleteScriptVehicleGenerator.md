# Vehicle::deleteScriptVehicleGenerator

## Syntax

```js
mp.game.vehicle.deleteScriptVehicleGenerator(vehicleGenerator);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleGenerator:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>