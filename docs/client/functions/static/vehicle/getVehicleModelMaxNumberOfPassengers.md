# Vehicle::getVehicleModelMaxNumberOfPassengers

Returns max number of passengers (including the driver) for the specified vehicle model.

For a full list, see [here](https://pastebin.com/MdETCS1j)


## Syntax

```js
mp.game.vehicle.getVehicleModelMaxNumberOfPassengers(modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>