# Vehicle::requestVehicleAsset
`REQUEST_VEHICLE_ASSET(GET_HASH_KEY(cargobob3), 3);`

vehicle found that have asset's:

* cargobob3
* submersible
* blazer


## Syntax

```js
mp.game.vehicle.requestVehicleAsset(vehicleHash, vehicleAsset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleHash:</b> Model hash or name</li>
<li><b>vehicleAsset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>