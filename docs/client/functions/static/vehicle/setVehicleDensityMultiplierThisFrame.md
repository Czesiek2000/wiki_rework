# Vehicle::setVehicleDensityMultiplierThisFrame

� Usage

<p>Use this native inside a looped function.</p>

Values: 

<p>> 0.0 = no vehicles on streets</p>
<p>> 1.0 = normal vehicles on streets</p>


## Syntax

```js
mp.game.vehicle.setVehicleDensityMultiplierThisFrame(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>