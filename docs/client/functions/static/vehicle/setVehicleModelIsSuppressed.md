# Vehicle::setVehicleModelIsSuppressed

seems to make the vehicle stop spawning naturally in traffic. 

Here's an essential example:
```cpp
VEHICLE::SET_VEHICLE_MODEL_IS_SUPPRESSED(GAMEPLAY::GET_HASH_KEY('taco'), true);
```
god I hate taco vansConfirmed to work? Needs to be looped? Can not get it to work.


## Syntax

```js
mp.game.vehicle.setVehicleModelIsSuppressed(model, suppressed);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>
<li><b>suppressed:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>