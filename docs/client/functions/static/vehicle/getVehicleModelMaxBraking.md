# Vehicle::getVehicleModelMaxBraking

Returns max braking of the specified vehicle model.

For a full list, see [here](https://pastebin.com/3N8DVbpG)


## Syntax

```js
mp.game.vehicle.getVehicleModelMaxBraking(modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>