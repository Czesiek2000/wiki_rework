# Vehicle::doesScriptVehicleGeneratorExist

## Syntax

```js
mp.game.vehicle.doesScriptVehicleGeneratorExist(vehicleGenerator);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleGenerator:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>