# Vehicle::getRandomVehicleInSphere


Gets a random vehicle in a sphere at the specified position, of the specified radius.

x: The X-component of the position of the sphere.

y: The Y-component of the position of the sphere.

z: The Z-component of the position of the sphere.

radius: The radius of the sphere. Max is 9999.9004.

modelHash: The vehicle model to limit the selection to. Pass 0 for any model.

flags: The bitwise flags that modifies the behaviour of this function.


## Syntax

```js
mp.game.vehicle.getRandomVehicleInSphere(x, y, z, radius, modelHash, flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Vehicle handle or object</b></li>