# Vehicle::getTimePositionInRecording

## Syntax

```js
mp.game.vehicle.getTimePositionInRecording(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>