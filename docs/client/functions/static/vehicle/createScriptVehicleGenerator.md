# Vehicle::createScriptVehicleGenerator
Creates a script vehicle generator at the given coordinates. Most parameters after the model hash are unknown.

Parameters:

x/y/z - Generator position

heading - Generator heading

p4 - Unknown (always 5.0)

p5 - Unknown (always 3.0)

modelHash - Vehicle model hash

p7/8/9/10 - Unknown (always -1)

p11 - Unknown (usually TRUE, only one instance of FALSE)

p12/13 - Unknown (always FALSE)

p14 - Unknown (usally FALSE, only two instances of TRUE)

p15 - Unknown (always TRUE)

p16 - Unknown (always -1)

```cpp
Vector3 coords = GET_ENTITY_COORDS(PLAYER_PED_ID(), 0);	
CREATE_SCRIPT_VEHICLE_GENERATOR(coords.x, coords.y, coords.z, 1.0f, 5.0f, 3.0f, GET_HASH_KEY('adder'), -1. -1, -1, -1, -1, true, false, false, false, true, -1);
```
 

## Syntax

```js
mp.game.vehicle.createScriptVehicleGenerator(x, y, z, heading, p4, p5, modelHash, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>p7:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p10:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p11:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p12:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p13:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p14:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p15:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p16:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>