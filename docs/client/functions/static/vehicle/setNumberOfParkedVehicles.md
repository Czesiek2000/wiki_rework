# Vehicle::setNumberOfParkedVehicles

## Syntax

```js
mp.game.vehicle.setNumberOfParkedVehicles(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>