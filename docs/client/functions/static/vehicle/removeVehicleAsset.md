# Vehicle::removeVehicleAsset

## Syntax

```js
mp.game.vehicle.removeVehicleAsset(vehicleAsset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleAsset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>