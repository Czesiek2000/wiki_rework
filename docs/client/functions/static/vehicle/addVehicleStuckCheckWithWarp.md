# Vehicle::addVehicleStuckCheckWithWarp

## Syntax

```js
mp.game.vehicle.addVehicleStuckCheckWithWarp(p0, p1, p2, p3, p4, p5, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> <b><span style="color:#008017">Boolan</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolan</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolan</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>