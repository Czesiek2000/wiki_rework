# Vehicle::disableVehicleWeapon

## Syntax

```js
mp.game.vehicle.disableVehicleWeapon(disabled, weaponHash, vehicle, owner);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>disabled:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>weaponHash:</b> Model hash or name</li>
<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>owner:</b> Ped handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>