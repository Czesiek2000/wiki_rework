# Vehicle::setMissionTrainAsNoLongerNeeded

p1 is always 0


## Syntax

```js
mp.game.vehicle.setMissionTrainAsNoLongerNeeded(train, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>train:</b> <b><span style="color:#008017">Vehicle</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vehicle</span></b></li>