# Vehicle::getVehicleClassFromName

For a full enum, see [here](https://pastebin.com/i2GGAjY0)

```cpp
char buffer[128];
std::sprintf(buffer, 'VEH_CLASS_%i', VEHICLE::GET_VEHICLE_CLASS_FROM_NAME (hash));
char* className = UI::_GET_LABEL_TEXT(buffer);
```


## Syntax

```js
mp.game.vehicle.getVehicleClassFromName(modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>