# Vehicle::getRandomVehicleModelInMemory

Not present in the retail version! It's just a nullsub.

p0 always true (except in one case)

p1 returns a random vehicle hash loaded in memoryp2 unsure, maybe returns a different model


## Syntax

```js
mp.game.vehicle.getRandomVehicleModelInMemory(p0, modelHash, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>modelHash:</b> Hash</li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> modelHash, p2</li>