# Vehicle::getRandomVehicleBackBumperInSphere

## Syntax

```js
mp.game.vehicle.getRandomVehicleBackBumperInSphere(p0, p1, p2, p3, p4, p5, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Vehicle handle or object</b></li>