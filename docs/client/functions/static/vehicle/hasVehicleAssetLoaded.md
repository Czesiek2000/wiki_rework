# Vehicle::hasVehicleAssetLoaded

## Syntax

```js
mp.game.vehicle.hasVehicleAssetLoaded(vehicleAsset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleAsset:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>