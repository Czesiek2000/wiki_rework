# Vehicle::deleteMissionTrain

## Syntax

```js
mp.game.vehicle.deleteMissionTrain(train);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>train:</b> <b><span style="color:#008017">Vehicle</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vehicle</span></b></li>