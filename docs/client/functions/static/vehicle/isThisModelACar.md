# Vehicle::isThisModelACar

To check if the model is an amphibious car, see [gtaforums](https://gtaforums.com/topic/717612-v-scriptnative-documentation-and-research/page-33#entry1069317363) (for build 944 and above only!)


## Syntax

```js
mp.game.vehicle.isThisModelACar(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>