# Vehicle::switchTrainTrack

## Syntax

```js
mp.game.vehicle.switchTrainTrack(intersectionId, state);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>intersectionId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>state:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>