# Vehicle::setVehicleShootAtTarget

Commands the driver of an armed vehicle (p0) to shoot its weapon at a target (p1). 

p3, p4 and p5 are the coordinates of the target. 

Example:

```cpp
WEAPON::SET_CURRENT_PED_VEHICLE_WEAPON(pilot,GAMEPLAY::GET_HASH_KEY('VEHICLE_WEAPON_PLANE_ROCKET')); 
VEHICLE::SET_VEHICLE_SHOOT_AT_TARGET(pilot, target, targPos.x, targPos.y, targPos.z);
```


## Syntax

```js
mp.game.vehicle.setVehicleShootAtTarget(driver, entity, xTarget, yTarget, zTarget);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>driver:</b> Ped handle or object</li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>xTarget:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yTarget:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zTarget:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>