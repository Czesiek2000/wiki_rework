# Vehicle::isVehicleInGarageArea

garageName example 'Michael - Beverly Hills'

For a full list, see [here](https://pastebin.com/73VfwsmS)


## Syntax

```js
mp.game.vehicle.isVehicleInGarageArea(garageName, vehicle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>garageName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>vehicle:</b> Vehicle handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>