# Vehicle::isThisModelAnEmergencyBoat

Checks if model is a boat, then checks an additional flag.

Returns true for these models:

* PREDATOR
* SEASHARK2
* SPEEDER


## Syntax

```js
mp.game.vehicle.isThisModelAnEmergencyBoat(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>