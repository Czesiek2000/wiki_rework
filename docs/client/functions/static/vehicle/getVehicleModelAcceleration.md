# Vehicle::getVehicleModelAcceleration

Returns the acceleration of the specified model.

For a full list, see [here](https://pastebin.com/GaN6vT4R)


## Syntax

```js
mp.game.vehicle.getVehicleModelAcceleration(modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>