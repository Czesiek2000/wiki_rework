# Vehicle::createMissionTrain

Train models HAVE TO be loaded (requested) before you use this.

For variation 15 - request:

* freight
* freightcar
* freightgrain
* freightcont1
* freightcont2
* freighttrailer


## Syntax

```js
mp.game.vehicle.createMissionTrain(variation, x, y, z, direction);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>variation:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>direction:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Vehicle handle or object</b></li>