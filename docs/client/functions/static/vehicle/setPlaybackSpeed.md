# Vehicle::setPlaybackSpeed

## Syntax

```js
mp.game.vehicle.setPlaybackSpeed(p0, speed);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>speed:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>