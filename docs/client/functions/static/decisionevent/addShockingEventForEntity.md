# Decisionevent::addShockingEventForEntity
duration is float here
[Event types](https://camx.me/gtav/tasks/shockingevents.txt)
 

## Syntax

```js
mp.game.decisionevent.addShockingEventForEntity(type, entity, duration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>duration:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>ScrHandle</b></li>