# Decisionevent::suppressShockingEventTypeNextFrame

## Syntax

```js
mp.game.decisionevent.suppressShockingEventTypeNextFrame(type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>