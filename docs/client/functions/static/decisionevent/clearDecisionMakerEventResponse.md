# Decisionevent::clearDecisionMakerEventResponse

## Syntax

```js
mp.game.decisionevent.clearDecisionMakerEventResponse(name, type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>name:</b> Model hash or name</li>
<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>