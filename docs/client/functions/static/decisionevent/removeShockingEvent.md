# Decisionevent::removeShockingEvent

## Syntax

```js
mp.game.decisionevent.removeShockingEvent(event);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>event:</b> ScrHandle</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>