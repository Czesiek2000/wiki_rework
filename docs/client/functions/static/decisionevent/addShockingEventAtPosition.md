# Decisionevent::addShockingEventAtPosition

duration is float here
[Event types](https://camx.me/gtav/tasks/shockingevents.txt)
 

## Syntax

```js
mp.game.decisionevent.addShockingEventAtPosition(type, x, y, z, duration);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>ScrHandle</b></li>