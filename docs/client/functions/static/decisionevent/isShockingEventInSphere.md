# Decisionevent::isShockingEventInSphere

Some events that i found, not sure about them, but seems to have logic based on my tests: 

* 82: dead body 
* 86: explosion 
* 87: fire 
* 88: shooting, fire extinguisher in use 
* 89: shooting 
* 93: ped using horn 
* 95: ped receiving melee attack 
* 102: living ped receiving shot 
* 104: player thrown grenade, tear gas, smoke grenade, jerry can dropping gasoline 
* 105: melee attack against veh 
* 106: player running 
* 108: vehicle theft 
* 112: melee attack 
* 113: veh rollover ped 
* 114: dead ped receiving shot 
* 116: aiming at ped 
* 121: armed

*JulioNIB* Full dump of shocking event types from the exe [here](https://camx.me/gtav/tasks/shockingevents.txt) (Looks like julios guesses are pretty accurate)
 

## Syntax

```js
mp.game.decisionevent.isShockingEventInSphere(type, x, y, z, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>