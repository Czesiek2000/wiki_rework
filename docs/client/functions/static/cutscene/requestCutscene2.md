# Cutscene::requestCutscene2

Example:

```cpp
CUTSCENE::_0xC23DE0E91C30B58C('JOSH_1_INT_CONCAT', 13, 8);
```

## Syntax

```js
mp.game.cutscene.requestCutscene2(cutsceneName, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutsceneName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>