# Cutscene::hasThisCutsceneLoaded

## Syntax

```js
mp.game.cutscene.hasThisCutsceneLoaded(cutsceneName);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>cutsceneName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>