# Cutscene::startCutscene

some kind of flag. Usually 0.
 

## Syntax

```js
mp.game.cutscene.startCutscene(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>