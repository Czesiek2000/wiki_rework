# Cutscene::setCutscenePedComponentVariation

## Syntax

```js
mp.game.cutscene.setCutscenePedComponentVariation(cutsceneEntName, p1, p2, p3, modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutsceneEntName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>