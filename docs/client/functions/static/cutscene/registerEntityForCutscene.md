# Cutscene::registerEntityForCutscene

## Syntax

```js
mp.game.cutscene.registerEntityForCutscene(cutscenePed, cutsceneEntName, p2, modelHash, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutscenePed:</b> Ped handle or object</li>
<li><b>cutsceneEntName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>