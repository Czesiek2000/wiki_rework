# Cutscene::setCutscenePedPropVariation

Thanks R*! ;)

```cpp
if ((l_161 == 0) || (l_161 == 2)) { 
    sub_2ea27('Trying to set Jimmy prop variation'); 
    CUTSCENE::_0546524ADE2E9723('Jimmy_Boston', 1, 0, 0, 0);
}
```

## Syntax

```js
mp.game.cutscene.setCutscenePedPropVariation(cutsceneEntName, p1, p2, p3, modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutsceneEntName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>