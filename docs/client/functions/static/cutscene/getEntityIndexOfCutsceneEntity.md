# Cutscene::getEntityIndexOfCutsceneEntity

## Syntax

```js
mp.game.cutscene.getEntityIndexOfCutsceneEntity(cutsceneEntName, modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutsceneEntName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Entity handle or object</b></li>