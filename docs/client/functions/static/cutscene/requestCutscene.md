# Cutscene::requestCutscene

p1: usually 8
 

## Syntax

```js
mp.game.cutscene.requestCutscene(cutsceneName, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cutsceneName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>