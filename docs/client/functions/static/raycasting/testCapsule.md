# Raycasting::testCapsule

Raycast from point to point, where the ray has a radius.
 

## Syntax

```js
mp.raycasting.testCapsule(pos1, pos2, radius, [ignoredEntity], [flags]);
```

## Example

```js
mp.events.add('render', () => {
	const startPosition = player.getBoneCoords(12844, 0.5, 0, 0);
	const endPosition = new mp.Vector3(0, 0, 75);

	const hitData = mp.raycasting.testCapsule(startPosition, endPosition, 0.5);
	if (!hitData) {
		mp.game.graphics.drawLine(startPosition.x, startPosition.y, startPosition.z, endPosition.x, endPosition.y, endPosition.z, 255, 255, 255, 255); // Is in line of sight
	} else {
		mp.game.graphics.drawLine(startPosition.x, startPosition.y, startPosition.z, endPosition.x, endPosition.y, endPosition.z, 255, 0, 0, 255); // Is NOT in line of sight
	}
});
```


<br />


## Parameters

<li><b>pos1:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>pos2:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>ignoreEntity:</b> Array of entities handle or object</li>
<li><b>flags:</b> Array of ints</li>

<br />
<br />

## Return value

<li><b>object:</b> position (Entity Coordinates) , surfaceNormal, material (Entity Model) , entity (Handle)</li>