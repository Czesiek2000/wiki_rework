# Raycasting::testPointToPoint

This function casts a ray from Point1 to Point2 and returns the position and entity of what's in the way, or undefined if the way is cleared.

Flags are intersection bit flags. They tell the ray what to care about and what not to care about when casting. 

Passing -1 will intersect with everything, presumably.

Flags:
* 1 Intersect with map
* 2 Intersect with vehicles (used to be mission entities?) (includes train)
* 4 Intersect with peds? (same as 8)
* 8 Intersect with peds? (same as 4)
* 16 Intersect with objects
* 32 Unknown
* 64 Unknown
* 128 Unknown
* 256 Intersect with vegetation (plants, coral. trees not included)

**NOTE: Raycasts that intersect with mission_entites (flag = 2) has limited range and will not register for far away entites.**

The range seems to be about 30 metres.
 

Example of return object
 

This function returns undefined or a valid result, if you point with your camera on something.
 

## Syntax

```js
mp.raycasting.testPointToPoint(pos1, pos2, [ignoredEntity], [flags])

```

## Example

```js
{
	"position": {
		"x": 13.01,
		"y": 12.23,
		"z": 70.02
	},
	"surfaceNormal": {
		"x": 0.93,
		"y": -0.04,
		"z": -0.34
	},
	"entity": <entity object>
}
```


<br />


## Parameters

<li><b>pos1:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>pos2:</b> <b><span style="color:#008017">Vector3</span></b></li>

<br />
<br />

### Optional arguments

<li><b>ignoredEntity:</b> Entity handle or object - example: mp.players.local</li>
<li><b>flags:</b> Int representing sum of flags - example: 17 (intersect with map [1] and objects [16])</li>

## Return value

<li><b><span style="color:#008017">Object</span></b></li>
