# Graphics::setTrackedPointInfo

## Syntax

```js
mp.game.graphics.setTrackedPointInfo(point, x, y, z, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>point:</b> Object handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>