# Graphics::hasStreamedTextureDictLoaded

## Syntax

```js
mp.game.graphics.hasStreamedTextureDictLoaded(textureDict);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>