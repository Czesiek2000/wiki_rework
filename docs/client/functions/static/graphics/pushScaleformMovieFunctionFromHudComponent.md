# Graphics::pushScaleformMovieFunctionFromHudComponent

Pushes a function from the Hud component Scaleform onto the stack. 
Same behavior as `GRAPHICS::_PUSH_SCALEFORM_MOVIE_FUNCTION`, just a hud component id instead of a Scaleform.

Known components:
* 19 - MP_RANK_BAR
* 20 - HUD_DIRECTOR_MODE

This native requires more research - all information can be found inside of `hud.gfx`. 
Using a decompiler, the different components are located under `scripts/__Packages/com/rockstargames/gtav/hud/hudComponents` and `scripts/__Packages/com/rockstargames/gtav/Multiplayer`.
 

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunctionFromHudComponent(hudComponent, functionName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hudComponent:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>functionName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>