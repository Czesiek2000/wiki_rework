# Graphics::setScreenDrawPosition

Seems to move all the drawn text on the screen to given coordinates.

It also removed all the drawn sprites of my screen so not to sure what the exact function is.
 

## Syntax

```js
mp.game.graphics.setScreenDrawPosition(x, y);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>