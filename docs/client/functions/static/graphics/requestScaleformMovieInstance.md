# Graphics::requestScaleformMovieInstance

Also used by `0x67D02A194A2FC2BD`
 

## Syntax

```js
mp.game.graphics.requestScaleformMovieInstance(scaleformName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>