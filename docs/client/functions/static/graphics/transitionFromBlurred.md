# Graphics::transitionFromBlurred

Time in ms to transition from fully blurred to normal. 

RU: время в мс для перехода от полностью размытого экрана до нормального
 

## Syntax

```js
mp.game.graphics.transitionFromBlurred(transitionTime);
```

## Example

```js
mp.game.graphics.transitionFromBlurred(1000); // Remove blur completely after 1000 ms (RU: Полностью убираем размытие через 1000 мс)
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>transitionTime:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>