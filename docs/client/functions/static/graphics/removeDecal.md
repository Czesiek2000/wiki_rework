# Graphics::removeDecal

## Syntax

```js
mp.game.graphics.removeDecal(decal);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>decal:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>