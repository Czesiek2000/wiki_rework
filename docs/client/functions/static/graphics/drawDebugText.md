# Graphics::drawDebugText

**NOTE: Debugging functions are not present in the retail version of the game.**
 

## Syntax

```js
mp.game.graphics.drawDebugText(text, x, y, z, r, g, b, alpha);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">float</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>