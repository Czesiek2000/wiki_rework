# Graphics::setTvAudioFrontend
Might be more appropriate in AUDIO?

Rockstar made it like this.

Probably changes tvs from being a 3d audio to being 'global' audio
 

## Syntax

```js
mp.game.graphics.setTvAudioFrontend(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>