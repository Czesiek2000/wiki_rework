# Graphics::isDecalAlive

## Syntax

```js
mp.game.graphics.isDecalAlive(decal);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>decal:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>