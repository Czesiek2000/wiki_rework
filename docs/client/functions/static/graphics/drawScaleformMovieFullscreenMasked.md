# Graphics::drawScaleformMovieFullscreenMasked

## Syntax

```js
mp.game.graphics.drawScaleformMovieFullscreenMasked(scaleform1, scaleform2, red, green, blue, alpha);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scaleform2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>red:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>green:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>blue:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>