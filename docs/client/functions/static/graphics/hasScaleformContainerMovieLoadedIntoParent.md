# Graphics::hasScaleformContainerMovieLoadedIntoParent

## Syntax

```js
mp.game.graphics.hasScaleformContainerMovieLoadedIntoParent(scaleformHandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformHandle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>