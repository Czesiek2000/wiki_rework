# Graphics::stopScreenEffect

## Syntax

```js
mp.game.graphics.stopScreenEffect(effectName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>