# Graphics::setStreamedTextureDictAsNoLongerNeeded

## Syntax

```js
mp.game.graphics.setStreamedTextureDictAsNoLongerNeeded(textureDict);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>