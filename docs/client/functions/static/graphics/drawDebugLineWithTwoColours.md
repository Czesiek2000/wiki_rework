# Graphics::drawDebugLineWithTwoColours

**NOTE: Debugging functions are not present in the retail version of the game.**
 

## Syntax

```js
mp.game.graphics.drawDebugLineWithTwoColours(x1, y1, z1, x2, y2, z2, r1, g1, b1, r2, g2, b2, alpha1, alpha2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>r1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>r2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>