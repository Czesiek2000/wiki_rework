# Graphics::setFlash

Purpose of p0 and p1 unknown.
 

## Syntax

```js
mp.game.graphics.setFlash(p0, p1, fadeIn, duration, fadeOut);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fadeIn:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>fadeOut:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>