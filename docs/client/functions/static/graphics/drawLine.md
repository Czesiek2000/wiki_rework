# Graphics::drawLine
Draws a depth-tested line from one point to another.----------------x1, y1, z1 : Coordinates for the first pointx2, y2, z2 : Coordinates for the second pointr, g, b, alpha : Color with RGBA-ValuesI recommend using a predefined function to call this.
[VB.NET]
```vb
Public Sub DrawLine(from As Vector3, [to] As Vector3, col As Color) 
[Function].Call(Hash.DRAW_LINE, from.X, from.Y, from.Z, [to].X, [to].Y, [to].Z, col.R, col.G, col.B, col.A)End Sub
```
[C#]
```csharp
public void DrawLine(Vector3 from, Vector3 to, Color col){ 
    Function.Call(Hash.DRAW_LINE, from.X, from.Y, from.Z, to.X, to.Y, to.Z, col.R, col.G, col.B, col.A);
}
```
 

## Syntax

```js
mp.game.graphics.drawLine(x1, y1, z1, x2, y2, z2, r, g, b, alpha);
```

## Example

```js
pos = mp.players.local.getCoords(true);        

mp.game.graphics.drawLine(pos.x-1, pos.y, pos.z, pos.x+1, pos.y, pos.z, 255, 0, 0, 255);     
mp.game.graphics.drawLine(pos.x, pos.y-1, pos.z, pos.x, pos.y+1, pos.z, 0, 255, 0, 255);       
mp.game.graphics.drawLine(pos.x, pos.y, pos.z-0.5, pos.x, pos.y, pos.z+1.5, 0, 0, 255, 255);
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>