# Graphics::setTimecycleModifier

Loads the specified timecycle modifier. 

Modifier listParameters:

modifierName - The modifier to load (e.g. 'V_FIB_IT3', 'scanline_cam', etc.)
 

## Syntax

```js
mp.game.graphics.setTimecycleModifier(modifierName);
```

## Example

```js
// todo

```


<br />


## Parameters

<li><b>modifierName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>