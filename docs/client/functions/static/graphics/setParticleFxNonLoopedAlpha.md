# Graphics::setParticleFxNonLoopedAlpha

Usage example for C#:

```csharp
Function.Call(Hash.SET_PARTICLE_FX_NON_LOOPED_ALPHA, new InputArgument[] { 0.1f });
```

**Note: the argument alpha ranges from 0.0f-1.0f !**
 

## Syntax

```js
mp.game.graphics.setParticleFxNonLoopedAlpha(alpha);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>alpha:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>