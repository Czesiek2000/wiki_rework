# Graphics::getDecalWashLevel

## Syntax

```js
mp.game.graphics.getDecalWashLevel(decal);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>decal:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>