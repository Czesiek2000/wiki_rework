# Graphics::requestScaleformMovie3

Similar to `REQUEST_SCALEFORM_MOVIE`, but seems to be some kind of 'interactive' scaleform movie?

These seem to be the only scaleforms ever requested by this native: `breaking_news``desktop_pc``ECG_MONITOR``Hacking_PC``TEETH_PULLING`

**Note: Unless this hash is out-of-order, this native is next-gen only.**
 

## Syntax

```js
mp.game.graphics.requestScaleformMovie3(scaleformName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>