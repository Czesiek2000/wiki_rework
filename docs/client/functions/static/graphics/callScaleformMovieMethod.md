# Graphics::callScaleformMovieMethod

Calls the Scaleform function.
 

## Syntax

```js
mp.game.graphics.callScaleformMovieMethod(scaleform, method);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>method:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>