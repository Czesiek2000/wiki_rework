# Graphics::doesParticleFxLoopedExist

## Syntax

```js
mp.game.graphics.doesParticleFxLoopedExist(ptfxHandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>