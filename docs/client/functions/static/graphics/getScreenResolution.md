# Graphics::getScreenResolution

int screenresx,screenresy;`GET_SCREEN_RESOLUTION(&screenresx,&screenresy);`
 
The returned object seems to be static.
 

## Syntax

```js
mp.game.graphics.getScreenResolution(x, y);
```

## Example

```js
{ "x": 1280, "y": 720 }
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b> x, y</li>