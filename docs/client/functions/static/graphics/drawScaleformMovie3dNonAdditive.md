# Graphics::drawScaleformMovie3dNonAdditive

Originally called `_DRAW_SCALEFORM_MOVIE_3D`. Since the actual `DRAW_SCALEFORM_MOVIE_3D` native was found, what the heck does this one do differently?
 

## Syntax

```js
mp.game.graphics.drawScaleformMovie3dNonAdditive(scaleform, posX, posY, posZ, rotX, rotY, rotZ, p7, p8, p9, scaleX, scaleY, scaleZ, p13);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scaleX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scaleY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scaleZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p13:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>