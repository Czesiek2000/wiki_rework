# Graphics::removeDecalsFromObject

## Syntax

```js
mp.game.graphics.removeDecalsFromObject(obj);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>obj:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>