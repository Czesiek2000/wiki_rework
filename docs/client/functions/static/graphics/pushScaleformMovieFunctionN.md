# Graphics::pushScaleformMovieFunctionN

Possibly calls 'global' Scaleform functions - needs more research!
 

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunctionN(functionName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>functionName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>