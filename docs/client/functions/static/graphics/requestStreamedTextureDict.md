# Graphics::requestStreamedTextureDict

Requests the specified texture dictionary.
 

## Syntax

```js
mp.game.graphics.requestStreamedTextureDict(textureDict, p1);
```

## Example

```js
// This function will load the specified texture dictionary.
function loadTextureDictionary(textureDict) {
    if (!mp.game.graphics.hasStreamedTextureDictLoaded(textureDict)) {
        mp.game.graphics.requestStreamedTextureDict(textureDict, true);
        while (!mp.game.graphics.hasStreamedTextureDictLoaded(textureDict)) mp.game.wait(0);
    }
}

loadTextureDictionary("commonmenu"); // will load commonmenu.ytd
```


<br />


## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>