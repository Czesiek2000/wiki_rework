# Graphics::setTransitionTimecycleModifier

## Syntax

```js
mp.game.graphics.setTransitionTimecycleModifier(modifierName, transition);
```

## Example

```js
// todo

```


<br />


## Parameters

<li><b>modifierName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>transition:</b> <b><span style="color:#008017">Float</span></b></li>

<br />

[Timecycle modyfiers](../../../../tables/timecycleModyfiers.md)
<br />

## Return value

<li><b>Undefined</b></li>