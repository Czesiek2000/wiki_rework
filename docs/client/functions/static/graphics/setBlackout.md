# Graphics::setBlackout

Disables all emissive textures and lights like city lights, car lights, cop car lights. 
Particles still emit lightUsed in Humane Labs Heist for EMP.
 

## Syntax

```js
mp.game.graphics.setBlackout(enable);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>enable:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>