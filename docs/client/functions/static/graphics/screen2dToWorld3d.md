# Graphics::screen2dToWorld3d

Returns world position from screen position
 
Inverted function: `Graphics::world3dToScreen2d`
 

## Syntax

```js
mp.game.graphics.screen2dToWorld3d(coords2d);
```

## Example

```js
let pos3d = mp.game.graphics.screen2dToWorld3d(new mp.Vector3(screenX, screenY, 0));
```


<br />


## Parameters

<li><b><span style="color:#008017">Vector3</span></b> 2d coord</li>

<br />
<br />

## Return value

<li><b>position:</b> <b><span style="color:#008017">Vector3</span></b></li>