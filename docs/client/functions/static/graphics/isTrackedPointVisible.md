# Graphics::isTrackedPointVisible

## Syntax

```js
mp.game.graphics.isTrackedPointVisible(point);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>point:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>