# Graphics::setPtfxAssetNextCall

From the *b678d* decompiled scripts: 

```cpp
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('FM_Mission_Controler'); GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_apartment_mp'); 
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_indep_fireworks'); 
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_mp_cig_plane'); 
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_mp_creator'); 
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_ornate_heist'); 
GRAPHICS::_SET_PTFX_ASSET_NEXT_CALL('scr_prison_break_heist_station');
```

## Syntax

```js
mp.game.graphics.setPtfxAssetNextCall(name);
```

## Example
```js
// todo
```

<br />


## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>