# Graphics::pushScaleformMovieFunctionParameterString

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunctionParameterString(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>