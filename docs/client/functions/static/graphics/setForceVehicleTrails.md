# Graphics::setForceVehicleTrails

Forces vehicle trails on all surfaces.
 

## Syntax

```js
mp.game.graphics.setForceVehicleTrails(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>