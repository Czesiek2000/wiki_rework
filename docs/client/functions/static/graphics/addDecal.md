# Graphics::addDecal

thanks to *JulioNIB* for figuring out the parameters.

decal types:
```cpp
public enum DecalTypes { 
    splatters_blood = 1010,
    splatters_blood_dir = 1015, 
    splatters_blood_mist = 1017, 
    splatters_mud = 1020, 
    splatters_paint = 1030, 
    splatters_water = 1040, 
    splatters_water_hydrant = 1050, 
    splatters_blood2 = 1110, 
    weapImpact_metal = 4010, 
    weapImpact_concrete = 4020, 
    weapImpact_mattress = 4030, 
    weapImpact_mud = 4032, 
    weapImpact_wood = 4050, 
    weapImpact_sand = 4053, 
    weapImpact_cardboard = 4040, 
    weapImpact_melee_glass = 4100, 
    weapImpact_glass_blood = 4102, 
    weapImpact_glass_blood2 = 4104, 
    weapImpact_shotgun_paper = 4200, 
    weapImpact_shotgun_mattress, 
    weapImpact_shotgun_metal, 
    weapImpact_shotgun_wood, 
    weapImpact_shotgun_dirt, 
    weapImpact_shotgun_tvscreen, 
    weapImpact_shotgun_tvscreen2, 
    weapImpact_shotgun_tvscreen3, 
    weapImpact_melee_concrete = 4310, 
    weapImpact_melee_wood = 4312, 
    weapImpact_melee_metal = 4314, 
    burn1 = 4421, 
    burn2, 
    burn3, 
    burn4, 
    burn5, 
    bang_concrete_bang = 5000, 
    bang_concrete_bang2, 
    bang_bullet_bang, 
    bang_bullet_bang2 = 5004, 
    bang_glass = 5031, 
    bang_glass2, 
    solidPool_water = 9000, 
    solidPool_blood, 
    solidPool_oil, 
    solidPool_petrol, 
    solidPool_mud, 
    porousPool_water, 
    porousPool_blood, 
    porousPool_oil, 
    porousPool_petrol, 
    porousPool_mud, 
    porousPool_water_ped_drip, 
    liquidTrail_water = 9050
}
```

 

## Syntax

```js
mp.game.graphics.addDecal(decaltype, x, y, z, dirX, dirY, dirZ,p7, p8, p9, width, height, rCoef, gCoef, bCoef, opacity, timeout, p17, p18, p19);
```

## Example

```js
// Will create a large red blood splatter on the ground at x,y,z
mp.game.graphics.addDecal(1110 /*splatters_blood2 */ , x, y, z, 0 /*dirX*/ , 0 /*dirY*/ , -1 /*dirZ*/ , 0, 1, 0, 4 /*width*/ , 4 /*height*/ , 255, 0.1, 0.1, 1.0, 150.0, false, false, false);
```


<br />


## Parameters

<li><b>decaltype:</b> string(to be checked)</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>width:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>height:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rCoef:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>gCoef:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>bCoef:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>opacity:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>timeout:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p17:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p18:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p19:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>