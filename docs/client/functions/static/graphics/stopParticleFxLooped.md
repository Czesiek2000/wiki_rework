# Graphics::stopParticleFxLooped

p1 is always 0 in the native scripts
 

## Syntax

```js
mp.game.graphics.stopParticleFxLooped(ptfxHandle, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>