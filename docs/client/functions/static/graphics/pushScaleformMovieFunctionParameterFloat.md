# Graphics::pushScaleformMovieFunctionParameterFloat

Pushes a float for the Scaleform function onto the stack.
 

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunctionParameterFloat(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>