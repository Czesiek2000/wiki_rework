# Graphics::setParticleFxLoopedScale

## Syntax

```js
mp.game.graphics.setParticleFxLoopedScale(ptfxHandle, scale);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>