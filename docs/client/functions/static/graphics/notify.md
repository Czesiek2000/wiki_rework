# Graphics::notify

This function sends a notification message to the player.
 
You can also use the colour codes seen here: [Fonts and colors](../../../../tables/font.md)
 
 

## Syntax

```js
mp.game.graphics.notify(message)

```

## Example

This example will send a notification 'Hello World' to the player with the word 'Hello' in green.

```js
mp.game.graphics.notify('~g~Hello ~w~World');

```


<br />


## Parameters

<li><b>message:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

No return value
