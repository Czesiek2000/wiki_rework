# Graphics::setParticleFxNonLoopedColour

only works on some fx's
 

## Syntax

```js
mp.game.graphics.setParticleFxNonLoopedColour(r, g, b);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>r:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>