# Graphics::pushScaleformMovieFunctionParameterBool

Pushes a boolean for the Scaleform function onto the stack.
 

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunctionParameterBool(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>