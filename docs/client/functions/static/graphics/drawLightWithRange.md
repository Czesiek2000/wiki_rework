# Graphics::drawLightWithRange

## Syntax

```js
mp.game.graphics.drawLightWithRange(posX, posY, posZ, colorR, colorG, colorB, range, intensity);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorR:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorG:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorB:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>intensity:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>