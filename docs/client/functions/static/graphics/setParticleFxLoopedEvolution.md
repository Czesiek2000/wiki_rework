# Graphics::setParticleFxLoopedEvolution

## Syntax

```js
mp.game.graphics.setParticleFxLoopedEvolution(ptfxHandle, propertyName, amount, Id);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>propertyName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>amount:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Id:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>