# Graphics::set2dLayer

Must be called before drawing a rectangle, text, etc.

Lowest number is below everything else. Highest number is above everything else.
 

## Syntax

```js
mp.game.graphics.set2dLayer(layer);
```

## Example

```js
mp.game.graphics.set2dLayer(1);
mp.game.graphics.drawRect();
```


<br />


## Parameters

<li><b>layer:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>