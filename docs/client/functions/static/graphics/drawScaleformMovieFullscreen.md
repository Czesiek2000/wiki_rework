# Graphics::drawScaleformMovieFullscreen

unk is not used so no need
 

## Syntax

```js
mp.game.graphics.drawScaleformMovieFullscreen(scaleform, red, green, blue, alpha, unk);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>red:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>green:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>blue:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unk:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>