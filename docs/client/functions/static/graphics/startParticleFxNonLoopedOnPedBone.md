# Graphics::startParticleFxNonLoopedOnPedBone

```csharp
GRAPHICS::START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE('scr_sh_bong_smoke', PLAYER::PLAYER_PED_ID(), -0.025f, 0.13f, 0f, 0f, 0f, 0f, 31086, 0x3F800000, 0, 0, 0);
```
Axis - Invert Axis Flags
[list](https://pastebin.com/N9unUFWY)
 

## Syntax

```js
mp.game.graphics.startParticleFxNonLoopedOnPedBone(effectName, ped, offsetX, offsetY, offsetZ, rotX, rotY, rotZ, boneIndex, scale, axisX, axisY, axisZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>ped:</b> Ped handle or object</li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>axisX:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisY:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisZ:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>