# Graphics::setParticleFxLoopedRange

## Syntax

```js
mp.game.graphics.setParticleFxLoopedRange(ptfxHandle, range);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>