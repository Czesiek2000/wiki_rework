# Graphics::requestScaleformMovie

Gets a new native after almost every update.

Update 1.0.393.20x67D02A194A2FC2BD
Update 1.0.463.10xC97D787CE7726A2F
Update 1.0.505.20x36ECDA4DD9A3F08D
Update 1.0.573.10xE3C796DC28BC3254
Update 1.0.678.10x2F14983962462691
 

## Syntax

```js
mp.game.graphics.requestScaleformMovie(scaleformName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>