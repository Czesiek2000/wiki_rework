# Graphics::beginTextComponent

Called prior to adding a text component to the UI. After doing so, `GRAPHICS::_END_TEXT_COMPONENT` is called.

Examples:
```cpp
GRAPHICS::_BEGIN_TEXT_COMMAND_SCALEFORM('NUMBER');
UI::ADD_TEXT_COMPONENT_INTEGER(GAMEPLAY::ABSI(a_1));
GRAPHICS::_END_TEXT_COMPONENT();GRAPHICS::_BEGIN_TEXT_COMPONENT('STRING');
UI::_ADD_TEXT_COMPONENT_STRING(a_2);GRAPHICS::_END_TEXT_COMPONENT();
GRAPHICS::_BEGIN_TEXT_COMPONENT('STRTNM2');UI::_0x17299B63C7683A2B(v_3);
UI::_0x17299B63C7683A2B(v_4);GRAPHICS::_END_TEXT_COMPONENT();
GRAPHICS::_BEGIN_TEXT_COMPONENT('STRTNM1');UI::_0x17299B63C7683A2B(v_3);
GRAPHICS::_END_TEXT_COMPONENT();
```
This is `_BEGIN_TEXT_COMPONENT`
 

## Syntax

```js
mp.game.graphics.beginTextComponent(componentType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>