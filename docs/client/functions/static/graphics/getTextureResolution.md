# Graphics::getTextureResolution

Returns the texture resolution of the passed texture dict+name.

**Note: Most texture resolutions are doubled compared to the console version of the game.**
 

## Syntax

```js
mp.game.graphics.getTextureResolution(textureDict, textureName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>textureName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>