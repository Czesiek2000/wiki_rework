# Graphics::startParticleFxNonLoopedAtCoord

```cpp
GRAPHICS::START_PARTICLE_FX_NON_LOOPED_AT_COORD('scr_paleto_roof_impact', -140.8576f, 6420.789f, 41.1391f, 0f, 0f, 267.3957f, 0x3F800000, 0, 0, 0);
```

Axis - Invert Axis Flags[list](https://pastebin.com/N9unUFWY)

Alright, So, I will try to explain a bit here, since, myself was confused with the default explanation, and this is because, I am really a 'noob' when it comes to programming.

but somehow after a lot of research, try and fail method I got it to work. So I will try to explain as much I could.
C#
```csharp
Function.Call<int>(Hash.START_PARTICLE_FX_NON_LOOPED_AT_COORD, //you are calling this function.
char *effectname //This is an in-game effect name, for e.g. 'scr_fbi4_trucks_crash' is used to give the effects when truck crashes etc
float x, y, z pos //this one is Simple, you just have to declare, where do you want this effect to take place at, so declare the ordinates
float xrot, yrot, zrot //Again simple? just mention the value in case if you want the effect to rotate.
float scale //is declare the scale of the effect, this may vary as per the effects for e.g 1.0f
bool xaxis, yaxis, zaxis //To bool the axis values.So here is final example - 
Function.Call<int>(Hash.START_PARTICLE_FX_NON_LOOPED_AT_COORD, 'scr_fbi4_trucks_crash', GTA.Game.Player.Character.Position.X, GTA.Game.Player.Character.Position.Y, GTA.Game.Player.Character.Position.Z + 4f, 0, 0, 0, 5.5f, 0, 0, 0);Hope this helps.
```
 

## Syntax

```js
mp.game.graphics.startParticleFxNonLoopedAtCoord(effectName, xPos, yPos, zPos, xRot, yRot, zRot, scale, xAxis, yAxis, zAxis);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>yAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>zAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>