# Graphics::addPetrolDecal

## Syntax

```js
mp.game.graphics.addPetrolDecal(x, y, z, groundLvl, width, transparency);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>groundLvl:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>width:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>transparency:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>