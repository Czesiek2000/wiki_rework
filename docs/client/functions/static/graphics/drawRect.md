# Graphics::drawRect

Draws a rectangle on the screen.

*The total number of rectangles to be drawn in one frame is apparently limited to 399.*

 

## Syntax

```js
mp.game.graphics.drawRect(pos_x, pos_y, width, height, color_r, color_g, color_b, color_a);

```

## Example

```js
mp.events.add('render', () => {
    if (condition) {
        mp.game.graphics.drawRect(0.35, 0.4, 0.3, 0.2, 215, 55, 55, 155);
    }
});

```


<br />


## Parameters

<li><b>pos_x:</b> <b><span style="color:#008017">Float</span></b> (0.0-1.0, 0.0 is the left edge of the screen, 1.0 is the right edge of the screen)</li>
<li><b>pos_y:</b> <b><span style="color:#008017">Float</span></b> (0.0-1.0, 0.0 is the top edge of the screen, 1.0 is the bottom edge of the screen)</li>
<li><b>width:</b> <b><span style="color:#008017">Float</span></b> (0.0-1.0, 1.0 means the whole screen width)</li>
<li><b>height:</b> <b><span style="color:#008017">Float</span></b> (0.0-1.0, 1.0 means the whole screen height)</li>
<li><b>color_r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>color_g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>color_b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>color_a:</b> <b><span style="color:#008017">Int</span></b> (0-255, 0 means totally transparent, 255 means totally opaque)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>