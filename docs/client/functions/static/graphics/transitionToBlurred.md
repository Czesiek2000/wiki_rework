# Graphics::transitionToBlurred

Time in ms to transition to fully blurred screen.

RU: время в мс до перехода к полностью размытому экрану.
 

## Syntax

```js
mp.game.graphics.transitionToBlurred(transitionTime);
```

## Example

```js
mp.game.graphics.transitionToBlurred(1000); // Blur the screen after 1000ms (RU: Размываем экран через 1000 мс)
```


<br />


## Parameters

<li><b>transitionTime:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>