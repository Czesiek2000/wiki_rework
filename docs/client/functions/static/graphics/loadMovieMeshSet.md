# Graphics::loadMovieMeshSet

## Syntax

```js
mp.game.graphics.loadMovieMeshSet(movieMeshSetName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>movieMeshSetName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>