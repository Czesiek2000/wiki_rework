# Graphics::startParticleFxLoopedOnPedBone

## Syntax

```js
mp.game.graphics.startParticleFxLoopedOnPedBone(effectName, ped, xOffset, yOffset, zOffset, xRot, yRot, zRot, boneIndex, scale, xAxis, yAxis, zAxis);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>ped:</b> Ped handle or object</li>
<li><b>xOffset:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yOffset:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zOffset:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRot:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>yAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>zAxis:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>