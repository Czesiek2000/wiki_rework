# Graphics::destroyTrackedPoint

## Syntax

```js
mp.game.graphics.destroyTrackedPoint(point);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>point:</b> Object handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>