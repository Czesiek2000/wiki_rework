# Graphics::sittingTv

Unsurprisingly, this native is incorrectly named. Instead, this returns the name of the scaleform movie.

HASH COLLISION. PLEASE REMOVE NATIVE NAME.
 

## Syntax

```js
mp.game.graphics.sittingTv(scaleform);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>