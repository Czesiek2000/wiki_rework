# Graphics::setNightvision

Enables Night Vision.

Example:

C#: 
```csharp
Function.Call(Hash.SET_NIGHTVISION, true);
```

C++: 
```cpp
GRAPHICS::SET_NIGHTVISION(true);
```
BOOL toggle:

true = turns night vision on for your player.

false = turns night vision off for your player.
 

## Syntax

```js
mp.game.graphics.setNightvision(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>