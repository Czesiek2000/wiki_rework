# Graphics::world3dToScreen2d

Convert a world coordinate into its relative screen coordinate. (WorldToScreen)
 
Inverted function: [Graphics::screen2dToWorld3d](./screen2dToWorld3d.md)
 

## Syntax

```js
mp.game.graphics.world3dToScreen2d(new mp.Vector3(worldX, worldY, worldZ));
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>worldX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>worldY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>worldZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b> x, y</li>