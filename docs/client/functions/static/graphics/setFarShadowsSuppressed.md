# Graphics::setFarShadowsSuppressed

When this is set to ON, shadows only draw as you get nearer.

When OFF, they draw from a further distance.
 

## Syntax

```js
mp.game.graphics.setFarShadowsSuppressed(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>