# Graphics::removeDecalsFromObjectFacing

## Syntax

```js
mp.game.graphics.removeDecalsFromObjectFacing(obj, x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>obj:</b> Object handle or object</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>