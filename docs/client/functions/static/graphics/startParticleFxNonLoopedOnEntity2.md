# Graphics::startParticleFxNonLoopedOnEntity2

Console hash: `0x469A2B4A`

network fx
 

## Syntax

```js
mp.game.graphics.startParticleFxNonLoopedOnEntity2(effectName, entity, offsetX, offsetY, offsetZ, rotX, rotY, rotZ, scale, axisX, axisY, axisZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>axisX:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisY:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisZ:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>