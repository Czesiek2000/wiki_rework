# Graphics::setPtfxAssetOldToNew

console hash: `0xC92719A7`
 

## Syntax

```js
mp.game.graphics.setPtfxAssetOldToNew(oldAsset, newAsset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>oldAsset:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>newAsset:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>