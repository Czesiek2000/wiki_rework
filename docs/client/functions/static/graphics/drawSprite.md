# Graphics::drawSprite

Draws a 2D Sprite / Texture on the screen. This can also be done through DLCPacks.
 
[Textures & Sprite List](https://czesiek2000.gitlab.io/docs)
 

## Syntax

```js
mp.game.graphics.drawSprite(textureDict, textureName, screenX, screenY, scaleX, scaleY, heading, colorR, colorG, colorB, alpha);
```

## Example

```js
if (!mp.game.graphics.hasStreamedTextureDictLoaded("mponmissmarkers")) {
    mp.game.graphics.requestStreamedTextureDict("mponmissmarkers", true);
}
    
if (mp.game.graphics.hasStreamedTextureDictLoaded("mponmissmarkers")) {
    mp.game.graphics.drawSprite("mponmissmarkers", "capture_the_flag_base_icon", 0.5, 0.5, 0.1, 0.1, 0, 255, 255, 255, 100);
}
```


<br />


## Parameters

<li><b>textureDict:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>textureName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>screenX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>screenY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scaleX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scaleY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorR:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorG:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorB:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>