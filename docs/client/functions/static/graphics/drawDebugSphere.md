# Graphics::drawDebugSphere

**NOTE: Debugging functions are not present in the retail version of the game.**
 

## Syntax

```js
mp.game.graphics.drawDebugSphere(x, y, z, radius, r, g, b, alpha);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>