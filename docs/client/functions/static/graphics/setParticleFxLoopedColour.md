# Graphics::setParticleFxLoopedColour
only works on some fx'sp4 = 0
 

## Syntax

```js
mp.game.graphics.setParticleFxLoopedColour(ptfxHandle, r, g, b, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>