# Graphics::createCheckpoint

The contents of this page are possibly invalid or incomplete. Please treat the contents of this page with caution!
 
If you are able to, please modify the page so that it's factually correct.
 
Creates a checkpoint. Returns the handle of the checkpoint.

Parameters:
* type - The type of checkpoint to create. See below for a list of checkpoint types.
* pos1 - The position of the checkpoint.
* pos2 - The position of the next checkpoint to point to.
* radius - The radius of the checkpoint.
* color - The color of the checkpoint.
* reserved - Special parameter, see below for details. Usually set to 0 in the scripts.

Checkpoint types:
* 0-4---------Cylinder: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker
* 5-9---------Cylinder: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker
* 10-14-------Ring: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker
* 15-19-------1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker 
* 20-24-------Cylinder: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker 
* 25-29-------Cylinder: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker 
* 30-34-------Cylinder: 1 arrow, 2 arrow, 3 arrows, CycleArrow, Checker 
* 35-38-------Ring: Airplane Up, Left, Right, UpsideDown
* 39----------?
* 40----------Ring: just a ring
* 41----------?
* 42-44-------Cylinder w/ number (uses 'reserved' parameter)
* 45-47-------Cylinder no arrow or number

If using type 42-44, reserved sets number / number and shape to display
* 0-99------------Just numbers (0-99)
* 100-109-----------------Arrow (0-9)
* 110-119------------Two arrows (0-9)
* 120-129----------Three arrows (0-9)
* 130-139----------------Circle (0-9)
* 140-149------------CycleArrow (0-9)
* 150-159----------------Circle (0-9)
* 160-169----Circle w/ pointer (0-9)
* 170-179-------Perforated ring (0-9)
* 180-189----------------Sphere (0-9)
 
 

## Syntax

```js
mp.game.graphics.createCheckpoint(type, posX1, posY1, posZ1, posX2, posY2, posZ2, radius, colorR, colorG, colorB, alpha, reserved);
```

## Example

This example creates a ring checkpoint with white color.

```js
mp.game.graphics.createCheckpoint(35, -115, -848, 36, -115, -848, 39, 1, 255, 255, 255, 150,0);
// Vector3 doesn't work
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>posX1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posX2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorR:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorG:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorB:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>reserved:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>