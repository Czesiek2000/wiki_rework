# Graphics::pushScaleformMovieFunction

Push a function from the Scaleform onto the stack
 

## Syntax

```js
mp.game.graphics.pushScaleformMovieFunction(scaleform, functionName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>functionName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>