# Graphics::drawSpotLight
Parameters:
* pos - coordinate where the spotlight is located
* dir - the direction vector the spotlight should aim at from its current position
* r,g,b - color of the spotlight
* distance - the maximum distance the light can reach
* brightness - the brightness of the light
* roundness - 'smoothness' of the circle edge
* radius - the radius size of the spotlight
* falloff - the falloff size of the light's edge [example](https://www.i.imgur.com/DemAWeO.jpg)

Example in C# (spotlight aims at the closest vehicle):
```cpp
Vector3 myPos = Game.Player.Character.Position;
Vehicle nearest = World.GetClosestVehicle(myPos , 1000f);
Vector3 destinationCoords = nearest.Position;
Vector3 dirVector = destinationCoords - myPos;
dirVector.Normalize();
Function.Call(Hash.DRAW_SPOT_LIGHT, pos.X, pos.Y, pos.Z, dirVector.X, dirVector.Y, dirVector.Z, 255, 255, 255, 100.0f, 1f, 0.0f, 13.0f, 1f);
```
 
 

## Syntax

```js
mp.game.graphics.drawSpotLight(posX, posY, posZ, dirX, dirY, dirZ, colorR, colorG, colorB, distance, brightness, roundness, radius, falloff);
```

## Example

This example creates an illumination on top of the player and reflecting on the ground as well, light is flashing blue

```js
let lightRender
function switchLight() {
  if (!lightRender) {
    // light ON
    lightRender = new mp.Event('render', ()=> {
      let pos = mp.players.local.position
      mp.game.graphics.drawSpotLight(pos.x,pos.y,pos.z+10, 0,0,-1, 0,0,255, 20,20,5,10,1)
    })
  } else {
    // light OFF
    lightRender.destroy()
    lightRender = null
  }
}
let lightInterval = setInterval(switchLight, 500)
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorR:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorG:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorB:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>brightness:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>roundness:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>falloff:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>