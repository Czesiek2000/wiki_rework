# Graphics::removeDecalsInRange

Removes all decals in range from a position, it includes the bullet holes, blood pools, petrol...
 

## Syntax

```js
mp.game.graphics.removeDecalsInRange(x, y, z, range);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>