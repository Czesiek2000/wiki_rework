# Graphics::hasHudScaleformLoaded

Check to see if hud component Scaleform has loaded?
 

## Syntax

```js
mp.game.graphics.hasHudScaleformLoaded(hudComponent);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hudComponent:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>