# Graphics::drawSpotLightWithShadow

This draws a spotlight with shadow from the source to the target, like a helicopter spotlight.
 

## Syntax

```js
mp.game.graphics.drawSpotLightWithShadow(posX, posY, posZ, dirX, dirY, dirZ, colorR, colorG, colorB, distance, brightness, roundness, radius, falloff, shadow);
```

## Example

```js
// Clientside
let veh = mp.vehicles.at(0); 
let target = mp.players.at(0);
// Examples, use any entity or even just a position as target

mp.events.add('render', () => {
    let forward = veh.getForwardVector();
    // returns the entity forward vector; Example: {"x": 0.5455624, "y": 0.00233556, "z": 0.00021313}
    
    let dist = Math.round(mp.game.system.vdist2(veh.position.x, veh.position.y, veh.position.z, target.position.x, target.position.y, target.position.z) / 3);
    // Calculates the distance between the given coords; you can also use mp.game.gameplay.getDistanceBetweenCoords(), but it's slower

    let dest = new mp.Vector3((target.position.x - veh.position.x - forward.x * 3.4), (target.position.y - veh.position.y - forward.y * 3.4), (target.position.z - veh.position.z - forward.z * 3.4));
    // This is the destinated direction of the spotlight relative to the starting point (start.pos - target.pos - forward.pos * 3.4); "3.4" is just a multiplier to get the correct starting point of the spotlight
    // These are NOT the destinated coordinates, it's the direction where the spotlight have to aim at, relative to the global coordinate system

    let origin = new mp.Vector3(veh.position.x + forward.x * 3.4, veh.position.y + forward.y * 3.4, (veh.position.z + forward.z * 3.4) - 1);
    // Starting point
                                         
    mp.game.graphics.drawSpotLightWithShadow(origin.x, origin.y, origin.z, dest.x, dest.y, dest.z, 255, 255, 255, dist + 10, 10, 1, 5, 1, 0);
    // Draws the spotlight every frame.
});
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>dirZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorR:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorG:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>colorB:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>brightness:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>roundness:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>falloff:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>shadow:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>