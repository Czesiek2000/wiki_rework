# Graphics::enableClownBloodVfx

Creates cartoon effect when Michel smokes the weed
 

## Syntax

```js
mp.game.graphics.enableClownBloodVfx(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>