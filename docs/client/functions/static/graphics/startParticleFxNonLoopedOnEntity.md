# Graphics::startParticleFxNonLoopedOnEntity
Starts a particle effect on an entity for example your player.

[List](https://pastebin.com/N9unUFWY)

Example:C#:
```csharp
Function.Call(Hash.REQUEST_NAMED_PTFX_ASSET, 'scr_rcbarry2'); 
Function.Call(Hash._SET_PTFX_ASSET_NEXT_CALL, 'scr_rcbarry2'); 
Function.Call(Hash.START_PARTICLE_FX_NON_LOOPED_ON_ENTITY, 'scr_clown_appears', Game.Player.Character, 0.0, 0.0, -0.5, 0.0, 0.0, 0.0, 1.0, false, false, false);
//Internally this calls the same function as 
GRAPHICS::START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE
//however it uses -1 for the specified bone index, so it should be possible to start a non looped fx on an entity bone using that native-can confirm 
START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE
// does NOT work on vehicle bones.
```
 

## Syntax

```js
mp.game.graphics.startParticleFxNonLoopedOnEntity(effectName, entity, offsetX, offsetY, offsetZ, rotX, rotY, rotZ, scale, axisX, axisY, axisZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>axisX:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisY:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>axisZ:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>