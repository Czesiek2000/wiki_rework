# Graphics::hasNamedScaleformMovieLoaded

Pretty sure it's the real name (not 100% sure so I added the _ prefix); can someone else confirm it?

Only values used in the scripts are:`heist_mp``heistmap_mp``instructional_buttons``heist_pre`
 

## Syntax

```js
mp.game.graphics.hasNamedScaleformMovieLoaded(scaleformName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>