# Graphics::getScreenActiveResolution

Returns current screen resolution.


## Syntax

```js
var {x, y} = mp.game.graphics.getScreenActiveResolution(x, y);
```

## Example

```js
let x = mp.game.graphics.getScreenActiveResolution(100, 100).x) // Saves the width of the active resolution in x
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b> x, y</li>