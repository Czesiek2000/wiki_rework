# Graphics::loadTvChannel

## Syntax

```js
mp.game.graphics.loadTvChannel(tvChannel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>tvChannel:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>