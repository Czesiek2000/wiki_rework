# Graphics::releaseMovieMeshSet

## Syntax

```js
mp.game.graphics.releaseMovieMeshSet(movieMeshSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>movieMeshSet:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>