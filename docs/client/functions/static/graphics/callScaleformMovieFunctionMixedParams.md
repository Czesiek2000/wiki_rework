# Graphics::callScaleformMovieFunctionMixedParams

Calls the Scaleform function and passes both float and string parameters (in their respective order).

The number of parameters passed to the function varies, so the end of the float parameters is represented by -1.0, and the end of the string parameters is represented by 0 (NULL).

**NOTE: The order of parameters in the function prototype is important! All float parameters must come first, followed by the string parameters.**

Examples:
```cpp
// function MY_FUNCTION(floatParam1, floatParam2, stringParam)
GRAPHICS::_CALL_SCALEFORM_MOVIE_FUNCTION_MIXED_PARAMS(scaleform, 'MY_FUNCTION', 10.0, 20.0, -1.0, -1.0, -1.0, 'String param', 0, 0, 0, 0);
// function MY_FUNCTION_2(floatParam, stringParam1, stringParam2)
GRAPHICS::_CALL_SCALEFORM_MOVIE_FUNCTION_MIXED_PARAMS(scaleform, 'MY_FUNCTION_2', 10.0, -1.0, -1.0, -1.0, -1.0, 'String param #1', 'String param #2', 0, 0, 0);
```
 

## Syntax

```js
mp.game.graphics.callScaleformMovieFunctionMixedParams(scaleform, functionName, floatParam1, floatParam2, floatParam3, floatParam4, floatParam5, stringParam1, stringParam2, stringParam3, stringParam4, stringParam5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>functionName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>floatParam1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>floatParam2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>floatParam3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>floatParam4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>floatParam5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>stringParam1:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>stringParam2:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>stringParam3:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>stringParam4:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>stringParam5:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>