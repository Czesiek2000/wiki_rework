# Graphics::setParticleFxLoopedOffsets

## Syntax

```js
mp.game.graphics.setParticleFxLoopedOffsets(ptfxHandle, x, y, z, rotX, rotY, rotZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ptfxHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>