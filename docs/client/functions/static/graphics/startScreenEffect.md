# Graphics::startScreenEffect

duration - is how long to play the effect for in milliseconds. 

If 0, it plays the default lengthif loop is true, the effect wont stop until you call `_STOP_SCREEN_EFFECT` on it. (only loopable effects)

Example and list of screen FX: [here](https://www.pastebin.com/dafBAjs0)
 

## Syntax

```js
mp.game.graphics.startScreenEffect(effectName, duration, looped);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>effectName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>duration:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>looped:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>