# Graphics::requestHudScaleform
[gtaforums link](http://gtaforums.com/topic/717612-v-scriptnative-documentation-and-research/?p=1068285912)
 

## Syntax

```js
mp.game.graphics.requestHudScaleform(hudComponent);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>hudComponent:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>