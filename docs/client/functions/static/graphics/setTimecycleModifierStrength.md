# Graphics::setTimecycleModifierStrength

## Syntax

```js
mp.game.graphics.setTimecycleModifierStrength(strength);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>strength:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>