# Graphics::setScaleformMovieToUseSystemTime

## Syntax

```js
mp.game.graphics.setScaleformMovieToUseSystemTime(scaleform, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>