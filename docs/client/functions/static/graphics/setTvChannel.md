# Graphics::setTvChannel

## Syntax

```js
mp.game.graphics.setTvChannel(channel);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>channel:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>