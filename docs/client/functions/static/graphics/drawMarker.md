# Graphics::drawMarker

Use this function to draw a marker.
 
This function is used in a *render* event.
 
 

## Syntax

```js
mp.game.graphics.drawMarker(type,
  posX, posY, posZ,
  dirX, dirY, dirZ,
  rotX, rotY, rotZ,
  scaleX, scaleY, scaleZ,
  colorR, colorG, colorB, alpha,
  bobUpAndDown, faceCamera, p19,
  rotate, textureDict, textureName, drawOnEnts
);
```

## Example

This example will create a little marker above the player's head.

```js
new mp.Event('render', () => {
  let pos = mp.players.local.position;

  mp.game.graphics.drawMarker(
    0,
    pos.x, pos.y, pos.z + 2,
    0, 0, 0,
    0, 0, 0,
    1.0, 1.0, 1.0,
    255, 255, 255, 255,
    false, false, 2,
    false, "", "",false
  );
});
```


<br />


## Parameters

<li><b>type</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>posX</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>posY</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>posZ:</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>dirX</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>dirY</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>dirZ</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>rotX</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>rotY</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>rotZ</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>scaleX</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>scaleY</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>scaleZ</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>colorR</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>colorG</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>colorB</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>alpha</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>bobUpAndDown</b>: <b><span style="color:#008017">boolean</span></b></li>
<li><b>faceCamera</b>: <b><span style="color:#008017">boolean</span></b></li>
<li><b>p19</b>: <b><span style="color:#008017">number</span></b></li>
<li><b>rotate</b>: <b><span style="color:#008017">boolean</span></b></li>
<li><b>textureDict</b>: <b><span style="color:#008017">string</span></b></li>
<li><b>textureName</b>: <b><span style="color:#008017">string</span></b></li>
<li><b>drawOnEnts</b>: <b><span style="color:#008017">boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">void</span></b></li>
