# Graphics::drawScaleformMovie

## Syntax

```js
mp.game.graphics.drawScaleformMovie(scaleformHandle, x, y, width, height, red, green, blue, alpha, p9);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleformHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>width:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>height:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>red:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>green:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>blue:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>alpha:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>