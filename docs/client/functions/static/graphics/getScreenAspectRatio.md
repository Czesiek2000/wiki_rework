# Graphics::getScreenAspectRatio

## Syntax

```js
mp.game.graphics.getScreenAspectRatio(b);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>b:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>