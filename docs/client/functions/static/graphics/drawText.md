# Graphics::drawText
 

## Syntax

```js
mp.game.graphics.drawText(text, [x, y [, z]], { font, color, scale, outline, centre });

```

## Example

```js
mp.events.add('render', () => {
    // Draw to screen.
    mp.game.graphics.drawText("Text at the top of the screen", [0.5, 0.005], { 
      font: 7, 
      color: [255, 255, 255, 185], 
      scale: [1.2, 1.2], 
      outline: true
    });

    // Draw to world.
    mp.game.graphics.drawText("Text in the world", [-1234, 1337, 15], { 
      font: 7, 
      color: [255, 255, 255, 185], 
      scale: [1.2, 1.2], 
      outline: true,
      centre: true
    });

    // Draw to screen using red color on a next line.
    mp.game.graphics.drawText("Text at the top of the screen\n~r~Next line in red color", [0.5, 0.005], { 
      font: 7, 
      color: [255, 255, 255, 185], 
      scale: [1.2, 1.2], 
      outline: true
    });
});

```


<br />


## Parameters

<li><b>text:</b> <b><span style="color:#008017">String</span></b>: structured text to show (utilizes <a href="https://wiki.rage.mp/index.php?title=Fonts_and_Colors" title="Fonts and Colors">Fonts_and_Colors</a> and <a class="external text" href="https://en.wikipedia.org/wiki/Control_character" rel="nofollow">Control Characters</a>)</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b>: X position in the screen (0.0 to 1.0)</li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b>: Y position in the screen (0.0 to 1.0)</li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b>: Include Z parameter when rendering to a 3D space (change X and Y to world co-ordinates)</li>
<li><b>font:</b> <b><span style="color:#008017">Int</span></b>: font id</li>
<li><b>color:</b> <b><span style="color:#008017">Array</span></b>: Color of the text plus alpha</li>
<li><b>scale:</b> <b><span style="color:#008017">Array</span></b> ([x, y]): scale of the text (1.0 is a good value)</li>
<li><b>outline:</b> <b><span style="color:#008017">Bool</span></b>: Text has borders or not</li>

<br />
<br />

## Return value

Unknown
