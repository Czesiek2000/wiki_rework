# Graphics::callScaleformMovieFunctionFloatParams

Calls the Scaleform function and passes the parameters as floats.

The number of parameters passed to the function varies, so the end of the parameter list is represented by *-1.0*.
 

## Syntax

```js
mp.game.graphics.callScaleformMovieFunctionFloatParams(scaleform, functionName, param1, param2, param3, param4, param5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scaleform:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>functionName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>param1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>param2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>param3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>param4:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>param5:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>