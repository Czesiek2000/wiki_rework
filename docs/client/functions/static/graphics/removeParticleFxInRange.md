# Graphics::removeParticleFxInRange

## Syntax

```js
mp.game.graphics.removeParticleFxInRange(X, Y, Z, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>X:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>