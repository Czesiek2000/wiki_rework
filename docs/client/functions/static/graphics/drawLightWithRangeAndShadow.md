# Graphics::drawLightWithRangeAndShadow

## Syntax

```js
mp.game.graphics.drawLightWithRangeAndShadow(x, y, z, r, g, b, range, intensity, shadow);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>r:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>g:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>b:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>intensity:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>shadow:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>