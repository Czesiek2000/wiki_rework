# Ped::setSynchronizedSceneLooped

## Syntax

```js
mp.game.ped.setSynchronizedSceneLooped(sceneID, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>