# Ped::detachSynchronizedScene

## Syntax

```js
mp.game.ped.detachSynchronizedScene(sceneID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>