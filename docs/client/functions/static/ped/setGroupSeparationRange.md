# Ped::setGroupSeparationRange

Sets the range at which members will automatically leave the group.
 

## Syntax

```js
mp.game.ped.setGroupSeparationRange(groupHandle, separationRange);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>separationRange:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>