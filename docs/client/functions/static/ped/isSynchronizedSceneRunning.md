# Ped::isSynchronizedSceneRunning

Returns true if a synchronized scene is running
 

## Syntax

```js
mp.game.ped.isSynchronizedSceneRunning(sceneId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>