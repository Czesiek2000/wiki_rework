# Ped::removeGroup

## Syntax

```js
mp.game.ped.removeGroup(groupId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>