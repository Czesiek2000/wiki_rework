# Ped::isSynchronizedSceneLooped

## Syntax

```js
mp.game.ped.isSynchronizedSceneLooped(sceneID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>