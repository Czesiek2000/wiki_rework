# Ped::addRelationshipGroup

Can't select void. 

This function returns nothing.

The hash of the created relationship group is output in the second parameter.
 

## Syntax

```js
mp.game.ped.addRelationshipGroup(name, groupHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>groupHash:</b> Hash</li>

<br />
<br />

## Return value

<li><b>Hash</b></li>