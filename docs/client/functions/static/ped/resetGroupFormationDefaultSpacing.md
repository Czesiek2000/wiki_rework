# Ped::resetGroupFormationDefaultSpacing

## Syntax

```js
mp.game.ped.resetGroupFormationDefaultSpacing(groupHandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupHandle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>