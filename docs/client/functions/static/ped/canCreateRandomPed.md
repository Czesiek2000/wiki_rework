# Ped::canCreateRandomPed

## Syntax

```js
mp.game.ped.canCreateRandomPed(unk);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>unk:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>