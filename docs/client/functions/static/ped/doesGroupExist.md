# Ped::doesGroupExist

## Syntax

```js
mp.game.ped.doesGroupExist(groupId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>