# Ped::getGroupSize

p1 may be a BOOL representing whether or not the group even exists
 

## Syntax

```js
mp.game.ped.getGroupSize(groupID, unknown, sizeInMembers);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>groupID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unknown:</b> unknown (to be checked)</li>
<li><b>sizeInMembers:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b>*<b><span style="color:#008017">Int</span></b></li>