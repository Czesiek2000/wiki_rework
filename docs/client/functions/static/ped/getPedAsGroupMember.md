# Ped::getPedAsGroupMember

from `fm_mission_controller.c4` (variable names changed for clarity):

```cpp
int groupID = PLAYER::GET_PLAYER_GROUP(PLAYER::PLAYER_ID());
PED::GET_GROUP_SIZE(group, &unused, &groupSize);
if (groupSize >= 1) {
    . . . . 
    for (int memberNumber = 0; memberNumber < groupSize; memberNumber++) {
        . . . . . . . . Ped ped1 = PED::GET_PED_AS_GROUP_MEMBER(groupID, memberNumber);
        . . . . . . . . //and so on
```

## Syntax

```js
mp.game.ped.getPedAsGroupMember(groupID, memberNumber);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>memberNumber:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>