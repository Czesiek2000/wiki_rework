# Ped::setPedDensityMultiplierThisFrame

� Usage
<p>
> Use this native inside a looped function.</p>
> Values:

<p>> 0.0 = no peds on streets</p>

<p>> 1.0 = normal peds on streets</p>
 

## Syntax

```js
mp.game.ped.setPedDensityMultiplierThisFrame(multiplier);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>multiplier:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>