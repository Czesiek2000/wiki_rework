# Ped::setGroupFormationSpacing

## Syntax

```js
mp.game.ped.setGroupFormationSpacing(groupId, p1, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>