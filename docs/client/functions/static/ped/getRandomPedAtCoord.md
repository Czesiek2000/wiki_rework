# Ped::getRandomPedAtCoord

Gets a random ped in the x/y/zRadius near the x/y/z coordinates passed. 

Ped Types:

* Any = -1

* Player = 1

* Male = 4

* Female = 5

* Cop = 6

* Human = 26

* SWAT = 27 

* Animal = 28

* Army = 29
 

## Syntax

```js
mp.game.ped.getRandomPedAtCoord(x, y, z, xRadius, yRadius, zRadius, pedType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xRadius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yRadius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zRadius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>pedType:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>