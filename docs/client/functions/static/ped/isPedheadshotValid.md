# Ped::isPedheadshotValid

## Syntax

```js
mp.game.ped.isPedheadshotValid(handle);
```

## Example

```js
let pedHeadShot;
if (pedHeadShot == null) {
    pedHeadShot = mp.players.local.registerheadshot();
    mp.gui.chat.push(`pedHeadShot: ${pedHeadShot}`);
}
if (mp.game.ped.isPedheadshotValid(pedHeadShot) && mp.game.ped.isPedheadshotReady(pedHeadShot)) {
    let headshotTexture = mp.game.ped.getPedheadshotTxdString(pedHeadShot);

    mp.events.add(`render`, () => {
        mp.game.graphics.drawSprite(headshotTexture, headshotTexture, 0.5, 0.5, 0.1, 0.1, 0, 255, 255, 255, 100);
    })
}
```


<br />


## Parameters

<li><b>handle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>