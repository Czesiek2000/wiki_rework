# Ped::disposeSynchronizedScene

## Syntax

```js
mp.game.ped.disposeSynchronizedScene(scene);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scene:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>