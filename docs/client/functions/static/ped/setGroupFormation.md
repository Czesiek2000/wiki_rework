# Ped::setGroupFormation

* 0 Default
* 1 Circle Around Leader
* 2 Alternative Circle Around Leader
* 3 Line, with Leader at center
 

## Syntax

```js
mp.game.ped.setGroupFormation(groupId, formationType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>formationType:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>