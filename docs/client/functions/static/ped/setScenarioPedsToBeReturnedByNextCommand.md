# Ped::setScenarioPedsToBeReturnedByNextCommand

Sets a value indicating whether scenario peds should be returned by the next call to a command that returns peds. 

Eg. `GET_CLOSEST_PED`.
 

## Syntax

```js
mp.game.ped.setScenarioPedsToBeReturnedByNextCommand(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>