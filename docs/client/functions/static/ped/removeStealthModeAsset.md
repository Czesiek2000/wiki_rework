# Ped::removeStealthModeAsset

## Syntax

```js
mp.game.ped.removeStealthModeAsset(asset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>asset:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>