# Ped::isAValidBlushColor

## Syntax

```js
mp.game.ped.isAValidBlushColor(colorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>colorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>