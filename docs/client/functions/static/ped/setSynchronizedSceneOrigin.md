# Ped::setSynchronizedSceneOrigin

## Syntax

```js
mp.game.ped.setSynchronizedSceneOrigin(sceneID, x, y, z, roll, pitch, yaw, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>roll:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>pitch:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yaw:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>