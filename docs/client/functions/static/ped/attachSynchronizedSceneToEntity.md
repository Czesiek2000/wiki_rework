# Ped::attachSynchronizedSceneToEntity

## Syntax

```js
mp.game.ped.attachSynchronizedSceneToEntity(sceneID, entity, boneIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>boneIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>