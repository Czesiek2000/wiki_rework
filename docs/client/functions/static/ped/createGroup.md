# Ped::createGroup

Creates a new ped group.Groups can contain up to 8 peds.

The parameter is unused.

Returns a handle to the created group, or 0 if a group couldn't be created.
 

## Syntax

```js
mp.game.ped.createGroup(unused);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>unused:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>