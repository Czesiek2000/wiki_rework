# Ped::removeRelationshipGroup

## Syntax

```js
mp.game.ped.removeRelationshipGroup(groupHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>