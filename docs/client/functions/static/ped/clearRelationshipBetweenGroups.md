# Ped::clearRelationshipBetweenGroups

Clears the relationship between two groups. This should be called twice (once for each group).

Relationship types:

* 0 Companion
* 1 Respect
* 2 Like
* 3 Neutral
* 4 Dislike
* 5 Hate25
* 5 Pedestrians

*(Credits: Inco)*

Example:
```cpp
PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(2, l_1017, 0xA49E591C);
PED::CLEAR_RELATIONSHIP_BETWEEN_GROUPS(2, 0xA49E591C, l_1017);
```
 

## Syntax

```js
mp.game.ped.clearRelationshipBetweenGroups(relationship, group1, group2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>relationship:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>group1:</b> Model hash or name</li>
<li><b>group2:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>