# Ped::getNumParentPedsOfType

Type equals 0 for male non-dlc, 1 for female non-dlc, 2 for male dlc, and 3 for female dlc.
 

## Syntax

```js
mp.game.ped.getNumParentPedsOfType(type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>