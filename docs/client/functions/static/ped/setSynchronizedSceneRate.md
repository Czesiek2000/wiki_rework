# Ped::setSynchronizedSceneRate

## Syntax

```js
mp.game.ped.setSynchronizedSceneRate(sceneID, rate);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>rate:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>