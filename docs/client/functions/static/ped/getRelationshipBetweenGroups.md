# Ped::getRelationshipBetweenGroups

Gets the relationship between two groups. This should be called twice (once for each group).

Relationship types:
* 0 Companion
* 1 Respect
* 2 Like
* 3 Neutral
* 4 Dislike
* 5 Hate25
* 5 Pedestrians

Example:
```cpp
PED::GET_RELATIONSHIP_BETWEEN_GROUPS(l_1017, 0xA49E591C);
PED::GET_RELATIONSHIP_BETWEEN_GROUPS(0xA49E591C, l_1017);
```
 

## Syntax

```js
mp.game.ped.getRelationshipBetweenGroups(group1, group2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>group1:</b> Model hash or name</li>
<li><b>group2:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>