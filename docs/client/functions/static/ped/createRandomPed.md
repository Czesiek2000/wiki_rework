# Ped::createRandomPed

vb.net

```vb
Dim ped_handle As Integer With Game.
Player.Character Dim pos As Vector3 = .Position + .ForwardVector * 3 
ped_handle = Native.Function.Call(Of Integer)(Hash.CREATE_RANDOM_PED, pos.X, pos.Y, pos.Z)
End
```

WithCreates a Ped at the specified location, returns the Ped Handle. Ped will not act until `SET_PED_AS_NO_LONGER_NEEDED` is called.


## Syntax

```js
mp.game.ped.createRandomPed(posX, posY, posZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>