# Ped::createSynchronizedScene

p6 always 2 (but it doesnt seem to matter...)

roll and pitch 0

yaw to Ped.rotation
 

## Syntax

```js
mp.game.ped.createSynchronizedScene(x, y, z, roll, pitch, yaw, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>roll:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>pitch:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yaw:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>