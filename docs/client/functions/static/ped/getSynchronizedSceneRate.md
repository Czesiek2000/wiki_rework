# Ped::getSynchronizedSceneRate

## Syntax

```js
mp.game.ped.getSynchronizedSceneRate(sceneID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>