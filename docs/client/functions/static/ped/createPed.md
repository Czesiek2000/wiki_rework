# Ped::createPed

-- OUTDATED --
 
p7 - last parameter does not mean ped handle is returnedmaybe a quick view in disassembly will tell us what is actually does

*Heading*: 0.0

*Heading* is the Z axis spawn rotation of the ped 0->5th parameter.

Ped Types:[Player,1|Male,4|Female,5|Cop,6|Human,26|SWAT,27|Animal,28|Army,29]

You can also use `GET_PED_TYPE`
 

## Syntax

```js
mp.game.ped.createPed(pedType, modelHash, x, y, z, heading, networkHandle, pedHandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pedType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>networkHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>pedHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>