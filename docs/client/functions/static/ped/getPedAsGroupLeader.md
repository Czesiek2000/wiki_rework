# Ped::getPedAsGroupLeader

## Syntax

```js
mp.game.ped.getPedAsGroupLeader(groupID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>groupID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Ped handle or object</b></li>