# Ped::getNumHeadOverlayValues

Used with freemode (online) characters.
 

## Syntax

```js
mp.game.ped.getNumHeadOverlayValues(overlayID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>overlayID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>