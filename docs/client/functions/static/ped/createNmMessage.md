# Ped::createNmMessage

*MulleDK19*: Creates a new NaturalMotion message.

startImmediately: If set to true, the character will perform the message the moment it receives it by `GIVE_PED_NM_MESSAGE`. 

If false, the Ped will get the message but won't perform it yet. While it's a boolean value, if negative, the message will not be initialized.

messageId: The ID of the NaturalMotion message.

If a message already exists, this function does nothing. A message exists until the point it has been successfully dispatched by `GIVE_PED_NM_MESSAGE`.
 

## Syntax

```js
mp.game.ped.createNmMessage(startImmediately, messageId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>startImmediately:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>messageId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>