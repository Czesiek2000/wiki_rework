# Ped::setAiWeaponDamageModifier

This will change from 10 shots until that with normal pistol to 3 or 4 shots.
 

## Syntax

```js
mp.game.ped.setAiWeaponDamageModifier(value);
```

## Example

```js
mp.game.ped.setAiWeaponDamageModifier(1.5);
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>