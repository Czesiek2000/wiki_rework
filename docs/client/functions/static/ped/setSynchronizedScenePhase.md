# Ped::setSynchronizedScenePhase

## Syntax

```js
mp.game.ped.setSynchronizedScenePhase(sceneID, phase);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>phase:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>