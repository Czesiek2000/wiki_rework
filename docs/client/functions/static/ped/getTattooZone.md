# Ped::getTattooZone

Returns the zoneID for the overlay if it is a member of collection.

```cpp
enum TattooZoneData { 
    ZONE_TORSO = 0,
    ZONE_HEAD = 1,
    ZONE_LEFT_ARM = 2,
    ZONE_RIGHT_ARM = 3,
    ZONE_LEFT_LEG = 4,
    ZONE_RIGHT_LEG = 5,
    ZONE_UNKNOWN = 6,
    ZONE_NONE = 7,
};
```
 

## Syntax

```js
mp.game.ped.getTattooZone(collection, overlay);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>collection:</b> Model hash or name</li>
<li><b>overlay:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>