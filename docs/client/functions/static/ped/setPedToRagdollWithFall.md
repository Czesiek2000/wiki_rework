# Ped::setPedToRagdollWithFall

## Syntax

```js
mp.game.ped.setPedToRagdollWithFall(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>p8:</b> unknown (to be checked)</li>
<li><b>p9:</b> unknown (to be checked)</li>
<li><b>p10:</b> unknown (to be checked)</li>
<li><b>p11:</b> unknown (to be checked)</li>
<li><b>p12:</b> unknown (to be checked)</li>
<li><b>p13:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>