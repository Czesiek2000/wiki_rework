# Ped::getFirstParentIdForPedType

Type equals 0 for male non-dlc, 1 for female non-dlc, 2 for male dlc, and 3 for female dlc.

Used when calling SET_PED_HEAD_BLEND_DATA.
 

## Syntax

```js
mp.game.ped.getFirstParentIdForPedType(type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>