# Streaming::removeNamedPtfxAsset

console hash: `0xC44762A1`


## Syntax

```js
mp.game.streaming.removeNamedPtfxAsset(fxName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>fxName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>