# Streaming::removeClipSet

Alias for `REMOVE_ANIM_SET`.


## Syntax

```js
mp.game.streaming.removeClipSet(clipSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>clipSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>