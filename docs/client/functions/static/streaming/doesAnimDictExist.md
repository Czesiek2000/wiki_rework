# Streaming::doesAnimDictExist

## Syntax

```js
mp.game.streaming.doesAnimDictExist(animDict);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>