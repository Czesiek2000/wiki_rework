# Streaming::requestCollisionAtCoord

## Syntax

```js
mp.game.streaming.requestCollisionAtCoord(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>