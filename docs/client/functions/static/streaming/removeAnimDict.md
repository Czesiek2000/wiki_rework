# Streaming::removeAnimDict

## Syntax

```js
mp.game.streaming.removeAnimDict(animDict);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>