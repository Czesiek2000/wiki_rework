# Streaming::isModelValid

Returns whether the specified model exists in the game.


## Syntax

```js
mp.game.streaming.isModelValid(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>