# Streaming::requestAnimSet

Starts loading the specified animation set. An animation set provides movement animations for a ped. 

See `SET_PED_MOVEMENT_CLIPSET`.
 

## Syntax

```js
mp.game.streaming.requestAnimSet(animSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>animSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>