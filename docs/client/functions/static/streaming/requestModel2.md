# Streaming::requestModel2

It appears that this native requests only ped models, more specifically the main characters (Michael, Franklin and Trevor). 

Unconfirmed!
 

## Syntax

```js
mp.game.streaming.requestModel2(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>