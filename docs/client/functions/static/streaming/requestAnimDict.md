# Streaming::requestAnimDict

This function is required when using client-side animations, you should preload the anim dictionary before using it, otherwise the animation won't work.

The example below preloads an animation and applies it into the local player.


## Syntax

```js
mp.game.streaming.requestAnimDict(animDict);
```

## Example

```js
mp.game.streaming.requestAnimDict("[email protected]_robbery");
mp.players.local.taskPlayAnim("[email protected]_robbery", "robbery_action_f", 8.0, 1.0, -1, 1, 1.0, false, false, false);
```


<br />


## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>