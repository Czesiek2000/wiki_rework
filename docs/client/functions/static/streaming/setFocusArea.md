# Streaming::setFocusArea

Override the area where the camera will render the terrain.

p3, p4 and p5 are usually set to 0.0

**After moving to another area, you need to clear focus. This can be done using the following native:**

```js
mp.game.invoke('0x31B73D1EA9F01DA2');
```

## Syntax

```js
mp.game.streaming.setFocusArea(x, y, z, offsetX, offsetY, offsetZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>offsetZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>