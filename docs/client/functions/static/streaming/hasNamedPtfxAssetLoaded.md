# Streaming::hasNamedPtfxAssetLoaded

## Syntax

```js
mp.game.streaming.hasNamedPtfxAssetLoaded(fxName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>fxName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>