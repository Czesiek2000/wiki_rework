# Streaming::isModelInCdimage

Check if model is in cdimage(rpf)


## Syntax

```js
mp.game.streaming.isModelInCdimage(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>