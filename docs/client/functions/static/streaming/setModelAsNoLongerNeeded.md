# Streaming::setModelAsNoLongerNeeded

Unloads model from memory


## Syntax

```js
mp.game.streaming.setModelAsNoLongerNeeded(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>