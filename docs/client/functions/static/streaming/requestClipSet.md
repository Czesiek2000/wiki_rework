# Streaming::requestClipSet

## Syntax

```js
mp.game.streaming.requestClipSet(clipSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>clipSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>