# Streaming::setDitchPoliceModels

*MulleDK19*: This is a NOP function. It does nothing at all.


## Syntax

```js
mp.game.streaming.setDitchPoliceModels(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>