# Streaming::requestIpl

Requests for the IPL to be loaded into the map

List of IPLs [here](http://pastebin.com/FyV5mMma) or [here](https://wiki.rage.mp/index.php?title=Interiors_and_Locations)
 
Loads the FIB interior so it can be accessed.
 

## Syntax

```js
mp.game.streaming.requestIpl(iplName);

```

## Example

```js
mp.game.streaming.requestIpl("FIBlobbyfake");

```


<br />


## Parameters

<li><b>iplName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>