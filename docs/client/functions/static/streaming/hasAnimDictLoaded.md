# Streaming::hasAnimDictLoaded

## Syntax

```js
mp.game.streaming.hasAnimDictLoaded(animDict);

```

## Example

```js
mp.game.streaming.requestAnimDict("[email protected][email protected]@base");
new Promise((resolve, reject) => {
	const timer = setInterval(() => {
		if(mp.game.streaming.hasAnimDictLoaded("[email protected][email protected]@base")) {
			clearInterval(timer);
			resolve();
		}
	}, 100);
});

```


<br />


## Parameters

<li><b>animDict:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>