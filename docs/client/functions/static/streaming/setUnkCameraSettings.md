# Streaming::setUnkCameraSettings

## Syntax

```js
mp.game.streaming.setUnkCameraSettings(x, y, z, rad, p4, p5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rad:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>