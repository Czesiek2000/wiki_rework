# Streaming::isModelAVehicle

Returns whether the specified model represents a vehicle.


## Syntax

```js
mp.game.streaming.isModelAVehicle(model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>