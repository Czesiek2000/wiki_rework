# Streaming::removeIpl

Removes an IPL from the map.

IPL List [here](https://pastebin.com/pwkh0uRP) 

Example:

C#:
```csharp
Function.Call(Hash.REMOVE_IPL, 'trevorstrailertidy');
```

C++:
```cpp
STREAMING::REMOVE_IPL('trevorstrailertidy');
```

iplName = Name of IPL you want to remove.

## Syntax

```js
mp.game.streaming.removeIpl(iplName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>iplName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>