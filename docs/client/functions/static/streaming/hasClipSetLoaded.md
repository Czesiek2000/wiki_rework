# Streaming::hasClipSetLoaded

Alias for `HAS_ANIM_SET_LOADED`.


## Syntax

```js
mp.game.streaming.hasClipSetLoaded(clipSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>clipSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>