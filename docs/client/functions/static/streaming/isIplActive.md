# Streaming::isIplActive

## Syntax

```js
mp.game.streaming.isIplActive(iplName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>iplName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>