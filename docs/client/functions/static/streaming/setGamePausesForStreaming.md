# Streaming::setGamePausesForStreaming

## Syntax

```js
mp.game.streaming.setGamePausesForStreaming(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>