# Streaming::removeAnimSet
Unloads the specified animation set. An animation set provides movement animations for a ped. 

See `SET_PED_MOVEMENT_CLIPSET`.

Animation set and clip set are synonymous.
 

## Syntax

```js
mp.game.streaming.removeAnimSet(animSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>animSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>