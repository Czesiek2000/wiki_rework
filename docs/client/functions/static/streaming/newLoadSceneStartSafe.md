# Streaming::newLoadSceneStartSafe

```cpp
if (!sub_8f12('START LOAD SCENE SAFE')) { 
    if (CUTSCENE::GET_CUTSCENE_TIME() > 4178) { 
        STREAMING::_ACCFB4ACF53551B0(1973.845458984375, 3818.447265625, 32.43629837036133, 15.0, 2); 
        sub_8e9e('START LOAD SCENE SAFE', 1); 
    }
}
```
 

## Syntax

```js
mp.game.streaming.newLoadSceneStartSafe(p0, p1, p2, p3, p4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>