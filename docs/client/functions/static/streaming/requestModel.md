# Streaming::requestModel

Request a model to be loaded into memoryLooking it the disassembly, it seems like it actually returns the model if it's already loaded.


## Syntax

```js
mp.game.streaming.requestModel(modelHash);
```

## Example

```js
mp.game.streaming.requestModel(mp.game.joaat('a_f_y_juggalo_01'));
```


<br />


## Parameters

<li><b>model:</b> Model hash</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>