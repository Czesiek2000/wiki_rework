# App::appSetBlock

## Syntax

```js
mp.game.app.appSetBlock(blockName);
```

## Example

```js
// todo
```


<br />


## Parameters


<li><b>blockName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>