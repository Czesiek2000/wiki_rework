# App::appSetApp
Called in the gamescripts like:APP::APP_SET_APP('car');APP::APP_SET_APP('dog');
 

## Syntax

```js
mp.game.app.appSetApp(appName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>appName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>