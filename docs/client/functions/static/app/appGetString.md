# App::appGetString

## Syntax

```js
mp.game.app.appGetString(property);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>property:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>