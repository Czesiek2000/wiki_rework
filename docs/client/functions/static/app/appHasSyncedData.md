# App::appHasSyncedData

## Syntax

```js
mp.game.app.appHasSyncedData(appName);
```

## Example

```js
// todo
```


<br />


## Parameters


<li><b>appName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>