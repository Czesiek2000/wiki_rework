# App::appGetFloat

## Syntax

```js
mp.game.app.appGetFloat(property);
```

## Example

```js
// todo
```


<br />


## Parameters


<li><b>property:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>