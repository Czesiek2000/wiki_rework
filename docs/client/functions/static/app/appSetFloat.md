# App::appSetFloat

## Syntax

```js
mp.game.app.appSetFloat(property, value);
```

## Example

```js
// todo
```


<br />


## Parameters


<li><b>property:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>