# Interior::unpinInterior

Does something similar to `INTERIOR::DISABLE_INTERIOR`.

You don't fall through the floor but everything is invisible inside and looks the same as when `INTERIOR::DISABLE_INTERIOR` is used. 

Peds behaves normally inside.
 

## Syntax

```js
mp.game.interior.unpinInterior(interiorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>