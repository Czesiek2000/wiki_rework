# Interior::refreshInterior

## Syntax

```js
mp.game.interior.refreshInterior(interiorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>