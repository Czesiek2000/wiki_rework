# Interior::capInterior

Does something similar to `INTERIOR::DISABLE_INTERIOR`
 

## Syntax

```js
mp.game.interior.capInterior(interiorID, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>