# Interior::hideMapObjectThisFrame

This is the native that is used to hide the exterior of GTA Online apartment buildings when you are inside an apartment.

More info [here](http://gtaforums.com/topic/836301-hiding-gta-online-apartment-exteriors/)
 

## Syntax

```js
mp.game.interior.hideMapObjectThisFrame(mapObjectHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>mapObjectHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>