# Interior::getInteriorAtCoords

Returns interior ID from specified coordinates. If coordinates are outside, then it returns 0.

Example for VB.NET

```vb
Dim interiorID As Integer = Native.Function.Call(Of Integer)(Hash.GET_INTERIOR_AT_COORDS, X, Y, Z)
```

## Syntax

```js
mp.game.interior.getInteriorAtCoords(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>int</b></li>