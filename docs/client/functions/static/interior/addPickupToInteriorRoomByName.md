# Interior::addPickupToInteriorRoomByName

## Syntax

```js
mp.game.interior.addPickupToInteriorRoomByName(pickup, roomName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickup:</b> <b><span style="color:#008017">Pickup</span></b></li>
<li><b>roomName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>