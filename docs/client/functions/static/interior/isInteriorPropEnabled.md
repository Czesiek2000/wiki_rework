# Interior::isInteriorPropEnabled

See [Interior Props](https://czesiek2000.gitlab.io/docs/interiors) page for a list of interior prop names and interior IDs.

## Syntax

```js
mp.game.interior.isInteriorPropEnabled(interiorID, propName);
```

## Example

Check if bunker interior has "gun_range_blocker_set" prop enabled:

```js
if (mp.game.interior.isInteriorPropEnabled(258561, "gun_range_blocker_set")) {
    mp.gui.chat.push("Bunker doesn't have gun range.");
} else {
    mp.gui.chat.push("Bunker has gun range.");
}

```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>propName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>