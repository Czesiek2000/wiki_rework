# Interior::getInteriorGroupId
Returns the group ID of the specified interior. For example, regular interiors have group 0, subway interiors have group 1. There are a few other groups too.
 

## Syntax

```js
mp.game.interior.getInteriorGroupId(interiorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>