# Interior::enableInteriorProp

See Interior Props page for a list of interior prop names and interior IDs.
 
You need to refresh the interior by using Interior::refreshInterior after enabling/disabling interior props.
 
Change the floor of the CEO mod shop at Maze Bank Building:
 

## Syntax

```js
mp.game.interior.enableInteriorProp(interiorID, propName);
```

## Example

```js
mp.game.interior.enableInteriorProp(255233, "floor_vinyl_08");
mp.game.interior.refreshInterior(255233);

```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>propName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>