# Interior::getInteriorAtCoordsWithType

Returns the interior ID representing the requested interior at that location (if found?). 

The supplied interior string is not the same as the one used to load the interior.

Use: `INTERIOR::UNPIN_INTERIOR(INTERIOR::GET_INTERIOR_AT_COORDS_WITH_TYPE(x, y, z, interior))`

Interior types include: 'V_Michael', 'V_Franklins', 'V_Franklinshouse', etc.. you can find them in the scripts.

Not a very useful native as you could just use GET_INTERIOR_AT_COORDS instead and get the same result, without even having to specify the interior type.
 

## Syntax

```js
mp.game.interior.getInteriorAtCoordsWithType(x, y, z, interiorType);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>interiorType:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>