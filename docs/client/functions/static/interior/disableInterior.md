# Interior::disableInterior

Example: This removes the interior from the strip club and when trying to walk inside the player just falls:`INTERIOR::DISABLE_INTERIOR(118018, true);`
 

## Syntax

```js
mp.game.interior.disableInterior(interiorID, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>