# Interior::unkGetInteriorAtCoords

Returns the interior ID at the given coords, but only if the unknown variable is set to 0, otherwise it will return 0.
 

## Syntax

```js
mp.game.interior.unkGetInteriorAtCoords(x, y, z, unk);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unk:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>