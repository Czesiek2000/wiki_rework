# Interior::disableInteriorProp

See Interior Props page for a list of interior prop names and interior IDs.
 
You need to refresh the interior by using Interior::refreshInterior after enabling/disabling interior props.
 
Remove clutter from the document forgery office:
 

## Syntax

```js
mp.game.interior.disableInteriorProp(interiorID, propName);
```

## Example

```js
mp.game.interior.disableInteriorProp(246785, "clutter");
mp.game.interior.refreshInterior(246785);
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>propName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>