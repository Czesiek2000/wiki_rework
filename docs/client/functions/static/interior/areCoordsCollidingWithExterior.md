# Interior::areCoordsCollidingWithExterior

Returns true if the coords are colliding with the outdoors, and false if they collide with an interior.
 

## Syntax

```js
mp.game.interior.areCoordsCollidingWithExterior(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>