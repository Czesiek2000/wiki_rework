# Interior::isInteriorCapped

## Syntax

```js
mp.game.interior.isInteriorCapped(interiorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>