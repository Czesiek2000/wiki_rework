# Interior::isInteriorReady

## Syntax

```js
mp.game.interior.isInteriorReady(interiorID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>interiorID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>