# Object::removeDoorFromSystem

## Syntax

```js
mp.game.object.removeDoorFromSystem(doorHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>