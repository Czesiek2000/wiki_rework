# Object::getPickupCoords

## Syntax

```js
mp.game.object.getPickupCoords(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>