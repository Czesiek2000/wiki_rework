# Object::getStateOfClosestDoorOfType

locked is 0 if no door is foundlocked is 0 if door is unlockedlocked is 1 if door is found and unlocked.

the locked bool is either 0(unlocked)(false) or 1(locked)(true)
 

## Syntax

```js
mp.game.object.getStateOfClosestDoorOfType(type, x, y, z, locked, heading);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>locked:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> locked, heading</li>