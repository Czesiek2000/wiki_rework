# Object::createPickupRotate

## Syntax

```js
mp.game.object.createPickupRotate(pickupHash, posX, posY, posZ, rotX, rotY, rotZ, flag, amount, p9, p10, modelHash);
```

## Example

```js
// adds a health pickup of 50 hp at the bar in sandy shores on the ground.
   mp.game.object.createPickupRotate(-1888453608, 1985.56, 3050.966 + 3, 47.21, 0, 0, 0, 8, 50, 0, true, mp.game.joaat('prop_ld_health_pack'));

// adds a health pickup of 50 hp at the bar in sandy shores spinning in the air.
   mp.game.object.createPickupRotate(-1888453608, 1985.56, 3050.966 + 3, 47.21, 0, 0, 0, 512, 50, 0, true, mp.game.joaat('prop_ld_health_pack'));
```


<br />


## Parameters

<li><b>8</b>: Places the pickup on the ground.</li>
<li><b>512</b>: Spins the pickup 360°</li>

<br />
<br />

## Return value

<li><b>pickupHash:</b> <b>Model</b> (<b>Hash <font color="red">int</font> or Name <font color="gray">String</font></b>)</li>