# Object::doorControl

when you set locked to false the door open and to true the door close
```js
mp.game.object.doorControl(mp.game.joaat('v_ilev_cs_door'), 482.911, -1312.584, 29.20103, false, 0.0, 50.0, 0); //door open
mp.game.object.doorControl(mp.game.joaat('v_ilev_cs_door'), 482.911, -1312.584, 29.20103, true, 0.0, 50.0, 0); //door close
```

p5-7 - Rot?

[doorHash](https://wiki.gtanet.work/index.php?title=Doors)
 

## Syntax

```js
mp.game.object.doorControl(doorHash, x, y, z, locked, p5, p6, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>locked:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>