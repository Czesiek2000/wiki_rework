# Object::setDoorAjarAngle

Sets the ajar angle of a door.

Ranges from -1.0 to 1.0, and 0.0 is closed / default.

p2 is always false, p3 is always true.
 

## Syntax

```js
mp.game.object.setDoorAjarAngle(doorHash, ajar, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>
<li><b>ajar:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>