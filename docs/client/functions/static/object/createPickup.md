# Object::createPickup

## Syntax

```js
mp.game.object.createPickup(pickupHash, posX, posY, posZ, p4, value, p6, modelHash);
```

## Example

```js
/*create a health pickup 1 time 
mp.game.object.createPickup(pickupHash, posX, posY, posZ, p4(rotation), value(ammo, money depends on pickup), p6(bool), modelHash); 

P6 = didn't see a difference between true/false

*/

mp.events.add("pickup", () => { mp.game.object.createPickup(2406513688, -605.2752, -1631.4392, 33.325176, 250, 1, true, 2406513688); });
```


<br />


## Parameters

<li><b>pickupHash:</b> Model hash or name</li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Pickup</span></b></li>