# Object::setDoorAccelerationLimit

Sets the acceleration limit of a door.

How fast it can open, or the inverse hinge resistance.

A limit of 0 seems to lock doors.

p2 is always 0, p3 is always 1.
 

## Syntax

```js
mp.game.object.setDoorAccelerationLimit(doorHash, limit, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>
<li><b>limit:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>