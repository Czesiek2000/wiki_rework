# Object::getObjectFragmentDamageHealth

## Syntax

```js
mp.game.object.getObjectFragmentDamageHealth(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>