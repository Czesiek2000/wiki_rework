# Object::removeAllPickupsOfType

## Syntax

```js
mp.game.object.removeAllPickupsOfType(pickupHash);
```

## Example

```js
let pickHash = 0x2E4C762D;
mp.game.object.removeAllPickupsOfType(pickHash);
```


<br />


## Parameters

<li><b>pickupHash:</b> Hash</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>