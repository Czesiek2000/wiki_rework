# Object::doesDoorExist

Always used prior a door functions 

Example 
```cpp
if (OBJECT::_DOES_DOOR_EXIST(doorHash)) { 
    OBJECT::REMOVE_DOOR_FROM_SYSTEM(doorHash);
}
```
 

## Syntax

```js
mp.game.object.doesDoorExist(doorHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>