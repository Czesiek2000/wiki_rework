# Object::highlightPlacementCoords

draws circular marker at pos

* -1 none
* 0 red
* 1 green
* 2 blue
* 3 green larger
* 4 nothing
* 5 green small
 

## Syntax

```js
mp.game.object.highlightPlacementCoords(x, y, z, colorIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>colorIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>