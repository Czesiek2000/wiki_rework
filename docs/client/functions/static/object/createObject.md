# Object::createObject

p5 - last parameter does not mean object handle is returnedmaybe a quick view in disassembly will tell us what is actually does
 

## Syntax

```js
mp.game.object.createObject(modelHash, x, y, z, networkHandle, createHandle, dynamic);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>modelHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>networkHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>createHandle:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>dynamic:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Object handle or object</b></li>