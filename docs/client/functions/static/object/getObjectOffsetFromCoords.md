# Object::getObjectOffsetFromCoords

## Syntax

```js
mp.game.object.getObjectOffsetFromCoords(xPos, yPos, zPos, heading, xOffset, yOffset, zOffset);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>xPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zPos:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>xOffset:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>yOffset:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>zOffset:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>