# Object::removePickup

## Syntax

```js
mp.game.object.removePickup(pickup);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickup:</b> <b><span style="color:#008017">Pickup</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>