# Object::isObjectNearPoint

## Syntax

```js
mp.game.object.isObjectNearPoint(objectHash, x, y, z, range);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>objectHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>range:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>