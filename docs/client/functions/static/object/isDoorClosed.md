# Object::isDoorClosed

## Syntax

```js
mp.game.object.isDoorClosed(door);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>door:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>