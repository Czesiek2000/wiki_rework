# Object::createPortablePickup2

## Syntax

```js
mp.game.object.createPortablePickup2(pickupHash, x, y, z, placeOnGround, modelHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickupHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>placeOnGround:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Pickup</span></b></li>