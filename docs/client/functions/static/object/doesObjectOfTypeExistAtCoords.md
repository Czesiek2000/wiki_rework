# Object::doesObjectOfTypeExistAtCoords

p5 is usually 0.
 

## Syntax

```js
mp.game.object.doesObjectOfTypeExistAtCoords(x, y, z, radius, hash, p5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>hash:</b> Model hash or name</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>