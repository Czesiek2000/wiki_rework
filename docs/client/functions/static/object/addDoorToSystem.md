# Object::addDoorToSystem

## Syntax

```js
mp.game.object.addDoorToSystem(doorHash, modelHash, x, y, z, p5, p6, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>doorHash:</b> Model hash or name</li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>