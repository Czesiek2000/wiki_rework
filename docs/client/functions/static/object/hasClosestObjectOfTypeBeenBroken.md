# Object::hasClosestObjectOfTypeBeenBroken

## Syntax

```js
mp.game.object.hasClosestObjectOfTypeBeenBroken(p0, p1, p2, p3, modelHash, p5);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>p5:</b> unknown boolean</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>