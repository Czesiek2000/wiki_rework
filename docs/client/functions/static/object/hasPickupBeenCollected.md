# Object::hasPickupBeenCollected

## Syntax

```js
mp.game.object.hasPickupBeenCollected(p0);
```

## Example

```js
let pickupHandle = mp.game.object.createPickupRotate(-1888453608, 1985.56, 3050.966 + 3, 47.21, 0, 0, 0, 8, 50, 0, true, mp.game.joaat('prop_ld_health_pack'));
if (mp.game.object.hasPickupBeenCollected(pickupHandle)) // do something
```


<br />


## Parameters

<li><b>Pickup:</b> <b><span style="color:#008017">Int</span></b> (Pickup handle)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>