# Object::createAmbientPickup

## Syntax

```js
mp.game.object.createAmbientPickup(pickupHash, posX, posY, posZ, p4, value, modelHash, p7, p8);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickupHash:</b> Model hash or name</li>
<li><b>posX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>posZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Pickup</b></li>