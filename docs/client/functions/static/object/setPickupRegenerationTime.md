# Object::setPickupRegenerationTime

## Syntax

```js
mp.game.object.setPickupRegenerationTime(Pickup, Duration);
```

## Example

```js
// Creates a rotating health pickup with 50hp and respawning time 10 mins
let pickup = mp.game.object.createPickupRotate(-1888453608, 1985.56, 3050.966 + 3, 47.21, 0, 0, 0, 9, 50, 0, true, mp.game.joaat('prop_ld_health_pack'));
mp.game.object.setPickupRegenerationTime(pickup, 600000);
```


<br />


## Parameters

<li><b>Pickup:</b> Pickup handle <font color="green"><b>int</b></font></li>
<li><b>Duration:</b> Time <font color="red"><b>int (ms)</b></font></li>

<br />
<br />

## Return value

<li><b>Unknown</b></li>