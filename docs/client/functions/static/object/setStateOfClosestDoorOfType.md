# Object::setStateOfClosestDoorOfType

Hardcoded to not work in multiplayer.Used to lock/unlock doors to interior areas of the game.

[(Possible) Door Types](https://pastebin.com/9S2m3qA4)

Heading is either 1, 0 or -1 in the scripts. Means default closed(0) or opened either into(1) or out(-1) of the interior.

Locked means that the heading is locked. p6 is always 0. 225 door types, model names and coords found in stripclub.c4:[pastebin](https://pastebin.com/gywnbzsH)

get door info [here](https://pastebin.com/i14rbekD)
 

## Syntax

```js
mp.game.object.setStateOfClosestDoorOfType(type, x, y, z, locked, heading, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>type:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>locked:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>