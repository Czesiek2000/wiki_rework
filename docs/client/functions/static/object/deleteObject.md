# Object::deleteObject

Deletes the specified object, then sets the handle pointed to by the pointer to NULL.
 

## Syntax

```js
mp.game.object.deleteObject(object);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>object:</b> <b><span style="color:#008017">Object</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Object</span></b></li>