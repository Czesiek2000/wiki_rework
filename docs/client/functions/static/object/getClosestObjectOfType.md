# Object::getClosestObjectOfType

Has 8 params in the latest patches.

isMission - if true doesn't return mission objects
 

## Syntax

```js
mp.game.object.getClosestObjectOfType(x, y, z, radius, modelHash, isMission, p6, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>modelHash:</b> Model hash or name</li>
<li><b>isMission:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Object handle</b></li>