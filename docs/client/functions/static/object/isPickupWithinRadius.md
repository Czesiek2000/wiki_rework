# Object::isPickupWithinRadius

## Syntax

```js
mp.game.object.isPickupWithinRadius(pickupHash, x, y, z, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>pickupHash:</b> Model hash or name</li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>