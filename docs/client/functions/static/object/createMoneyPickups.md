# Object::createMoneyPickups

Spawns one or more money pickups.

x: The X-component of the world position to spawn the money pickups at.

y: The Y-component of the world position to spawn the money pickups at.

z: The Z-component of the world position to spawn the money pickups at.

value: The combined value of the pickups (in dollars).

amount: The number of pickups to spawn.

model: The model to use, or 0 for default money model.

Example:
```cpp
CREATE_MONEY_PICKUPS(x, y, z, 1000, 3, 0x684a97ae);
```
Spawns 3 spray cans that'll collectively give $1000 when picked up. (Three spray cans, each giving $334, $334, $332 = $1000).
 

## Syntax

```js
mp.game.object.createMoneyPickups(x, y, z, value, amount, model);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>amount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>model:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>