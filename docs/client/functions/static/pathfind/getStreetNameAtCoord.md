# Pathfind::getStreetNameAtCoord
Determines the name of the street which is the closest to the given coordinates.x,y,z - the coordinates of the streetstreetName - returns a hash to the name of the street the coords are oncrossingRoad - if the coordinates are on an intersection, a hash to the name of the crossing 

**roadNote: the names are returned as hashes, the strings can be returned using the function `UI::GET_STREET_NAME_FROM_HASH_KEY`**.
 
If you want the real streetname you have to use:
[Ui::getStreetNameFromHashKey(ragemp wiki)](https://wiki.rage.mp/index.php?title=Ui::getStreetNameFromHashKey) [Ui::getStreetNameFromHashKey (this wiki)](../ui/getStreetNameFromHashKey.md)
 

## Syntax

```js
mp.game.pathfind.getStreetNameAtCoord(x, y, z, streetName, crossingRoad);
```

## Example

```js
// Clientside
const local = mp.players.local; 

let getStreet = mp.game.pathfind.getStreetNameAtCoord(local.position.x, local.position.y, local.position.z, 0, 0);
// Returns obj {"streetName": hash, crossingRoad: hash}
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>streetName:</b> Hash</li>
<li><b>crossingRoad:</b> Hash</li>

<br />
<br />

## Return value

<li><b>object:</b> streetName, crossingRoad</li>