# Pathfind::loadAllPathNodes

Loads all path nodes.

If keepInMemory is true, all path nodes will be loaded and be kept in memory; otherwise, all path nodes will be loaded, but unloaded as the game sees fit.

*MulleDK19.*
 

## Syntax

```js
mp.game.pathfind.loadAllPathNodes(keepInMemory);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>keepInMemory:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>