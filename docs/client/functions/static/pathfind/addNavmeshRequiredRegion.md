# Pathfind::addNavmeshRequiredRegion

## Syntax

```js
mp.game.pathfind.addNavmeshRequiredRegion(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>