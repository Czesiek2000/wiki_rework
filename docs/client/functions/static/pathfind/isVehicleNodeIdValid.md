# Pathfind::isVehicleNodeIdValid

Returns true if the id is non zero.
 

## Syntax

```js
mp.game.pathfind.isVehicleNodeIdValid(vehicleNodeId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>vehicleNodeId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>