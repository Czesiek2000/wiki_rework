# Pathfind::getNthClosestVehicleNodeId

Returns the id.
 

## Syntax

```js
mp.game.pathfind.getNthClosestVehicleNodeId(x, y, z, nth, nodetype, p5, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nth:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>nodetype:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>int</b></li>