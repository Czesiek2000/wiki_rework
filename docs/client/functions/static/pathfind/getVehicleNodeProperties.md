# Pathfind::getVehicleNodeProperties

*MulleDK19*: Gets the density and flags of the closest node to the specified position.Density is a value between 0 and 15, indicating how busy the road is.

Flags is a bit field.
 

## Syntax

```js
mp.game.pathfind.getVehicleNodeProperties(x, y, z, density, flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>density:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> density, flags</li>