# Pathfind::getSafeCoordForPed
When onGround == true outPosition is a position located on the nearest pavement.

When a safe coord could not be found the result of a function is false and outPosition == Vector3.Zero.

In the scripts these flags are used: 0, 14, 12, 16, 20, 21, 28. 0 is most commonly used, then 16. 16 works for me, 0 crashed the script.
 

## Syntax

```js
mp.game.pathfind.getSafeCoordForPed(x, y, z, onGround, outPosition, flags);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>onGround:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>