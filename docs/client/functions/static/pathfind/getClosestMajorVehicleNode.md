# Pathfind::getClosestMajorVehicleNode

Get the closest vehicle node to a given position, unknown1 = 3.0, unknown2 = 0
 

## Syntax

```js
mp.game.pathfind.getClosestMajorVehicleNode(x, y, z, outPosition, unknown1, unknown2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>unknown1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>