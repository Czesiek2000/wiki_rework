# Pathfind::getRandomVehicleNode

## Syntax

```js
mp.game.pathfind.getRandomVehicleNode(x, y, z, radius, p4, p5, p6, outPosition, heading);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> outPosition, heading</li>