# Pathfind::getSupportsGpsRouteFlag
p0 = `VEHICLE_NODE_ID`

Returns false for nodes that aren't used for GPS routes.

Example: Nodes in Fort Zancudo and LSIA are false
 

## Syntax

```js
mp.game.pathfind.getSupportsGpsRouteFlag(nodeID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>nodeID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>