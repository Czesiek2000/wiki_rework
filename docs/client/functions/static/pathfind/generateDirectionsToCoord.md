# Pathfind::generateDirectionsToCoord

I'm Not MentaL: 

Example Usage:
```vb
Public Function GenerateDirectionsToCoord(Pos As Vector3) As Tuple(Of String, Single, Single) 
Dim f4, f5, f6 As New OutputArgument() 
Native.Function.Call(Hash.GENERATE_DIRECTIONS_TO_COORD, Pos.X, Pos.Y, Pos.Z, True, f4, f5, f6) 
Dim direction As String = f4.GetResult(Of Single)() 
Return New Tuple(Of String, Single, Single)(direction.Substring(0, 1), f5.GetResult(Of Single)(), f6.GetResult(Of Single)()) 
End Function
```
p3 I use 1 direction:
* 0 = You Have Arrive
* 1 = Recalculating Route, Please make a u-turn where safe
* 2 = Please Proceed the Highlighted Route
* 3 = Keep Left (unsure)
* 4 = In (distToNxJunction) Turn Left
* 5 = In (distToNxJunction) Turn Right
* 6 = Keep Right (unsure)
* 7 = In (distToNxJunction) Go Straight Ahead
* 8 = In (distToNxJunction) Join the freeway
* 9 = In (distToNxJunction) Exit Freeway return value set to 0 always
 

## Syntax

```js
mp.game.pathfind.generateDirectionsToCoord(x, y, z, p3, p4, vehicle, p6);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b> (true)</li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>vehicle:</b> <b><span style="color:#008017">Vehicle</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> p4, vehicle, p6</li>