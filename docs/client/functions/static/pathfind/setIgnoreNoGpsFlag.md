# Pathfind::setIgnoreNoGpsFlag

## Syntax

```js
mp.game.pathfind.setIgnoreNoGpsFlag(ignore);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ignore:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>