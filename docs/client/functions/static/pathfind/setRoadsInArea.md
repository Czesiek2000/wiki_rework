# Pathfind::setRoadsInArea

/* Corrected conflicting parameter names */
 

## Syntax

```js
mp.game.pathfind.setRoadsInArea(x1, y1, z1, x2, y2, z2, unknown1, unknown2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>unknown2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>