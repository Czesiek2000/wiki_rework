# Pathfind::getClosestRoad

## Syntax

```js
mp.game.pathfind.getClosestRoad(x, y, z, p3, p4, p5, p6, p7, p8, p9, p10);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> unknown (to be checked)</li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> unknown (to be checked)</li>
<li><b>p8:</b> unknown (to be checked)</li>
<li><b>p9:</b> unknown (to be checked)</li>
<li><b>p10:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>