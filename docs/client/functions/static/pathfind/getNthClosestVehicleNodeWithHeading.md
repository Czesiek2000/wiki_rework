# Pathfind::getNthClosestVehicleNodeWithHeading

Get the nth closest vehicle node and its heading. (unknown2 = 9, unknown3 = 3.0, unknown4 = 2.5)
 

## Syntax

```js
mp.game.pathfind.getNthClosestVehicleNodeWithHeading(x, y, z, nthClosest, outPosition, heading, unknown1, unknown2, unknown3, unknown4);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nthClosest:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>heading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown1:</b> unknown (to be checked)</li>
<li><b>unknown2:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>unknown3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>unknown4:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>object:</b> outPosition, heading, unknown1, , ,</li>