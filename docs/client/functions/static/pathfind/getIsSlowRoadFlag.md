# Pathfind::getIsSlowRoadFlag

p0 = VEHICLE_NODE_ID

Returns true when the node is Offroad. Alleys, some dirt roads, and carparks return true.Normal roads where plenty of Peds spawn will return false
 

## Syntax

```js
mp.game.pathfind.getIsSlowRoadFlag(nodeID);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>nodeID:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>