# Pathfind::getNthClosestVehicleNodeIdWithHeading

## Syntax

```js
mp.game.pathfind.getNthClosestVehicleNodeIdWithHeading(x, y, z, nthClosest, outPosition, outHeading, p6, p7, p8);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nthClosest:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>outHeading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p6:</b> unknown (to be checked)</li>
<li><b>p7:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>