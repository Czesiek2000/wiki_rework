# Pathfind::isPointOnRoad

Gets a value indicating whether the specified position is on a road.The vehicle parameter is not implemented (ignored).

*MulleDK19*
 

## Syntax

```js
mp.game.pathfind.isPointOnRoad(x, y, z, vehicle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>vehicle:</b> Vehicle handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>