# Pathfind::getNthClosestVehicleNode

## Syntax

```js
mp.game.pathfind.getNthClosestVehicleNode(x, y, z, nthClosest, outPosition, unknown1, unknown2, unknown3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nthClosest:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>unknown1:</b> unknown (to be checked)</li>
<li><b>unknown2:</b> unknown (to be checked)</li>
<li><b>unknown3:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>