# Pathfind::getNthClosestVehicleNodeFavourDirection

[See](https://gtaforums.com/topic/843561-pathfind-node-types) for node type info. 
0 = paved road only, 1 = any road, 3 = water

p10 always equal `0x40400000`

p11 always equal 0
 

## Syntax

```js
mp.game.pathfind.getNthClosestVehicleNodeFavourDirection(x, y, z, desiredX, desiredY, desiredZ, nthClosest, outPosition, outHeading, nodetype, p10, p11);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>desiredX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>desiredY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>desiredZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nthClosest:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>outHeading:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>nodetype:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p10:</b> unknown (to be checked)</li>
<li><b>p11:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>object:</b> outPosition, outHeading, , ,</li>