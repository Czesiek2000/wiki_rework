# Fire::isExplosionInAngledArea

## Syntax

```js
mp.game.fire.isExplosionInAngledArea(explosionType, x1, y1, z1, x2, y2, z2, angle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>explosionType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>angle:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Boolean</b></li>