# Fire::startScriptFire

Starts a fire:

xyz: Location of fire

maxChildren: The max amount of times a fire can spread to other objects. Must be 25 or less, or the function will do nothing.

isGasFire: Whether or not the fire is powered by gasoline.
 

## Syntax

```js
mp.game.fire.startScriptFire(X, Y, Z, maxChildren, isGasFire);

```

## Example

```js
mp.events.add('StartFire', (posX, posY, posZ, maxChildren, isGasFire) => {
    // The fireId is a int
    let fireId = mp.game.fire.startScriptFire(posX, posY, posZ, maxChildren, isGasFire);
});

```


<br />


## Parameters

<li><b>X:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>Z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>maxChildren:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>isGasFire:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>