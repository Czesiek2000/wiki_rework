# Fire::addSpecfxExplosion

## Syntax

```js
mp.game.fire.addSpecfxExplosion(x, y, z, explosionType, explosionFx, damageScale, isAudible, isInvisible, cameraShake);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>explosionType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>explosionFx:</b> Model hash or name</li>
<li><b>damageScale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>isAudible:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>isInvisible:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>cameraShake:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>