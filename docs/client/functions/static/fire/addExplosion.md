# Fire::addExplosion
Creates an explosion at given coords.

This function is missing last argument, so you cant create explosion with damage.
 

## Syntax

```js
mp.game.fire.addExplosion(x, y, z, explosionType, damageScale, isAudible, isInvisible, cameraShake);
```

## Example

```js


```


<br />


## Parameters

<li><b>x: <font color="green">float</font></b></li>
<li><b>y: <font color="green">float</font></b></li>
<li><b>z: <font color="green">float</font></b></li>
<li><b>explosionType: <font color="red">int</font></b>
<ul><li><a href="/index.php?title=Explosions" title="Explosions">Explosion Types</a></li></ul></li>
<li><b>damageScale: <font color="green">float</font></b></li>
<li><b>isAudible: <font color="blue">Boolean</font></b> (Trigger explosion sound)</li>
<li><b>isInvisible: <font color="blue">Boolean</font></b> (Trigger visibility)</li>
<li><b>cameraShake: <font color="green">float</font></b></li>

<br />

[Explosion types](../../../../tables/explosionTypes.md)

<br />

## Return value

<li><b>Undefined</b></li>