# Fire::getClosestFirePos

Returns Vector3 if it found something. FALSE if not.
 

## Syntax

```js
mp.game.fire.getClosestFirePos(outPosition, x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>outPosition:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Vector3</span></b></li>