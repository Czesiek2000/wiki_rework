# Fire::getPedInsideExplosionArea

Returns a handle to the first entity within the a circle spawned inside the 2 points from a radius. 

It could return a ped or an entity, but the scripts expect a ped, but still check if it's a ped.
 

## Syntax

```js
mp.game.fire.getPedInsideExplosionArea(explosionType, x1, y1, z1, x2, y2, z2, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>explosionType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Entity handle or object</b></li>