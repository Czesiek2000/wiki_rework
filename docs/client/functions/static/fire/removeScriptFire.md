# Fire::removeScriptFire

## Syntax

```js
mp.game.fire.removeScriptFire(fireHandle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>fireHandle:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>