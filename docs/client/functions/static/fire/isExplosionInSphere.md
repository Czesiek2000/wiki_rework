# Fire::isExplosionInSphere

## Syntax

```js
mp.game.fire.isExplosionInSphere(explosionType, x, y, z, radius);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>explosionType:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>