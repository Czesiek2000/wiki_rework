# Weapon::getWeapontypeModel

Returns the model of any weapon.

Can also take an ammo hash?

`sub_6663a(&l_115B, WEAPON::GET_WEAPONTYPE_MODEL(${ammo_rpg}));`


## Syntax

```js
mp.game.weapon.getWeapontypeModel(weaponHash);
```

## Example

```js
mp.keys.bind(0x76, false, function () { // F7 key
    let weaponHash = mp.game.invoke(`0x0A6DB4965674D243`, mp.players.local.handle); //GET_SELECTED_PED_WEAPON
    let weaponModel = mp.game.weapon.getWeapontypeModel(weaponHash);
    mp.gui.chat.push(`hash: ${weaponHash}, model: ${weaponModel}`);
})
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>