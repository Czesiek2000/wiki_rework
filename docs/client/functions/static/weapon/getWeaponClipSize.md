# Weapon::getWeaponClipSize

Returns the size of the default weapon component clip.

Use it like this:
```cpp
char cClipSize[32];
Hash cur;
if (WEAPON::GET_CURRENT_PED_WEAPON(playerPed, &cur, 1)){ 
    if (WEAPON::IS_WEAPON_VALID(cur)) { 
        int iClipSize = WEAPON::GET_WEAPON_CLIP_SIZE(cur); 
        sprintf_s(cClipSize, 'ClipSize: %.d', iClipSize); 
        vDrawString(cClipSize, 0.5f, 0.5f); 
    }
}
```
 

## Syntax

```js
mp.game.weapon.getWeaponClipSize(weaponHash);
```

## Example

```js
mp.keys.bind(0x76, false, function () { // F7 key
    let weaponHash = mp.game.invoke(`0x0A6DB4965674D243`, mp.players.local.handle); //GET_SELECTED_PED_WEAPON
    let clipSize = mp.game.weapon.getWeaponClipSize(weaponHash);
    mp.gui.chat.push(`hash: ${weaponHash}, clipSize: ${clipSize}`);
})
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>