# Weapon::removeAllProjectilesOfType

p1 seems always to be 0


## Syntax

```js
mp.game.weapon.removeAllProjectilesOfType(weaponHash, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>