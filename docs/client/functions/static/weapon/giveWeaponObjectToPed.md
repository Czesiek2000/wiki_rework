# Weapon::giveWeaponObjectToPed

## Syntax

```js
mp.game.weapon.giveWeaponObjectToPed(weaponObject, ped);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponObject:</b> Object handle or object</li>
<li><b>ped:</b> Ped handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>