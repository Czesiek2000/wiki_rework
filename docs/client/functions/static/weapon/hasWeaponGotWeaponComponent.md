# Weapon::hasWeaponGotWeaponComponent

## Syntax

```js
mp.game.weapon.hasWeaponGotWeaponComponent(weapon, addonHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weapon:</b> Object handle or object</li>
<li><b>addonHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>