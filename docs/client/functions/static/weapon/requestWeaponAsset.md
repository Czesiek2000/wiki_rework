# Weapon::requestWeaponAsset

Nearly every instance of p1 I found was 31. Nearly every instance of p2 I found was 0.

`REQUEST_WEAPON_ASSET(iLocal_1888, 31, 26);`


## Syntax

```js
mp.game.weapon.requestWeaponAsset(weaponHash, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>