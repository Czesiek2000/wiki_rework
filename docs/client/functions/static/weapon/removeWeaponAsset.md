# Weapon::removeWeaponAsset

## Syntax

```js
mp.game.weapon.removeWeaponAsset(weaponHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>