# Weapon::setWeaponObjectTintIndex

## Syntax

```js
mp.game.weapon.setWeaponObjectTintIndex(weapon, tint);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weapon:</b> Entity handle or object</li>
<li><b>tint:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>