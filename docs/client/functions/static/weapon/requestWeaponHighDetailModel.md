# Weapon::requestWeaponHighDetailModel

## Syntax

```js
mp.game.weapon.requestWeaponHighDetailModel(weaponObject);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponObject:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>