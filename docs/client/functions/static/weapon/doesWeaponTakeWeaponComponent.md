# Weapon::doesWeaponTakeWeaponComponent

## Syntax

```js
mp.game.weapon.doesWeaponTakeWeaponComponent(weaponHash, componentHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>
<li><b>componentHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>