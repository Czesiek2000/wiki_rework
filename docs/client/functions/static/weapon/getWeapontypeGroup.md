# Weapon::getWeapontypeGroup

Retrieves the weapon group of the supplied weapon hash.

Weapon Groups (Incomplete list)

<li><b>melee</b>: 2685387236</li>
<li><b>Handguns</b>: 416676503</li>
<li><b>Submachine Gun</b>: 3337201093</li>
<li><b>Shotgun</b>: 860033945</li>
<li><b>Assault Rifle</b>: 970310034</li>
<li><b>Light Machine Gun</b>: 1159398588</li>
<li><b>Sniper</b>: 3082541095</li>
<li><b>Heavy Weapon</b>: 2725924767</li>
<li><b>Throwables</b>: 1548507267</li>
<li><b>Misc</b>: 4257178988</li>

## Syntax

```js
mp.game.weapon.getWeapontypeGroup(weaponHash);
```

## Example

```js
mp.keys.bind(0x76, false, function () { // F7 key
    let weaponHash = mp.game.invoke(`0x0A6DB4965674D243`, mp.players.local.handle); //GET_SELECTED_PED_WEAPON
    let groupHash = mp.game.weapon.getWeapontypeGroup(weaponHash);
    mp.gui.chat.push(`hash: ${weaponHash}, groupHash: ${groupHash}`);
})
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Weapon Group Hash</b></li>
