# Weapon::getWeaponDamageType

* 0: unknown (or incorrect weaponHash)
* 1  no damage (flare,snowball, petrolcan)
* 2 melee
* 3: bullet
* 4: force ragdoll fall
* 5: explosive (RPG, Railgun, grenade)
* 6: fire(molotov)
* 8: fall(WEAPON_HELI_CRASH)
* 10: electric
* 11: barbed wire
* 12: extinguisher
* 13: gas
* 14: water cannon(WEAPON_HIT_BY_WATER_CANNON)


## Syntax

```js
mp.game.weapon.getWeaponDamageType(weaponHash);
```

## Example

```js
mp.keys.bind(0x76, false, function () { // F7 key
    let weaponHash = mp.game.invoke(`0x0A6DB4965674D243`, mp.players.local.handle); //GET_SELECTED_PED_WEAPON
    let damageType = mp.game.weapon.getWeaponDamageType(weaponHash);
    mp.gui.chat.push(`hash: ${weaponHash}, damageType: ${damageType}`);
})
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>