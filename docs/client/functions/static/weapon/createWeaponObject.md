# Weapon::createWeaponObject

Now has 8 params.


## Syntax

```js
mp.game.weapon.createWeaponObject(weaponHash, ammoCount, x, y, z, showWorldModel, scale, p7);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>
<li><b>ammoCount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>showWorldModel:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>scale:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p7:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Object handle or object</b></li>