# Weapon::getWeaponHudStats
```cpp
struct WeaponHudStatsData { 
    BYTE hudDamage; // 0x0000
    char _0x0001[0x7]; // 0x0001 
    BYTE hudSpeed; // 0x0008 
    char _0x0009[0x7]; // 0x0009
    BYTE hudCapacity; // 0x0010 
    char _0x0011[0x7]; // 0x0011
    BYTE hudAccuracy; // 0x0018 
    char _0x0019[0x7]; // 0x0019 
    BYTE hudRange; // 0x0020
};
```
Usage:
```cpp
WeaponHudStatsData data;
if (GET_WEAPON_HUD_STATS(weaponHash, (int *)&data)){ 
    // BYTE damagePercentage = data.hudDamage and so on
}
```
 

## Syntax

```js
mp.game.weapon.getWeaponHudStats(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>