# Weapon::getWeaponTintCount

## Syntax

```js
mp.game.weapon.getWeaponTintCount(weaponHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>