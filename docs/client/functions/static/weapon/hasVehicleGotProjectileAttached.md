# Weapon::hasVehicleGotProjectileAttached

Third Parameter = unsure, but pretty sure it is weapon hash --> get_hash_key('weapon_stickybomb')

Fourth Parameter = unsure, almost always -1


## Syntax

```js
mp.game.weapon.hasVehicleGotProjectileAttached(driver, vehicle, weapon, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>driver:</b> Ped handle or object</li>
<li><b>vehicle:</b> Vehicle handle or object</li>
<li><b>weapon:</b> Model hash or name</li>
<li><b>p3:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>