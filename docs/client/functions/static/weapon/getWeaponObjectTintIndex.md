# Weapon::getWeaponObjectTintIndex

## Syntax

```js
mp.game.weapon.getWeaponObjectTintIndex(weapon);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weapon:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>