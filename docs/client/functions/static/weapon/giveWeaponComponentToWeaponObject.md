# Weapon::giveWeaponComponentToWeaponObject

addonHash:
```
(use WEAPON::GET_WEAPON_COMPONENT_TYPE_MODEL() to get hash value)

${component_at_ar_flsh}, ${component_at_ar_supp}, ${component_at_pi_flsh}, ${component_at_scope_large}, ${component_at_ar_supp_02}
```
 

## Syntax

```js
mp.game.weapon.giveWeaponComponentToWeaponObject(weaponObject, addonHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponObject:</b> Object handle or object</li>
<li><b>addonHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>