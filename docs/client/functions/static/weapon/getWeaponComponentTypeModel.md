# Weapon::getWeaponComponentTypeModel

## Syntax

```js
mp.game.weapon.getWeaponComponentTypeModel(componentHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>componentHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>