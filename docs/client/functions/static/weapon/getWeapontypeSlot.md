# Weapon::getWeapontypeSlot

## Syntax

```js
mp.game.weapon.getWeapontypeSlot(weaponHash);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>