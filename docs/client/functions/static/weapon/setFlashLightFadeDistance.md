# Weapon::setFlashLightFadeDistance

## Syntax

```js
mp.game.weapon.setFlashLightFadeDistance(distance);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>distance:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>