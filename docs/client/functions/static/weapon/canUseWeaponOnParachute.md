# Weapon::canUseWeaponOnParachute

## Syntax

```js
mp.game.weapon.canUseWeaponOnParachute(weaponHash);
```

## Example

```js
mp.keys.bind(0x76, false, function () { // F7 key
    let weaponHash = mp.game.invoke(`0x0A6DB4965674D243`, mp.players.local.handle); //GET_SELECTED_PED_WEAPON
    let canUse = mp.game.weapon.canUseWeaponOnParachute(weaponHash);
    mp.gui.chat.push(`hash: ${weaponHash}, canUse: ${canUse}`);
})
```


<br />


## Parameters

<li><b>weaponHash:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>