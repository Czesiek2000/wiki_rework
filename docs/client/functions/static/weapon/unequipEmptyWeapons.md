# Weapon::unequipEmptyWeapons

Toggle if you want to stop the player from putting away a weapon when it runs out of ammo.


## Syntax

```js
mp.game.weapon.unequipEmptyWeapons = toggle;
```

## Example

Disabled unequipping the weapon when it runs out of ammo.

```js
mp.game.weapon.unequipEmptyWeapons = false;
```


<br />


## Parameters

<li><b><span style="color:#008017">Bool</span></b> - Enable/Disable unequip</li>


<br />
<br />

## Return value

No return value here
