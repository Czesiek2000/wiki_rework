# Recorder::start
Starts recording a replay.

If mode is 0, turns on action replay.

If mode is 1, starts recording.

If already recording a replay, does nothing.

## Syntax

```js
mp.game.recorder.start(mode);
```

## Example

```js
mp.keys.bind(0x72, false, () => { // F3
  if (!mp.game.recorder.isRecording()) {
    mp.game.recorder.start(1);
  } else {
    mp.game.recorder.stop();
  }
});
```


<br />


## Parameters

<li><b>mode:</b> Number: recording mode</li>

<br />
<br />

## Return value

<li><b>Unknown</b></li>