# Datafile::arrayValueGetType

Types:
* 1: Boolean
* 2: Integer
* 3: Float
* 4: String
* 5: Vector3
* 6: Object
* 7: Array
 

## Syntax

```js
mp.game.datafile.arrayValueGetType(arrayData, arrayIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>
<li><b>arrayIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>