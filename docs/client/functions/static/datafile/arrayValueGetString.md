# Datafile::arrayValueGetString

## Syntax

```js
mp.game.datafile.arrayValueGetString(arrayData, arrayIndex);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>
<li><b>arrayIndex:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>