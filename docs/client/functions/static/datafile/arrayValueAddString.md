# Datafile::arrayValueAddString

## Syntax

```js
mp.game.datafile.arrayValueAddString(arrayData, value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>
<li><b>value:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>