# Datafile::arrayValueAddBoolean

## Syntax

```js
mp.game.datafile.arrayValueAddBoolean(arrayData, value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>
<li><b>value:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>