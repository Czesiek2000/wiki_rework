# Datafile::objectValueGetType
Types:1 = Boolean2 = Integer3 = Float4 = String5 = Vector36 = Object7 = Array
 

## Syntax

```js
mp.game.datafile.objectValueGetType(objectData, key);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>objectData:</b> unknown (to be checked)</li>
<li><b>key:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>