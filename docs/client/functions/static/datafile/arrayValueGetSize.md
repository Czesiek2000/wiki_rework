# Datafile::arrayValueGetSize

## Syntax

```js
mp.game.datafile.arrayValueGetSize(arrayData);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>