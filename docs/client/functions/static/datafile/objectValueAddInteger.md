# Datafile::objectValueAddInteger

## Syntax

```js
mp.game.datafile.objectValueAddInteger(objectData, key, value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>objectData:</b> unknown (to be checked)</li>
<li><b>key:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>