# Datafile::objectValueAddVector3

## Syntax

```js
mp.game.datafile.objectValueAddVector3(objectData, key, valueX, valueY, valueZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>objectData:</b> unknown (to be checked)</li>
<li><b>key:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>valueX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>valueY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>valueZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>