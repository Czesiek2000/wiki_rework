# Datafile::arrayValueAddVector3

## Syntax

```js
mp.game.datafile.arrayValueAddVector3(arrayData, valueX, valueY, valueZ);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>arrayData:</b> unknown (to be checked)</li>
<li><b>valueX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>valueY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>valueZ:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>