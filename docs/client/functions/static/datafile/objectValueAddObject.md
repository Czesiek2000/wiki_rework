# Datafile::objectValueAddObject

## Syntax

```js
mp.game.datafile.objectValueAddObject(objectData, key);
```

## Example

```js
// todo
```


<br />


## Parameters

to highlight parameters: : <b><span style="color:#008017">String</span></b>

<li><b>objectData:</b> unknown (to be checked)</li>
<li><b>key:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>