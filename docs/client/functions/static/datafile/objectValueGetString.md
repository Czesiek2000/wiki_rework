# Datafile::objectValueGetString

## Syntax

```js
mp.game.datafile.objectValueGetString(objectData, key);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>objectData:</b> unknown (to be checked)</li>
<li><b>key:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>