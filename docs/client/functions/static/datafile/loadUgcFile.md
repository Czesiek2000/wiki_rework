# Datafile::loadUgcFile

Loads a User-Generated Content (UGC) file. 

These files can be found in `[GTA5]/data/ugc` and `[GTA5]/common/patch/ugc`. 

They seem to follow a naming convention, most likely of `[name]_[part].ugc`. 

See example below for usage.Returns whether or not the file was successfully loaded.

Example:
```cpp
DATAFILE::_LOAD_UGC_FILE('RockstarPlaylists') // loads 'rockstarplaylists_00.ugc'
``` 

## Syntax

```js
mp.game.datafile.loadUgcFile(filename);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>filename:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>