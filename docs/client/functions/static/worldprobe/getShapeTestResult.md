# Worldprobe::getShapeTestResult

Parameters:

rayHandle - Ray Handle from a casted ray, as returned by `CAST_RAY_POINT_TO_POINT`

hit - Where to store whether or not it hit anything. False is when the ray reached its destination.

endCoords - Where to store the world-coords of where the ray was stopped (by hitting its desired max range or by colliding with an entity/the map)

surfaceNormal - Where to store the surface-normal coords (NOT relative to the game world) of where the entity was hit by the ray

entityHit - Where to store the handle of the entity hit by the rayReturns:Result? Some type of enum.

**NOTE: To get the offset-coords of where the ray hit relative to the entity that it hit (which is NOT the same as surfaceNormal), you can use these two natives:**

```cpp
Vector3 offset = ENTITY::GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(entityHit, endCoords.x, endCoords.y, endCoords.z);
Vector3 entitySpotCoords = ENTITY::GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityHit, offset.x, offset.y, offset.z);
```
Use `ENTITY::GET_ENTITY_TYPE(entityHit)` to quickly distinguish what type of entity you hit (ped/vehicle/object - 1/2/3)
 

## Syntax

```js
mp.game.worldprobe.getShapeTestResult(rayHandle, hit, endCoords, surfaceNormal, entityHit);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rayHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>hit:</b> <b><span style="color:#008017">Bool</span></b></li>
<li><b>endCoords:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>surfaceNormal:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>entityHit:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>object:</b> hit, endCoords, surfaceNormal, entityHit</li>