# Worldprobe::startShapeTestLosProbe

Returns a ray (?) going from x1, y1, z1 to x2, y2, z2.

entity = 0 most of the time.

p8 = 7 most of the time.

Result of this function is passed to `WORLDPROBE::_GET_RAYCAST_RESULT` as a first argument.


## Syntax

```js
mp.game.worldprobe.startShapeTestLosProbe(x1, y1, z1, x2, y2, z2, flags, entity, p8);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>p8:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>