# Worldprobe::startShapeTestCapsule

Raycast from point to point, where the ray has a radius. 

flags:
* vehicles = 10
* peds = 12

Iterating through flags yields many ped / vehicle/ object combinations

p9 = 7, but no idea what it doesEntity is an entity to ignore


## Syntax

```js
mp.game.worldprobe.startShapeTestCapsule(x1, y1, z1, x2, y2, z2, radius, flags, entity, p9);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>radius:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>p9:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>