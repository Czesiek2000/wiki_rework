# Worldprobe::startShapeTestBox

## Syntax

```js
mp.game.worldprobe.startShapeTestBox(x, y, z, sizeX, sizeY, sizeZ, rotationX, rotationY, rotationZ, rotationOrder, flags, entity, p12);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>sizeX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>sizeY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>sizeZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotationX:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotationY:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotationZ:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>rotationOrder:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>flags:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>ignoreEntity:</b> <b><span style="color:#008017">Int</span></b> (Entity handle)</li>
<li><b>p12:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>