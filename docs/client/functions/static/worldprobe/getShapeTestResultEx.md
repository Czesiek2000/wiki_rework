# Worldprobe::getShapeTestResultEx

behaves exactly the same way as `GET_SHAPE_TEST_RESULT` except it has one extra parameter (_materialHash).

Quick disassembly seems to indicate that the unknown is a hash. 

EDIT: Seems to be the hash of the hit material or surface type.

found a `materialFX.dat` list of them but not sure if it has to do with this native yet.
 

## Syntax

```js
mp.game.worldprobe.getShapeTestResultEx(rayHandle, hit, endCoords, surfaceNormal, _materialHash, entityHit);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>rayHandle:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>hit:</b> <b><span style="color:#008017">Bool</span></b></li>
<li><b>endCoords:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>surfaceNormal:</b> <b><span style="color:#008017">Vector3</span></b></li>
<li><b>_materialHash:</b> Hash</li>
<li><b>entityHit:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>object:</b> hit, endCoords, surfaceNormal, _materialHash, entityHit</li>