# Decorator::decorIsRegisteredAsType

Is property of that type.
* 1: float
* 2: bool
* 3: int
* 5: time
 

## Syntax

```js
mp.game.decorator.decorIsRegisteredAsType(propertyName, type);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>propertyName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>type:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>