# System::startNewStreamedScriptWithArgs

## Syntax

```js
mp.game.system.startNewStreamedScriptWithArgs(scriptHash, args, argCount, stackSize);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>
<li><b>args:</b> unknown (to be checked)</li>
<li><b>argCount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>stackSize:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>