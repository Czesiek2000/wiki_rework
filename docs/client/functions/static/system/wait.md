# System::wait

Pauses execution of the current script, please note this behavior is only seen when called from one of the game script files(ysc). In order to wait an asi script use `static void WAIT(DWORD time);`

found in `main.h` 

It does not actually seem to wait the amount of milliseconds stated like the normal `WAIT()` command does, but it does seem to make task sequences work more smoothlySystem native hashes do not change on gameupdate
 

## Syntax

```js
mp.game.system.wait(ms);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ms:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>