# System::ceil

I'm guessing this rounds a float value up to the next whole number, and FLOOR rounds it down
 

## Syntax

```js
mp.game.system.ceil(value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>