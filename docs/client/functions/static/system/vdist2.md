# System::vdist2

Calculates distance between vectors but does not perform Sqrt operations. (Its way faster)


## Syntax

```js
mp.game.system.vdist2(x1, y1, z1, x2, y2, z2);
```

## Example

```js
// see image above

// orange point
const x1 = 0;
const y1 = 0;
const z1 = 0;

// red point
const x2 = 0;
const y2 = 0;
const z2 = 2;

const vdist2Result = mp.game.system.vdist2(
    x1, y1, z1, 
    x2, y2, z2,
); // returns (!) 4 (need to square root before using as a distance value)

const vdistResult = mp.game.system.vdist(
    x1, y1, z1, 
    x2, y2, z2,
); // returns 2 (real distance between points)
```


<br />


## Parameters

<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>