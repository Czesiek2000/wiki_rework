# System::isFocused


Value to show if the user has RAGE Multiplayer focused or if they're tabbed out.
 

 

## Syntax

```js
mp.system.isFocused;

```

## Example

```js
// To do

```


<br />


## Parameters

No paramters here

<br />
<br />

## Return value

No return value here