# System::startNewScriptWithArgs

return : script thread id, 0 if failedPass pointer to struct of args in p1, size of struct goes into p2


## Syntax

```js
mp.game.system.startNewScriptWithArgs(scriptName, args, argCount, stackSize);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>args:</b> unknown (to be checked)</li>
<li><b>argCount:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>stackSize:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>