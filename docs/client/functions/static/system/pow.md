# System::pow

## Syntax

```js
mp.game.system.pow(base, exponent);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>base:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>exponent:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>