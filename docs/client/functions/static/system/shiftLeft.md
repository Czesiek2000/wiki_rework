# System::shiftLeft

## Syntax

```js
mp.game.system.shiftLeft(value, bitShift);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>bitShift:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>