# System::isFullscreen


True/False if the client's window is in Fullscreen mode.
 

 

## Syntax

```js
mp.system.isFullscreen;

```

## Example

```js
// To do

```


<br />


## Parameters

No parameters here

<br />
<br />

## Return value

No return value here
