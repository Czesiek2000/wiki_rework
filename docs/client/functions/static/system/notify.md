# System::notify

Sends the user a Windows notification.

A server will notify a player 5 minutes before they're kicked for AFK idle.


## Syntax

```js
mp.system.notify({ title, text, attribute, duration, silent });
```

## Example

```js
mp.events.add('AFK_Reminder', () => {
    mp.system.notify({
        title: 'AFK Timer',
        text: `You have 5 minutes until you're kicked from the server.`,
        attribute: 'Test Roleplay',
        duration: 5,
        silent: true
    });
});
```


<br />


## Parameters

<li><b>title</b> <b><span style="color:#008017">String</span></b></li>
<li><b>text</b> <b><span style="color:#008017">String</span></b></li>
<li><b>attribute</b> <b><span style="color:#008017">String</span></b></li>
<li><b>duration</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>silent</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

No return value here
