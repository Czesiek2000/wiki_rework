# System::startNewStreamedScript

## Syntax

```js
mp.game.system.startNewStreamedScript(scriptHash, stackSize);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scriptHash:</b> Model hash or name</li>
<li><b>stackSize:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>