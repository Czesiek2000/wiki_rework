# Stats::statGetString
p1 is always -1 in the script files
 

## Syntax

```js
mp.game.stats.statGetString(statHash, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statHash:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>