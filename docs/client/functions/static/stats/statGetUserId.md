# Stats::statGetUserId

Needs more research. Seems to return 'STAT_UNKNOWN' if no such user id exists.


## Syntax

```js
mp.game.stats.statGetUserId(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>