# Stats::statSave

## Syntax

```js
mp.game.stats.statSave(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>