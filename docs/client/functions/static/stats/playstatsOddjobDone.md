# Stats::playstatsOddjobDone

## Syntax

```js
mp.game.stats.playstatsOddjobDone(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>