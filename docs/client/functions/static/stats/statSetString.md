# Stats::statSetString

## Syntax

```js
mp.game.stats.statSetString(statName, value, save);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>value:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>save:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>