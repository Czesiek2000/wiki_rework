# Stats::statSetInt

Add Cash example:

```cpp
for (int i = 0; i < 3; i++){
    char statNameFull[32];
    sprintf_s(statNameFull, 'SP%d_TOTAL_CASH', i);
    Hash hash = GAMEPLAY::GET_HASH_KEY(statNameFull);
    int val;
    STATS::STAT_GET_INT(hash, &val, -1);
    val += 1000000;
    STATS::STAT_SET_INT(hash, val, 1);
}
```
 

## Syntax

```js
mp.game.stats.statSetInt(statName, value, save);
```

## Example

```js
mp.game.stats.statSetInt(mp.game.joaat("SP0_TOTAL_CASH"), 1000, false);
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>save:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>