# Stats::statSetBoolMasked

## Syntax

```js
mp.game.stats.statSetBoolMasked(statName, value, mask, save);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>value:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>mask:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>save:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>