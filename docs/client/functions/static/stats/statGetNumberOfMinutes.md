# Stats::statGetNumberOfMinutes

## Syntax

```js
mp.game.stats.statGetNumberOfMinutes(statName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>