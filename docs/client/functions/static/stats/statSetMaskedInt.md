# Stats::statSetMaskedInt

## Syntax

```js
mp.game.stats.statSetMaskedInt(statName, p1, p2, p3, save);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>save:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>