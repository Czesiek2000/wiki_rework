# Stats::statSetProfileSetting

Does not take effect immediately, unfortunately.

profileSetting seems to only be 936, 937 and 938 in scripts


## Syntax

```js
mp.game.stats.statSetProfileSetting(profileSetting, value);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>profileSetting:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>value:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>