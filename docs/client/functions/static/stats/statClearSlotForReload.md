# Stats::statClearSlotForReload
Please change to 'void'!---------------------------------Example:for (v_2 = 0; v_2 <= 4; v_2 += 1) { STATS::STAT_CLEAR_SLOT_FOR_RELOAD(v_2);}
 

## Syntax

```js
mp.game.stats.statClearSlotForReload(statSlot);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statSlot:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>