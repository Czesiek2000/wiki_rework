# Stats::statGetFloat

## Syntax

```js
mp.game.stats.statGetFloat(statHash, outValue, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statHash:</b> Model hash or name</li>
<li><b>outValue:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Float</span></b></li>