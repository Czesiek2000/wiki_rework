# Stats::statGetInt

p2 appears to always be -1


## Syntax

```js
mp.game.stats.statGetInt(statHash, outValue, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statHash:</b> Model hash or name</li>
<li><b>outValue:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>