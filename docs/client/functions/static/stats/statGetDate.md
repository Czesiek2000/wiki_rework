# Stats::statGetDate

## Syntax

```js
mp.game.stats.statGetDate(statHash, p1, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statHash:</b> Model hash or name</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>