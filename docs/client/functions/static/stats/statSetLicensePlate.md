# Stats::statSetLicensePlate

## Syntax

```js
mp.game.stats.statSetLicensePlate(statName, str);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>str:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>