# Stats::playstatsCheatApplied

## Syntax

```js
mp.game.stats.playstatsCheatApplied(cheat);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>cheat:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>