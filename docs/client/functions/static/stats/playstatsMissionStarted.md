# Stats::playstatsMissionStarted

## Syntax

```js
mp.game.stats.playstatsMissionStarted(p0, p1, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> Boolean</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>