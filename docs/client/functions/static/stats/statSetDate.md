# Stats::statSetDate

'value' is a structure to a structure, 'numFields' is how many fields there are in said structure (usually 7).

The structure looks like this:int yearint monthint dayint hourint minuteint secondint millisecondThe decompiled scripts use `TIME::GET_POSIX_TIME` to fill this structure.


## Syntax

```js
mp.game.stats.statSetDate(statName, value, numFields, save);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>value:</b> unknown (to be checked)</li>
<li><b>numFields:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>save:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>