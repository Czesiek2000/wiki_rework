# Stats::leaderboards2ReadByScoreFloat

## Syntax

```js
mp.game.stats.leaderboards2ReadByScoreFloat(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>