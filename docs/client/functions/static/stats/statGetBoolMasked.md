# Stats::statGetBoolMasked

p2 - Default value? Seems to be -1 most of the time.


## Syntax

```js
mp.game.stats.statGetBoolMasked(statName, mask, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>mask:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>