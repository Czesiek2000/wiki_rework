# Stats::statSetCurrentPosixTime

p1 always true.


## Syntax

```js
mp.game.stats.statSetCurrentPosixTime(statName, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>statName:</b> Model hash or name</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>