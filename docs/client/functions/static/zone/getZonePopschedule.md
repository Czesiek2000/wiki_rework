# Zone::getZonePopschedule

## Syntax

```js
mp.game.zone.getZonePopschedule(zoneId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>zoneId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>