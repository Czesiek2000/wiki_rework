# Zone::getZoneScumminess

cellphone range 1- 5 used for signal bar in iFruit phone


## Syntax

```js
mp.game.zone.getZoneScumminess(zoneId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>zoneId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>