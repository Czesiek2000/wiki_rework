# Zone::getHashOfMapAreaAtCoords

Returns a hash representing which part of the map the given coords are located.

Possible return values:

(Hash of) city -> -289320599

(Hash of) countryside -> 207209373

C# Example:
```cpp
Ped player = Game.Player.Character;
Hash h = Function.Call<Hash>(Hash.GET_HASH_OF_MAP_AREA_AT_COORDS, player.Position.X, player.Position.Y, player.Position.Z);
```


## Syntax

```js
mp.game.zone.getHashOfMapAreaAtCoords(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>