# Audio::playPedRingtone

All found occurrences in **b617d**, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/RFb4GTny)
```cpp
AUDIO::PLAY_PED_RINGTONE('Remote_Ring', PLAYER::PLAYER_PED_ID(), 1);AUDIO::PLAY_PED_RINGTONE('Dial_and_Remote_Ring', PLAYER::PLAYER_PED_ID(), 1);
```

## Syntax

```js
mp.game.audio.playPedRingtone(ringtoneName, ped, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ringtoneName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>ped:</b> Ped handle or object</li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>