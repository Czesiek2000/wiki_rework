# Audio::loadStream

Example:

`AUDIO::LOAD_STREAM('CAR_STEAL_1_PASSBY', 'CAR_STEAL_1_SOUNDSET');`

All found occurrences in the **b678d** decompiled scripts: [pastebin](pastebin.com/3rma6w5w)

Stream names often ends with '_MASTER', '_SMALL' or '_STREAM'. Also '_IN', '_OUT' and numbers. 

soundSet is often set to 0 in the scripts. These are common to end the soundSets: '_SOUNDS', '_SOUNDSET' and numbers.
 

## Syntax

```js
mp.game.audio.loadStream(streamName, soundSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>streamName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>soundSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>