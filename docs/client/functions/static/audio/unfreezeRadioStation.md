# Audio::unfreezeRadioStation

## Syntax

```js
mp.game.audio.unfreezeRadioStation(radioStation);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>radioStation:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>