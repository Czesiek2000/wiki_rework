# Audio::playPain

Last 2 parameters always seem to be 0.

EX: 
```csharp
Function.Call(Hash.PLAY_PAIN, TestPed, 6, 0, 0);
```
Known Pain IDs:
* 1 - CRASHES GAME (DON'T USE) - (EDIT from someone else: Does NOT crash in 1.0.617.1 for me)
* 6 - Scream (Short)
* 7 - Scared Scream (Kinda Long)
* 8 - On Fire- jedijosh920
 
Needs another parameter [int p2]. 

The signature is PED::PLAY_PAIN(Ped ped, int painID, int p1, int p2);
*sollaholla*
 

## Syntax

```js
mp.game.audio.playPain(painID, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>painID:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>