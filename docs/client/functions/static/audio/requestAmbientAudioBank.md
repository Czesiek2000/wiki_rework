# Audio::requestAmbientAudioBank

All occurrences and usages found in **b617d**, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/XZ1tmGEz)
 

## Syntax

```js
mp.game.audio.requestAmbientAudioBank(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>