# Audio::stopAllAlarms

## Syntax

```js
mp.game.audio.stopAllAlarms(stop);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>stop:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>