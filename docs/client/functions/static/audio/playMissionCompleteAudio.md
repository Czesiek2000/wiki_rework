# Audio::playMissionCompleteAudio

Called 38 times in the scripts. There are 5 different audioNames used.  One unknown removed below.  

```js
AUDIO::PLAY_MISSION_COMPLETE_AUDIO('DEAD'); 
AUDIO::PLAY_MISSION_COMPLETE_AUDIO('FRANKLIN_BIG_01'); AUDIO::PLAY_MISSION_COMPLETE_AUDIO('GENERIC_FAILED'); AUDIO::PLAY_MISSION_COMPLETE_AUDIO('TREVOR_SMALL_01');
```

## Syntax

```js
mp.game.audio.playMissionCompleteAudio(audioName);
```

## Example
```js
// todo
```


<br />


## Parameters

<li><b>audioName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>