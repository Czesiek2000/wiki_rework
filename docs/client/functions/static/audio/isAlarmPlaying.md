# Audio::isAlarmPlaying

Example:
```
bool playing = AUDIO::IS_ALARM_PLAYING('PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS');```
*Zerovv*
 

## Syntax

```js
mp.game.audio.isAlarmPlaying(alarmName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>alarmName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>