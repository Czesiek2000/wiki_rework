# Audio::requestMissionAudioBank

All occurrences and usages found in **b617d**: [pastebin](https://pastebin.com/NzZZ2Tmm)

## Syntax


```js
mp.game.audio.requestMissionAudioBank(p0, p1);
```

## Example

```js
// todo
```

<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>