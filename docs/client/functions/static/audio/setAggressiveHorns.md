# Audio::setAggressiveHorns

Makes pedestrians sound their horn longer, faster and more agressive when they use their horn.
 

## Syntax

```js
mp.game.audio.setAggressiveHorns(toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>