# Audio::setRadioAutoUnfreeze

## Syntax

```js
mp.game.audio.setRadioAutoUnfreeze(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>