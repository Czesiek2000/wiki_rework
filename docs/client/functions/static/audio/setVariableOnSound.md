# Audio::setVariableOnSound

## Syntax

```js
mp.game.audio.setVariableOnSound(soundId, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>