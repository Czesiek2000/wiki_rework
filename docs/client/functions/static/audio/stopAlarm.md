# Audio::stopAlarm

Example:

This will stop the alarm at Fort Zancudo.

```cpp
AUDIO::STOP_ALARM('PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS', 1);
```

First parameter (char) is the name of the alarm.Second parameter (bool) has to be true (1) to have any effect.

**Zerovv**
 

## Syntax

```js
mp.game.audio.stopAlarm(alarmName, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>alarmName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>