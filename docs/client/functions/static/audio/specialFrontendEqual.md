# Audio::specialFrontendEqual

Hash collision!!! `PLAY_STREAM_FROM_POSITION` is the correct name!
 

## Syntax

```js
mp.game.audio.specialFrontendEqual(x, y, z);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>