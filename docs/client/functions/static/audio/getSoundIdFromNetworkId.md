# Audio::getSoundIdFromNetworkId

## Syntax

```js
mp.game.audio.getSoundIdFromNetworkId(netId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>netId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>