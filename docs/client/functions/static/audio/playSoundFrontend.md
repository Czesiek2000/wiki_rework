# Audio::playSoundFrontend

**WARNING: use different ID may crash the client.**
 

## Syntax

```js
mp.game.audio.playSoundFrontend(ID, SoundName, SoundSetName, instant);
```

## Example

```js
mp.game.audio.playSoundFrontend(-1, "Beep_Green", "DLC_HEIST_HACKING_SNAKE_SOUNDS", true);
```


<br />


## Parameters

<li><b>ID:</b> <font color="red"><b>int</b></font> (<b>Put -1 for sounds played once, use GET_SOUND_ID otherwise.</b>)</li>
<li><b>SoundName:</b> <font color="green"><b>String</b></font></li>
<li><b>SoundSetName:</b> <font color="green"><b>String</b></font></li>
<li><b>instant:</b> <font color="blue"><b>Boolean</b></font></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>