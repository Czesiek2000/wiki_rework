# Audio::clearAmbientZoneState

This function also has a p2, unknown. Signature `AUDIO::CLEAR_AMBIENT_ZONE_STATE(char* zoneName, bool p1, Any p2);`

Still needs more research. Here are the names I've found: [pastebin](https://pastebin.com/AfA0Qjyv) - *Sollaholla*
 

## Syntax

```js
mp.game.audio.clearAmbientZoneState(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>