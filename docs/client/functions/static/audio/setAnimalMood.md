# Audio::setAnimalMood

mood can be 0 or 1 (it's not a boolean value!). Effects audio of the animal.
 

## Syntax

```js
mp.game.audio.setAnimalMood(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>