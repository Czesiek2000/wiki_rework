# Audio::releaseSoundId

## Syntax

```js
mp.game.audio.releaseSoundId(soundId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>