# Audio::getRadioStationName

Returns String with radio station name.
 

## Syntax

```js
mp.game.audio.getRadioStationName(radioStation);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>radioStation:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">String</span></b></li>