# Audio::setPlayerAngry

MulleDK19: Hash collision! Disables speech.
 

## Syntax

```js
mp.game.audio.setPlayerAngry(playerPed, disabled);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>playerPed:</b> Ped handle or object</li>
<li><b>disabled:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>