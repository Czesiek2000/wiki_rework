# Audio::prepareSynchronizedAudioEvent

## Syntax

```js
mp.game.audio.prepareSynchronizedAudioEvent(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>