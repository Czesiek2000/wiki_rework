# Audio::playAmbientSpeechAtCoords

## Syntax

```js
mp.game.audio.playAmbientSpeechAtCoords(speechName, voiceName, x, y, z, speechParam);
```

## Example

```js
//Plays tazer electrocution pain sound at 0,0,0

mp.game.audio.playAmbientSpeechAtCoords("PAIN_TAZER", "WAVELOAD_PAIN_MALE", 0, 0, 0, "SPEECH_PARAMS_INTERRUPT_SHOUTED_CRITICAL");
```


<br />


## Parameters

<li><b>speechName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>voiceName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>speechParam:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>