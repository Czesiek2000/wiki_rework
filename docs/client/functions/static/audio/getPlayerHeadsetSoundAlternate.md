# Audio::getPlayerHeadsetSoundAlternate

Called 5 times in the scripts. All occurrences found in **b617d**, sorted alphabetically and identical lines removed: 

```cpp
AUDIO::GET_PLAYER_HEADSET_SOUND_ALTERNATE('INOUT', 0.0);
AUDIO::GET_PLAYER_HEADSET_SOUND_ALTERNATE('INOUT', 1.0);
```

## Syntax

```js
mp.game.audio.getPlayerHeadsetSoundAlternate(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>