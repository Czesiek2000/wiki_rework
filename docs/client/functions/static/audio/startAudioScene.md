# Audio::startAudioScene

Used to prepare a scene where the surrounding sound is muted or a bit changed. This does not play any sound.

List of all usable scene names found in **b617d**. 

Sorted alphabetically and identical names removed: [pastebin](https://pastebin.com/MtM9N9CC)
 

## Syntax

```js
mp.game.audio.startAudioScene(sceneName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>sceneName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>