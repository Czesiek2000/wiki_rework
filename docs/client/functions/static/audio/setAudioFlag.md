# Audio::setAudioFlag
Possible flag names:(updated from **b393d** scripts - **MoMadenU** - 8/23/15)
* ActivateSwitchWheelAudio
* AllowAmbientSpeechInSlowMo
* AllowCutsceneOverScreenFade
* AllowForceRadioAfterRetune
* AllowPainAndAmbientSpeechToPlayDuringCutscene
* AllowPlayerAIOnMission
* AllowPoliceScannerWhenPlayerHasNoControl
* AllowRadioDuringSwitch
* AllowRadioOverScreenFade
* AllowScoreAndRadio
* AllowScriptedSpeechInSlowMo
* AvoidMissionCompleteDelay
* DisableAbortConversationForDeathAndInjury
* DisableAbortConversationForRagdoll
* DisableBarks
* DisableFlightMusic
* DisableReplayScriptStreamRecording
* EnableHeadsetBeep
* ForceConversationInterrupt
* ForceSeamlessRadioSwitch
* ForceSniperAudio
* FrontendRadioDisabled
* HoldMissionCompleteWhenPrepared
* IsDirectorModeActive
* IsPlayerOnMissionForSpeech
* ListenerReverbDisabled
* LoadMPData
* MobileRadioInGame
* OnlyAllowScriptTriggerPoliceScanner
* PlayMenuMusic
* PoliceScannerDisabled
* ScriptedConvListenerMaySpeak
* SpeechDucksScore
* SuppressPlayerScubaBreathing
* WantedMusicDisabled
* WantedMusicOnMission

No added flag names between **b393d** and **b573d**, including **b573d**.

IsDirectorModeActive' is an audio flag which will allow you to play speech infinitely without any pauses like in Director Mode.- jedijosh920

All flag IDs and hashes:

| ID 	| HASH       	|
|----	|------------	|
| 01 	| 0x20A7858F 	|
| 02 	| 0xA11C2259 	|
| 03 	| 0x08DE4700 	|
| 04 	| 0x989F652F 	|
| 05 	| 0x3C9E76BA 	|
| 06 	| 0xA805FEB0 	|
| 07 	| 0x4B94EA26 	|
| 08 	| 0x803ACD34 	|
| 09 	| 0x7C741226 	|
| 10 	| 0x31DB9EBD 	|
| 11 	| 0xDF386F18 	|
| 12 	| 0x669CED42 	|
| 13 	| 0x51F22743 	|
| 14 	| 0x2052B35C 	|
| 15 	| 0x071472DC 	|
| 16 	| 0xF9928BCC 	|
| 17 	| 0x7ADBDD48 	|
| 18 	| 0xA959BA1A 	|
| 19 	| 0xBBE89B60 	|
| 20 	| 0x87A08871 	|
| 21 	| 0xED1057CE 	|
| 22 	| 0x1584AD7A 	|
| 23 	| 0x8582CFCB 	|
| 24 	| 0x7E5E2FB0 	|
| 25 	| 0xAE4F72DB 	|
| 26 	| 0x5D16D1FA 	|
| 27 	| 0x06B2F4B8 	|
| 28 	| 0x5D4CDC96 	|
| 29 	| 0x8B5A48BA 	|
| 30 	| 0x98FBD539 	|
| 31 	| 0xD8CB0473 	|
| 32 	| 0x5CBB4874 	|
| 33 	| 0x2E9F93A9 	|
| 34 	| 0xD93BEA86 	|
| 35 	| 0x92109B7D 	|
| 36 	| 0xB7EC9E4D 	|
| 37 	| 0xCABDBB1D 	|
| 38 	| 0xB3FD4A52 	|
| 39 	| 0x370D94E5 	|
| 40 	| 0xA0F7938F 	|
| 41 	| 0xCBE1CE81 	|
| 42 	| 0xC27F1271 	|
| 43 	| 0x9E3258EB 	|
| 44 	| 0x551CDA5B 	|
| 45 	| 0xCB6D663C 	|
| 46 	| 0x7DACE87F 	|
| 47 	| 0xF9DE416F 	|
| 48 	| 0x882E6E9E 	|
| 49 	| 0x16B447E7 	|
| 50 	| 0xBD867739 	|
| 51 	| 0xA3A58604 	|
| 52 	| 0x7E046BBC 	|
| 53 	| 0xD95FDB98 	|
| 54 	| 0x5842C0ED 	|
| 55 	| 0x285FECC6 	|
| 56 	| 0x9351AC43 	|
| 57 	| 0x50032E75 	|
| 58 	| 0xAE6D0D59 	|
| 59 	| 0xD6351785 	|
| 60 	| 0xD25D71BC 	|
| 61 	| 0x1F7F6423 	|
| 62 	| 0xE24C3AA6 	|
| 63 	| 0xBFFDD2B7 	|

## Syntax

```js
mp.game.audio.setAudioFlag(flagName, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>flagName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>