# Audio::cancelMusicEvent

All music event names found in the b617d scripts: pastebin.com/GnYt0R3P
 

## Syntax

```js
mp.game.audio.cancelMusicEvent(eventName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>eventName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>