# Audio::playSoundFromEntity

All found occurrences in b617d, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/f2A7vTj0)

No changes made in b678d.

[gta forums](htyps://gtaforums.com/topic/795622-audio-for-mods)
 

## Syntax

```js
mp.game.audio.playSoundFromEntity(soundId, audioName, entity, audioRef, p4, p5);
```

## Example

```js
function playerEnterVehicleHandler(vehicle) {
    mp.game.audio.playSoundFromEntity(1, "CONFIRM_BEEP", vehicle.handle, "HUD_MINI_GAME_SOUNDSET", true, 0);
}

mp.events.add("playerEnterVehicle", playerEnterVehicleHandler);
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>audioName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>entity:</b> Entity handle or object</li>
<li><b>audioRef:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p5:</b> unknown (to be checked)</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>