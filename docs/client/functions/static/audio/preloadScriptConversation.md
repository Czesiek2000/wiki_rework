# Audio::preloadScriptConversation

## Syntax

```js
mp.game.audio.preloadScriptConversation(p0, p1, p2, p3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>