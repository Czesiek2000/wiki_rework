# Audio::playEndCreditsMusic

## Syntax

```js
mp.game.audio.playEndCreditsMusic(play);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>play:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>