# Audio::triggerMusicEvent

List of all usable event names found in **b617d** used with this native. 

Sorted alphabetically and identical names removed: [pastebin](https://pastebin.com/RzDFmB1WAll)

Music event names found in the **b617d** scripts: [pastebin](https://pastebin.com/GnYt0R3P)
 

## Syntax

```js
mp.game.audio.triggerMusicEvent(eventName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>eventName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>