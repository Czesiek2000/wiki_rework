# Audio::addPedToConversation

4 calls in the b617d scripts. The only one with p0 and p2 in clear text: `AUDIO::ADD_PED_TO_CONVERSATION(5, l_AF, 'DINAPOLI');`

One of the 2 calls in dialogue_handler.c p0 is in a while-loop, and so is determined to also possibly be 0 - 15. 

Based on it asking if does_entity_exist for the global I have determined that p1 is, in fact, the ped, but could be wrong.
 

## Syntax

```js
mp.game.audio.addPedToConversation(p0, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>