# Audio::setMobilePhoneRadioState

## Syntax

```js
mp.game.audio.setMobilePhoneRadioState(state);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>state:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>