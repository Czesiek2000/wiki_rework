# Audio::isAmbientSpeechPlaying

## Syntax

```js
mp.game.audio.isAmbientSpeechPlaying(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> Ped handle or object</li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>