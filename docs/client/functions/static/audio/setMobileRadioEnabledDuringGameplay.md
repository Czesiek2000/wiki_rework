# Audio::setMobileRadioEnabledDuringGameplay

Enables Radio on phone.
 

## Syntax

```js
mp.game.audio.setMobileRadioEnabledDuringGameplay(Toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>Toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>