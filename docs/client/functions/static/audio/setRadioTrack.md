# Audio::setRadioTrack

Only found this one in the decompiled scripts:
```cpp
AUDIO::SET_RADIO_TRACK('RADIO_03_HIPHOP_NEW', 'ARM1_RADIO_STARTS');
```

## Syntax

```js
mp.game.audio.setRadioTrack(radioStation, radioTrack);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>radioStation:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>radioTrack:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>