# Audio::playPoliceReport

Please change to void. (Does not return anything!)

Plays the given police radio message. 
All found occurrences in **b617d**, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/GBnsQ5hr)

Changing the time halts the police reports, which means scripts that constantly update the time break this function.
 

## Syntax

```js
mp.game.audio.playPoliceReport(name, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>name:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>unknown (to be checked)</b></li>