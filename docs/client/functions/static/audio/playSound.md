# Audio::playSound

All found occurrences in b617d, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/A8Ny8AHZ)
 

## Syntax

```js
mp.game.audio.playSound(soundId, audioName, audioRef, p3, p4, p5);
```

## Example

```js
mp.game.audio.playSound(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET", true, 0, true);
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>audioName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>audioRef:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p3:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>