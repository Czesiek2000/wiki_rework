# Audio::prepareAlarm

Example:

```cpp
bool prepareAlarm = AUDIO::PREPARE_ALARM('PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS');
```

*Zerovv*

## Syntax

```js
mp.game.audio.prepareAlarm(alarmName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>alarmName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>