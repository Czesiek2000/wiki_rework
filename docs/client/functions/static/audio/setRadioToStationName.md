# Audio::setRadioToStationName

For a full list, see here: [pastebin](https://pastebin.com/Kj9t38KF)
 

## Syntax

```js
mp.game.audio.setRadioToStationName(stationName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>stationName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>