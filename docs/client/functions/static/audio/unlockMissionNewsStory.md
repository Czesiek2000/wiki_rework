# Audio::unlockMissionNewsStory

I see this as a native that would of been used back in GTA III when you finally unlocked the bridge to the next island and such.
 

## Syntax

```js
mp.game.audio.unlockMissionNewsStory(newsStory);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>newsStory:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>