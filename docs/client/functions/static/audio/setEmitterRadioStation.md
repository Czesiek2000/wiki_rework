# Audio::setEmitterRadioStation

## Syntax

```js
mp.game.audio.setEmitterRadioStation(emitterName, radioStation);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>emitterName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>radioStation:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>