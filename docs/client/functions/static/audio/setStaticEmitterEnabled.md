# Audio::setStaticEmitterEnabled

Example:
```cpp
AUDIO::SET_STATIC_EMITTER_ENABLED((Any*)'LOS_SANTOS_VANILLA_UNICORN_01_STAGE', false);
AUDIO::SET_STATIC_EMITTER_ENABLED((Any*)'LOS_SANTOS_VANILLA_UNICORN_02_MAIN_ROOM', false);	
AUDIO::SET_STATIC_EMITTER_ENABLED((Any*)'LOS_SANTOS_VANILLA_UNICORN_03_BACK_ROOM', false);
```
This turns off surrounding sounds not connected directly to peds. 
 

## Syntax

```js
mp.game.audio.setStaticEmitterEnabled(emitterName, toggle);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>emitterName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>toggle:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>