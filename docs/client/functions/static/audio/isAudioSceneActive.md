# Audio::isAudioSceneActive

## Syntax

```js
mp.game.audio.isAudioSceneActive(scene);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>scene:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>