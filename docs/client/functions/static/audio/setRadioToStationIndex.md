# Audio::setRadioToStationIndex

Sets radio station by index.
 
### Radio Stations
<li>LosSantosRockRadio = 0</li>
<li>NonStopPopFM = 1</li>
<li>RadioLosSantos = 2</li>
<li>ChannelX = 3</li>
<li>WestCoastTalkRadio = 4</li>
<li>RebelRadio = 5</li>
<li>SoulwaxFM = 6</li>
<li>EastLosFM = 7</li>
<li>WestCoastClassics = 8</li>
<li>BlaineCountyRadio = 9</li>
<li>TheBlueArk = 10</li>
<li>WorldWideFM = 11</li>
<li>FlyloFM = 12</li>
<li>TheLowdown = 13</li>
<li>RadioMirrorPark = 14</li>
<li>Space = 15</li>
<li>VinewoodBoulevardRadio = 16</li>
<li>SelfRadio = 17</li>
<li>TheLab = 18</li>
<li>RadioOff = 255</li>

## Syntax

```js
mp.game.audio.setRadioToStationIndex(radioStation);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>radioStation:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>