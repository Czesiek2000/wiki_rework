# Audio::setMicrophonePosition

If this is the correct name, what microphone? 

I know your TV isn't going to reach out and adjust your headset so..
 

## Syntax

```js
mp.game.audio.setMicrophonePosition(p0, x1, y1, z1, x2, y2, z2, x3, y3, z3);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>x1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z1:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z2:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>x3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y3:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z3:</b> <b><span style="color:#008017">Float</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>