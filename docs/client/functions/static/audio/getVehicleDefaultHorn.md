# Audio::getVehicleDefaultHorn

Returns hash of default vehicle hornHash is stored in audVehicleAudioEntity
 

## Syntax

```js
mp.game.audio.getVehicleDefaultHorn(veh);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>veh:</b> Vehicle handle or object</li>

<br />
<br />

## Return value

<li><b>Model hash or name</b></li>