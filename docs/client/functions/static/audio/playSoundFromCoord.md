# Audio::playSoundFromCoord

All found occurrences in **b617d**, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/eeFc5DiW) [gta forums](https://gtaforums.com/topic/795622-audio-for-mods)
 

## Syntax

```js
mp.game.audio.playSoundFromCoord(soundId, audioName, x, y, z, audioRef, p6, p7, p8);
```

## Example

```js
let pos = mp.players.local.position;

mp.game.audio.playSoundFromCoord(1, "CONFIRM_BEEP", pos.x, pos.y, pos.z, "HUD_MINI_GAME_SOUNDSET", false, 0, false);
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>audioName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>x:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>y:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>z:</b> <b><span style="color:#008017">Float</span></b></li>
<li><b>audioRef:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>