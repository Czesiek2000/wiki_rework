# Audio::getNetworkIdFromSoundId

Could this be used alongside either, `SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES` or `_SET_NETWORK_ID_SYNC_TO_PLAYER` to make it so other players can hear the sound while online? It'd be a bit troll-fun to be able to play the Zancudo UFO creepy sounds globally.
 

## Syntax

```js
mp.game.audio.getNetworkIdFromSoundId(soundId);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>soundId:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>