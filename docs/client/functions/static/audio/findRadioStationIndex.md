# Audio::findRadioStationIndex

## Syntax

```js
mp.game.audio.findRadioStationIndex(station);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>station:</b> <b><span style="color:#008017">Int</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Int</span></b></li>