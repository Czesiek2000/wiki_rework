# Audio::setSynchronizedAudioEventPositionThisFrame

Sets the position of the audio event to the entity's position for one frame(?)
```cpp
if (l_8C3 == 0) {
    sub_27fd1(0, -1, 1); 
    if (PED::IS_SYNCHRONIZED_SCENE_RUNNING(l_87D)) {
        AUDIO::STOP_SYNCHRONIZED_AUDIO_EVENT(l_87D); 
    } 
    
    if (sub_7dd(l_A00)) {
        AUDIO::_950A154B8DAB6185('PAP2_IG1_POPPYSEX', l_A00); 
    } 
    
    sub_91c('TK************ SETTING SYNCH SCENE AUDIO POSITION THIS FRAME ************TK'); 
    l_8C3 = 1;
    }
```
Found in the b617d scripts, duplicates removed:
```cpp    
AUDIO::_950A154B8DAB6185('CAR_5_IG_6', l_7FE[1/*1*/]);
AUDIO::_950A154B8DAB6185('EX03_TRAIN_BIKE_LAND', PLAYER::PLAYER_PED_ID());
AUDIO::_950A154B8DAB6185('FBI_2_MCS_1_LeadIn', l_40[2/*1*/]);
AUDIO::_950A154B8DAB6185('FIN_C2_MCS_1', l_24C[0/*1*/]);
AUDIO::_950A154B8DAB6185('MNT_DNC', l_5F);
AUDIO::_950A154B8DAB6185('PAP2_IG1_POPPYSEX', l_A00);
```
 

## Syntax

```js
mp.game.audio.setSynchronizedAudioEventPositionThisFrame(p0, p1);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> Entity handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>