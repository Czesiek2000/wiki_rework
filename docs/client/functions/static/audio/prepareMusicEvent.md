# Audio::prepareMusicEvent

All music event names found in the *b617d* scripts: [pastebin](https://pastebin.com/GnYt0R3P)
 

## Syntax

```js
mp.game.audio.prepareMusicEvent(eventName);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>eventName:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>