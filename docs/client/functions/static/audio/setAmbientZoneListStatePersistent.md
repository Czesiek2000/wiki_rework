# Audio::setAmbientZoneListStatePersistent
All occurrences found in b617d, sorted alphabetically and identical lines removed: [pastebin](https://pastebin.com/WkXDGgQL)
 

## Syntax

```js
mp.game.audio.setAmbientZoneListStatePersistent(ambientZone, p1, p2);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ambientZone:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p1:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p2:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>