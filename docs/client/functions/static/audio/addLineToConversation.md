# Audio::addLineToConversation

**NOTE**: ones that are -1, 0 - 35 are determined by a function where it gets a TextLabel from a global then runs, `_GET_TEXT_SUBSTRING` and depending on what the result is it goes in check order of 0 - 9 then A - Z then z (lowercase). 

So it will then return 0 - 35 or -1 if it's 'z'. The func to handle that ^^ is func_67 in dialog_handler.c atleast in TU27 Xbox360 scripts.

* p0 is -1, 0 - 35

* p1 is a char or string (whatever you wanna call it)

* p2 is Global 10597 + i * 6. 'i' is a while(i < 70) loop

* p3 is again -1, 0 - 35 

* p4 is again -1, 0 - 35 

* p5 is either 0 or 1 (bool ?)

* p6 is either 0 or 1 (The func to determine this is bool)

* p7 is either 0 or 1 (The func to determine this is bool)

* p8 is either 0 or 1 (The func to determine this is bool)

* p9 is 0 - 3 (Determined by func_60 in dialogue_handler.c)

* p10 is either 0 or 1 (The func to determine this is bool)

* p11 is either 0 or 1 (The func to determine this is bool)

* p12 is unknown as in TU27 X360 scripts it only goes to p11.
 

## Syntax

```js
mp.game.audio.addLineToConversation(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> unknown (to be checked)</li>
<li><b>p1:</b> unknown (to be checked)</li>
<li><b>p2:</b> unknown (to be checked)</li>
<li><b>p3:</b> unknown (to be checked)</li>
<li><b>p4:</b> unknown (to be checked)</li>
<li><b>p5:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p6:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p7:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p8:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p9:</b> unknown (to be checked)</li>
<li><b>p10:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p11:</b> <b><span style="color:#008017">Boolean</span></b></li>
<li><b>p12:</b> <b><span style="color:#008017">Boolean</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>