# Audio::isAmbientZoneEnabled

## Syntax

```js
mp.game.audio.isAmbientZoneEnabled(ambientZone);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>ambientZone:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>