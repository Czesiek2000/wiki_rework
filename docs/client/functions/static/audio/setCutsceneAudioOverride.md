# Audio::setCutsceneAudioOverride
All occurrences found in **b617d**, sorted alphabetically and identical lines removed: 
```cpp
AUDIO::SET_CUTSCENE_AUDIO_OVERRIDE('_AK');
AUDIO::SET_CUTSCENE_AUDIO_OVERRIDE('_CUSTOM');
AUDIO::SET_CUTSCENE_AUDIO_OVERRIDE('_TOOTHLESS');
```

## Syntax

```js
mp.game.audio.setCutsceneAudioOverride(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>