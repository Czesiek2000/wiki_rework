# Audio::loadStreamWithStartOffset

Example:

```cpp
AUDIO::LOAD_STREAM_WITH_START_OFFSET('STASH_TOXIN_STREAM', 2400, 'FBI_05_SOUNDS');
```
Only called a few times in the scripts.

## Syntax

```js
mp.game.audio.loadStreamWithStartOffset(streamName, startOffset, soundSet);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>streamName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>startOffset:</b> <b><span style="color:#008017">Int</span></b></li>
<li><b>soundSet:</b> <b><span style="color:#008017">String</span></b></li>

<br />
<br />

## Return value

<li><b><span style="color:#008017">Boolean</span></b></li>