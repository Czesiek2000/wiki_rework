# Audio::playAmbientSpeechWithVoice

This is the same as `_PLAY_AMBIENT_SPEECH1` and `_PLAY_AMBIENT_SPEECH2` but it will allow you to play a speech file from a specific voice file. 

It works on players and all peds, even animals.

EX (C#):
```csharp
GTA.Native.Function.Call(Hash._0x3523634255FC3318, Game.Player.Character, 'GENERIC_INSULT_HIGH', 's_m_y_sheriff_01_white_full_01', 'SPEECH_PARAMS_FORCE_SHOUTED', 0);
```

The **first** param is the ped you want to play it on, the **second** is the speech name, the **third** is the voice name, the fourth is the speech param, and **the last** param is usually always 0.
 

[Speech list](../../../../tables/speechList.md)


## Syntax

```js
mp.game.audio.playAmbientSpeechWithVoice(p0, speechName, voiceName, speechParam, p4);
```

## Example

```js
// todo
```


<br />


## Parameters



<ul><li><b>p0:</b> Ped handle or object</li>
<li><b>speechName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>voiceName:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>speechParam:</b> <b><span style="color:#008017">String</span></b></li>
<li><b>p4:</b> <b><span style="color:#008017">Boolean</span></b></li></ul>

<br />
<br />

## Return value

<li><b>p0:</b> Ped handle or object</li>