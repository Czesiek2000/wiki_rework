# Audio::stopCurrentPlayingAmbientSpeech

Needs to be called every frame.


## Syntax

```js
mp.game.audio.stopCurrentPlayingAmbientSpeech(p0);
```

## Example

```js
// todo
```


<br />


## Parameters

<li><b>p0:</b> Ped handle or object</li>

<br />
<br />

## Return value

<li><b>Undefined</b></li>