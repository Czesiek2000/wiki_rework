<div align="center"><h1>WIKI REWORK</h1></div>

![MIT](https://img.shields.io/badge/License-MIT-blue.svg?style=flat)

This is simple redising [RAGE Multiplayer Wiki](https://wiki.rage.mp/index.php?title=Main_Page). It is changed to be single page app and use dark theme as main theme. There is also option to change to light theme.

It includes all data from RAGE Multiplayer wiki, and it is categoriesed the same as wiki page. It has easy to navigate sidebar menu where all functions are categoriesed for client / server side events and functions. Also it has some pure tables added from wiki.

Each page has navigation to items inside it's namespace, so you can easily navigate between pages.

## Technology
This project is made with [docsifyjs](https://docsifyjs.org) and available plugins. Content is made with markdown files.


## Contribution
If you want to contribute to this project get familiar with [docsifyjs](https://docsifyjs.org) and then with [RAGE Multiplayer Wiki](https://wiki.rage.mp/index.php?title=Main_Page)


## Preview
### Homepage preview
![homepage](images/homepage.png)

### Sidebar preview
![sidebar](images/sidebar.png)

### Page preview
![page](images/page.png)


## Licence
This project is under MIT licence.